<?php

if (isset($GLOBALS['version'])) {
    $version = $GLOBALS['version'];
} else {
    $version = '';
}
if (isset($GLOBALS['updated'])) {
    $updated = $GLOBALS['updated'];
} else {
    $updated = '';
}
if (isset($GLOBALS['website_title'])) {
    $website_title = $GLOBALS['website_title'];
} else {
    $website_title = '';
}

$lang_arr = array(
    "_ABOUT" => "About $website_title",
    "_ABOUT_TEXT" => "<h3>This portal page serves as the entry page for students for all digital examinations at ETH Zurich</h3>
                        <p>Version " . $version . " (last update: " . $updated . ")</p>
                        <p>Concept: Service Digital Examinations (<a target=\"_blank\" href=\"https://ethz.ch/en/the-eth-zurich/organisation/departments/informatikdienste/personen/educational-it-services.html\">Educational IT Services</a>)</p>
                        <h5>Project Lead:</h5>
                        <ul>
                            <li>Bruno R&uuml;tsche</li>
                            <li>Tony Moser</li>
                        </ul>
                        <h5>Technical Implementation:</h5>
                        <ul>
                            <li>Flavio Steger</li>
                            <li>Nando Rigonalli</li>
                            <li>Jonas Wiesendanger</li>
                            <li>Michael Kern</li>
                            <li>Michael Odermatt</li>
                            <li>Bengt Giger</li>
                        </ul>
                        <h5>Further information:</h5>
                        <ul>
                            <li><a target=\"_blank\" href=\"https://ethz.ch/digital-examinations\">Digital examinations at ETH Zurich</a></li>
                            <li><a target=\"_blank\" href=\"https://blogs.ethz.ch/letblog/2021/06/01/going-paperless-the-revised-portal-page-in-online-examinations-at-eth-zurich-introduced-in-spring-semester-2020\">Blog about the portal page</a></li>
                        </ul>",
    "_ACCEPTED" => "I have read the information above and confirm that my data is correct.",
    "_ACTIVATION" => "Activation",
    "_ADD" => "Add",
    "_ADMIN" => "Administrators",
    "_ADMINISTRATION" => "Administration",
    "_ALLOCATION" => "Allocation",
    "_ALLOCATION_FORM" => "Form (F)",
    "_ALLOCATION_TEXT" => "If you are the person mentioned above, select \"Next\".",
    "_ARCHIVE" => "Archive",
    "_ARCHIVED" => "Archived",
    "_ASSIGNED" => "Already assigned",
    "_ASSISTANTS" => "Assistants",
    "_AUTO_ALLOCATION_BACKWARD" => "Automatic allocation: backward (Enter)",
    "_AUTO_ALLOCATION_FORWARD" => "Automatic allocation: forward (Space)",
    "_AUTO_REFRESH" => "Form/allocation reload automatically",
    "_BACK" => "Back",
    "_BACKUP" => "Create backup",
    "_CALCULATOR" => "Calculator",
    "_CANCEL" => "Cancel",
    "_CHOOSE_LIST" => "Choose active participant list for the rooms",
    "_CLEANUP" => "Cleanup",
    "_CLEANUP_PORTAL" => "Cleanup portal",
    "_CLEANUP_TEXT" => "All enrollments, students without enrollment and exams including allocated examiners older <br>than the chosen date will be deleted (examiners won't be removed from the database).",
    "_CLICK_TO_LOAD" => "Click to load",
    "_CLIPBOARD" => "Copy to clipboard",
    "_COLOR" => "Color",
    "_COMPUTER_NAME" => "Computer name",
    "_COMPUTER" => "Computer",
    "_COMPUTERS" => "Computers",
    "_CONFIRM" => "Please confirm that your data is correct.",
    "_CONFIRMED" => "Confirmed",
    "_CONFIG_LIST" => "Exam configurations",
    "_CONFIGURATION" => "Configuration",
    "_CONSIDER_ORDER" => "Consider order",
    "_COUNT" => "row(s)",
    "_CREATED" => "Created",
    "_DATE" => "Date",
    "_DATE_MOD" => "Date modified",
    "_DATES" => "Dates",
    "_DEADLINE" => "Deadline",
    "_DELETE" => "Delete",
    "_DELETE_EXAM" => "Delete allocation",
    "_DELETE_LIST" => "Delete list",
    "_DELETE_MACHINES" => "Delete all machines from pool",
    "_DEPLOY" => "Deploy snapshot",
    "_DESC" => "Descending",
    "_DESCRIPTION" => "Description",
    "_DIGITAL_EXAMS" => "Digital Examinations",
    "_DISABLED" => "Disabled",
    "_DISCONNECT" => "Disconnect session",
    "_DOE" => "Smith",
    "_DOWNLOAD" => "Download",
    "_EDIT" => "Edit",
    "_EMAIL" => "E-Mail",
    "_ENABLED" => "Enabled",
    "_ENROLLED" => "Enrolled",
    "_ENROLLMENT" => "Enrollment",
    "_ENROLLMENTS" => "Enrollments",
    "_ENTRY" => "Entry",
    "_ERROR" => "Error",
    "_EXAM" => "Exam",
    "_EXAM_2" => "Exam",
    "_EXAM_DATE" => "Exam date",
    "_EXAM_NR" => "Exam no.",
    "_EXAM_PEOPLE" => "Examiners",
    "_EXAM_SELECTION" => "Exam selection",
    "_EXAM_USER" => "Exam user",
    "_EXAMS" => "Exams",
    "_EXISTING_COMPUTER" => "Computer already existing",
    "_EXISTING_LIST" => "Existing list",
    "_EXPIRED" => "expired",
    "_EXPLORER" => "File explorer",
    "_FAIL" => "Fail",
    "_FALSE" => "False",
    "_FILE" => "File",
    "_FILE_INFO" => "Are files made available to students?",
    "_FILE_PHOTO" => "File / Photo",
    "_FILE_NOT_FOUND" => "File not found",
    "_FILENAME" => "Filename",
    "_FILES" => "Files",
    "_FILES_OLD" => "Files from previous semesters were not transferred.",
    "_FILESIZE" => "Filesize",
    "_FILEVIEW" => "Files",
    "_FINALIZED" => "Finalized",
    "_FIRSTNAME" => "First name",
    "_FOLDERS" => "Folders",
    "_FORM" => "Form",
    "_FORM_DISABLED" => "<p><i class=\"fa fa-times\" aria-hidden=\"true\"></i> Form disabled</p>",
    "_FORM_TEXT_1" => "Fill in the following form truthfully and click on \"Next\".",
    "_FORM_TEXT_2" => "The screen is recorded during the examination as an additional security measure.",
    "_FORM_TEXT_3" => "Technical problems have to be reported immediately by raising your hand.",
    "_FORM_TEXT_4" => "Please check your data and select \"Next\".",
    "_FROM" => "from",
    "_FUNCTION" => "Function",
    "_GENERAL" => "General",
    "_GENERATE_VHO" => "Generate VHO accounts",
    "_GROUP" => "Group",
    "_GROUPS" => "Groups",
    "_HIDDEN" => "Hidden",
    "_HIDE" => "Hide",
    "_HIDE_EXISTING" => "Hide unmodified",
    "_HOSTNAME" => "Host",
    "_ICS_FILE" => "ICS file",
    "_ID" => "ID",
    "_IGNORE_REGISTRATION" => "Ignore this warning and save anyway",
    "_IMPORT" => "Import",
    "_IMPORT_LDAP_USERS" => "Import LDAP users",
    "_IN_USE" => "In use",
    "_INCIDENTS" => "Incidents (Logsheet)",
    "_INFO" => "Info",
    "_INSTRUCTION_DURING_EXAM_CONTENT" => "<br><h5>Technical problems</h5><ul><li>Technical problems have to be reported immediately by raising your hand.</li><li>Do not attempt to solve the problem yourself.</li><li>The examination time can be extended by the time lost due to the problem.</li></ul><br><h5>Moodle</h5><ul><img src=\"img/figures/navigation_en.png\" alt=\"screenshot_navigation\" width=\"600\" hspace=\"20\"><br><br><li>(1) After the examination has started, the question navigation appears on the right-hand side of the screen.</li><li>(2) The remaining exam time is displayed in the upper right corner.</li><li>(3) The number of points achievable per question is displayed in the upper left corner.</li><li>(4) Questions can be tagged with the flag symbol.</li><li>Your answers in Moodle are automatically saved at regular intervals and when you change questions.</li><li>The content of the site can be resized (by using Ctrl + \"+\", \"-\" on the numpad or \"pinch-to-zoom\" on devices with trackpad/touchscreen).</li><li>On some sites, scrolling is required.</li></ul><br><h5>Additional software</h5><ul><li>In case additional software is available in your examination (e.g., calculator / R / Matlab / Excel / Adobe Reader), you will be able to get used to the environment before the examination starts.</li><li>You can switch between the different applications by clicking on the respective icons in the lower left corner of the screen or by using Alt + Tab.</li><li>Save your files exclusively in the folder <b>C:\Exam\Files</b>. <b>Save regularly</b>.</li><li>You can resize the windows and display them side by side.</li></ul>",
    "_INSTRUCTION_DURING_EXAM_TITLE" => "During the examination",
    "_INSTRUCTION_FINISH_EXAM_CONTENT" => "<br><ul><li>If your time runs out, the examination will auto-submit itself.</li><li>If you want to submit early, select \"Finish attempt...\", then \"Submit all and finish\" and confirm the submission.</li><li>You do not have to log out of Moodle.</li><li>If leaving the examination room ahead of time is allowed:<br>Raise your hand and a supervisor will come to you. Leave the room <b>silently</b>. Only start talking once the door to the examination room has been completely closed.</li><li>If leaving the examination room ahead of time is <i>not</i> allowed:<br>Wait <b>silently</b> until the supervisor reports that everybody has finished the examination.</li></ul>",
    "_INSTRUCTION_FINISH_EXAM_TITLE" => "Finalizing the examination",
    "_INSTRUCTION_LOGIN_CONTENT" => "<br>Follow these steps until you reach the prompt for the examination password (see picture below):<ul><li>Select your institution (e.g.,\"ETH Zurich\")</li><li>Login with username and password</li><li>Click on \"Enrol me\" (scroll down if needed)</li><li>Click on \"Attempt quiz\" to open the prompt for the examination password</li><li>This will not yet start the examination. The examination time starts individually once you have entered the examination password. The password will be communicated once everyone is ready.</li><br><img src=\"img/figures/exampassword_en.png\" alt=\"screenshot_exampassword\" width=\"350\"></ul>",
    "_INSTRUCTION_LOGIN_TITLE" => "Before the examination",
    "_INSTRUCTION_SHEET" => "Manual",
    "_INTERNAL_INFORMATION" => "Internal information",
    "_IP_ADDRESS" => "IP address",
    "_JOHN" => "John",
    "_KEYBOARD" => "US Keyboard",
    "_LASTNAME" => "Last name",
    "_LECT" => "Lect.",
    "_LECTURER" => "Lecturer",
    "_LECTURERS" => "Lecturers",
    "_LANGUAGE" => "Language",
    "_LINK" => "Link",
    "_LINUX_INSTRUCTION_DURING_EXAM_CONTENT" => "<ul><li><b>Save your files exclusively in the folder \"questions\". Save regularly.</b> Only documents saved in this folder will be backed up and evaluated.</li><li><b>Technical problems and error messages have to be reported immediately by raising your hand.</b></li><li>Do not close any error message.</li><li>Do not attempt to solve the problem yourself.</li><li>Report immediately if files were overwritten. Rename the corrupted files to prevent the backup process to fetch the wrong content.</li><li>The examination time can be extended by the time lost due to the problem.</li><li>The screen is recorded during the examination as an additional security measure.</li></ul>",
    "_LINUX_INSTRUCTION_DURING_EXAM_TITLE" => "During the examination",
    "_LINUX_INSTRUCTION_FINISH_EXAM_CONTENT" => "<ul><li>If leaving the examination room ahead of time is allowed:<br>Raise your hand and a supervisor will come to you. Lock your computer (do not log out) and leave the room <b>silently</b>. Only start talking once the door to the examination room has been completely closed.</li><li>If leaving the examination room ahead of time is <i>not</i> allowed:<br>Wait <b>silently</b> until the screens are locked and the supervisor reports that everybody has finished the examination.</li></ul>",
    "_LINUX_INSTRUCTION_FINISH_EXAM_TITLE" => "Finalizing the examination",
    "_LINUX_INSTRUCTION_LOGIN_CONTENT" => "<ul><li>Place your student card well visible on the table.</li><li>After the screen has been unlocked, you will be asked for your  name, first name and ETH user name. Enter your information carefully, or your results may not be taken into account.</li><li>The start and end time of the examination will be communicated to you.</li></ul>",
    "_LINUX_INSTRUCTION_LOGIN_TITLE" => "Before the examination",
    "_LISTED" => "Listed but not allocated",
    "_LOADING" => "loading...",
    "_LOGIN" => "Login",
    "_LOGOFF" => "Log off session",
    "_LOGSHEET" => "Logsheet",
    "_MANUAL_ALLOCATION" => "Manual allocation",
    "_MANUAL_ALLOCATION_TEXT" => "Please choose a student:",
    "_MISSING" => "missing",
    "_MOODLE" => "Moodle course link",
    "_MULTIPLE_ALLOCATION_TEXT" => "Multiple exams or students allocated",
    "_MULTIPLE_COMPUTERS" => "multiple computers",
    "_MULTIPLE_EXAMS" => "multiple entries exist",
    "_NAME" => "Name",
    "_NAVIGATION" => "Navigation",
    "_NEW" => "New",
    "_NEW_LIST" => "New list",
    "_NEXT" => "Next",
    "_NOREGISTRATION" => "I do not have a student number",
    "_NO_ALLOCATION_FOUND" => "This site is not available for this computer.",
    "_NO_ALLOCATION_TEXT" => "Please select your preferred option:",
    "_NO_ALLOCATION_TITLE" => "No allocation",
    "_NO_CHECKBOX" => "Check this box if you want to proceed.",
    "_NO_COMPUTER" => "No computer",
    "_NO_COMPUTER_UNKNOWN" => "None or unknown computer",
    "_NO_COMPUTERS" => "No computers in this room",
    "_NO_COMPUTERS_LEFT" => "Please find the computer reserved for you.",
    "_NO_CONFIG" => "Select configuration",
    "_NO_DATA" => "No data",
    "_NO_EXAM" => "Please select your exam",
    "_NO_EXAM_2" => "Select an exam",
    "_NO_EXAM_ACTIVE" => "No exam active",
    "_NO_LECTURER" => "Select a lecturer",
    "_NO_LIST" => "Select a list",
    "_NO_LOGIN" => "The sign-in option is disabled because of failed sign-in attempts.<br>Please try again in a few minutes.",
    "_NO_PARENT" => "No parent",
    "_NO_PEOPLE" => "Select exam people",
    "_NO_ROOM" => "Select a room",
    "_NO_SECTOR" => "Select a sector",
    "_NO_SNAPSHOT" => "No snapshot",
    "_NO_STUDENT" => "Select a student",
    "_NO_STUDENT_2" => "No student",
    "_NO_STUDENTS_TEXT" => "No students to be chosen",
    "_NO_SYSADMIN" => "Select a sysadmin",
    "_NO_THEME" => "Select a theme",
    "_NO_TYPE" => "Select a type",
    "_NO_VDI_EXAM" => "There is no VDI-Exam today.",
    "_NO_VDI_PROVISIONING" => "VDI Provisioning disabled",
    "_NO_VDP" => "Select a RdE",
    "_NONE" => "None",
    "_NOT_ADDED" => "Students already existing in database",
    "_NOT_FOUND" => "not found",
    "_NUMBER" => "Number",
    "_OR" => "or",
    "_OTHER" => "Other",
    "_OVERWRITE_SEB_CONFIG_1" => "Overwriting SEB config?",
    "_OVERWRITE_SEB_CONFIG_2" => "Should the SEB config file be overwritten or renamed?",
    "_PARENT" => "Parent",
    "_PARTICIPANTS" => "Participants",
    "_PASSWORD" => "Password",
    "_PAUSE" => "Disable",
    "_PEOPLE" => "Exam people",
    "_PLAY" => "Enable",
    "_POOL" => "Pool",
    "_POOLS" => "Desktop Pools",
    "_POOLNAME" => "Pool name",
    "_PORTFOLIO" => "Portfolio",
    "_PREF" => "Preference",
    "_PREFERENCES" => "Preferences",
    "_PREPARED" => "Prepared",
    "_PROGRESS" => "Progress",
    "_PROMOTE" => "Promote image",
    "_PUBLISHER" => "Publisher",
    "_README_1" => "After clicking on \"To the exam\", you are on the Moodle Login page. <b>Please then open the manual</b> by clicking on <img src=\"img/icon.png\" width=\"20\"/> in the lower left corner of the screen.",
    "_README_2" => "The manual is also available during the examination.<br>You can switch between the browser (Moodle) and the manual by clicking the respective icons.<br>You can also resize the windows and display them side by side.",
    "_README_3" => "Follow the section \"Before the examination\" in the manual until you arrive at the <b>prompt for the examination password</b>.<br>Afterwards, carefully read the rest of the manual.",
    "_READY_TO_FINALIZE" => "Ready to finalize",
    "_READY_TO_PREPARE" => "Ready to prepare",
    "_REFRESH" => "Refresh",
    "_REGISTRATION" => "Student no.",
    "_REGISTRATION_ERROR" => "This student number already exists with another name in the database: ",
    "_REJECTED" => "Rejected",
    "_REMARK" => "Remark",
    "_REMARKS" => "Remarks",
    "_REMINDER" => "Reminder",
    "_REMOVE" => "Remove",
    "_REMOVE_COMPUTER" => "Remove computer allocations",
    "_REPORTED_BY" => "Reported by",
    "_REQUIRED" => "Please fill out this field.",
    "_RESET" => "Reset",
    "_RESTART" => "Restart",
    "_RESTORE" => "Restore",
    "_ROOM" => "Room",
    "_ROOM_DISPLAY_NAME" => "Room diplay name",
    "_ROOM_MAPS" => "Room occupancy",
    "_ROOMS" => "Rooms",
    "_ROOMS_COMPUTERS" => "Rooms / Computers",
    "_ROOMS_SECTORS" => "Rooms / Sectors",
    "_ROOMS_SECTORS_USERS" => "Rooms / Sectors / Users",
    "_SAVE" => "Save",
    "_SAVETIME" => "Savetime",
    "_SEB" => "SEB Config",
    "_SECTOR" => "Sector",
    "_SECTOR_DISPLAY_NAME" => "Sector display name",
    "_SECTORS" => "Sectors",
    "_SECONDARY" => "Secondary",
    "_SELECT_ALL" => "Select all",
    "_SELF_TESTING" => "Self testing",
    "_SEMESTER" => "Semester",
    "_SESSION" => "Sessions",
    "_SETUP" => "Setup",
    "_SHAREPOINT" => "Sharepoint",
    "_SHAREPOINT_DATA" => "Get data from Sharepoint",
    "_SHOW" => "Show",
    "_SHOW_UNALLOCATED" => "Show unenrolled",
    "_SID" => "User SID",
    "_SITE_ADMINISTRATION" => "Site administration",
    "_SNAPSHOT" => "Snapshot",
    "_SOFTWARE" => "Software",
    "_SOME_DISABLED" => "Some students disabled",
    "_SORT_BY" => "Sort by",
    "_STANDARD" => "Standard",
    "_STATUS" => "Status",
    "_STATUS_CODE" => "Status code",
    "_STUDENT_LIST" => "Participant list",
    "_STUDENT" => "Student",
    "_STUDENTS" => "Students",
    "_SUCCESS" => "Success",
    "_SW_CONNECTIONS" => "Connections",
    "_SYSADMIN" => "Sysadmin",
    "_TABLE_TEXT" => "<b>Click on your examination</b> and continue until you arrive at the <b>prompt for the examination password</b>.",
    "_TEMPLATE" => "Template",
    "_TEMPLATES" => "Templates",
    "_TESTING" => "Testing",
    "_TESTING_DATE" => "Testing date",
    "_TESTING_USER" => "Testing user",
    "_THEME" => "Theme",
    "_TO" => "to",
    "_TO_EXAM" => "To the exam",
    "_TOGGLE" => "Show/Hide",
    "_TOO_SHORT" => "too short",
    "_TRUE" => "True",
    "_TYPE" => "Setup",
    "_UNASSIGN" => "Unassign user",
    "_UPDATE" => "Update",
    "_UPDATE_COMPUTERS" => "Update computer lists",
    "_UPDATE_LDAP_USERS" => "Update LDAP users",
    "_UPDATED" => "Updated",
    "_UPLOAD" => "Upload",
    "_UPLOAD_COMPUTERS" => "Upload computer list",
    "_UPLOAD_SEB" => "Upload SEB-Config file",
    "_URL" => "Url",
    "_USED_BY" => "Used by",
    "_USER" => "User",
    "_USER_ADMIN" => "User Admin",
    "_USERNAME" => "Username",
    "_USERS" => "Users",
    "_USERVIEW" => "Users",
    "_VALID_TILL" => "Valid until",
    "_VALUE" => "Value",
    "_VALUES" => "Values",
    "_VDI" => "VDI",
    "_VDI_AVAILABLE" => "VDI available",
    "_VDI_BACKUPS" => "VDI Backups",
    "_VDI_CONNECTED" => "VDI connected",
    "_VDI_COORDINATION" => "VDI coordination",
    "_VDI_EXAMS" => "VDI Exams",
    "_VDI_MAX" => "VDI Max",
    "_VDI_POOL" => "VDI-Pool",
    "_VDI_POOLS" => "VDI-Pools",
    "_VDI_PROVISIONING" => "VDI Provisioning",
    "_VDI_SOFTWARE" => "VDI Software",
    "_VDI_TESTING" => "VDI Testing",
    "_VDI_USERS" => "VDI users",
    "_VDP" => "RdE",
    "_VERSION" => "Version",
    "_VHO" => "Assistents",
    "_VHO_ACCOUNTS" => "Temporary accounts",
    "_VHO_INFO_1" => "The password can only be displayed after selecting exam and student and after clicking on the letter symbol.",
    "_VHO_INFO_2" => "IMPORTANT: select \"<b>Virtual Home Organisation@SWITCHaai</b>\" as the institution.",
    "_VHO_THEMES" => "VHO themes",
    "_VISIBLE" => "Visible",
    "_VM" => "Virtual Machines",
    "_WELCOME" => "Welcome",
    "_WRONG" => "invalid",
    "_WRONG_PARAMETER" => "Invalid url",
    "_ZIP" => "ZIP file"
);

foreach ($lang_arr as $key => $value) {
    if (!defined($key)) {
        define($key, $value);
    }
}
