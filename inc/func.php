<?php

function left($str, $length)
{
	return substr($str, 0, $length);
}

function right($str, $length)
{
	return substr($str, -$length);
}

function clean($string)
{
	// Replaces all spaces
	$string = str_replace(' ', '', $string);
	// Replaces all ö with o
	$string = str_replace('ö', 'o', $string);
	// Replaces all ä with a
	$string = str_replace('ä', 'a', $string);
	// Replaces all ü with u
	$string = str_replace('ü', 'u', $string);
	 // Removes special chars
	return preg_replace('[^A-Za-z0-9_.]', '', $string);
}

function cacheStart($cachetime = 300) {
	$cachefile = $_SERVER["DOCUMENT_ROOT"] . '/admin/_cache/' . basename($_SERVER['PHP_SELF']) . '.cache';
	if (file_exists($cachefile) && time() - $cachetime <= filemtime($cachefile)) {
		$c = @file_get_contents($cachefile);
		echo $c;
		exit;
	} elseif (file_exists($cachefile)) {
		unlink($cachefile);
	} else {
		// nothing to do
	}
	ob_start();
	return $cachefile;
}

function cacheEnd($cachefile){
	$c = ob_get_contents();
	file_put_contents($cachefile, $c);
}

function isValidDate($date)
{
	return strtotime($date) !== false;
}

function inArrayR($needle, $haystack, $strict = false)
{
	foreach ($haystack as $item) {
		if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && inArrayR($needle, $item, $strict))) {
			return true;
		}
	}

	return false;
}

function invertColor($hex)
{
	$hex = str_replace('#', '', $hex);
	if (strlen($hex) !== 6) {
		return '#000000';
	}
	$new = '';
	for ($i = 0; $i < 3; $i++) {
		$rgbDigits = 255 - hexdec(substr($hex, (2 * $i), 2));
		$hexDigits = ($rgbDigits < 0) ? 0 : dechex($rgbDigits);
		$new .= (strlen($hexDigits) < 2) ? '0' . $hexDigits : $hexDigits;
	}
	return '#' . $new;
}

function getSemester($date)
{
	include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
	include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

	$qry = $SELECT_pref . " WHERE pref_name = 'kw_fs'";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$kwFS = $row['pref_value'];
	$params = explode(', ', $kwFS);

	$date = new DateTime($date);
	$year = $date->format("y");
	$week = $date->format("W");

	if (in_array($week, call_user_func_array('range', $params))) {
		$semester = 'FS' . $year;
	} elseif ($week < $params[0]) {
		$semester = 'HS' . $year-1;
	} else {
		$semester = 'HS' . $year;
	}

	return $semester;
}

function getTextColour($hex)
{
	list($red, $green, $blue) = sscanf($hex, "#%02x%02x%02x");
	$luma = ($red + $green + $blue) / 3;

	if ($luma < 128) {
		$textcolour = "white";
	} else {
		$textcolour = "black";
	}
	return $textcolour;
}
