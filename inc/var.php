<?php

//Database connection
$db_name				= getenv('DB_NAME');
$db_password			= getenv('DB_PASSWORD');
$db_port				= getenv('DB_PORT');
$db_server				= getenv('DB_SERVER');
$db_user				= getenv('DB_USER');

//Directories
$upload_dir	 			= $_SERVER['DOCUMENT_ROOT'] . "/upload";
$csv_dir				= "upload/students-csv/";
$json_dir				= "upload/vdi-file-backups/";
$logsheet_dir			= "upload/logsheet/";
$seb_dir				= "upload/seb-config/";
$vdi_dir				= "upload/vdi-files/";

//LDAP
$ldap_address 			= getenv('LDAP_SERVER');
$ldap_port 				= getenv('LDAP_PORT');
$ldap_user 				= getenv('LDAP_USER');
$ldap_password 			= getenv('LDAP_PASSWORD');
$ldap_dn				= "cn=$ldap_user,ou=admins,ou=nethz,ou=id,ou=auth,o=ethz,c=ch";
$ldap_base_dn			= "ou=nethz,ou=id,ou=auth,o=ethz,c=ch";

//Login
$login_user 			= getenv('LOGIN_USER');
$login_admin			= getenv('LOGIN_ADMIN');
$login_vho 				= getenv('LOGIN_VHO');
// keep the order like this!
$login_groups			= array($login_admin, $login_user, $login_vho);
$login_rights			= array(array($login_admin, 1, 1, 0), array($login_user, 1, 0, 0), array($login_vho, 0, 0, 1));

//VDI testing
$vdi_testing 			= getenv('VDI_TESTING');

//Moodle
$moodle_token 			= getenv('MOODLE_TOKEN');

//SQL Queries
$i = 0;
$CLEANUP[$i]['sql']		= "FROM tbl_exam_student WHERE updated_at";
$CLEANUP[$i]['id']		= "exam_student_id";
$CLEANUP[$i]['name']	= "enrollments";
$i++;
$CLEANUP[$i]['sql']		= "FROM tbl_exam_people WHERE exam_id IN (SELECT exam_id FROM tbl_exam WHERE archive = true AND exam_date";
$CLEANUP[$i]['id']		= "exam_people_id";
$CLEANUP[$i]['name']	= "exam_people";
$i++;
$CLEANUP[$i]['sql']		= "FROM tbl_logsheet WHERE exam_id IN (SELECT exam_id FROM tbl_exam WHERE archive = true AND exam_date";
$CLEANUP[$i]['id']		= "incident_id";
$CLEANUP[$i]['name']	= "incidents";
$i++;
$CLEANUP[$i]['sql']		= "FROM tbl_vho_accounts WHERE exam_id IN (SELECT exam_id FROM tbl_exam WHERE archive = true AND exam_date";
$CLEANUP[$i]['id']		= "account_id";
$CLEANUP[$i]['name']	= "vho_accounts";
$i++;
$CLEANUP[$i]['sql']		= "FROM tbl_vdi_exam WHERE exam_id IN (SELECT exam_id FROM tbl_exam WHERE archive = true AND exam_date";
$CLEANUP[$i]['id']		= "vdi_exam_id";
$CLEANUP[$i]['name']	= "vdi_exams";
$i++;
$CLEANUP[$i]['sql']		= "FROM tbl_exam WHERE archive = true AND exam_id NOT IN (SELECT exam_id FROM tbl_exam_student WHERE updated_at";
$CLEANUP[$i]['id']		= "exam_id";
$CLEANUP[$i]['name']	= "exams";
$i++;
$CLEANUP[$i]['sql']		= "FROM tbl_student WHERE student_id NOT IN (SELECT student_id FROM tbl_exam_student) AND student_id NOT IN (SELECT student_id FROM tbl_vho_accounts)";
$CLEANUP[$i]['id']		= "student_id";
$CLEANUP[$i]['name']	= "students";
$i++;
$CLEANUP[$i]['sql']		= "FROM tbl_computer WHERE computer_id NOT IN (SELECT computer_id FROM tbl_logsheet WHERE computer_id IS NOT NULL GROUP BY computer_id) AND computer_id NOT IN (SELECT computer_id FROM tbl_exam_student WHERE computer_id IS NOT NULL GROUP BY computer_id) AND room_sector_id=0";
$CLEANUP[$i]['id']		= "computer_id";
$CLEANUP[$i]['name']	= "computers";
$i++;
$CLEANUP[$i]['sql']		= "FROM tbl_computer WHERE computer_id NOT IN (SELECT computer_id FROM tbl_logsheet WHERE computer_id IS NOT NULL GROUP BY computer_id) AND computer_id NOT IN (SELECT computer_id FROM tbl_exam_student WHERE computer_id IS NOT NULL GROUP BY computer_id) AND vdi='t'";
$CLEANUP[$i]['id']		= "computer_id";
$CLEANUP[$i]['name']	= "vdi";

$DELETE_computer		= "DELETE FROM tbl_computer";

$DELETE_config_people	= "DELETE FROM tbl_config_people";

$DELETE_config_sw		= "DELETE FROM tbl_config_sw";

$DELETE_exam			= "DELETE FROM tbl_exam";

$DELETE_exam_config		= "DELETE FROM tbl_exam_config";

$DELETE_exam_name		= "DELETE FROM tbl_exam_name";

$DELETE_exam_people		= "DELETE FROM tbl_exam_people";

$DELETE_exam_status		= "DELETE FROM tbl_exam_status";

$DELETE_exam_student	= "DELETE FROM tbl_exam_student";

$DELETE_exam_user		= "DELETE FROM tbl_exam_user";

$DELETE_incident		= "DELETE FROM tbl_logsheet";

$DELETE_people			= "DELETE FROM tbl_people";

$DELETE_pool_lang		= "DELETE FROM tbl_pool_lang";

$DELETE_publisher		= "DELETE FROM tbl_publisher";

$DELETE_room			= "DELETE FROM tbl_room";

$DELETE_room_sector		= "DELETE FROM tbl_room_sector";

$DELETE_sector			= "DELETE FROM tbl_sector";

$DELETE_software		= "DELETE FROM tbl_vdi_software";

$DELETE_status_lang		= "DELETE FROM tbl_status_lang";

$DELETE_student			= "DELETE FROM tbl_student";

$DELETE_sw_connection	= "DELETE FROM tbl_sw_connection";

$DELETE_template		= "DELETE FROM tbl_template";

$DELETE_vdi_exam		= "DELETE FROM tbl_vdi_exam";

$DELETE_vdi_pool		= "DELETE FROM tbl_vdi_pool";

$DELETE_VHO_account		= "DELETE FROM tbl_vho_accounts";

$DELETE_VHO_theme		= "DELETE FROM tbl_vho_themes";

$INSERT_computer		= "INSERT INTO tbl_computer(computer_name, ip_address, room_sector_id, vdi, exam_user_id)";

$INSERT_config_people	= "INSERT INTO tbl_config_people(config_id, people_id)";

$INSERT_config_sw		= "INSERT INTO tbl_config_sw(config_id, sw_id)";

$INSERT_exam			= "INSERT INTO tbl_exam(exam_date, exam_link, exam_seb, exam_name_id, type_id, lang_id, pool_id)";

$INSERT_exam_config		= "INSERT INTO tbl_exam_config(exam_name_id, semester, files, filepath, config_remarks, people_id)";

$INSERT_exam_name		= "INSERT INTO tbl_exam_name(exam_nr, exam_name)";

$INSERT_exam_people		= "INSERT INTO tbl_exam_people(exam_id, people_id, function_id)";

$INSERT_exam_status		= "INSERT INTO tbl_exam_status(status, color, standard)";

$INSERT_exam_student	= "INSERT INTO tbl_exam_student(room_id, student_id, computer_id, accepted, exam_id, vdi_name, form)";

$INSERT_exam_student_2	= "INSERT INTO tbl_exam_student(room_id, student_id, computer_id, exam_group, us_keyboard, remark, url, exam_id, active, form)";

$INSERT_exam_user		= "INSERT INTO tbl_exam_user(exam_username, exam_user_sid)";

$INSERT_exam_type		= "INSERT INTO tbl_exam_type(type_name)";

$INSERT_hostname_ip		= "INSERT INTO tbl_computer(computer_name, ip_address)";

$INSERT_incident		= "INSERT INTO tbl_logsheet(exam_id, student_id, room_id, computer_id, multiple_computers, description, file, reported_by)";

$INSERT_login_attempt	= "INSERT INTO tbl_login_attempts(ip_address, attempts)";

$INSERT_people			= "INSERT INTO tbl_people(people_username, people_lastname, people_firstname)";

$INSERT_pool_lang		= "INSERT INTO tbl_pool_lang(pool_id, pool_description, info_text, lang_id)";

$INSERT_publisher		= "INSERT INTO tbl_publisher(publisher_name)";

$INSERT_room			= "INSERT INTO tbl_room(room_name, room_display_name, auto_refresh)";

$INSERT_room_sector		= "INSERT INTO tbl_room_sector(room_id, sector_id, sector_display_name)";

$INSERT_sector			= "INSERT INTO tbl_sector(sector_name)";

$INSERT_software		= "INSERT INTO tbl_vdi_software(sw_name, publisher_id, sw_version, sw_remarks, sw_visible)";

$INSERT_status_lang		= "INSERT INTO tbl_status_lang(status_id, status, lang_id)";

$INSERT_student			= "INSERT INTO tbl_student(student_matrikelnr, student_lastname, student_firstname)";

$INSERT_sw_connection	= "INSERT INTO tbl_sw_connection(entry_id, sw_id)";

$INSERT_template		= "INSERT INTO tbl_template(text, lang_id, type)";

$INSERT_vdi_exam		= "INSERT INTO tbl_vdi_exam(exam_id, config_id, testing_from, testing_to, self_testing, pool_id, vdi_pool, remarks, deadline, status_id, created_by, internal_info)";

$INSERT_vdi_exams		= "INSERT INTO tbl_vdi_exam(exam_id, pool_id, vdi_pool, status_id, created_by)";

$INSERT_vdi_pool		= "INSERT INTO tbl_vdi_pool(pool_description)";

$INSERT_VHO_accounts	= "INSERT INTO tbl_vho_accounts(username, password)";

$INSERT_VHO_theme		= "INSERT INTO tbl_vho_themes(theme_name, theme_values)";

$SELECT_all_exams		= "SELECT
								exam_name_id AS id,
								exam_nr,
								exam_name
							FROM tbl_exam_name
							ORDER BY exam_name";

$SELECT_all_subscriptions = "SELECT
							  tbl_exam.exam_date AS exam_date,
							  tbl_exam_name.exam_nr AS exam_nr,
							  tbl_exam_name.exam_name AS exam_name,
							  tbl_exam_type.type_name AS exam_type,
							  tbl_function.function_name AS function,
							  tbl_exam_student.exam_student_id AS student_id,
							  tbl_student.student_matrikelnr AS student_registration,
							  tbl_student.student_lastname AS student_lastname,
							  tbl_student.student_firstname AS student_firstname,
							  tbl_computer.computer_name AS computer_name,
							  tbl_computer.ip_address AS ip_address,
							  tbl_exam_student.vdi_name AS vdi_name,
							  tbl_exam_student.form AS form,
							  tbl_exam_student.created_at AS created,
							  tbl_exam_student.updated_at AS updated
							FROM
							  tbl_exam_student
							  LEFT JOIN tbl_student ON tbl_exam_student.student_id = tbl_student.student_id
							  LEFT JOIN tbl_computer ON tbl_exam_student.computer_id = tbl_computer.computer_id
							  LEFT JOIN(
								tbl_exam
								LEFT JOIN tbl_exam_name ON tbl_exam.exam_name_id = tbl_exam_name.exam_name_id
								LEFT JOIN(
									tbl_exam_people
									LEFT JOIN tbl_people ON tbl_exam_people.people_id = tbl_people.people_id
									LEFT JOIN tbl_function ON tbl_exam_people.function_id = tbl_function.function_id
								)ON tbl_exam.exam_id = tbl_exam_people.exam_id
								LEFT JOIN tbl_exam_type ON tbl_exam.type_id = tbl_exam_type.type_id
							  )ON tbl_exam_student.exam_id = tbl_exam.exam_id
							  WHERE tbl_exam_student.accepted = 't' AND ";

$SELECT_computer_by_ip	= "SELECT * FROM tbl_computer WHERE ip_address = ";

$SELECT_computer_by_VDI_user = "SELECT * FROM tbl_computer WHERE vdi = false AND exam_user_id = ";

$SELECT_computers		= "SELECT
								tbl_computer.computer_id AS computer_id,
								tbl_computer.computer_name AS computer,
								tbl_computer.ip_address AS ip_address,
								tbl_sector.sector_name AS sector_name,
								tbl_room_sector.sector_display_name AS sector_display_name,
								tbl_room.room_id AS room_id,
								tbl_room.room_name AS room_name,
								tbl_room.room_display_name AS room_display_name,
								tbl_computer.vdi AS vdi,
								tbl_computer.exam_user_id AS exam_user_id,
								tbl_exam_user.exam_username AS exam_user,
								tbl_room.auto_refresh AS auto_refresh
							FROM
								tbl_computer,
								tbl_room_sector,
								tbl_sector,
								tbl_room,
								tbl_exam_user
							WHERE
								tbl_computer.room_sector_id = tbl_room_sector.room_sector_id AND
								tbl_room_sector.sector_id = tbl_sector.sector_id AND
								tbl_room_sector.room_id = tbl_room.room_id AND
								tbl_exam_user.exam_user_id = tbl_computer.exam_user_id";

$SELECT_computers_listed = "SELECT
								computer_id AS computer_id,
								computer_name AS computer,
								ip_address AS ip
							FROM
								tbl_computer,
								tbl_room_sector,
								tbl_sector,
								tbl_room
							WHERE
								tbl_computer.room_sector_id = tbl_room_sector.room_sector_id AND
								tbl_room_sector.sector_id = tbl_sector.sector_id AND
								tbl_room_sector.room_id = tbl_room.room_id AND
								tbl_computer.vdi = FALSE";

$SELECT_config_people		= "SELECT
								tbl_config_people.people_id AS people_id,
								tbl_people.people_username AS username,
								tbl_people.people_firstname AS firstname,
								tbl_people.people_lastname AS lastname
							FROM
								tbl_config_people,
								tbl_exam_config,
								tbl_people
							WHERE
								tbl_exam_config.config_id = tbl_config_people.config_id AND
								tbl_config_people.people_id = tbl_people.people_id";

$SELECT_config_sw		= "SELECT
								tbl_config_sw.sw_id AS sw_id,
								tbl_vdi_software.sw_name AS sw_name
							FROM
								tbl_config_sw,
								tbl_exam_config,
								tbl_vdi_software
							WHERE
								tbl_exam_config.config_id = tbl_config_sw.config_id AND
								tbl_config_sw.sw_id = tbl_vdi_software.sw_id";

$SELECT_dates			= "SELECT
							tbl_exam.exam_id AS id,
							doz.people_id AS lecturer_id,
							doz.people_lastname AS lecturer_lastname,
							doz.people_firstname AS lecturer_firstname,
							vdp.people_id AS vdp_id,
							vdp.people_lastname AS vdp_lastname,
							vdp.people_firstname AS vdp_firstname,
							sys.people_id AS sys_id,
							sys.people_lastname AS sys_lastname,
							sys.people_firstname AS sys_firstname,
							tbl_exam.exam_date AS date,
							tbl_exam.exam_link AS link,
							tbl_exam.exam_seb AS seb,
							tbl_exam.lang_id AS lang_id,
							tbl_exam.disabled AS disabled,
							tbl_exam.archive AS archived,
							tbl_exam_name.exam_name_id AS exam_name_id,
							tbl_exam_name.exam_nr AS number,
							tbl_exam_name.exam_name as exam_name,
							tbl_exam_type.type_id AS type_id,
							tbl_exam_type.type_name AS type,
							tbl_language.lang_code AS lang_code,
							tbl_language.language AS language
							FROM
								tbl_exam
								LEFT JOIN tbl_exam_name ON tbl_exam.exam_name_id = tbl_exam_name.exam_name_id
								LEFT JOIN tbl_exam_type ON tbl_exam.type_id = tbl_exam_type.type_id
								LEFT JOIN tbl_language ON tbl_exam.lang_id = tbl_language.lang_id
								JOIN (SELECT
									tbl_exam_people.exam_id,
									tbl_exam_people.people_id,
									tbl_people.people_lastname,
									tbl_people.people_firstname
								FROM
									tbl_exam_people
									LEFT JOIN tbl_people ON tbl_exam_people.people_id = tbl_people.people_id
								WHERE
									tbl_exam_people.function_id = 1) doz ON doz.exam_id = tbl_exam.exam_id
								LEFT JOIN (SELECT
									tbl_exam_people.exam_id,
									tbl_exam_people.people_id,
									tbl_people.people_lastname,
									tbl_people.people_firstname
								FROM
									tbl_exam_people
									LEFT JOIN tbl_people ON tbl_exam_people.people_id = tbl_people.people_id
								WHERE
									tbl_exam_people.function_id = 3) vdp ON vdp.exam_id = tbl_exam.exam_id
								LEFT JOIN (SELECT
									tbl_exam_people.exam_id,
									tbl_exam_people.people_id,
									tbl_people.people_lastname,
									tbl_people.people_firstname
								FROM
									tbl_exam_people
									LEFT JOIN tbl_people ON tbl_exam_people.people_id = tbl_people.people_id
								WHERE
									tbl_exam_people.function_id = 4) sys ON sys.exam_id = tbl_exam.exam_id
							WHERE
							tbl_exam.exam_name_id = tbl_exam_name.exam_name_id AND
							tbl_exam.exam_id = doz.exam_id AND
							tbl_exam.lang_id = tbl_language.lang_id AND
							tbl_exam.type_id = tbl_exam_type.type_id";

$SELECT_dates_light		= "SELECT
								tbl_exam.exam_id AS id,
								doz.people_lastname AS lecturer_lastname,
								tbl_exam.exam_date AS date,
								tbl_exam_name.exam_name_id AS exam_name_id,
								tbl_exam_name.exam_nr AS exam_nr,
								tbl_exam_name.exam_name as exam_name
								FROM
									tbl_exam
									LEFT JOIN tbl_exam_name ON tbl_exam.exam_name_id = tbl_exam_name.exam_name_id
									JOIN (SELECT
										tbl_exam_people.exam_id,
										tbl_exam_people.people_id,
										tbl_people.people_lastname,
										tbl_people.people_firstname
									FROM
										tbl_exam_people
										LEFT JOIN tbl_people ON tbl_exam_people.people_id = tbl_people.people_id
									WHERE
										tbl_exam_people.function_id = 1) doz ON doz.exam_id = tbl_exam.exam_id
								WHERE
								tbl_exam.exam_name_id = tbl_exam_name.exam_name_id AND
								tbl_exam.exam_id = doz.exam_id";

$SELECT_enrollments_by_exam = "SELECT
									exam_student_id
								FROM
									tbl_exam_student
								WHERE
									exam_id = ";

$SELECT_exam_by_computer = "SELECT
								tbl_exam_student.exam_student_id AS exam_student_id,
								tbl_exam_student.exam_id AS exam_id,
								tbl_exam_student.active AS active,
								tbl_room_sector.room_id AS room_id,
								tbl_exam_name.exam_nr AS exam_nr,
								tbl_exam_name.exam_name AS exam_name,
								tbl_exam.exam_date AS exam_date,
								tbl_exam.exam_link AS exam_link,
								tbl_exam.exam_seb AS exam_seb,
								tbl_room.room_name AS room_name,
								tbl_room.room_display_name AS room_display_name,
								tbl_sector.sector_name AS sector_name,
								tbl_room_sector.sector_display_name AS sector_display_name,
								tbl_computer.ip_address AS ip_address,
								tbl_computer.computer_name AS computer_name,
								tbl_people.people_id AS people_id,
								tbl_people.people_lastname AS lecturer_lastname,
								tbl_people.people_firstname AS lecturer_firstname
							FROM
								tbl_computer LEFT JOIN (
									tbl_room_sector
									LEFT JOIN tbl_room ON tbl_room_sector.room_id = tbl_room.room_id
									LEFT JOIN tbl_sector ON tbl_room_sector.sector_id = tbl_sector.sector_id
								)ON tbl_computer.room_sector_id = tbl_room_sector.room_sector_id,
								tbl_exam_student LEFT JOIN(
									tbl_exam
									LEFT JOIN tbl_exam_name ON tbl_exam.exam_name_id = tbl_exam_name.exam_name_id
									LEFT JOIN(
										tbl_exam_people
										LEFT JOIN tbl_people ON tbl_exam_people.people_id = tbl_people.people_id
										LEFT JOIN tbl_function ON tbl_exam_people.function_id = tbl_function.function_id
									)ON tbl_exam.exam_id = tbl_exam_people.exam_id
								)ON tbl_exam_student.exam_id = tbl_exam.exam_id
							WHERE tbl_computer.computer_id = tbl_exam_student.computer_id AND
								tbl_exam_student.active = true AND
								tbl_function.function_id = 1";

$SELECT_exam_by_file	= "SELECT
								exam_id,
								exam_name,
								exam_date
							FROM
								tbl_exam, tbl_exam_name
							WHERE
								tbl_exam.exam_name_id = tbl_exam_name.exam_name_id AND
								tbl_exam.archive = false AND
								exam_seb LIKE '%";

$SELECT_exam_by_room	= "SELECT
								tbl_exam.exam_id AS exam_id,
								tbl_exam_name.exam_name_id AS exam_name_id,
								tbl_exam_name.exam_nr AS number,
								tbl_exam_name.exam_name as exam_name,
								tbl_exam.exam_date AS date,
								tbl_people.people_id AS people_id,
								tbl_people.people_lastname AS lastname,
								tbl_people.people_firstname AS firstname,
								tbl_exam_student.room_id AS room_id,
								COUNT(tbl_exam_student.student_id) AS students,
								bool_or(tbl_exam_student.active) AS active
							FROM
								tbl_exam_student,
								tbl_exam,
								tbl_exam_name,
								tbl_people,
								tbl_exam_people,
								tbl_function
							WHERE
								tbl_exam_student.exam_id = tbl_exam.exam_id	AND
								tbl_exam.exam_name_id = tbl_exam_name.exam_name_id AND
								tbl_exam.exam_id = tbl_exam_people.exam_id AND
								tbl_people.people_id = tbl_exam_people.people_id AND
								tbl_function.function_id = tbl_exam_people.function_id AND
								tbl_function.function_id = 1";

$SELECT_exam_computers	= "SELECT
								tbl_computer.computer_id AS computer_id
							FROM
								tbl_exam_student
								LEFT JOIN tbl_student ON tbl_exam_student.student_id = tbl_student.student_id
								LEFT JOIN tbl_room ON tbl_exam_student.room_id = tbl_room.room_id
								LEFT JOIN tbl_computer ON tbl_exam_student.computer_id = tbl_computer.computer_id
								LEFT JOIN(
									tbl_exam
									LEFT JOIN tbl_exam_name ON tbl_exam.exam_name_id = tbl_exam_name.exam_name_id
									LEFT JOIN(
										tbl_exam_people
										LEFT JOIN tbl_people ON tbl_exam_people.people_id = tbl_people.people_id
										LEFT JOIN tbl_function ON tbl_exam_people.function_id = tbl_function.function_id
									)ON tbl_exam.exam_id = tbl_exam_people.exam_id
									LEFT JOIN tbl_exam_type ON tbl_exam.type_id = tbl_exam_type.type_id
								)ON tbl_exam_student.exam_id = tbl_exam.exam_id
							WHERE
								tbl_exam_student.room_id = tbl_room.room_id AND
								tbl_exam_student.student_id = tbl_student.student_id AND
								tbl_exam_student.exam_id = tbl_exam.exam_id AND
								tbl_exam.exam_name_id = tbl_exam_name.exam_name_id AND
								tbl_computer.computer_id IS NOT NULL ";

$SELECT_exam_config		= "SELECT
							tbl_exam_config.config_id AS id,
							tbl_exam_config.exam_name_id AS exam_name_id,
							tbl_exam_name.exam_nr AS exam_nr,
							tbl_exam_name.exam_name AS exam_name,
							tbl_exam_config.semester AS semester,
							tbl_exam_config.files AS files,
							tbl_exam_config.filepath AS filepath,
							tbl_exam_config.config_remarks AS remarks,
							tbl_exam_config.people_id AS people_id,
							tbl_people.people_username AS username,
							tbl_people.people_lastname AS lastname,
							tbl_people.people_firstname AS firstname,
							tbl_exam_config.updated_at AS updated,
							tbl_exam_config.confirmed AS confirmed
						FROM
							tbl_exam_config,
							tbl_exam_name,
							tbl_people
						WHERE
							tbl_exam_config.exam_name_id = tbl_exam_name.exam_name_id AND
							tbl_exam_config.people_id = tbl_people.people_id";

$SELECT_exam_deadline	= "SELECT deadline FROM tbl_vdi_exam WHERE config_id = ";

$SELECT_exam_name		= "SELECT
							  exam_name_id AS exam_name_id,
							  exam_nr AS exam_number,
							  exam_name as exam_name
							FROM
							  tbl_exam_name
							ORDER BY tbl_exam_name.exam_name";

$SELECT_exam_name_by_id = "SELECT
							  tbl_exam_name.exam_nr AS exam_nr,
							  tbl_exam_name.exam_name AS exam_name,
							  tbl_exam.exam_date AS exam_date
							FROM
							  tbl_exam_name,
							  tbl_exam
							WHERE
							  tbl_exam.exam_name_id = tbl_exam_name.exam_name_id AND
							  tbl_exam.exam_id = ";

$SELECT_exam_name_by_exam = "SELECT
								tbl_exam_name.exam_name_id AS exam_name_id
							FROM
								tbl_exam_name,
								tbl_exam
							WHERE
								tbl_exam.exam_name_id = tbl_exam_name.exam_name_id AND
								tbl_exam.exam_id = ";

$SELECT_exam_people		= "SELECT
							  tbl_exam.exam_id AS id,
							  tbl_exam_name.exam_name_id AS exam_name_id,
							  tbl_exam_name.exam_nr AS exam_nr,
							  tbl_exam_name.exam_name as exam_name,
							  tbl_exam_type.type_id AS type_id,
							  tbl_exam_type.type_name AS type,
							  tbl_exam.exam_date AS date,
							  tbl_exam.exam_link AS link,
							  tbl_exam.exam_seb AS seb,
							  tbl_people.people_id AS people_id,
							  tbl_people.people_lastname AS lastname,
							  tbl_people.people_firstname AS firstname,
							  tbl_exam.disabled AS disabled,
							  tbl_exam.archive AS archived
							FROM
							  tbl_exam,
							  tbl_exam_name,
							  tbl_people,
							  tbl_exam_people,
							  tbl_function,
							  tbl_exam_type
							WHERE
							  tbl_exam.exam_name_id = tbl_exam_name.exam_name_id AND
							  tbl_exam.exam_id = tbl_exam_people.exam_id AND
							  tbl_people.people_id = tbl_exam_people.people_id AND
							  tbl_function.function_id = tbl_exam_people.function_id AND
							  tbl_function.function_id = 1 AND
							  tbl_exam.type_id = tbl_exam_type.type_id AND
							  tbl_exam.disabled = false AND
							  tbl_exam.archive = false
							ORDER BY tbl_exam.exam_date, tbl_exam_name.exam_name";

$SELECT_exam_status		= "SELECT
								tbl_exam_status.status_id AS status_id,
								tbl_exam_status.status AS status,
								tbl_status_lang.status AS status_lang,
								tbl_exam_status.color AS color,
								tbl_exam_status.standard AS standard
							FROM
								tbl_exam_status,
								tbl_status_lang,
								tbl_language
							WHERE
								tbl_exam_status.status_id = tbl_status_lang.status_id AND
								tbl_status_lang.lang_id = tbl_language.lang_id";

$SELECT_exam_students	= "SELECT
								tbl_exam_student.exam_student_id AS exam_student_id,
								tbl_student.student_id AS student_id,
								tbl_student.student_lastname AS lastname,
								tbl_student.student_firstname AS firstname,
								tbl_student.student_matrikelnr AS registration,
								tbl_room.room_id AS room_id,
								tbl_room.room_name AS room_name,
								tbl_room.room_display_name AS room_display_name,
								tbl_exam_student.us_keyboard AS us_keyboard,
								tbl_exam_student.exam_group AS group,
								tbl_exam_student.remark AS remark,
								tbl_exam_student.url AS url,
								tbl_exam_student.active AS active,
								tbl_exam_student.accepted AS accepted,
								tbl_computer.computer_id AS computer_id,
								tbl_computer.computer_name AS computer,
								tbl_computer.ip_address AS ip_address,
								tbl_exam.exam_id AS exam_id,
								tbl_exam_name.exam_name as exam_name,
								tbl_exam.exam_link AS exam_link,
								tbl_exam.exam_seb AS exam_seb,
								tbl_exam.exam_date AS exam_date,
								tbl_exam.type_id AS exam_type,
								tbl_exam_type.type_name AS exam_type_name
							FROM
								tbl_exam_student
								LEFT JOIN tbl_student ON tbl_exam_student.student_id = tbl_student.student_id
								LEFT JOIN tbl_room ON tbl_exam_student.room_id = tbl_room.room_id
								LEFT JOIN tbl_computer ON tbl_exam_student.computer_id = tbl_computer.computer_id
								LEFT JOIN(
									tbl_exam
									LEFT JOIN tbl_exam_name ON tbl_exam.exam_name_id = tbl_exam_name.exam_name_id
									LEFT JOIN(
										tbl_exam_people
										LEFT JOIN tbl_people ON tbl_exam_people.people_id = tbl_people.people_id
										LEFT JOIN tbl_function ON tbl_exam_people.function_id = tbl_function.function_id
									)ON tbl_exam.exam_id = tbl_exam_people.exam_id
									LEFT JOIN tbl_exam_type ON tbl_exam.type_id = tbl_exam_type.type_id
								)ON tbl_exam_student.exam_id = tbl_exam.exam_id
							WHERE
								tbl_exam_student.room_id = tbl_room.room_id AND
								tbl_exam_student.student_id = tbl_student.student_id AND
								tbl_exam_student.exam_id = tbl_exam.exam_id AND
								tbl_exam.exam_name_id = tbl_exam_name.exam_name_id AND
								tbl_exam_people.function_id = 1";

$SELECT_exam_students_by_exam_id = "SELECT
									tbl_student.student_id AS student_id,
									tbl_student.student_lastname AS lastname,
									tbl_student.student_firstname AS firstname,
									tbl_student.student_matrikelnr AS registration,
									tbl_exam.exam_id AS exam_id
								FROM
									tbl_exam_student
									LEFT JOIN tbl_student ON tbl_exam_student.student_id = tbl_student.student_id
									LEFT JOIN tbl_exam ON tbl_exam_student.exam_id = tbl_exam.exam_id
								WHERE
									tbl_exam_student.student_id = tbl_student.student_id AND
									tbl_exam_student.exam_id = tbl_exam.exam_id";

$SELECT_exam_students_by_room = "SELECT
								tbl_exam_student.exam_student_id AS exam_student_id,
								tbl_student.student_id AS student_id,
								tbl_student.student_lastname AS lastname,
								tbl_student.student_firstname AS firstname,
								tbl_student.student_matrikelnr AS registration,
								tbl_room.room_id AS room_id,
								tbl_room.room_name AS room_name,
								tbl_room.room_display_name AS room_display_name,
								tbl_exam_student.us_keyboard AS us_keyboard,
								tbl_exam_student.exam_group AS group,
								tbl_exam_student.remark AS remark,
								tbl_exam_student.url AS url,
								tbl_exam_student.active AS active,
								tbl_computer.computer_id AS computer_id,
								tbl_computer.computer_name AS computer,
								tbl_computer.ip_address AS ip_address,
								tbl_computer.vdi AS vdi,
								tbl_computer.exam_user_id AS exam_user_id,
								tbl_exam.exam_id AS exam_id,
								tbl_exam_name.exam_name AS exam_name,
								tbl_exam.exam_date AS exam_date,
								tbl_exam.exam_link AS exam_link,
								tbl_exam.exam_seb AS exam_seb,
								tbl_exam.archive AS archive,
								tbl_exam.disabled AS disabled,
								tbl_people.people_lastname AS lecturer_lastname,
								tbl_people.people_firstname AS lecturer_firstname
							FROM
								tbl_exam_student
								LEFT JOIN tbl_student ON tbl_exam_student.student_id = tbl_student.student_id
								LEFT JOIN tbl_room ON tbl_exam_student.room_id = tbl_room.room_id
								LEFT JOIN tbl_computer ON tbl_exam_student.computer_id = tbl_computer.computer_id
								LEFT JOIN(
									tbl_exam
									LEFT JOIN tbl_exam_name ON tbl_exam.exam_name_id = tbl_exam_name.exam_name_id
									LEFT JOIN(
										tbl_exam_people
										LEFT JOIN tbl_people ON tbl_exam_people.people_id = tbl_people.people_id
										LEFT JOIN tbl_function ON tbl_exam_people.function_id = tbl_function.function_id
									)ON tbl_exam.exam_id = tbl_exam_people.exam_id
									LEFT JOIN tbl_exam_type ON tbl_exam.type_id = tbl_exam_type.type_id
								)ON tbl_exam_student.exam_id = tbl_exam.exam_id
							WHERE
								tbl_function.function_id = 1 AND
								tbl_exam_student.room_id = ";

$SELECT_exam_students_by_VDI_user = "SELECT
								tbl_exam_student.exam_student_id AS exam_student_id,
								tbl_exam_student.accepted AS accepted,
								tbl_student.student_id AS student_id,
								tbl_student.student_lastname AS lastname,
								tbl_student.student_firstname AS firstname,
								tbl_student.student_matrikelnr AS registration,
								tbl_room.room_id AS room_id,
								tbl_room.room_name AS room_name,
								tbl_room.room_display_name AS room_display_name,
								tbl_computer.computer_id AS computer_id,
								tbl_computer.computer_name AS computer,
								tbl_computer.ip_address AS ip_address,
								tbl_exam.exam_id AS exam_id,
								tbl_exam_name.exam_name AS exam_name,
								tbl_exam.exam_date AS exam_date,
								tbl_people.people_lastname AS lecturer_lastname,
								tbl_people.people_firstname AS lecturer_firstname
							FROM
								tbl_exam_student
								LEFT JOIN tbl_student ON tbl_exam_student.student_id = tbl_student.student_id
								LEFT JOIN tbl_room ON tbl_exam_student.room_id = tbl_room.room_id
								LEFT JOIN(
									tbl_computer
									LEFT JOIN tbl_exam_user ON tbl_computer.exam_user_id = tbl_exam_user.exam_user_id
									)ON tbl_exam_student.computer_id = tbl_computer.computer_id
								LEFT JOIN(
									tbl_exam
									LEFT JOIN tbl_exam_name ON tbl_exam.exam_name_id = tbl_exam_name.exam_name_id
									LEFT JOIN(
										tbl_exam_people
										LEFT JOIN tbl_people ON tbl_exam_people.people_id = tbl_people.people_id
										LEFT JOIN tbl_function ON tbl_exam_people.function_id = tbl_function.function_id
									)ON tbl_exam.exam_id = tbl_exam_people.exam_id
									LEFT JOIN tbl_exam_type ON tbl_exam.type_id = tbl_exam_type.type_id
								)ON tbl_exam_student.exam_id = tbl_exam.exam_id
							WHERE
								tbl_function.function_id = 1 AND
								tbl_exam_student.accepted = true AND
								tbl_exam.archive = false AND
								tbl_exam.disabled = false AND
								tbl_exam.exam_date = CURRENT_DATE AND
								tbl_exam_user.exam_username = ";

$SELECT_exam_type		= "SELECT
							   type_id AS type_id,
							   type_name AS type
							FROM
							   tbl_exam_type";

$SELECT_exam_url_by_exam_id = "SELECT
								exam_link,
								exam_seb
							FROM
								tbl_exam
							WHERE
								exam_id =";

$SELECT_exam_url_by_student = "SELECT
								tbl_exam_student.url
							FROM
								tbl_exam_student
							WHERE
								tbl_exam_student.active = true AND
								tbl_exam_student.exam_student_id=";

$SELECT_exam_user_id 	= "SELECT exam_user_id FROM tbl_exam_user WHERE tbl_exam_user.exam_username=";

$SELECT_exam_users		= "SELECT * FROM tbl_exam_user";

$SELECT_exams			= "SELECT * FROM tbl_exam";

$SELECT_functions		= "SELECT * FROM tbl_function";

$SELECT_language		= "SELECT * FROM tbl_language";

$SELECT_lang_default	= "SELECT lang_id AS lang_id, lang_code AS lang FROM tbl_language LIMIT 1";

$SELECT_lang_refresh_by_computer = "SELECT
								tbl_exam.lang_id,
								tbl_language.lang_code,
								tbl_exam_student.computer_id,
								tbl_computer.ip_address,
								tbl_room.auto_refresh
							FROM
								tbl_exam,
								tbl_language,
								tbl_exam_student,
								tbl_computer,
								tbl_room_sector,
								tbl_room
							WHERE
								tbl_exam.exam_id = tbl_exam_student.exam_id AND
								tbl_exam.lang_id = tbl_language.lang_id AND
								tbl_exam_student.computer_id = tbl_computer.computer_id AND
								tbl_computer.room_sector_id = tbl_room_sector.room_sector_id AND
								tbl_room_sector.room_id = tbl_room.room_id AND
								tbl_exam_student.active = true AND
								tbl_computer.ip_address = ";

$SELECT_lang_refresh_by_room = "SELECT
								tbl_exam.lang_id,
								tbl_language.lang_code,
								tbl_exam_student.room_id,
								tbl_room.auto_refresh
							FROM
								tbl_exam,
								tbl_language,
								tbl_exam_student,
								tbl_room
							WHERE
								tbl_exam.exam_id = tbl_exam_student.exam_id AND
								tbl_exam.lang_id = tbl_language.lang_id AND
								tbl_exam_student.room_id = tbl_room.room_id AND
								tbl_exam_student.room_id = ";

$SELECT_login_attempt	= "SELECT * from tbl_login_attempts WHERE ip_address=";

$SELECT_logsheet		= "SELECT
								tbl_logsheet.incident_id,
								tbl_logsheet.student_id,
								tbl_logsheet.exam_id,
								tbl_logsheet.room_id,
								tbl_logsheet.computer_id,
								tbl_logsheet.multiple_computers,
								tbl_logsheet.description,
								tbl_logsheet.file,
								tbl_logsheet.reported_by,
								tbl_logsheet.created_at,
								tbl_logsheet.updated_at,
								tbl_exam.archive
							FROM
								tbl_logsheet,
								tbl_exam
							WHERE
								tbl_logsheet.exam_id = tbl_exam.exam_id";

$SELECT_navigation		= "SELECT * FROM tbl_navigation";

$SELECT_navigation_groups = "SELECT nav_group FROM tbl_navigation GROUP BY nav_group ORDER BY nav_group";

$SELECT_people			= "SELECT
							  tbl_people.people_id AS id,
							  tbl_people.people_username AS username,
							  tbl_people.people_lastname AS lastname,
							  tbl_people.people_firstname AS firstname
							FROM
							  tbl_people";

$SELECT_people_id		= "SELECT people_id FROM tbl_people";

$SELECT_people_without_function = "SELECT
							  tbl_people.people_id AS id,
							  tbl_people.people_username AS username,
							  tbl_people.people_lastname AS lastname,
							  tbl_people.people_firstname AS firstname
							FROM
							  tbl_people
							WHERE NOT EXISTS(
								SELECT
								FROM tbl_exam_people
								WHERE people_id = tbl_people.people_id
							)
							ORDER BY tbl_people.people_lastname";

$SELECT_pool_lang_info_text = "SELECT info_text FROM tbl_pool_lang";

$SELECT_pref			= "SELECT pref_value FROM tbl_preferences";

$SELECT_prefs			= "SELECT * FROM tbl_preferences ORDER BY pref_name";

$SELECT_publisher		= "SELECT * FROM tbl_publisher";

$SELECT_refresh_by_room = "SELECT
								tbl_room.auto_refresh
							FROM
								tbl_room
							WHERE
								tbl_room.room_id = ";

$SELECT_room_by_computer = "SELECT
								tbl_room.room_id AS room_id,
								tbl_room.room_name AS room_name,
								tbl_room.room_display_name AS room_display_name
							FROM
								tbl_room,
								tbl_computer,
								tbl_room_sector
							WHERE
								tbl_computer.room_sector_id = tbl_room_sector.room_sector_id AND
								tbl_room_sector.room_id = tbl_room.room_id";

$SELECT_room_by_name	= "SELECT * FROM tbl_room WHERE tbl_room.room_name=";

$SELECT_room_by_sector	= "SELECT room_id FROM tbl_room_sector WHERE room_sector_id=";

$SELECT_room_sector		= "SELECT
								tbl_room_sector.room_sector_id,
								tbl_room_sector.room_id,
								tbl_room.room_name,
								tbl_room_sector.sector_id,
								tbl_sector.sector_name,
								tbl_room_sector.sector_display_name
							FROM tbl_room_sector
							LEFT JOIN tbl_room ON tbl_room_sector.room_id = tbl_room.room_id
							LEFT JOIN tbl_sector ON tbl_room_sector.sector_id = tbl_sector.sector_id
							WHERE tbl_room_sector.room_id > 0
							ORDER BY room_name, sector_name";

$SELECT_room_sector_by_computer	= "SELECT tbl_computer.room_sector_id AS room_sector_id, tbl_room.room_name AS room_name FROM tbl_computer LEFT JOIN (
									tbl_room_sector
									LEFT JOIN tbl_room ON tbl_room_sector.room_id = tbl_room.room_id
									LEFT JOIN tbl_sector ON tbl_room_sector.sector_id = tbl_sector.sector_id
								)ON tbl_computer.room_sector_id = tbl_room_sector.room_sector_id";

$SELECT_room_sector_id_by_room = "SELECT tbl_room_sector.room_sector_id
							FROM tbl_room_sector
							LEFT JOIN tbl_room ON tbl_room_sector.room_id = tbl_room.room_id
							WHERE tbl_room.room_name=";

$SELECT_rooms				= "SELECT * FROM tbl_room WHERE room_id > 0";

$SELECT_rooms_plus_empty 	= "SELECT * FROM tbl_room ORDER BY room_name";

$SELECT_status_inactive 	= "SELECT status_id FROM tbl_exam_status WHERE standard='false'";

$SELECT_sector 				= "SELECT
								tbl_room_sector.room_sector_id,
								tbl_room_sector.sector_id,
								tbl_room_sector.room_id,
								tbl_sector.sector_name,
								tbl_room_sector.sector_display_name AS sector_display_name
							FROM tbl_room_sector LEFT JOIN tbl_sector ON tbl_room_sector.sector_id = tbl_sector.sector_id
							WHERE tbl_room_sector.room_id=";

$SELECT_sectors			= "SELECT * FROM tbl_sector WHERE sector_id > 0";

$SELECT_software		= "SELECT
								tbl_vdi_software.sw_id as id,
								tbl_vdi_software.sw_name as software,
								tbl_vdi_software.publisher_id as publisher_id,
								tbl_publisher.publisher_name as publisher,
								tbl_vdi_software.sw_version as version,
								tbl_vdi_software.sw_remarks as remarks,
								tbl_vdi_software.sw_visible as visible
							FROM tbl_vdi_software, tbl_publisher
							WHERE tbl_vdi_software.publisher_id = tbl_publisher.publisher_id";

$SELECT_student_by_computer = "SELECT
								tbl_exam_student.exam_student_id AS exam_student_id,
								tbl_exam_student.exam_id AS exam_id,
								tbl_exam_name.exam_nr AS exam_nr,
								tbl_exam.exam_link AS exam_link,
								tbl_exam.exam_seb AS exam_seb,
								tbl_exam.type_id AS exam_type,
								tbl_exam_student.student_id AS student_id,
								tbl_student.student_matrikelnr AS student_matrikelnr,
								tbl_student.student_lastname AS student_lastname,
								tbl_student.student_firstname AS student_firstname,
								tbl_computer.ip_address AS ip_address,
								tbl_computer.computer_name AS computer_name,
								tbl_computer.vdi AS vdi,
								tbl_exam_student.us_keyboard AS us_keyboard,
								tbl_exam_student.exam_group AS group,
								tbl_exam_student.remark AS remark,
								tbl_exam_student.active AS active
							   FROM
								tbl_exam_student
								LEFT JOIN tbl_student ON tbl_exam_student.student_id = tbl_student.student_id
								LEFT JOIN tbl_computer ON tbl_exam_student.computer_id = tbl_computer.computer_id
								LEFT JOIN(
									tbl_exam LEFT JOIN tbl_exam_name ON tbl_exam.exam_name_id = tbl_exam_name.exam_name_id
								)ON tbl_exam_student.exam_id = tbl_exam.exam_id
								WHERE tbl_exam_student.active = true AND tbl_computer.ip_address = ";

$SELECT_students		= "SELECT * FROM tbl_student";

$SELECT_sw_connections 	= "SELECT entry_id FROM tbl_sw_connection GROUP BY entry_id ORDER BY entry_id";

$SELECT_sw_conn_entry 	= "SELECT
								tbl_sw_connection.sw_id AS sw_id,
								tbl_vdi_software.sw_name AS sw_name
							FROM
								tbl_sw_connection,
								tbl_vdi_software
							WHERE
								tbl_sw_connection.sw_id = tbl_vdi_software.sw_id AND
								tbl_sw_connection.entry_id=";

$SELECT_template_types	= "SELECT tbl_template.type AS type FROM tbl_template GROUP BY type ORDER BY type";

$SELECT_templates		= "SELECT
								tbl_template.template_id AS template_id,
								tbl_template.text AS text,
								tbl_template.lang_id AS lang_id,
								tbl_language.lang_code AS lang_code,
								tbl_template.type AS type
							 FROM
							 	tbl_template,
								tbl_language
							WHERE
								tbl_template.lang_id = tbl_language.lang_id";

$SELECT_vdi_exam		= "SELECT
								tbl_vdi_exam.vdi_exam_id AS id,
								tbl_vdi_exam.exam_id AS exam_id,
								tbl_exam.exam_date AS exam_date,
								tbl_exam_name.exam_name_id AS exam_name_id,
								tbl_exam_name.exam_nr AS exam_nr,
								tbl_exam_name.exam_name AS exam_name,
								doz.people_id AS lecturer_id,
								doz.people_lastname AS lecturer_lastname,
								doz.people_firstname AS lecturer_firstname,
								tbl_vdi_exam.config_id AS config_id,
								tbl_exam_config.people_id AS config_user_id,
								tbl_people.people_username AS config_username,
								tbl_people.people_lastname AS config_lastname,
								tbl_people.people_firstname AS config_firstname,
								tbl_exam_config.config_remarks AS config_remarks,
								tbl_exam_config.files AS config_files,
								tbl_exam_config.filepath AS config_filepath,
								tbl_exam_config.updated_at AS updated,
								tbl_exam_config.semester AS semester,
								tbl_vdi_exam.pool_id AS pool_id,
								tbl_pool_lang.pool_description AS vdi_pool_setup,
								tbl_vdi_exam.vdi_pool AS vdi_pool,
								tbl_vdi_exam.testing_from AS testing_from,
								tbl_vdi_exam.testing_to AS testing_to,
								tbl_vdi_exam.self_testing AS self_testing,
								tbl_vdi_exam.remarks AS remarks,
								tbl_vdi_exam.deadline AS deadline,
								tbl_vdi_exam.status_id AS status_id,
								tbl_status_lang.status AS status,
								tbl_exam_status.color AS color,
								tbl_vdi_exam.created_by AS creator
							FROM
								tbl_vdi_exam
								LEFT JOIN(
									tbl_exam_config
									LEFT JOIN tbl_people ON tbl_exam_config.people_id = tbl_people.people_id
								)ON tbl_vdi_exam.config_id = tbl_exam_config.config_id
								LEFT JOIN(
									tbl_exam
									LEFT JOIN tbl_exam_name ON tbl_exam.exam_name_id = tbl_exam_name.exam_name_id
								)ON tbl_vdi_exam.exam_id = tbl_exam.exam_id
								LEFT JOIN(
									tbl_vdi_pool
									LEFT JOIN(
										tbl_pool_lang
										LEFT JOIN tbl_language t_lang1 ON tbl_pool_lang.lang_id = t_lang1.lang_id
									)ON tbl_vdi_pool.pool_id = tbl_pool_lang.pool_id
								)ON tbl_vdi_pool.pool_id = tbl_vdi_exam.pool_id
								LEFT JOIN(
									tbl_exam_status
									LEFT JOIN(
										tbl_status_lang
										LEFT JOIN tbl_language t_lang2 ON tbl_status_lang.lang_id = t_lang2.lang_id
									)ON tbl_exam_status.status_id = tbl_status_lang.status_id
								)ON tbl_vdi_exam.status_id = tbl_exam_status.status_id
								JOIN (SELECT
									tbl_exam_people.exam_id,
									tbl_exam_people.people_id,
									tbl_people.people_lastname,
									tbl_people.people_firstname
								FROM
									tbl_exam_people
									LEFT JOIN tbl_people ON tbl_exam_people.people_id = tbl_people.people_id
								WHERE
									tbl_exam_people.function_id = 1) doz ON doz.exam_id = tbl_exam.exam_id";

$SELECT_vdi_info		= "SELECT internal_info AS info FROM tbl_vdi_exam";

$SELECT_vdi_pool 		= "SELECT
								tbl_vdi_pool.pool_id AS pool_id
							FROM
								tbl_vdi_pool,
								tbl_pool_lang
							WHERE
								tbl_vdi_pool.pool_id = tbl_pool_lang.pool_id AND
								tbl_pool_lang.lang_id = 1";

$SELECT_vdi_pools		= "SELECT
								tbl_vdi_pool.pool_id AS pool_id,
								tbl_vdi_pool.pool_description AS description,
								tbl_pool_lang.pool_description AS description_lang,
								tbl_pool_lang.info_text AS info_text
							FROM
								tbl_vdi_pool,
								tbl_pool_lang,
								tbl_language
							WHERE
								tbl_vdi_pool.pool_id = tbl_pool_lang.pool_id AND
								tbl_pool_lang.lang_id = tbl_language.lang_id";

$SELECT_VHO_accounts	= "SELECT * FROM tbl_vho_accounts";

$SELECT_VHO_themes		= "SELECT * FROM tbl_vho_themes";

$UPDATE_computer		= "UPDATE tbl_computer";

$UPDATE_exam			= "UPDATE tbl_exam";

$UPDATE_exam_computers	= "UPDATE tbl_exam_student SET computer_id = NULL WHERE accepted = 'false' AND exam_id = ";

$UPDATE_exam_config		= "UPDATE tbl_exam_config";

$UPDATE_exam_name		= "UPDATE tbl_exam_name";

$UPDATE_exam_people		= "UPDATE tbl_exam_people";

$UPDATE_exam_status		= "UPDATE tbl_exam_status";

$UPDATE_exam_student	= "UPDATE tbl_exam_student";

$UPDATE_exam_user		= "UPDATE tbl_exam_user";

$UPDATE_incident		= "UPDATE tbl_logsheet";

$UPDATE_login_attempt	= "UPDATE tbl_login_attempts";

$UPDATE_navigation		= "UPDATE tbl_navigation";

$UPDATE_people			= "UPDATE tbl_people";

$UPDATE_pool_lang       = "UPDATE tbl_pool_lang";

$UPDATE_publisher		= "UPDATE tbl_publisher";

$UPDATE_preference		= "UPDATE tbl_preferences";

$UPDATE_room			= "UPDATE tbl_room";

$UPDATE_room_sector		= "UPDATE tbl_room_sector";

$UPDATE_sector			= "UPDATE tbl_sector";

$UPDATE_software		= "UPDATE tbl_vdi_software";

$UPDATE_status_lang 	= "UPDATE tbl_status_lang";

$UPDATE_student			= "UPDATE tbl_student";

$UPDATE_template		= "UPDATE tbl_template";

$UPDATE_vdi_exam		= "UPDATE tbl_vdi_exam";

$UPDATE_vdi_pool		= "UPDATE tbl_vdi_pool";

$UPDATE_VHO_account		= "UPDATE tbl_vho_accounts";

$UPDATE_VHO_theme		= "UPDATE tbl_vho_themes";
