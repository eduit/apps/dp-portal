<?php

$conn_string = "host=" . $db_server . " port=" . $db_port . " dbname=" . $db_name . " user=" . $db_user . " password=" . $db_password;
$con = pg_connect($conn_string) or die("The database is unavailable. " . pg_last_error() . "<br>Please contact the exam supervisors and select the direct link to the exam system: <a href=\"https://moodle-app6.let.ethz.ch\">LINK</a>");
