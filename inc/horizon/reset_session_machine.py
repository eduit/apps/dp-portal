import requests
import json
import vmware_horizon
import sys

requests.packages.urllib3.disable_warnings()

hvconnectionobj = vmware_horizon.Connection()
hvconnectionobj.hv_connect()

inventory = vmware_horizon.Inventory(
    url=hvconnectionobj.url, access_token=hvconnectionobj.access_token
)

session_id = [sys.argv[1]]
execute = inventory.reset_session_machines(session_ids=session_id)
res = json.dumps(execute).strip('"')
print(res)

end = hvconnectionobj.hv_disconnect()
