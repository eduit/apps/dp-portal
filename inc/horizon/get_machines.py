import requests
import json
import vmware_horizon
import sys

requests.packages.urllib3.disable_warnings()

hvconnectionobj = vmware_horizon.Connection()
hvconnectionobj.hv_connect()

inventory = vmware_horizon.Inventory(
    url=hvconnectionobj.url, access_token=hvconnectionobj.access_token
)

if len(sys.argv) >= 2:
    desktopPoolID = sys.argv[1]
    filter = {}
    filter["type"] = "And"
    filter["filters"] = []
    filter1 = {}
    filter1["type"] = "Equals"
    filter1["name"] = "desktop_pool_id"
    filter1["value"] = desktopPoolID
    filter["filters"].append(filter1)
else:
    filter = {}

machines = inventory.get_machines(maxpagesize=hvconnectionobj.licenses, filter=filter)
res = json.dumps(machines).strip('"')
print(res)

end = hvconnectionobj.hv_disconnect()
