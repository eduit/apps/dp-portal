import requests
import json
import vmware_horizon

requests.packages.urllib3.disable_warnings()

hvconnectionobj = vmware_horizon.Connection()
hvconnectionobj.hv_connect()

monitor = obj = vmware_horizon.Monitor(
    url=hvconnectionobj.url, access_token=hvconnectionobj.access_token
)
external = vmware_horizon.External(
    url=hvconnectionobj.url, access_token=hvconnectionobj.access_token
)

vcenters = monitor.virtual_centers()
vcid = vcenters[0]["id"]
dcs = external.get_datacenters(vcenter_id=vcid)
dcid = dcs[0]["id"]

parents = external.get_base_vms(
    vcenter_id=vcid, datacenter_id=dcid, filter_incompatible_vms=True
)
res = json.dumps(parents).strip('"')
print(res)

end = hvconnectionobj.hv_disconnect()
