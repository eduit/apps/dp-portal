import os
import json
import requests
import requests_cache
import urllib
import getpass

# run from local
#from dotenv import load_dotenv

#load_dotenv()

APPLICATION_JSON = "application/json"
ERROR = "Error: "
CACHING_TIME = 10


class HorizonError(Exception):
    """Exception class from which every exception in this library will derive.
    It enables other projects using this library to catch all errors coming
    from the library with a single "except" statement
    """

    pass


class BadUserInputError(HorizonError):
    """A specific error"""

    pass


class GeneralError(HorizonError):
    """A general error"""

    pass


class Connection:
    """The Connection class is used to handle connections and disconnections to and from the VMware Horizon REST API's"""

    def __init__(self):
        """ "The default object for the connection class needs to be created using username, password, domain and url in plain text."""
        self.domain = "d"
        self.url = os.getenv("HORIZON_URL")
        self.username = os.getenv("HORIZON_USER")
        self.password = os.getenv("HORIZON_PW")
        self.access_token = ""
        self.refresh_token = ""
        self.licenses = 1000

    def hv_connect(self):
        """Used to authenticate to the VMware Horizon REST API's"""
        headers = {
            "accept": "*/*",
            "Content-Type": APPLICATION_JSON,
        }

        data = {
            "domain": self.domain,
            "password": self.password,
            "username": self.username,
        }
        json_data = json.dumps(data)

        response = requests.post(
            f"{self.url}/rest/login", verify=True, headers=headers, data=json_data
        )
        if response.status_code == 400:
            error_message = response.json()
            raise BadUserInputError(f"Error {response.status_code}: {error_message}")
        elif response.status_code != 200:
            raise GeneralError(f"Error {response.status_code}: {response.reason}")
        else:
            try:
                response.raise_for_status()
            except requests.exceptions.RequestException as e:
                raise GeneralError(ERROR + str(e))
            else:
                data = response.json()
                self.access_token = {
                    "accept": "*/*",
                    "Authorization": "Bearer " + data["access_token"],
                }
                self.refresh_token = data["refresh_token"]
                return self

    def hv_disconnect(self):
        """ "Used to close close the connection with the VMware Horizon REST API's"""
        headers = {
            "accept": "*/*",
            "Content-Type": APPLICATION_JSON,
        }
        data = {"refresh_token": self.refresh_token}
        json_data = json.dumps(data)
        response = requests.post(
            f"{self.url}/rest/logout", verify=True, headers=headers, data=json_data
        )
        if response.status_code == 400:
            error_message = response.json()
            raise BadUserInputError(f"Error {response.status_code}: {error_message}")
        elif response.status_code != 200:
            raise GeneralError(f"Error {response.status_code}: {response.reason}")
        else:
            try:
                response.raise_for_status()
            except requests.exceptions.RequestException as e:
                raise GeneralError(ERROR + str(e))
            else:
                return response.status_code


class Inventory:
    def __init__(self, url: str, access_token: dict):
        """Default object for the pools class where all Desktop Pool Actions will be performed."""
        self.url = url
        self.access_token = access_token

    def get_machine(self, machine_id:str) -> dict:
        """Gets the Machine information.

        Requires id of a machine
        Available for Horizon 7.12 and later."""
        response = requests.get(f'{self.url}/rest/inventory/v5/machines/{machine_id}', verify=False,  headers=self.access_token)
        if response.status_code == 400:
            error_message = (response.json())["error_message"]
            raise GeneralError(f"Error {response.status_code}: {error_message}")
        elif response.status_code != 200:
            raise GeneralError(f"Error {response.status_code}: {response.reason}")
        else:
            try:
                response.raise_for_status()
            except requests.exceptions.RequestException as e:
                raise GeneralError(ERROR + str(e))
            else:
                return response.json()

    def get_machines(self, maxpagesize: int = 100, filter: dict = "") -> list:
        """Lists the Machines in the environment.

        For information on filtering see https://vdc-download.vmware.com/vmwb-repository/dcr-public/f92cce4b-9762-4ed0-acbd-f1d0591bd739/235dc19c-dabd-43f2-8d38-8a7a333e914e/HorizonServerRESTPaginationAndFilterGuide.doc
        Available for Horizon 8 2006 and later."""

        def int_get_machines(self, page: int, maxpagesize: int, filter: dict = ""):
            if filter != "":
                filter_url = urllib.parse.quote(
                    json.dumps(filter, separators=(", ", ":"))
                )
                add_filter = f"{filter_url}"
                requests_cache.install_cache("machine_cache", expire_after=CACHING_TIME)
                response = requests.get(
                    f"{self.url}/rest/inventory/v5/machines?filter={add_filter}&page={page}&size={maxpagesize}",
                    verify=True,
                    headers=self.access_token,
                )
            else:
                requests_cache.install_cache("machine_cache", expire_after=CACHING_TIME)
                response = requests.get(
                    f"{self.url}/rest/inventory/v5/machines?page={page}&size={maxpagesize}",
                    verify=True,
                    headers=self.access_token,
                )
            if response.status_code == 400:
                error_message = response.json()
                raise BadUserInputError(
                    f"Error {response.status_code}: {error_message}"
                )
            elif response.status_code != 200:
                raise GeneralError(f"Error {response.status_code}: {response.reason}")
            else:
                try:
                    response.raise_for_status()
                except requests.exceptions.RequestException as e:
                    raise GeneralError(ERROR + str(e))
                else:
                    return response

        if maxpagesize > 1000:
            maxpagesize = 1000
        page = 1
        response = int_get_machines(
            self, page=page, maxpagesize=maxpagesize, filter=filter
        )
        results = response.json()
        while "HAS_MORE_RECORDS" in response.headers:
            page += 1
            response = int_get_machines(
                self, page=page, maxpagesize=maxpagesize, filter=filter
            )
            results += response.json()
        return results

    def delete_machines(
        self,
        machine_ids: list,
        delete_from_multiple_pools: bool = False,
        force_logoff: bool = False,
        delete_from_disk: bool = False,
    ):
        """deletes the specified machines

        Requires list of ids of the machines to remove
        Optional arguments (all default to False): delete_from_multiple_pools, force_logoff and delete_from_disk
        Available for Horizon 8 2006 and later."""
        headers = self.access_token
        headers["Content-Type"] = APPLICATION_JSON
        data = {}
        machine_delete_data = {}
        machine_delete_data["allow_delete_from_multi_desktop_pools"] = (
            delete_from_multiple_pools
        )
        machine_delete_data["archive_persistent_disk"] = False
        machine_delete_data["delete_from_disk"] = delete_from_disk
        machine_delete_data["force_logoff_session"] = force_logoff
        data["machine_delete_data"] = machine_delete_data
        data["machine_ids"] = machine_ids
        json_data = json.dumps(data)
        response = requests.delete(
            f"{self.url}/rest/inventory/v1/machines",
            verify=True,
            headers=headers,
            data=json_data,
        )
        return response.json()

    def reset_machines(self, machine_ids: list):
        """Resets the specified machines.

        Requires a List of Machine Ids representing the machines to be reset.
        Available for Horizon 8 2006 and later."""
        headers = self.access_token
        headers["Content-Type"] = APPLICATION_JSON
        json_data = json.dumps(machine_ids)
        response = requests.post(
            f"{self.url}/rest/inventory/v1/machines/action/reset",
            verify=True,
            headers=headers,
            data=json_data,
        )
        return response.json()

    def restart_machines(self, machine_ids: list):
        """Restarts the specified machines.

        Requires a List of Machine Ids representing the machines to be restarted.
        Available for Horizon 8 2006 and later."""
        headers = self.access_token
        headers["Content-Type"] = APPLICATION_JSON
        json_data = json.dumps(machine_ids)
        response = requests.post(
            f"{self.url}/rest/inventory/v1/machines/action/restart",
            verify=True,
            headers=headers,
            data=json_data,
        )
        return response.json()

    def get_sessions(self, filter: dict = "", maxpagesize: int = 100) -> list:
        """Lists the Session information in the environment.

        Will default to 1000 results with a pagesize of 100, max pagesize is 1000.
        Available for Horizon 8 2006 and later, filtering available since Horizon 2103.
        """

        def int_get_sessions(self, page: int, maxpagesize: int, filter: dict = ""):
            if filter != "":
                filter_url = urllib.parse.quote(
                    json.dumps(filter, separators=(", ", ":"))
                )
                add_filter = f"{filter_url}"
                requests_cache.install_cache("session_cache", expire_after=CACHING_TIME)
                response = requests.get(
                    f"{self.url}/rest/inventory/v1/sessions?filter={add_filter}&page={page}&size={maxpagesize}",
                    verify=True,
                    headers=self.access_token,
                )
            else:
                requests_cache.install_cache("session_cache", expire_after=CACHING_TIME)
                response = requests.get(
                    f"{self.url}/rest/inventory/v1/sessions?page={page}&size={maxpagesize}",
                    verify=True,
                    headers=self.access_token,
                )

            if response.status_code == 400:
                error_message = response.json()
                raise BadUserInputError(
                    f"Error {response.status_code}: {error_message}"
                )
            elif response.status_code != 200:
                raise GeneralError(f"Error {response.status_code}: {response.reason}")
            else:
                try:
                    response.raise_for_status()
                except requests.exceptions.RequestException as e:
                    raise GeneralError(ERROR + str(e))
                else:
                    return response

        if maxpagesize > 1000:
            maxpagesize = 1000
        page = 1
        response = int_get_sessions(
            self, page=page, maxpagesize=maxpagesize, filter=filter
        )
        results = response.json()
        while "HAS_MORE_RECORDS" in response.headers:
            page += 1
            response = int_get_sessions(
                self, page=page, maxpagesize=maxpagesize, filter=filter
            )
            results += response.json()
        return results

    def disconnect_sessions(self, session_ids: list):
        """Disconnects user sessions.

        Requires list of session ids
        Available for Horizon 8 2006 and later."""
        headers = self.access_token
        headers["Content-Type"] = APPLICATION_JSON
        json_data = json.dumps(session_ids)
        response = requests.post(
            f"{self.url}/rest/inventory/v1/sessions/action/disconnect",
            verify=True,
            headers=headers,
            data=json_data,
        )
        return response.json()

    def logoff_sessions(self, session_ids: list, forced_logoff: bool = False):
        """Logs user sessions off.

        Requires list of session ids optional to set forced to True to force a log off (defaults to False)
        Available for Horizon 8 2006 and later."""
        headers = self.access_token
        headers["Content-Type"] = APPLICATION_JSON
        json_data = json.dumps(session_ids)
        if forced_logoff == True:
            forced_logoff = "true"
        else:
            forced_logoff = "false"
        response = requests.post(
            f"{self.url}/rest/inventory/v1/sessions/action/logoff?forced={forced_logoff}",
            verify=True,
            headers=headers,
            data=json_data,
        )
        return response.json()

    def reset_session_machines(self, session_ids: list):
        """Resets machine of user sessions. The machine must be managed by Virtual Center and the session cannot be an application or an RDS desktop session.

        Requires list of session ids
        Available for Horizon 8 2006 and later."""
        headers = self.access_token
        headers["Content-Type"] = APPLICATION_JSON
        json_data = json.dumps(session_ids)
        response = requests.post(
            f"{self.url}/rest/inventory/v1/sessions/action/reset",
            verify=True,
            headers=headers,
            data=json_data,
        )
        return response.json()

    def restart_session_machines(self, session_ids: list):
        """Restarts machine of user sessions. The machine must be managed by Virtual Center and the session cannot be an application or an RDS desktop session.

        Requires list of session ids
        Available for Horizon 8 2006 and later."""
        headers = self.access_token
        headers["Content-Type"] = APPLICATION_JSON
        json_data = json.dumps(session_ids)
        response = requests.post(
            f"{self.url}/rest/inventory/v1/sessions/action/restart",
            verify=True,
            headers=headers,
            data=json_data,
        )
        return response.json()

    def remove_machines(self, desktop_pool_id: str, machine_ids: list):
        """Removes machines from the given manual desktop pool.

        Requires list of machine_ids and desktop_pool_id to remove them from
        Available for Horizon 8 2006 and later."""
        headers = self.access_token
        headers["Content-Type"] = APPLICATION_JSON
        json_data = json.dumps(machine_ids)
        response = requests.post(
            f"{self.url}/rest/inventory/v1/desktop-pools/{desktop_pool_id}/action/remove-machines",
            verify=True,
            headers=headers,
            data=json_data,
        )
        return response.json()

    def assign_user_to_machine(self, machine_id: str, user_ids: list):
        """Assigns the specified users to the machine.

        Requires machine_id of the machine and list of user_ids.
        Available for Horizon 8 2006 and later."""
        headers = self.access_token
        headers["Content-Type"] = APPLICATION_JSON
        json_data = json.dumps(user_ids)
        response = requests.post(
            f"{self.url}/rest/inventory/v1/machines/{machine_id}/action/assign-users",
            verify=True,
            headers=headers,
            data=json_data,
        )
        return response.json()

    def unassign_user_to_machine(self, machine_id: str, user_ids: list):
        """Unassigns the specified users to the machine.

        Requires machine_id of the machine and list of user_ids.
        Available for Horizon 8 2006 and later."""
        headers = self.access_token
        headers["Content-Type"] = APPLICATION_JSON
        json_data = json.dumps(user_ids)
        response = requests.post(
            f"{self.url}/rest/inventory/v1/machines/{machine_id}/action/unassign-users",
            verify=True,
            headers=headers,
            data=json_data,
        )
        return response.json()

    def desktop_pool_push_image(
        self,
        desktop_pool_id: str,
        start_time: float,
        add_virtual_tpm: bool = False,
        im_stream_id: str = "",
        im_tag_id: str = "",
        logoff_policy: str = "WAIT_FOR_LOGOFF",
        parent_vm_id: str = "",
        selective_push_image: bool = False,
        snapshot_id: str = "",
        stop_on_first_error: bool = True,
    ):
        """Schedule/reschedule a request to update the image in an instant clone desktop pool

        Requires start_time in epoch, desktop_pool_id as string and either im_stream_id and im_tag_id OR parent_vm_id and snapshit_id as string.
        Optional: stop_on_first_error as bool, add_virtual_tpm as bool, logoff_policy as string with these options: FORCE_LOGOFF or WAIT_FOR_LOGOFF
        Available for Horizon 8 2012 and later."""
        headers = self.access_token
        headers["Content-Type"] = APPLICATION_JSON
        data = {}
        if add_virtual_tpm != False:
            data["add_virtual_tpm"] = add_virtual_tpm
        if im_stream_id != "" and im_tag_id != "":
            data["im_stream_id"] = im_stream_id
            data["im_tag_id"] = im_tag_id
        data["logoff_policy"] = logoff_policy
        if selective_push_image != False:
            data["selective_push_image"] = selective_push_image
        if parent_vm_id != "" and snapshot_id != "":
            data["parent_vm_id"] = parent_vm_id
            data["snapshot_id"] = snapshot_id
        data["start_time"] = start_time
        data["stop_on_first_error"] = stop_on_first_error
        json_data = json.dumps(data)
        response = requests.post(
            f"{self.url}/rest/inventory/v2/desktop-pools/{desktop_pool_id}/action/schedule-push-image",
            verify=True,
            headers=headers,
            data=json_data,
        )
        return response.json()

    def promote_pending_image(self, desktop_pool_id: str):
        response = requests.post(
            f"{self.url}/rest/inventory/v1/desktop-pools/{desktop_pool_id}/action/promote-pending-image",
            verify=True,
            headers=self.access_token,
        )
        return response.json()

    def get_desktop_pools_v5(self, maxpagesize: int = 100, filter: dict = "") -> list:
        """Returns a list of dictionaries with all available Desktop Pools.

        For information on filtering see https://vdc-download.vmware.com/vmwb-repository/dcr-public/f92cce4b-9762-4ed0-acbd-f1d0591bd739/235dc19c-dabd-43f2-8d38-8a7a333e914e/HorizonServerRESTPaginationAndFilterGuide.doc
        Available for Horizon 8 2111 and later."""

        def int_get_desktop_pools(self, page: int, maxpagesize: int, filter: list = ""):
            if filter != "":
                filter_url = urllib.parse.quote(
                    json.dumps(filter, separators=(", ", ":"))
                )
                add_filter = f"?filter={filter_url}"
                requests_cache.install_cache("pools_cache", expire_after=3600)
                response = requests.get(
                    f"{self.url}/rest/inventory/v5/desktop-pools{add_filter}&page={page}&size={maxpagesize}",
                    verify=True,
                    headers=self.access_token,
                )
            else:
                requests_cache.install_cache("pools_cache", expire_after=3600)
                response = requests.get(
                    f"{self.url}/rest/inventory/v5/desktop-pools?page={page}&size={maxpagesize}",
                    verify=True,
                    headers=self.access_token,
                )
            if response.status_code == 400:
                if "error_messages" in response.json():
                    error_message = (response.json())["error_messages"]
                else:
                    error_message = response.json()
                raise BadUserInputError(
                    f"Error {response.status_code}: {error_message}"
                )
            elif response.status_code != 200:
                raise GeneralError(f"Error {response.status_code}: {response.reason}")
            else:
                try:
                    response.raise_for_status()
                except requests.exceptions.RequestException as e:
                    raise GeneralError(ERROR + str(e))
                else:
                    return response

        if maxpagesize > 1000:
            maxpagesize = 1000
        page = 1
        response = int_get_desktop_pools(
            self, page=page, maxpagesize=maxpagesize, filter=filter
        )
        results = response.json()
        while "HAS_MORE_RECORDS" in response.headers:
            page += 1
            response = int_get_desktop_pools(
                self, page=page, maxpagesize=maxpagesize, filter=filter
            )
            results += response.json()
        return results

    def get_desktop_pool_v5(self, desktop_pool_id: str) -> dict:
        """Gets the Desktop Pool information.

        Requires id of a desktop pool
        Available for Horizon 8 2111 and later."""
        requests_cache.install_cache("pool_cache", expire_after=CACHING_TIME)
        response = requests.get(
            f"{self.url}/rest/inventory/v5/desktop-pools/{desktop_pool_id}",
            verify=True,
            headers=self.access_token,
        )
        if response.status_code == 400:
            error_message = (response.json())["error_message"]
            raise BadUserInputError(f"Error {response.status_code}: {error_message}")
        elif response.status_code != 200:
            raise GeneralError(f"Error {response.status_code}: {response.reason}")
        else:
            try:
                response.raise_for_status()
            except requests.exceptions.RequestException as e:
                raise GeneralError(ERROR + str(e))
            else:
                return response.json()

    def update_desktop_pool(self, pool_data: dict, desktop_pool_id: str):
        """Changes a Desktop_Pool.

        Requires pool_data as a dict
        Available for Horizon 8 2111 and later."""
        headers = self.access_token
        headers["Content-Type"] = APPLICATION_JSON
        json_data = json.dumps(pool_data)
        response = requests.put(
            f"{self.url}/rest/inventory/v1/desktop-pools/{desktop_pool_id}",
            verify=True,
            headers=headers,
            data=json_data,
        )
        if response.status_code == 400:
            error_message = (response.json())["error_message"]
            raise BadUserInputError(f"Error {response.status_code}: {error_message}")
        elif response.status_code == 404:
            error_message = (response.json())["error_message"]
            raise GeneralError(f"Error {response.status_code}: {response}")
        elif response.status_code == 401:
            raise GeneralError(f"Error {response.status_code}: {response.reason}")
        elif response.status_code == 403:
            raise GeneralError(f"Error {response.status_code}: {response.reason}")
        elif response.status_code != 204:
            raise GeneralError(f"Error {response.status_code}: {response.reason}")
        else:
            try:
                response.raise_for_status()
            except requests.exceptions.RequestException as e:
                raise GeneralError(ERROR + str(e))
            else:
                return response.status_code


class Monitor:
    def __init__(self, url: str, access_token: dict):
        """Default object for the monitor class used for the monitoring of the various VMware Horiozn services."""
        self.url = url
        self.access_token = access_token

    def virtual_centers(self) -> list:
        """Lists monitoring information related to Virtual Centers of the environment.

        Available for Horizon 7.10 and later."""
        requests_cache.install_cache("vc_cache", expire_after=CACHING_TIME)
        response = requests.get(
            f"{self.url}/rest/monitor/virtual-centers",
            verify=True,
            headers=self.access_token,
        )
        if response.status_code != 200:
            raise GeneralError(f"Error {response.status_code}: {response.reason}")
        else:
            try:
                response.raise_for_status()
            except requests.exceptions.RequestException as e:
                raise ERROR + str(e)
            else:
                return response.json()


class External:
    def __init__(self, url: str, access_token: dict):
        """Default object for the External class for resources that are external to Horizon environment."""
        self.url = url
        self.access_token = access_token

    def get_base_vms(
        self,
        vcenter_id: str,
        filter_incompatible_vms: bool = "",
        datacenter_id: str = "",
    ) -> list:
        """Lists all the VMs from a vCenter or a datacenter in that vCenter which may be suitable as snapshots for instant/linked clone desktop or farm creation.

        Requires vcenter_id, optionally datacenter id and since Horizon 2012 filter_incompatible_vms was added (defaults to false)
        Available for Horizon 7.12 and later and Horizon 8 2012 for filter_incompatible_vms.
        """

        if (
            filter_incompatible_vms == True or filter_incompatible_vms == False
        ) and datacenter_id != "":
            requests_cache.install_cache("vm_cache", expire_after=CACHING_TIME)
            response = requests.get(
                f"{self.url}/rest/external/v1/base-vms?datacenter_id={datacenter_id}&filter_incompatible_vms={filter_incompatible_vms}&vcenter_id={vcenter_id}",
                verify=True,
                headers=self.access_token,
            )
        elif (
            filter_incompatible_vms != True or filter_incompatible_vms != False
        ) and datacenter_id != "":
            requests_cache.install_cache("vm_cache", expire_after=CACHING_TIME)
            response = requests.get(
                f"{self.url}/rest/external/v1/base-vms?filter_incompatible_vms={filter_incompatible_vms}&vcenter_id={vcenter_id}",
                verify=True,
                headers=self.access_token,
            )
        elif datacenter_id != "":
            requests_cache.install_cache("vm_cache", expire_after=CACHING_TIME)
            response = requests.get(
                f"{self.url}/rest/external/v1/base-vms?datacenter_id={datacenter_id}&vcenter_id={vcenter_id}",
                verify=True,
                headers=self.access_token,
            )
        else:
            requests_cache.install_cache("vm_cache", expire_after=CACHING_TIME)
            response = requests.get(
                f"{self.url}/rest/external/v1/base-vms?vcenter_id={vcenter_id}",
                verify=True,
                headers=self.access_token,
            )
        if response.status_code == 400:
            error_message = response.json()
            raise BadUserInputError(f"Error {response.status_code}: {error_message}")
        elif response.status_code == 404:
            error_message = response.json()
            raise GeneralError(f"Error {response.status_code}: {response}")
        elif response.status_code != 200:
            raise GeneralError(f"Error {response.status_code}: {response.reason}")
        else:
            try:
                response.raise_for_status()
            except requests.exceptions.RequestException as e:
                raise GeneralError("Error: " + str(e))
            else:
                return response.json()

    def get_base_snapshots(self, vcenter_id: str, base_vm_id: str) -> list:
        """Lists all the VM snapshots from the vCenter for a given VM.

        Requires vcenter_id and base_vm_id
        Available for Horizon 8 2006."""
        requests_cache.install_cache("snapshot_cache", expire_after=CACHING_TIME)
        response = requests.get(
            f"{self.url}/rest/external/v1/base-snapshots?base_vm_id={base_vm_id}&vcenter_id={vcenter_id}",
            verify=True,
            headers=self.access_token,
        )

        if response.status_code == 400:
            error_message = response.json()
            raise BadUserInputError(f"Error {response.status_code}: {error_message}")
        elif response.status_code == 404:
            error_message = response.json()
            raise GeneralError(f"Error {response.status_code}: {response}")
        elif response.status_code != 200:
            raise GeneralError(f"Error {response.status_code}: {response.reason}")
        else:
            try:
                response.raise_for_status()
            except requests.exceptions.RequestException as e:
                raise GeneralError(ERROR + str(e))
            else:
                return response.json()

    def get_datacenters(self, vcenter_id: str) -> list:
        """Lists all the datacenters of a vCenter.

        Requires vcenter_id
        Available for Horizon 7.12 and later."""
        requests_cache.install_cache("datacenter_cache", expire_after=CACHING_TIME)
        response = requests.get(
            f"{self.url}/rest/external/v1/datacenters?vcenter_id={vcenter_id}",
            verify=True,
            headers=self.access_token,
        )
        if response.status_code == 400:
            error_message = response.json()
            raise BadUserInputError(f"Error {response.status_code}: {error_message}")
        elif response.status_code == 404:
            error_message = response.json()
            raise GeneralError(f"Error {response.status_code}: {response}")
        elif response.status_code != 200:
            raise GeneralError(f"Error {response.status_code}: {response.reason}")
        else:
            try:
                response.raise_for_status()
            except requests.exceptions.RequestException as e:
                raise GeneralError(ERROR + str(e))
            else:
                return response.json()

    def get_ad_user_or_group(self, id) -> dict:
        """Get information related to AD User or Group.

        Requires id of the user object
        Available for Horizon 7.12 and later."""
        requests_cache.install_cache("user_cache", expire_after=CACHING_TIME)
        response = requests.get(
            f"{self.url}/rest/external/v1/ad-users-or-groups/{id}",
            verify=True,
            headers=self.access_token,
        )
        return response.json()
