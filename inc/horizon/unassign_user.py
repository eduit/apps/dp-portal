import requests
import json
import vmware_horizon
import sys

requests.packages.urllib3.disable_warnings()

hvconnectionobj = vmware_horizon.Connection()
hvconnectionobj.hv_connect()

inventory = vmware_horizon.Inventory(
    url=hvconnectionobj.url, access_token=hvconnectionobj.access_token
)

machine_id = sys.argv[1]
user_id = [sys.argv[2]]
execute = inventory.unassign_user_to_machine(machine_id=machine_id, user_ids=user_id)
res = json.dumps(execute).strip('"')
print(res)

end = hvconnectionobj.hv_disconnect()
