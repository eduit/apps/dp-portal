import requests
import json
import vmware_horizon

requests.packages.urllib3.disable_warnings()

hvconnectionobj = vmware_horizon.Connection()
hvconnectionobj.hv_connect()

inventory = vmware_horizon.Inventory(
    url=hvconnectionobj.url, access_token=hvconnectionobj.access_token
)

sessions = inventory.get_sessions(maxpagesize=hvconnectionobj.licenses)
res = json.dumps(sessions).strip('"')
print(res)

end = hvconnectionobj.hv_disconnect()
