import requests
import json
import vmware_horizon
import sys

requests.packages.urllib3.disable_warnings()

desktop_pool_id = sys.argv[1]

hvconnectionobj = vmware_horizon.Connection()
hvconnectionobj.hv_connect()

inventory = vmware_horizon.Inventory(
    url=hvconnectionobj.url, access_token=hvconnectionobj.access_token
)

deployment = inventory.promote_pending_image(desktop_pool_id=desktop_pool_id)
res = json.dumps(deployment).strip('"')
print(res)

end = hvconnectionobj.hv_disconnect()
