import requests
import json
import vmware_horizon
import sys

requests.packages.urllib3.disable_warnings()

hvconnectionobj = vmware_horizon.Connection()
hvconnectionobj.hv_connect()

external = vmware_horizon.External(
    url=hvconnectionobj.url, access_token=hvconnectionobj.access_token
)

user_id = sys.argv[1]
user = external.get_ad_user_or_group(id=user_id)
res = json.dumps(user["name"]).strip('"')
print(res)

end = hvconnectionobj.hv_disconnect()
