import requests
import json
import vmware_horizon
import sys

requests.packages.urllib3.disable_warnings()

hvconnectionobj = vmware_horizon.Connection()
hvconnectionobj.hv_connect()

inventory = vmware_horizon.Inventory(
    url=hvconnectionobj.url, access_token=hvconnectionobj.access_token
)

desktop_pool_id = sys.argv[1]
if sys.argv[2] == "true":
    enable_provisioning = True
else:
    enable_provisioning = False
pool_data = inventory.get_desktop_pool_v5(desktop_pool_id=desktop_pool_id)

update_pool_data = {}
update_pool_data["enable_provisioning"] = enable_provisioning
update_pool_data["cloud_managed"] = pool_data["cloud_managed"]
update_pool_data["cloud_assigned"] = pool_data["cloud_assigned"]
update_pool_data["display_name"] = pool_data["display_name"]
update_pool_data["display_machine_alias"] = pool_data["display_machine_alias"]
update_pool_data["display_assigned_machine_name"] = pool_data[
    "display_assigned_machine_name"
]
update_pool_data["enabled"] = pool_data["enabled"]
update_pool_data["enable_client_restrictions"] = pool_data["enable_client_restrictions"]
update_pool_data["stop_provisioning_on_error"] = pool_data["stop_provisioning_on_error"]
update_pool_data["transparent_page_sharing_scope"] = pool_data[
    "transparent_page_sharing_scope"
]
update_pool_data["access_group_id"] = pool_data["access_group_id"]
update_pool_data["automatic_user_assignment"] = pool_data["automatic_user_assignment"]
update_pool_data["session_type"] = pool_data["session_type"]
update_pool_data["allow_multiple_user_assignments"] = pool_data[
    "allow_multiple_user_assignments"
]
update_pool_data["pattern_naming_settings"] = pool_data["pattern_naming_settings"]
update_pool_data["view_storage_accelerator_settings"] = pool_data[
    "view_storage_accelerator_settings"
]
update_pool_data["provisioning_settings"] = pool_data["provisioning_settings"]
update_pool_data["customization_settings"] = pool_data["customization_settings"]
update_pool_data["storage_settings"] = pool_data["storage_settings"]
update_pool_data["session_settings"] = pool_data["session_settings"]
update_pool_data["display_protocol_settings"] = pool_data["display_protocol_settings"]

execute = inventory.update_desktop_pool(
    desktop_pool_id=desktop_pool_id, pool_data=update_pool_data
)
res = json.dumps(execute).strip('"')
print(res)

end = hvconnectionobj.hv_disconnect()
