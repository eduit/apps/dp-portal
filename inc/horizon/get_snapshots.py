import requests
import json
import vmware_horizon
import sys

requests.packages.urllib3.disable_warnings()

hvconnectionobj = vmware_horizon.Connection()
hvconnectionobj.hv_connect()

monitor = obj = vmware_horizon.Monitor(
    url=hvconnectionobj.url, access_token=hvconnectionobj.access_token
)
external = vmware_horizon.External(
    url=hvconnectionobj.url, access_token=hvconnectionobj.access_token
)

vcenters = monitor.virtual_centers()
vcid = vcenters[0]["id"]
basevmid = sys.argv[1]
snapshots = external.get_base_snapshots(vcenter_id=vcid, base_vm_id=basevmid)
res = json.dumps(snapshots).strip('"')
print(res)

end = hvconnectionobj.hv_disconnect()
