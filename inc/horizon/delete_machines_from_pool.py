import requests
import json
import vmware_horizon
import sys

requests.packages.urllib3.disable_warnings()

hvconnectionobj = vmware_horizon.Connection()
hvconnectionobj.hv_connect()

inventory = vmware_horizon.Inventory(
    url=hvconnectionobj.url, access_token=hvconnectionobj.access_token
)

pool_id = sys.argv[1]

machines = inventory.get_machines(maxpagesize=hvconnectionobj.licenses)
pool_machines = [x for x in machines if x["desktop_pool_id"] == pool_id]
machine_ids = [item["id"] for item in pool_machines]

execute = inventory.delete_machines(
    machine_ids=machine_ids, force_logoff=True, delete_from_disk=True
)
res = json.dumps(execute).strip('"')
print(res)

end = hvconnectionobj.hv_disconnect()
