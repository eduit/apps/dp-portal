import requests
import json
import vmware_horizon
import time
import sys

requests.packages.urllib3.disable_warnings()

desktop_pool_id = sys.argv[1]
base_vm_id = sys.argv[2]
snapshot_id = sys.argv[3]
if sys.argv[4] == "True":
    secondary_image = True
else:
    secondary_image = False

hvconnectionobj = vmware_horizon.Connection()
hvconnectionobj.hv_connect()

inventory = vmware_horizon.Inventory(
    url=hvconnectionobj.url, access_token=hvconnectionobj.access_token
)

current_time = time.time()
deployment = inventory.desktop_pool_push_image(
    desktop_pool_id=desktop_pool_id,
    parent_vm_id=base_vm_id,
    selective_push_image=secondary_image,
    snapshot_id=snapshot_id,
    start_time=current_time,
    logoff_policy="FORCE_LOGOFF",
    stop_on_first_error=False,
)
res = json.dumps(deployment).strip('"')
print(res)

end = hvconnectionobj.hv_disconnect()
