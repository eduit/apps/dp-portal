import requests
import json
import vmware_horizon
import sys

requests.packages.urllib3.disable_warnings()

hvconnectionobj = vmware_horizon.Connection()
hvconnectionobj.hv_connect()

inventory = vmware_horizon.Inventory(
    url=hvconnectionobj.url, access_token=hvconnectionobj.access_token
)

machine_id = [sys.argv[1]]
execute = inventory.delete_machines(
    machine_ids=machine_id, force_logoff=True, delete_from_disk=True
)
res = json.dumps(execute).strip('"')
print(res)

end = hvconnectionobj.hv_disconnect()
