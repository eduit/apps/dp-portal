import msal
import json
import requests
import requests_cache
import os

# run from local
#from dotenv import load_dotenv
#load_dotenv()

client_id = os.getenv("SP_CLIENT_ID")
thumbprint = os.getenv("SP_THUMBPRINT")

tenant_id = "ethz.onmicrosoft.com"
authority = f"https://login.microsoftonline.com/{tenant_id}"
sp = "ethz.sharepoint.com"
scopes = ["https://graph.microsoft.com/.default"]
certfile = "../../inc/cert/LETExamAccess.pem"

app = msal.ConfidentialClientApplication(
    client_id,
    authority=authority,
    client_credential={"thumbprint": thumbprint, "private_key": open(certfile).read()},
)

result = None
result = app.acquire_token_silent(scopes, account=None)

if not result:
    # print("No suitable token exists in cache. Let's get a new one from Azure Active Directory.")
    result = app.acquire_token_for_client(scopes=scopes)

if "access_token" in result:
    site_id = "online-pruefungen"
    list_id = "Termine"

    # Define fields
    fields = "Title,LKTitle,Examinator,VdP,SysAdmins,Datum,Setup,Pruefungssprache,Pruefungslink,VDI_x002d_Pool"

    # Get path
    endpoint = f"https://graph.microsoft.com/v1.0/sites/{sp}:/sites/{site_id}"
    graph_data = requests.get(
        endpoint, headers={"Authorization": "Bearer " + result["access_token"]}
    ).json()

    path_id = graph_data["id"]

    # Load all list items
    endpoint = f"https://graph.microsoft.com/v1.0/sites/{path_id}/lists/{list_id}/items?expand=fields($select={fields})&$top=999"
    requests_cache.install_cache("sp_cache", expire_after=120)
    graph_data = requests.get(
        endpoint, headers={"Authorization": "Bearer " + result["access_token"]}
    ).json()
    print(json.dumps(graph_data["value"]).strip('"'))
else:
    print(result.get("error"))
    print(result.get("error_description"))
    print(result.get("correlation_id"))
