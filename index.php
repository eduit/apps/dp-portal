<?php

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/func.php";
include $_SERVER["DOCUMENT_ROOT"] . "/mod/get_content.php";

header('X-Content-Type-Options: nosniff');

session_start();

// init variables
$GLOBALS['version'] = 0;
$GLOBALS['updated'] = 0;

// get IP address
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
	$ip_address = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
	$ip_address = $_SERVER['REMOTE_ADDR'];
}

// Get language and auto_refresh from IP address
$qry = $SELECT_lang_refresh_by_computer . "'" . $ip_address . "'";
$res = pg_query($con, $qry);
$row = pg_fetch_assoc($res);
$lang = $row['lang_code'] ?? false;
$auto_refresh = $row['auto_refresh'] ?? false;

if (!$lang) {
	// Get room_id from computer
	$qry = $SELECT_room_by_computer . " AND tbl_computer.ip_address='" . $ip_address . "'";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$room_id = $row['room_id'] ?? 0;

	if ($room_id == '') {
		$room_id = 0;
	}

	// Get language from room
	$qry = $SELECT_lang_refresh_by_room . $room_id . " LIMIT 1";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$lang = $row['lang_code'] ?? false;
	$auto_refresh = $row['auto_refresh'] ?? false;
}

// Get auto_refresh if no allocation
if (!$auto_refresh) {
	$qry = $SELECT_refresh_by_room . "'" . $room_id . "'";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$auto_refresh = $row['auto_refresh'] ?? true;
}

// Set language
$qry = $SELECT_language;
$res = pg_query($con, $qry);
$num = pg_num_rows($res);
$arrLang = array();
for ($i = 0; $i < $num; $i++) {
	$row = pg_fetch_array($res);
	$arrLang[$i]['code'] = $row['lang_code'];
	$arrLang[$i]['language'] = $row['language'];
}

$lang = urlencode($_GET['lang'] ?? '');

if (isset($lang) && inArrayR($lang, $arrLang)) {
	$_SESSION['lang'] = $lang;

	if (isset($_SESSION['lang']) && $_SESSION['lang'] != $lang) {
		echo "<script type='text/javascript'> location.reload(); </script>";
	}
}

if (isset($_SESSION['lang'])) {
	$lang = $_SESSION['lang'];
} else {
	$lang = $arrLang[0]['code'];
}
include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

// Get preferences
$qry = $SELECT_prefs;
$res = pg_query($con, $qry);
$num = pg_num_rows($res);
for ($i = 0; $i < $num; $i++) {
	$row = pg_fetch_array($res);
	if ($row['pref_name'] == "allocation_refresh") {
		$allocation_refresh = $row['pref_value'];
	}
	if ($row['pref_name'] == "default_mode") {
		$default_mode = $row['pref_value'];
	}
	if ($row['pref_name'] == "environment") {
		$env = $row['pref_value'];
	}
	if ($row['pref_name'] == "form_enabled") {
		$form_enabled = $row['pref_value'];
	}
	if ($row['pref_name'] == "form_refresh") {
		$form_refresh = $row['pref_value'];
	}
	if ($row['pref_name'] == "instruction_sheet_header_color") {
		$instruction_sheet_header_color = $row['pref_value'];
	}
	if ($row['pref_name'] == "ip_filter_enabled") {
		$ip_filter_enabled = $row['pref_value'];
	}
	if ($row['pref_name'] == "website_header_color") {
		$website_header_color = $row['pref_value'];
	}
	if ($row['pref_name'] == "website_title") {
		$website_title = $row['pref_value'];
	}
	if ($row['pref_name'] == "copyright") {
		$copyright = $row['pref_value'];
	}
}

// Set default mode
$mode = htmlspecialchars($_GET['mode'] ?? '');
preg_replace('/\w/u', '', $mode);

// look for computer allocation (VDI)
$qry = $SELECT_computer_by_ip . "'" . $ip_address . "'";
$res1 = pg_query($con, $qry);
$num1 = pg_num_rows($res1);
$row1 = pg_fetch_assoc($res1);
$vdi = $row1['vdi'] ?? 'f';
$exam_user_id = $row1['exam_user_id'] ?? 0;

if ($vdi == 't') {
	$qry = $SELECT_computer_by_VDI_user . $exam_user_id;
	$res2 = pg_query($con, $qry);
	$num2 = pg_num_rows($res2);
	if ($num2 > 0) {
		$row2 = pg_fetch_assoc($res2);
		$ip_address = $row2['ip_address'];
	}
}

// look for computer allocation
$allocation = false;
$qry = $SELECT_student_by_computer . "'" . $ip_address . "'";
$res3 = pg_query($con, $qry);
$num3 = pg_num_rows($res3);

// get room_id from computer
$qry = $SELECT_room_by_computer . " AND tbl_computer.ip_address='" . $ip_address . "'";
$res4 = pg_query($con, $qry);
$row4 = pg_fetch_assoc($res4);
$computer_room_id = $row4['room_id'] ?? 0;

if (!$computer_room_id) {
	$computer_room_id = 0;
}

// get room if allocated
$qry = $SELECT_exam_students_by_room . $computer_room_id . " AND active='t'";
$res5 = pg_query($con, $qry);
$num5 = pg_num_rows($res5);

if ($ip_filter_enabled == "false") {
	if (empty($mode)) {
		$mode = $default_mode;
	}
} else {
	if ($num1 == 0 || $num3 > 0 || $num5 > 0) {
		$allocation = true;
	}

	// prevent form if not in database
	if ($num1 == 0 && $mode == "form") {
		$allocation = true;
		$mode = "allocation";
	}

	// set default mode if in database
	if (empty($mode)) {
		if ($allocation) {
			$mode = "allocation";
		} else {
			$mode = $default_mode;
		}
	}
}

$check = file_exists($_SERVER["DOCUMENT_ROOT"] . "/mod/get_$mode.php");

const INSTRUCTIONSHEET = "instruction_sheet";

// set header color
if (fnmatch(INSTRUCTIONSHEET, $mode)) {
	$headercolor = $instruction_sheet_header_color;
} else {
	$headercolor = $website_header_color;
}

// set favicon
$favicon = "favicon.ico";

pg_close($con);

?>

<!DOCTYPE html>

<?php
echo "<html dir=\"ltr\" lang=\"$lang\" xml:lang=\"$lang\">"
?>

<head>
	<?php
	echo "<title>$website_title ";
	if (fnmatch(INSTRUCTIONSHEET, $mode)) {
		echo "| " . _INSTRUCTION_SHEET;
		$favicon = INSTRUCTIONSHEET . ".ico";
	}
	if (!empty($env) && $env != "PRD") {
		echo " | " . $env;
	}
	echo "</title>";
	echo "<link rel=\"shortcut icon\" href=\"img/$favicon\" />";
	?>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8; X-Content-Type-Options=nosniff" />
	<meta name="keywords" content="<?php echo $website_title;
									if (!empty($env) && $env != "PRD") {
										echo " | " . $env;
									} ?>" />
	<meta name="author" content="Flavio Steger" />
	<meta name="gw-service-check-string" content="8988211873">
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Expires" content="0">
	<link rel="stylesheet" type="text/css" href="style/yui-moodlesimple-min.css" />
	<script id="firstthemesheet" type="text/css">
		/** Required in order to fix style inclusion problems in IE with YUI **/
	</script>
	<link rel="stylesheet" type="text/css" href="style/boost_ethz_all.css" />
	<link rel="stylesheet" type="text/css" href="style/ethz.css" />
	<link rel="stylesheet" type="text/css" href="style/jquery-ui.min.css" />
	<link rel="stylesheet" type="text/css" href="style/custom.css" />
	<?php
	echo
	"<style>
			h1,
			.sectionname span a,
			#page-site-index h2 {
				color: $headercolor;
			}
			
			#page-header-color,
			#page-header-color-left,
			#page-header-color-right,
			.drawer-open-left #page-header-color-nav,
			#page-site-index #page-header-color-block,
			.path-mod-forum:target~.forumpost:before {
				background-color: $headercolor;
			}
			
			.caret::before {
				color: $headercolor;
			}
	</style>\n";
	?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="js/jquery-3.4.1.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>
	<?php
	echo "<script src=\"lang/lang_$lang.js\"></script>\n";
	echo "	<script src=\"js/general.js\"></script>\n";
	if ($check) {
		if (str_starts_with($mode, INSTRUCTIONSHEET)) {
			echo "	<script src=\"js/" . INSTRUCTIONSHEET . ".js\"></script>\n";
		} else {
			echo "	<script src=\"js/$mode.js\"></script>\n";
		}
		if ($auto_refresh == 't') {
			if (fnmatch("allocation", $mode)) {
				echo "	<script>const form_reload = setInterval(function() {if (navigator.onLine) {location.reload();}}, " . $allocation_refresh . "000);</script>\n";
			}
			if (fnmatch("form", $mode)) {
				echo "	<script>const form_reload = setInterval(function() {if (navigator.onLine) {location.reload();}}, " . $form_refresh . "000);</script>\n";
			}
		}
	}
	?>
</head>

<body id="page-mod-quiz-view" class="format-topics path-mod path-mod-quiz gecko dir-ltr <?php echo "lang-$lang" ?> yui-skin-sam yui3-skin-sam pagelayout-incourse">

	<div id="page-header-color" class="hidden-print">
		<div id="page-header-color-left"></div>
		<div id="page-header-color-nav"></div>
		<div id="page-header-color-block"></div>
		<div id="page-header-color-right"></div>
	</div>
	<div id="page-wrapper">
		<header role="banner" id="header" class="fixed-top navbar row">
			<div id="header-container">
				<div id="header-right" class="hidden-print float-right">
					<ul class="userlang navbar-nav d-md-flex">
						<li class="dropdown nav-item">
							<a class="dropdown-toggle nav-link" id="id-drop-down" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
								<?php
								if (isset($_SESSION['lang'])) {
									echo $_SESSION['lang'];
								} else {
									echo $lang;
								}
								echo "&nbsp;</a>";
								echo "<div class=\"dropdown-menu dropdown-menu-right userlang-menu\" aria-labelledby=\"id-drop-down\">";
								foreach ($arrLang as $lang) {
									$code = $lang['code'];
									$language = $lang['language'];
									echo "<a class=\"dropdown-item lang-button\" href=\"";
									echo paramChanger("lang", $code);
									echo "\" title=\"$language ($code)\" name=\"$code\">$language</a>";
								}
								?>
				</div>
				</li>
				</ul>
			</div>
			<div id="header-left">
				<div id="logo-ethz">
					<?php
					if (isset($_GET['before'])) {
						$before = htmlspecialchars($_GET['before']);
					} else {
						$before = $default_mode;
					}

					if (fnmatch(INSTRUCTIONSHEET, $mode)) {
						echo "<img src=\"img/ethz_logo.svg\" alt=\"Logo ETH Z&uuml;rich\">";
					} elseif ($mode == "readme") {
						echo "<a href=\"index.php?mode=" . $before . "\" id=\"home-link\" target=\"_self\">
										<img src=\"img/ethz_logo.svg\" alt=\"Logo ETH Z&uuml;rich\">
									</a>";
					} else {
						echo "<a href=\"index.php\" id=\"home-link\" target=\"_self\">
										<img src=\"img/ethz_logo.svg\" alt=\"Logo ETH Z&uuml;rich\">
									</a>";
					}
					?>
				</div>
				<div id="logo-button" data-region="drawer-toggle">
				</div>
				<div id="logo-text" class="hidden-md-down"><span id="home-link"><?php echo $website_title;
																				if (!empty($env) && $env != "PRD") {
																					echo " | " . $env;
																				} ?></span></div>
			</div>
	</div>
	</header>
	<div id="page" class="container-fluid">
		<header id="page-header" class="row">
			<div class="col-12 pt-3 pb-3">
				<div class="card">
					<div class="card-body">
						<div class="d-flex">
							<div class="mr-auto">
								<div class="page-context-header">
									<div class="page-header-headings">
										<h1>
											<?php
											if ($mode == "table") {
												echo _EXAM_SELECTION;
											} elseif ($mode == "form") {
												echo _EXAM_SELECTION;
											} elseif (fnmatch(INSTRUCTIONSHEET, $mode)) {
												echo _INSTRUCTION_SHEET;
											} elseif ($mode == "allocation") {
												echo _WELCOME;
											} else {
												echo _DIGITAL_EXAMS;
											}
											?>
										</h1>
									</div>
								</div>
							</div>
						</div>
						<div class="d-flex flex-wrap">
							<div class="ml-auto d-flex"></div>
							<div id="course-header"></div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<div id="page-content" class="row">
			<div id="region-main-box" class="col-12">
				<section id="region-main">
					<div class="card">
						<div class="card-body">
							<?php
							getData($mode);
							if ($allocation) {
								echo "<span id=\"allocation\" class=\"hidden\"></span>";
							}
							?>
						</div>
					</div>
				</section>
				<div class="clearfix card-body">
					<?php
						echo "<p>" . $copyright;
						echo "<span id=\"ip_address\"";
						if (fnmatch(INSTRUCTIONSHEET, $mode)) {
							echo "style=\"display: none;\"";
						}
						echo "></span></p>";
					?>
				</div>
			</div>
		</div>
	</div>
	</div>
</body>

</html>
