<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

header('X-Content-Type-Options: nosniff');

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

$computer = urlencode($_GET["computer"]);
$ip_address = urlencode($_GET["ip"]);
$username = urlencode($_GET["user"]);

if (!str_starts_with($ip_address, "169.")) {
	// set vdi
	$vdi = "t";

	if (isset($_GET["vdi"])) {
		$vdi = urlencode($_GET["vdi"]);
	}
	if ($vdi == "false") {
		$vdi = "f";
	}
	if ($vdi == "true") {
		$vdi = "t";
	}

	$left = strpos($computer, "-") + 1;
	$right = strrpos($computer, "-");
	$room = str_replace("-", " ", substr($computer, $left, $right - $left));

	//get room_sector_id by computer
	$qry = $SELECT_room_sector_by_computer . ", (SELECT exam_user_id FROM tbl_computer WHERE tbl_computer.ip_address='$ip_address' ORDER BY updated_at DESC LIMIT 1) AS exam_user WHERE tbl_computer.exam_user_id = exam_user.exam_user_id AND vdi = false LIMIT 1;";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$room_sector_id = $row['room_sector_id'] ?? 0;
	$room_name = $row['room_name'] ?? '';

	if ($room != $room_name) {
		$qry = $SELECT_room_sector_id_by_room . "'$room' LIMIT 1";
		$res = pg_query($con, $qry);
		$row = pg_fetch_assoc($res);
		$room_sector_id = $row['room_sector_id'] ?? 0;
	}

	//delete computer if ip exists - only works if no allocations assigned
	$qry = $DELETE_computer . " WHERE ip_address = '$ip_address'";

	if (pg_send_query($con, $qry)) {
		$res = pg_get_result($con);
		if ($res) {
			$state = pg_result_error_field($res, PGSQL_DIAG_SQLSTATE);
			if ($state == 0) {
				$response_array['status1'] = 'success';
			} else {
				$response_array['status1'] = 'error';
			}
		}
	}

	//get room_id
	$qry = $SELECT_room_by_name . "'" . $room . "'";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$num = pg_num_rows($res);
	if ($num > 0) {
		$room_id = $row['room_id'];
	} else {
		$room_id = 0;
	}

	//get exam_user_id
	$qry = $SELECT_exam_user_id . "'" . $username . "'";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$exam_user_id = $row['exam_user_id'] ?? 0;
	if ($exam_user_id == null) {
		$exam_user_id = 0;
	}

	$response_array['computername'] = $computer;
	$response_array['vdi'] = $vdi;
	$response_array['ip'] = $ip_address;
	$response_array['user'] = $username;
	$response_array['room_id'] = $room_id;
	$response_array['room'] = $room;
	$response_array['room_sector_id_1'] = $room_sector_id;
	$response_array['exam_user_id'] = $exam_user_id;

	$qry = "UPDATE tbl_computer SET computer_name='$computer', room_sector_id=$room_sector_id, vdi='$vdi', exam_user_id=$exam_user_id WHERE ip_address='$ip_address';
			INSERT INTO tbl_computer (computer_name, ip_address, room_sector_id, vdi, exam_user_id)	SELECT '$computer', '$ip_address', $room_sector_id, '$vdi', $exam_user_id WHERE NOT EXISTS (SELECT 1 FROM tbl_computer WHERE computer_name='$computer' AND ip_address='$ip_address');";

	if (pg_query($con, $qry)) {
		$response_array['status2'] = 'success';
	} else {
		$response_array['status2'] = 'error';
	}

	//get room_sector_id by computer
	$qry = $SELECT_room_sector_by_computer . ", (SELECT exam_user_id FROM tbl_computer WHERE tbl_computer.ip_address='$ip_address' ORDER BY updated_at DESC LIMIT 1) AS exam_user WHERE tbl_computer.exam_user_id = exam_user.exam_user_id AND vdi = false LIMIT 1;";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$room_sector_id = $row['room_sector_id'] ?? 0;
	$room_name = $row['room_name'] ?? '';

	if ($room != $room_name) {
		$qry = $SELECT_room_sector_id_by_room . "'$room' LIMIT 1";
		$res = pg_query($con, $qry);
		$row = pg_fetch_assoc($res);
		$room_sector_id = $row['room_sector_id'] ?? 0;
	}

	if ($response_array['room_sector_id_1'] != $room_sector_id && $room_sector_id) {
		$qry = $UPDATE_computer . " SET room_sector_id = $room_sector_id WHERE ip_address = '$ip_address';";
		if (pg_query($con, $qry)) {
			$response_array['status3'] = 'success';
		} else {
			$response_array['status3'] = 'error';
		}
		$response_array['room_sector_id_2'] = $room_sector_id;
	}
} else {
	$response_array['status'] = 'invalid ip address';
}

pg_close($con);
echo json_encode($response_array);
exit();
