$(document).ready(function () {
	$("#mform1").change(function () {
		if (typeof form_reload !== 'undefined') {
			clearTimeout(form_reload);
		}
	});

	$("#form_button_save").click(function (e) {
		let $exam_type = $("#id_exam option:selected").attr("data-href");

		if ($("#id_lastname").val().length === 0 || $("#id_firstname").val().length === 0 || $("#id_matrikelnummer").val().length === 0) {
			alert(lang.form_error);
			return false;
		}

		if ($("#id_accepted").is(":checked")) {
			if ($("#id_exam").val() == null) {
				$("#id_error_exam").show($speed);
				$("#id_exam").css("border", "1px solid red");
				return false;
			}
			let $examID = $("#id_exam").val();
			$.ajax({
				url: "/mod/submit_student.php",
				type: "POST",
				async: true,
				data: {
					matrikelnummer: $("#id_matrikelnummer").val().replace(/(<([^>]+)>)/ig, ""),
					lastname: $("#id_lastname").val().replace(/(<([^>]+)>)/ig, ""),
					firstname: $("#id_firstname").val().replace(/(<([^>]+)>)/ig, ""),
					exam: $examID,
					hostname: $hostname,
					ip_address: $ip_address,
					ignored: $("#id_ignored").is(':checked')
				},
				success: function (data) {
					let $url = $("#id_exam option:selected").attr("name");
					if (data.status1 == "error" || data.status2 == "error" || data.status3 == "error") {
						alert(lang.saving_not_possible);
					} else if (data.status1 == "other existing" && !$("#id_ignored").is(":checked")) {
						$("#id_error_matrikelnummer").html(lang.other_existing);
						$("#id_error_matrikelnummer").show($speed);
						$("#id_ignore_registration").show($speed);
					} else if ($exam_type == 3) {
						location.href = "index.php?mode=instruction_sheet_linux";
					} else if ($url) {
						location.href = "index.php?mode=readme&before=form&id=" + $examID;
					} else {
						alert(lang.no_exam_link);
					}
				},
				error: function () {
					alert(lang.saving_not_possible);
				}
			});
			e.preventDefault(); //STOP default action
		} else {
			$("label[for='id_accepted']").css({
				"color": "red",
				"font-weight": "bold"
			});
			$("#id_accepted").tooltip('enable').tooltip('open');
		}
	});

	$("#id_accepted").tooltip({
		content: "<img src='/img/exclamation.png' width=\"32px\" /> " + $("#id_accepted").attr("title"),
		disabled: true,
		close: function () {
			$(this).tooltip('disable');
		}
	});

	$("#form_button_next").click(function () {
		let $submit = true;
		$('input').each(function () {
			if ($(this).val().match(/<(\w+)((?:\s+\w+(?:\s*=\s*(?:(?:"[^"]*")|(?:'[^']*')|[^>\s]+))?)*)\s*(\/?)>/)) {
				alert(lang.html_chars);
				$submit = false;
			}
		});

		if (!$("#id_lastname").val()) {
			$("#id_error_lastname").show();
			$("#id_lastname").css("border", "1px solid red");
			$submit = false;
		}

		if (!$("#id_firstname").val()) {
			$("#id_error_firstname").show();
			$("#id_firstname").css("border", "1px solid red");
			$submit = false;
		}
		if (!$("#id_matrikelnummer").val()) {
			$("#id_error_matrikelnummer").show();
			$("#id_matrikelnummer").css("border", "1px solid red");
			$submit = false;
		}

		if ($("#id_exam").val() == null) {
			$("#id_error_exam").show();
			$("#id_exam").css("border", "1px solid red");
			$submit = false;
		}
		return $submit;
	});

	$(".form-control").click(function () {
		$(".form-control-feedback").hide();
		$(".form-control").css("border", "1px solid #ced4da");
	});

	$("#id_accepted").click(function () {
		$("label[for='id_accepted']").css({
			"color": "",
			"font-weight": ""
		});
	});

	$("#id_noregistration").click(function () {
		if (this.checked) {
			$("#id_matrikelnummer").val("00-000-000");
			$("#id_matrikelnummer").attr('readonly', true);
		} else {
			$("#id_matrikelnummer").val("");
			$("#id_matrikelnummer").attr('readonly', false);
		}
	});
});

$(document).on("keydown", function (e) {
	if ($("#id_lastname").length) {
		// CTRL+ALT+T
		if (e.ctrlKey && e.altKey && e.which === 84) {
			location.href = "index.php?mode=table";
		}
		// CTRL+ALT+A
		if (e.ctrlKey && e.altKey && e.which === 65) {
			e.preventDefault();
			$("#id_lastname").val("test");
			$("#id_firstname").val("test");
			$("#id_noregistration").prop("checked", true);
			$("#id_matrikelnummer").val("00-000-000");
			$("#id_matrikelnummer").attr('readonly', true);
			$("#id_exam").focus();
		}
	}
});
