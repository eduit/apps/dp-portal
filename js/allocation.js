$(document).ready(function () {
	$("#no_allocation_auto_forward").click(function () {
		let dataArr = $(this).data("href").split(',');
		let $exam_student_id = dataArr[0];
		let $exam_id = dataArr[1];
		let $room_id = dataArr[2];
		let $mode = "forward";
		auto_allocate($exam_student_id, $exam_id, $room_id, $mode);
	});

	$("#no_allocation_auto_backward").click(function () {
		let dataArr = $(this).data("href").split(',');
		let $exam_student_id = dataArr[0];
		let $exam_id = dataArr[1];
		let $room_id = dataArr[2];
		let $mode = "backward";
		auto_allocate($exam_student_id, $exam_id, $room_id, $mode);
	});

	function auto_allocate($exam_student_id, $exam_id, $room_id, $mode) {
		$.ajax({
			url: "/mod/set_allocation.php",
			type: "POST",
			async: true,
			data: {
				exam_student_id: $exam_student_id,
				exam_id: $exam_id,
				room_id: $room_id,
				mode: $mode
			},
			success: function (data) {
				if (data.status1 == "error") {
					alert(lang.saving_not_possible);
				} else if (data.status1 == "no_students") {
					alert(lang.no_students);
				} else {
					location.reload();
				}
			},
			error: function () {
				alert(lang.saving_not_possible);
			}
		});
	}

	$("#no_allocation_manual").click(function () {
		location.href = "index.php?mode=allocation&screen=manual";
	});

	$("#no_allocation_form").click(function () {
		location.href = "index.php?mode=form";
	});

	$("#allocation_button_back").click(function (e) {
		$.ajax({
			url: "/mod/remove_allocation.php",
			type: "POST",
			async: true,
			data: {
				student_id: $("#student_id").val()
			},
			success: function (data) {
				if (data.status == "error") {
					alert(lang.saving_not_possible);
				} else if (data.affected_rows == 0) {
					alert(lang.shortcut_disabled);
				} else {
					location.reload();
				}
			},
			error: function () {
				alert(lang.saving_not_possible);
			}
		});
		e.preventDefault(); //STOP default action
	});

	$("#allocation_button_next").click(function (e) {
		let $exam_type = $(this).attr("data-href");
		if ($("#allocation_accept").is(":hidden")) {
			$("#allocation_accept").show($speed);
		} else if ($("#id_accepted").is(":checked")) {
			let $exam_student_id = $("#exam_student_id").val();
			$.ajax({
				url: "/mod/update_accept.php",
				type: "POST",
				async: true,
				data: {
					exam_student_id: $exam_student_id,
					student_id: $("#student_id").val(),
					ip_address: $ip_address
				},
				success: function (data) {
					let $url = $("#exam_url").val();
					let $exam_id = $("#exam_id").val();
					if (data.status == "error") {
						alert(lang.saving_not_possible);
					} else if ($exam_type == 3) {
						location.href = "index.php?mode=instruction_sheet_linux";
					} else if ($url) {
						location.href = "index.php?mode=readme&before=allocation&id=" + $exam_student_id;
					} else if ($exam_id) {
						location.href = "index.php?mode=readme&before=allocation&url=false&id=" + $exam_id;
					} else {
						alert(lang.no_exam_link);
					}
				},
				error: function () {
					alert(lang.saving_not_possible);
				}
			});
			e.preventDefault(); //STOP default action
		} else {
			$("label[for='id_accepted']").css({
				"color": "red",
				"font-weight": "bold"
			});
			$("#id_accepted").tooltip('enable').tooltip('open');
		}
	});

	$("#manual_allocation_button_back").click(function () {
		location.href = "index.php";
	});

	$(".clickable-row_manual_allocation").click(function (e) {
		let dataArr = $(this).data("href").split(',');
		let $exam_student_id = dataArr[0];
		let $student_id = dataArr[1];
		$.ajax({
			url: "/mod/set_allocation.php",
			type: "POST",
			async: true,
			data: {
				exam_student_id: $exam_student_id,
				student_id: $student_id,
				mode: 'manual'
			},
			success: function (data) {
				if (data.status1 == "error") {
					alert(lang.saving_not_possible);
				} else {
					location.href = "index.php";
				}
			},
			error: function () {
				alert(lang.saving_not_possible);
			}
		});
		e.preventDefault(); //STOP default action
	});
});

$(document).keypress(function (e) {
	let keycode = (e.keyCode ? e.keyCode : e.which);
	// ENTER
	if (keycode == '13') {
		if ($("#no_allocation_auto_backward").length) {
			$("#no_allocation_auto_backward").click();
		}
	}
	// SPACE
	if (keycode == '32') {
		if ($("#no_allocation_auto_forward").length) {
			$("#no_allocation_auto_forward").click();
		}
	}
	// F
	if (keycode == '102') {
		if ($("#no_allocation_form").length) {
			$("#no_allocation_form").click();
		}
	}
	// M
	if (keycode == '109') {
		if ($("#no_allocation_manual").length) {
			$("#no_allocation_manual").click();
		}
	}
});

$(document).on("keydown", function (e) {
	// CTRL+ALT+Q
	if ($("#allocation_button_back").length) {
		if (e.ctrlKey && e.altKey && e.which === 81) {
			e.preventDefault();
			$("#allocation_button_back").click();
		}
	}
	// CTRL+ALT+Q
	if ($("#manual_allocation_button_back").length) {
		if (e.ctrlKey && e.altKey && e.which === 81) {
			e.preventDefault();
			$("#manual_allocation_button_back").click();
		}
	}
	// CTRL+ALT+Q
	if ($("#allocation").length && !$("#manual_allocation_button_back").length && !$("#allocation_button_back").length) {
		if (e.ctrlKey && e.altKey && e.which === 81) {
			e.preventDefault();
			location.href = "index.php";
		}
	}	
});
