const $speed = 'slow';
let $hostname = '';
let $ip_address = '';

$(document).ready(function () {
	if ($("#id_lastname").length) {
		$("#id_lastname").focus();
	}

	$.getJSON('/mod/get_ip.php', function (data) {
		$hostname = data.hostname;
		$ip_address = data.ip;
		$("#ip_address").append(' | ' + $ip_address);
	});

	$('.tooltip-wrapper').tooltip();
});
