$(document).ready(function () {
	$(".clickable-row").click(function (e) {
		e.stopPropagation();
		if ($(this).data("href") == ""){
			alert(lang.no_exam_link);
		} else if (e.ctrlKey) {
			window.open($(this).data("href"), "_blank");
		} else {
			window.location = $(this).data("href");
		}
	});

	$(".clickable-row").mouseover(function (e) {
		$("#clipboard_nr_" + $(e.target).attr('name')).show();
		$("#clipboard_" + $(e.target).attr('name')).show();
	});

	$(".clickable-row").mouseleave(function (e) {
		$("#clipboard_nr_" + $(e.target).attr('name')).hide();
		$("#clipboard_" + $(e.target).attr('name')).hide();
	});

	$(".fa-clipboard").click(function (e) {
		e.stopPropagation();
		copyToClipboard($(e.target).attr('name'));

		$(this).after('<i class="message"> ' + lang.copied + '</i>');
		setTimeout(function () {
			$('.message').fadeOut();
		}, 1000);
	});

	$(".clickable-row .fa-globe").click(function (e) {
		e.stopPropagation();
	});    
});

function copyToClipboard(text) {
	let $temp = $("<input>");
	$("body").append($temp);
	$temp.val(text).select();
	navigator.clipboard.writeText(text);
	$temp.remove();
}
