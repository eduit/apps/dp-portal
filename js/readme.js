$(document).ready(function () {
    $("#form_button_to_exam").click(function () {
        let $url = this.value;
        if ($url == '') {
            alert(lang.no_exam_link);
        } else {
            $.ajax({
                url: '/mod/destroy_session.php',
                success: function () {
                    sessionStorage.removeItem('mode');
                    location.href = $url;
                }
            });
        }
    });
});
