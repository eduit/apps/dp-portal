# Run this for the K8s Cluster, prebuilt image with the pgsql extension
#FROM voltere5/dp-portal

FROM php:8.2-apache

RUN apt-get update && apt-get install -y \
libpq-dev \
libldap2-dev

RUN apt-get upgrade -y

RUN docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/
RUN docker-php-ext-install pdo pdo_pgsql pgsql ldap exif
RUN ln -s /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini
RUN sed -i -e 's/;session.cookie_secure =/session.cookie_secure = true/' /usr/local/etc/php/php.ini
RUN sed -i -e 's/session.cookie_samesite =/session.cookie_samesite = Strict/' /usr/local/etc/php/php.ini
RUN sed -i -e 's/session.cookie_httponly =/session.cookie_httponly = true/' /usr/local/etc/php/php.ini
RUN sed -i -e 's/SMTP = localhost/SMTP = smtp0.ethz.ch/' /usr/local/etc/php/php.ini
RUN sed -i -e 's/default_socket_timeout = 60/default_socket_timeout = 6000/' /usr/local/etc/php/php.ini
RUN sed -i '1s/^/ServerName  localhost\n/' /etc/apache2/apache2.conf

# Install python3 and pip because it is used in for some php functions
RUN echo "postfix postfix/mailname string dp-portal.ethz.ch" | debconf-set-selections
RUN echo "postfix postfix/main_mailer_type string 'Internet Site'" | debconf-set-selections
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y \
python3-pip \
libldap-common \
ldap-utils \
postfix

RUN mkfifo /var/spool/postfix/public/pickup
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /var/cache/apt/archives/*

RUN PIP_PROXY=http://proxy.ethz.ch:3128 pip install msal --break-system-packages
RUN PIP_PROXY=http://proxy.ethz.ch:3128 pip3 install requests_cache --break-system-packages

RUN a2enmod authnz_ldap
RUN a2enmod headers
RUN a2enmod rewrite

ADD start.sh /
RUN chmod +x /start.sh

CMD ["/start.sh"]

WORKDIR /var/www/html
COPY . .

EXPOSE 80
