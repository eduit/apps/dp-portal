$(document).ready(function () {

  // rooms
  $("#room_submit").click(function (e) {
    let $room = $("#add_room").val();
    let $room_display_name = $("#add_room_display_name").val();
    let $refresh = $("#add_refresh").is(':checked');

    if ($room == 0) {
      $(".invalid-feedback").hide();
      $("#room_missing").show();
    } else {
      $.ajax({
        url: "/admin/mod/insert_room.php",
        type: "POST",
        async: true,
        data: {
          room: $room,
          roomDN: $room_display_name,
          refresh: $refresh,
        },
        success: function (data) {
          $(".invalid-feedback").hide();
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          $(".invalid-feedback").hide();
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $(".save_room_icon").click(function (e) {
    let $room_id = $(this).attr("name");
    let $room = $("#edit_room_" + $room_id).val();
    let $room_display_name = $("#edit_room_display_name_" + $room_id).val();
    let $refresh = $("#edit_refresh_" + $room_id).is(":checked");

    if (!$("#edit_room_" + $room_id).val()) {
      alert(lang.room + " " + lang.missing);
    } else {
      $.ajax({
        url: "/admin/mod/update_room.php",
        type: "POST",
        async: true,
        data: {
          room_id: $room_id,
          room: $room,
          room_display_name: $room_display_name,
          refresh: $refresh,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $(".delete_room_icon").click(function (e) {
    let $room_id = $(this).attr("name");
    let $confirm = confirm(lang.delete_room_warning);

    if ($confirm) {
      $.ajax({
        url: "/admin/mod/delete_room.php",
        type: "POST",
        async: true,
        data: {
          room_id: $room_id,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.delete_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.delete_not_possible);
        },
      });
    }
    e.preventDefault(); //STOP default action
  });

  // sectors
  $("#sector_submit").click(function (e) {
    let $sector = $("#add_sector").val();
    if ($sector == 0) {
      $(".invalid-feedback").hide();
      $("#sector_missing").show();
    } else {
      $.ajax({
        url: "/admin/mod/insert_sector.php",
        type: "POST",
        async: true,
        data: {
          sector: $sector,
        },
        success: function (data) {
          $(".invalid-feedback").hide();
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          $(".invalid-feedback").hide();
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $(".save_sector_icon").click(function (e) {
    let $sector_id = $(this).attr("name");
    let $sector = $("#edit_sector_" + $sector_id).val();

    if (!$("#edit_sector_" + $sector_id).val()) {
      alert(lang.sector + " " + lang.missing);
    } else {
      $.ajax({
        url: "/admin/mod/update_sector.php",
        type: "POST",
        async: true,
        data: {
          sector_id: $sector_id,
          sector: $sector,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $(".delete_sector_icon").click(function (e) {
    let $sector_id = $(this).attr("name");
    let $confirm = confirm(lang.delete_sector_warning);

    if ($confirm) {
      $.ajax({
        url: "/admin/mod/delete_sector.php",
        type: "POST",
        async: true,
        data: {
          sector_id: $sector_id,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.delete_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.delete_not_possible);
        },
      });
    }
    e.preventDefault(); //STOP default action
  });
  
  // rooms <-> sectors
  $("#room_sector_submit").click(function (e) {
      let $room_id = $("#add_room option:selected").val();
      let $sector_id = $("#add_sector option:selected").val();
      let $sector_display_name = $("#add_sector_display_name").val();
      if ($room_id == 0) {
        $(".invalid-feedback").hide();
        $("#room_missing").show();
      } else if ($sector_id == 0) {
        $(".invalid-feedback").hide();
        $("#sector_missing").show();
      } else {
        $.ajax({
          url: "/admin/mod/insert_room_sector.php",
          type: "POST",
          async: true,
          data: {
            room_id: $room_id,
            sector_id: $sector_id,
            sector_display_name: $sector_display_name,
          },
          success: function (data) {
            $(".invalid-feedback").hide();
            if (data.status == "error") {
              alert(lang.saving_not_possible);
            } else {
              location.reload();
            }
          },
          error: function () {
            $(".invalid-feedback").hide();
            alert(lang.saving_not_possible);
          },
        });
        e.preventDefault(); //STOP default action
      }
  });

  $(".save_room_sector_icon").click(function (e) {
    let $room_sector_id = $(this).attr("name");
    let $room_id = $("#edit_room_" + $room_sector_id + " option:selected").val();
    let $sector_id = $("#edit_sector_" + $room_sector_id + " option:selected").val();
    let $sector_display_name = $("#edit_sector_display_name_" + $room_sector_id).val();

    if (!$("#edit_sector_" + $sector_id).val()) {
      alert(lang.sector + " " + lang.missing);
    } else {
      $.ajax({
        url: "/admin/mod/update_room_sector.php",
        type: "POST",
        async: true,
        data: {
          room_sector_id: $room_sector_id,
          room_id: $room_id,
          sector_id: $sector_id,
          sector_display_name: $sector_display_name,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $(".delete_room_sector_icon").click(function (e) {
    let $room_sector_id = $(this).attr("name");
    let $confirm = confirm(lang.delete_room_sector_warning);

    if ($confirm) {
      $.ajax({
        url: "/admin/mod/delete_room_sector.php",
        type: "POST",
        async: true,
        data: {
          room_sector_id: $room_sector_id,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.delete_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.delete_not_possible);
        },
      });
    }
    e.preventDefault(); //STOP default action
  });

  // users
  $("#exam_user_submit").click(function (e) {
    let $username = $("#add_username").val();
    let $sid = $("#add_sid").val();

    if ($username == 0) {
      $(".invalid-feedback").hide();
      $("#username_missing").show();
    } else {
      $.ajax({
        url: "/admin/mod/insert_exam_user.php",
        type: "POST",
        async: true,
        data: {
          username: $username,
          sid: $sid,
        },
        success: function (data) {
          $(".invalid-feedback").hide();
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          $(".invalid-feedback").hide();
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $(".save_exam_user_icon").click(function (e) {
    let $user_id = $(this).attr("name");
    let $username = $("#edit_exam_user_" + $user_id).val();
    let $sid = $("#edit_exam_user_sid_" + $user_id).val();

    if (!$("#edit_exam_user_" + $user_id).val()) {
      alert(lang.username + " " + lang.missing);
    } else {
      $.ajax({
        url: "/admin/mod/update_exam_user.php",
        type: "POST",
        async: true,
        data: {
          user_id: $user_id,
          username: $username,
          sid: $sid,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $(".delete_exam_user_icon").click(function (e) {
    let $user_id = $(this).attr("name");
    let $confirm = confirm(lang.delete_user_warning);

    if ($confirm) {
      $.ajax({
        url: "/admin/mod/delete_exam_user.php",
        type: "POST",
        async: true,
        data: {
          user_id: $user_id,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.delete_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.delete_not_possible);
        },
      });
    }
    e.preventDefault(); //STOP default action
  });
});