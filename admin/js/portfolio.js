$(document).ready(function () {
	$(".save_portfolio_icon").click(function (e) {
		let $row = $(this).attr('name');
		let $exam_nr = $("#edit_exam_nr_" + $row).val();
		let $exam_name = $("#edit_exam_name_" + $row).val();
		let $confirm = confirm(lang.save_warning);

		if (!$("#edit_exam_nr_" + $row).val()) {
			alert(lang.exam_nr + " " + lang.missing);
		} else if (!$("#edit_exam_name_" + $row).val()) {
			alert(lang.exam_name + " " + lang.missing);
		} else if ($confirm) {
			$.ajax({
				url: "/admin/mod/update_portfolio.php",
				type: "POST",
				async: true,
				data: {
					id: $row,
					exam_nr: $exam_nr,
					exam_name: $exam_name
				},
				success: function (data) {
					if (data.status == "error") {
						alert(lang.saving_not_possible);
					} else if (data.status == "already existing") {
						alert(lang.other_exam_existing);
					} else {
						location.reload();
					}
				},
				error: function () {
					alert(lang.saving_not_possible);
				}
			});
			e.preventDefault(); //STOP default action
		}
	});

	$("#portfolio_submit").click(function (e) {
		let $exam_nr = $("#add_exam_nr").val();
		let $exam_name = $("#add_exam_name").val();

		if (!$("#add_exam_nr").val()) {
			$(".invalid-feedback").hide();
			$("#exam_nr_missing").show();
		} else if (!$("#add_exam_name").val()) {
			$(".invalid-feedback").hide();
			$("#exam_name_missing").show();
		} else {
			$.ajax({
				url: "/admin/mod/insert_portfolio.php",
				type: "POST",
				async: true,
				data: {
					exam_nr: $exam_nr,
					exam_name: $exam_name
				},
				success: function (data) {
					$(".invalid-feedback").hide();
					if (data.status == "error") {
						alert(lang.saving_not_possible);
					} else if (data.status == "already existing") {
						alert(lang.other_exam_existing);
					} else {
						location.reload();
					}
				},
				error: function () {
					$(".invalid-feedback").hide();
					alert(lang.saving_not_possible);
				}
			});
			e.preventDefault(); //STOP default action
		}
	});

	$(".delete_portfolio_icon").click(function (e) {
		let $exam_name_id = $(this).attr('name');
		let $confirm = confirm(lang.delete_portfolio_warning);

		if ($confirm) {
			$.ajax({
				url: "/admin/mod/delete_portfolio.php",
				type: "POST",
				async: true,
				data: {
					exam_name_id: $exam_name_id
				},
				success: function (data) {
					if (data.status == "error") {
						alert(lang.delete_not_possible);
					} else {
						location.reload();
					}
				},
				error: function () {
					alert(lang.delete_not_possible);
				}
			});
			e.preventDefault(); //STOP default action
		}
	});
});
