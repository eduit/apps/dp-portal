$(document).ready(function () {
	$(".seat").addClass("clickable");

	$("#select_rooms").change(function () {
		let $room = $("option:selected", this).attr("name");
		$.urlChangeParam("room", $room);
		location.reload();
	});

	$("#student_list tr").hover(
		function () {
		if (!$(this).hasClass("active")) {
			let $computer = $(this).attr("name");
			if ($computer) {
			$("#" + $computer).css("fill", "#ff7518");
			}
		}
		},
		function () {
		if (!$(this).hasClass("active")) {
			let $computer = $(this).attr("name");
			if ($computer) {
			$("#" + $computer).css("fill", "black");
			}
		}
		}
	);

	$(".seat").hover(
		function () {
		if (!$(this).hasClass("active")) {
			let $computer = $(this).attr("id");
			$(".student[name='" + $computer + "']").css({
			"font-weight": "bold",
			"background-color": "#ff7518",
			});
		}
		},
		function () {
		if (!$(this).hasClass("active")) {
			let $computer = $(this).attr("id");
			$(".student[name='" + $computer + "']").css({
			"font-weight": "",
			"background-color": "",
			});
		}
		}
	);

	$(".seat").click(function () {
		let $computer = $(this).attr("id");
		let $seat = $(".student[name='" + $computer + "']");

		$("html,body").animate({ scrollTop: $seat.offset().top-200 }, "slow");

		if ($(this).hasClass("active")) {
			$(this).removeClass("active");
			$(this).css("fill", "black");
			$(".student[name='" + $computer + "']").removeClass("active");
			$(".student[name='" + $computer + "']").css({
				"font-weight": "",
				"background-color": "",
			});
		} else {
			$(this).addClass("active");
			$(this).css("fill", "#ff7518");
			$(".student[name='" + $computer + "']").addClass("active");
			$(".student[name='" + $computer + "']").css({
				"font-weight": "bold",
				"background-color": "#ff7518",
			});
		}
	});

	$(".seat").each(function () {
		let $computer = $(this).attr("id");
		if ($(".student[name='" + $computer + "']").length) {
		$("#" + $computer).css("fill", "black");
		}
	});

	$(window).on("scroll", function () {
		$("#room_map").css({
		top: $(window).scrollTop() + "px",
		});
	});

	if (displayMode == "dark") {
		$("g.svg-fill").addClass("dark");
		$("g.svg-fill").removeClass("light");
	}

	if (displayMode == "light") {
		$("g.svg-fill").addClass("light");
		$("g.svg-fill").removeClass("dark");
	}
});
