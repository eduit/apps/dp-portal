$(document).ready(function () {  
  $("#all_subscriptions").change(function () {
    let rowCount = $("#all_subscriptions tbody tr:visible").length;
    $("#count").html(rowCount + "/");
  });

  $(".delete_enrollment_icon").click(function (e) {
    let $exam_student_id = $(this).attr("name");
    let $confirm = confirm(lang.delete_enrollment_warning);

    if ($confirm) {
      $.ajax({
        url: "/admin/mod/delete_enrollment.php",
        type: "POST",
        async: true,
        data: {
          exam_student_id: $exam_student_id,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.delete_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.delete_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });
});
