$(document).ready(function () {
  /*** exams ***/
  let $info_text;

  // checkbox styling
  $(".button").checkboxradio({
    icon: false,
  });

  // show pools
  if ($.urlParam("tab") == null || $.urlParam("tab") == "exams") {
    $.fn.showPools();
  }

  // import function
  $("#import").click(function (e) {
    $.ajax({
      url: "/admin/mod/insert_vdi_exams.php",
      type: "POST",
      async: true,
      success: function (data) {
        if (data.status == "error") {
          alert(lang.saving_not_possible);
        } else {
          location.reload();
        }
      },
      error: function () {
        alert(lang.saving_not_possible);
      },
    });
    e.preventDefault(); //STOP default action
  });

  // sort function
  $("#sort").change(function () {
    let $sort = $(this).val();
    $.urlChangeParam("sort", $sort);
    location.reload();
  });

  // order function
  $("#order").change(function () {
    let $checked = $(this).is(":checked");
    let $order;
    if ($checked) {
      $order = "DESC";
    } else {
      $order = "ASC";
    }
    $.urlChangeParam("order", $order);
    location.reload();
  });

  // get vdi deadline from preferences
  let $vdi_deadline;
  $.ajax({
    url: "/admin/mod/get_preference.php",
    type: "POST",
    async: true,
    data: {
      pref: "vdi_deadline",
    },
    success: function (data) {
      $vdi_deadline = data.value;
    },
  });

  // get vdi standard pool from preferences
  let $vdi_standard_pool;
  $.ajax({
    url: "/admin/mod/get_preference.php",
    type: "POST",
    async: true,
    data: {
      pref: "vdi_standard_pool",
    },
    success: function (data) {
      $vdi_standard_pool = data.value;
    },
  });

  // get config from exam
  $("#add_exam").change(function () {
    let $examID = $(this).val();
    let $date = new Date($("option:selected", this).attr("data-href"));
    let $deadline = new Date($date.setDate($date.getDate() - $vdi_deadline));
    $("#add_deadline").val($.getFormattedDate($deadline));

    $.ajax({
      url: "/admin/mod/get_exam_configs.php",
      type: "POST",
      async: true,
      data: {
        examID: $examID,
        selected: 0,
      },
      success: function (data) {
        $("#add_config").prop("disabled", false);
        $("#add_config").html(data["content"]);
        $("#add_vdi_pool_setup").prop("disabled", false);
      },
    });
  });

  // get users from config
  $("#add_config").change(function () {
    let $configID = $(this).val();
    $.ajax({
      url: "/admin/mod/get_exam_config_users.php",
      type: "POST",
      async: true,
      data: {
        configID: $configID,
      },
      success: function (data) {
        $("#testing_people").html(data["content"]);
      },
    });
  });

  $(".edit_config").change(function () {
    let $configID = $(this).val();
    let $row = $(this).attr("name");
    $.ajax({
      url: "/admin/mod/get_exam_config_users.php",
      type: "POST",
      async: true,
      data: {
        configID: $configID,
      },
      success: function (data) {
        $("#testing_people_" + $row).html(data["content"]);
      },
    });
  });

  $("li.selected_people").each(function () {
    let $element = $(this).attr("name");
    let $id = $(this).attr("id");
    $("#edit_" + $element + " option[value=" + $id + "]").attr(
      "disabled",
      "disabled"
    );
  });

  $("li.selected_people").click(function () {
    let $element = $(this).attr("name");
    let $id = $(this).attr("id");
    $(this).remove();
    $("#edit_" + $element + " option[value=" + $id + "]").removeAttr(
      "disabled"
    );
  });

  // choose pool setup
  $("#add_vdi_pool_setup").change(function (e) {
    let $setup = $(this).val();
    let $pool = $("#add_vdi_pool option:selected").val();
    if ($setup > 1) {
      $("#add_vdi_pool").removeAttr("disabled");
      $("#add_vdi_pool option:contains(" + $vdi_standard_pool + ")").attr(
        "disabled",
        "disabled"
      );
      if ($pool == 1) {
        $("#add_vdi_pool option:selected").next().attr("selected", "selected");
      }
      $("#add_vdi_pool option:contains(" + $vdi_standard_pool + ")").prop(
        "selected",
        false
      );
    } else {
      $("#add_vdi_pool").attr("disabled", "disabled");
      $("#add_vdi_pool option:contains(" + $vdi_standard_pool + ")").removeAttr(
        "disabled"
      );
      $("#add_vdi_pool option").prop("selected", false);
      $("#add_vdi_pool option:contains(" + $vdi_standard_pool + ")").prop(
        "selected",
        true
      );
    }

    $.ajax({
      url: "/admin/mod/get_info_text.php",
      type: "POST",
      async: true,
      data: {
        id: $setup,
      },
      success: function (data) {
        if (data.status == "error") {
          alert(lang.error);
        } else {
          $info_text = data.info_text;
        }
      },
      error: function () {
        alert(lang.error);
      },
    });
    e.preventDefault(); //STOP default action
  });

  $(".vdi_pool_setup").change(function () {
    let $row = $(this).attr("name");
    let $pool = $("#edit_vdi_pool_" + $row + " option:selected").val();
    let $setup = $(this).val();
    if ($setup > 1) {
      $("#edit_vdi_pool_" + $row).removeAttr("disabled");
      $(
        "#edit_vdi_pool_" +
          $row +
          " option:contains(" +
          $vdi_standard_pool +
          ")"
      ).attr("disabled", "disabled");
      if ($pool == 1) {
        $("#edit_vdi_pool_" + $row + " option:selected")
          .next()
          .attr("selected", "selected");
      }
      $(
        "#edit_vdi_pool_" +
          $row +
          " option:contains(" +
          $vdi_standard_pool +
          ")"
      ).prop("selected", false);
    } else {
      $("#edit_vdi_pool_" + $row).attr("disabled", "disabled");
      $(
        "#edit_vdi_pool_" +
          $row +
          " option:contains(" +
          $vdi_standard_pool +
          ")"
      ).removeAttr("disabled");
      $("#edit_vdi_pool_" + $row + " option").prop("selected", false);
      $(
        "#edit_vdi_pool_" +
          $row +
          " option:contains(" +
          $vdi_standard_pool +
          ")"
      ).prop("selected", true);
    }
  });

  // add exam
  $("#vdi_exam_submit").click(function (e) {
    let $examID = $("#add_exam option:selected").val();
    let $configID = $("#add_config option:selected").val();
    let $testingFrom = $("#add_testing_from").val();
    let $testingTo = $("#add_testing_to").val();
    let $selfTesting = $("#add_self_testing").is(":checked");
    let $vdiPoolID = $("#add_vdi_pool_setup option:selected").val();
    let $vdiPool = $("#add_vdi_pool option:selected").text();
    let $remarks = $("#add_remarks").val();
    let $deadline = $("#add_deadline").val();
    let $statusID = $("#add_status option:selected").val();
    let $creator = $("#username").text();

    $(".invalid-feedback").hide();
    if ($examID == 0) {
      $("#exam_missing").show();
    } else if ($configID == 0) {
      $("#config_missing").show();
    } else if (!$testingFrom || !$testingTo) {
      $("#testing_date_missing").show();
    } else {
      $.ajax({
        url: "/admin/mod/insert_vdi_exam.php",
        type: "POST",
        async: true,
        data: {
          examID: $examID,
          configID: $configID,
          testingFrom: $testingFrom,
          testingTo: $testingTo,
          selfTesting: $selfTesting,
          vdiPoolID: $vdiPoolID,
          vdiPool: $vdiPool,
          remarks: $remarks,
          deadline: $deadline,
          statusID: $statusID,
          creator: $creator,
          info_text: $info_text,
        },
        success: function (data) {
          $(".invalid-feedback").hide();
          if (data.status1 == "error") {
            alert(lang.saving_not_possible);
          } else if (data.status1 == "already existing") {
            alert(lang.other_exam_existing);
          } else {
            location.reload();
          }
        },
        error: function () {
          $(".invalid-feedback").hide();
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  // edit exam
  $(".save_exam_icon").click(function (e) {
    let $row = $(this).attr("name");
    let $examID = $("#edit_exam_" + $row + " option:selected").val();
    let $configID = $("#edit_config_" + $row + " option:selected").val();
    let $testingFrom = $("#edit_testing_from_" + $row).val();
    let $testingTo = $("#edit_testing_to_" + $row).val();
    let $selfTesting = $("#edit_self_testing_" + $row).is(":checked");
    let $vdiPoolID = $(
      "#edit_vdi_pool_setup_" + $row + " option:selected"
    ).val();
    let $vdiPool = $("#edit_vdi_pool_" + $row + " option:selected").text();
    let $remarks = $("#edit_remarks_" + $row).val();
    let $deadline = $("#edit_deadline_" + $row).val();
    let $statusID = $("#edit_status_" + $row + " option:selected").val();
    let $function = "save";

    $.ajax({
      url: "/admin/mod/get_info_text.php",
      type: "POST",
      async: true,
      data: {
        id: $vdiPoolID,
      },
      success: function (data) {
        if (data.status == "error") {
          alert(lang.error);
        } else {
          $info_text = data.info_text;

          if (!$testingFrom || !$testingTo) {
            $(".invalid-feedback").hide();
            $("#testing_date_missing_" + $row).show();
          } else {
            $.ajax({
              url: "/admin/mod/update_vdi_exam.php",
              type: "POST",
              async: true,
              data: {
                id: $row,
                examID: $examID,
                configID: $configID,
                testingFrom: $testingFrom,
                testingTo: $testingTo,
                selfTesting: $selfTesting,
                vdiPoolID: $vdiPoolID,
                vdiPool: $vdiPool,
                remarks: $remarks,
                deadline: $deadline,
                statusID: $statusID,
                function: $function,
                info_text: $info_text,
              },
              success: function (data) {
                $(".invalid-feedback").hide();
                if (data.status1 == "error") {
                  alert(lang.saving_not_possible);
                } else {
                  location.reload();
                }
              },
              error: function () {
                $(".invalid-feedback").hide();
                alert(lang.saving_not_possible);
              },
            });
            e.preventDefault(); //STOP default action
          }
        }
      },
      error: function () {
        alert(lang.error);
      },
    });
    e.preventDefault(); //STOP default action
  });

  // delete exam
  $(".delete_exam_icon").click(function (e) {
    let $examID = $(this).attr("name");
    let $confirm = confirm(lang.delete_exam_warning);

    if ($confirm) {
      $.ajax({
        url: "/admin/mod/delete_vdi_exam.php",
        type: "POST",
        async: true,
        data: {
          examID: $examID,
        },
        success: function (data) {
          if (data.status1 == "error") {
            alert(lang.delete_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.delete_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  // show internal information
  $(".info_icon").click(function (e) {
    let $id = $(this).attr("name");

    $.ajax({
      url: "/admin/mod/get_internal_info.php",
      type: "POST",
      async: true,
      data: {
        id: $id,
      },
      success: function (data) {
        $("#dialog").html(data.content);
        $("#dialog").dialog({
          width: 600,
          height: 800,
        });

        $("#vdi_internal_info").richText({
          bold: false,
          italic: false,
          underline: false,
          leftAlign: false,
          centerAlign: false,
          rightAlign: false,
          justify: false,
          ol: false,
          ul: false,
          heading: false,
          fonts: false,
          fontColor: false,
          fontSize: false,
          imageUpload: false,
          fileUpload: false,
          videoEmbed: false,
          backgroundColor: false,
          urls: false,
          table: false,
          removeStyles: false,
          height: 500,
        });
      },
      complete: function () {
        $("#save_internal_info").click(function () {
          let $id = $(this).attr("name");
          let $text = $("#vdi_internal_info").val();

          $.ajax({
            url: "/admin/mod/update_internal_info.php",
            type: "POST",
            async: true,
            data: {
              id: $id,
              text: $text,
            },
            success: function (data) {
              $("#dialog").dialog("close");
              let $cleanText = removeTags($text);
              if ($cleanText == "") {
                $("#info_" + $id).css("color", "");
              } else {
                $("#info_" + $id).css("color", "red");
              }
            },
            error: function () {
              alert(lang.error);
            },
          });
          e.preventDefault(); //STOP default action
        });
      },
      error: function () {
        alert(lang.error);
      },
    });
    e.preventDefault(); //STOP default action
  });

  $(document).on("keyup", function (e) {
    if (e.key == "Escape") $("#dialog").dialog("close");
  });

  // finalize exam
  $(".status_icon").click(function (e) {
    let $row = $(this).attr("name");
    let $confirm = confirm(lang.change_status_warning);
    let $statusID = $(this).attr("data-href");
    let $function = "status";

    if ($confirm) {
      $.ajax({
        url: "/admin/mod/update_vdi_exam.php",
        type: "POST",
        async: true,
        data: {
          id: $row,
          status_id: $statusID,
          function: $function,
        },
        success: function (data) {
          if (data.status2 == "error") {
            alert(lang.saving_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  // download files
  $(".download_icon").click(function (e) {
    let $files = $(this).attr("href");
    window.location.href = $files;
    e.preventDefault(); //STOP default action
  });

  // download ics file
  $(".calendar_icon").click(function (e) {
    let $id = $(this).attr("name");
    window.location.href = "/admin/mod/get_ics_file.php?id=" + $id;
    e.preventDefault(); //STOP default action
  });

  /*** configs ***/

  let $configExists;

  // file information tooltip
  $("#files").tooltip();

  // sw multi select
  $("#sw_list_config").multiSelect();

  // group selection
  if ($("#sw_list_config").length) {
    $.ajax({
      url: "/admin/mod/get_vdi_sw_groups.php",
      type: "POST",
      async: true,
      success: function (data) {
        let grouped = data;
        $.each(grouped, function (_key, group) {
          let groupValues = Object.values(group);
          for (let i = 0; i < groupValues.length; i++) {
            for (let j = i + 1; j < groupValues.length; j++) {
              $(".ms-selectable")
                .find("span:contains('" + groupValues[i]["name"] + "')")
                .click(function () {
                  $(".ms-selectable")
                    .find("span:contains('" + groupValues[j]["name"] + "')")
                    .trigger("click");
                });
              $(".ms-selection")
                .find("span:contains('" + groupValues[j]["name"] + "')")
                .click(function () {
                  $(".ms-selection")
                    .find("span:contains('" + groupValues[i]["name"] + "')")
                    .trigger("click");
                });
            }
          }
        });
      },
    });
  }

  // people selection
  $("ul.people").menu();

  $(".select_people").change(function () {
    let $element = $(this).attr("name");
    let $id = $(this).find("option:selected").val();
    let $name = $(this).find("option:selected").text();

    $("#" + $element).append(
      '<li id="' +
        $id +
        '" class="selected_people clickable ui-menu-item">' +
        $name +
        "</li>"
    );

    $(this).find("option:selected").attr("disabled", "disabled");

    $("li.selected_people").click(function () {
      let $id = $(this).attr("id");
      $(this).remove();
      $("#add_" + $element + " option[value=" + $id + "]").removeAttr(
        "disabled"
      );
    });

    $(this).prop("selectedIndex", 0);
  });

  // check if config already exists
  $("#id_exam_name").change(function () {
    let $examNameID = $(this).val();
    let $semester = $("#semester").val();
    $configExists = false;

    $(".invalid-feedback").hide();
    if ($("#" + $examNameID + "_" + $semester).length) {
      $("#id_missing_exam_name").html(lang.other_config_existing);
      $("#id_missing_exam_name").show();
      $configExists = true;
    }
  });

  // files checkbox
  $("#files").click(function () {
    if ($(this).is(":checked")) {
      $("#id_file_upload").show();
      $("#id_files").show();
    } else {
      $("#id_file_upload").hide();
      $("#id_files").hide();
    }
  });

  // file upload
  $("#id_file_upload").click(function () {
    let $examID = $("#id_exam_name option:selected").val();
    $(".invalid-feedback").hide();
    if ($examID == 0) {
      $("#id_missing_exam_name").show();
    } else {
      $("#id_files").val("");
      $("#fileloader_vdi").click();
    }
  });

  $("#fileloader_vdi").change(function (e) {
    let $file = $("#fileloader_vdi")[0].files[0];
    let $filetype = "zip";
    let $exam = $("#id_exam_name option:selected")
      .attr("name")
      .replace(/ /g, "_");
    let $semester = $("#semester").val();
    let $filename = $semester + "_" + $exam;

    if ($file != undefined) {
      let $dir = $(this).attr("data-href");
      let $rename = true;
      let formData = new FormData();
      formData.append("file", $file);
      formData.append("dir", $dir);
      formData.append("filetype", $filetype);
      formData.append("rename", $rename);
      formData.append("new_filename", $filename);

      $.ajax({
        url: "/admin/mod/check_file.php",
        type: "POST",
        async: true,
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
          if (data.status == "already existing" && !$rename) {
            let $overwrite = confirm(lang.file_exists);
            if (!$overwrite) {
              return;
            }
          }
          $.ajax({
            url: "/admin/mod/upload_file.php",
            type: "POST",
            async: true,
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
              if (data.status1 == "already existing") {
                alert(lang.overwriting_file);
                $("#id_files").val(data.link);
              } else if (data.status1 == "too large") {
                alert(lang.file_too_large);
              } else if (data.status1 == "wrong file") {
                alert(lang.wrong_file + " (" + $filetype + ")");
              } else if (data.status2 == "error") {
                alert(lang.saving_not_possible);
              } else {
                $("#id_files").val(data.link);
              }
            },
            error: function () {
              alert(lang.saving_not_possible);
            },
          });
        },
        error: function () {
          alert(lang.saving_not_possible);
        },
      });
    }
    e.preventDefault(); //STOP default action
  });

  // save config
  $("#config_save_button").click(function () {
    let $sw_values = $("#sw_list_config").val();
    if ($("#id_exam_name").val() == null) {
      $(".invalid-feedback").hide();
      $("#id_missing_exam_name").show();
    } else if ($sw_values.length == 0) {
      $(".invalid-feedback").hide();
      $("#id_missing_software").show();
    } else {
      let $confirm;
      let $examNameID = $("#id_exam_name option:selected").val();
      let $semester = $("#semester").val();
      $configExists = false;
      if ($("#" + $examNameID + "_" + $semester).length) {
        $configExists = true;
      }
      if ($configExists) {
        $confirm = confirm(lang.replace_exam_config);
      } else {
        $confirm = true;
      }

      if ($confirm) {
        $(".invalid-feedback").hide();
        let $config_id = $("#config_id").val();
        let $exam_name_id = $("#id_exam_name option:selected").val();
        let $semester = $("#semester").val();
        let $files = $("#files").is(":checked");
        let $filepath = $("#id_files").val();
        let $remarks = $("#remarks").val();
        let $username = $("#config_user_empty").attr("name");
        let $users = [];
        $("#config_people li.selected_people").each(function (n, v) {
          $users.push($(this).attr("id"));
        });

        $.ajax({
          url: "/admin/mod/insert_exam_config.php",
          type: "POST",
          async: true,
          data: {
            config_id: $config_id,
            exam_name_id: $exam_name_id,
            semester: $semester,
            sw: $sw_values,
            files: $files,
            filepath: $filepath,
            remarks: $remarks,
            username: $username,
            users: $users,
          },
          success: function (data) {
            if (data.status == "error") {
              alert(lang.saving_not_possible);
            } else if (data.status == "deadline expired") {
              alert(lang.deadline_expired);
            } else {
              location.reload();
            }
          },
          error: function () {
            alert(lang.saving_not_possible);
          },
        });
      }
    }
  });

  // delete config
  $(".delete_config_icon").click(function (e) {
    let $config_id = $(this).attr("name");
    let $confirm = confirm(lang.delete_config_warning);

    if ($confirm) {
      $.ajax({
        url: "/admin/mod/delete_exam_config.php",
        type: "POST",
        async: true,
        data: {
          config_id: $config_id,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.delete_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.delete_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  // reload config on click
  $(".config").click(function () {
    let $configID = $(this).attr("name");
    let $examNameID = $(this).find("td.exam").attr("name");
    let $files = $(this).find("td.files i").attr("aria-label");
    let $filepath = $(this).find("td.files i").attr("name");
    let $remarks = $(this).find("td.remarks").html();
    let $semester = $("#semester").val();
    let swArray = [];
    let userArray = [];

    $(".invalid-feedback").hide();

    // config id
    $("#config_id").val($configID);

    // exam name
    $("#id_exam_name").val($examNameID);

    // software
    $(this)
      .find("td.software li")
      .each(function () {
        swArray.push($(this).text());
      });
    $("#sw_list_config").multiSelect("deselect_all");
    $.each(swArray, function (_key, item) {
      $(".ms-selectable")
        .find("span:contains('" + item + "')")
        .trigger("click");
    });

    // files
    if ($files == "true") {
      $("#files").prop("checked", true);
    } else {
      $("#files").prop("checked", false);
    }

    // filepath
    if ($("#files").is(":checked")) {
      $("#id_file_upload").show();
      $("#id_files").show();

      if ($filepath.match("^" + $semester) || $filepath == "") {
        $("#id_files").val($filepath);
      } else {
        $("#id_files").val("");
        $("#id_old_files").show();
      }
    } else {
      $("#id_files").val("");
      $("#id_file_upload").hide();
      $("#id_files").hide();
    }

    // remarks
    $("#remarks").val($remarks);

    // users
    $("#add_config_people option").removeAttr("disabled");
    $("#config_user_empty").html(
      $(this).find("td.people li.config_user").html()
    );
    let $config_user = $(this).find("td.people li.config_user").attr("id");
    $("#add_config_people option[value=" + $config_user + "]").attr(
      "disabled",
      "disabled"
    );
    $(this)
      .find("td.people li.selected_people")
      .each(function () {
        userArray.push($(this).attr("id"));
      });
    $("ul.people li.selected_people").remove();
    $.each(userArray, function (_key, item) {
      $("#add_config_people").val(item).change();
    });
  });

  // confirm config
  $(".confirm_icon").click(function (e) {
    let $configID = $(this).attr("name");
    let $confirm = confirm(lang.confirm_config_warning);

    if ($confirm) {
      $.ajax({
        url: "/admin/mod/update_exam_config.php",
        type: "POST",
        async: true,
        data: {
          config_id: $configID,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.saving_not_possible);
        },
      });
    }
    e.preventDefault(); //STOP default action
  });

  /*** software ***/

  // edit software
  $(".save_software_icon").click(function (e) {
    let $row = $(this).attr("name");
    let $software = $("#edit_software_" + $row).val();
    let $publisher_id = $("#edit_publisher_" + $row + " option:selected").val();
    let $version = $("#edit_version_" + $row).val();
    let $remarks = $("#edit_remarks_" + $row).val();
    let $visible = $("#edit_visible_" + $row).is(":checked");

    if (!$("#edit_software_" + $row).val()) {
      alert(lang.software + " " + lang.missing);
    } else if (!$("#edit_version_" + $row).val()) {
      alert(lang.version + " " + lang.missing);
    } else {
      $.ajax({
        url: "/admin/mod/update_software.php",
        type: "POST",
        async: true,
        data: {
          id: $row,
          software: $software,
          publisher_id: $publisher_id,
          version: $version,
          remarks: $remarks,
          visible: $visible,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else if (data.status == "already existing") {
            alert(lang.other_software_existing);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  // add software
  $("#software_submit").click(function (e) {
    let $software = $("#add_software").val();
    let $publisher_id = $("#add_publisher option:selected").val();
    let $version = $("#add_version").val();
    let $remarks = $("#add_remarks").val();
    let $visible = $("#add_visible").is(":checked");

    if (!$("#add_software").val()) {
      $(".invalid-feedback").hide();
      $("#software_missing").show();
    } else if (!$("#add_version").val()) {
      $(".invalid-feedback").hide();
      $("#version_missing").show();
    } else {
      $.ajax({
        url: "/admin/mod/insert_software.php",
        type: "POST",
        async: true,
        data: {
          software: $software,
          publisher_id: $publisher_id,
          version: $version,
          remarks: $remarks,
          visible: $visible,
        },
        success: function (data) {
          $(".invalid-feedback").hide();
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else if (data.status == "already existing") {
            alert(lang.other_software_existing);
          } else {
            location.reload();
          }
        },
        error: function () {
          $(".invalid-feedback").hide();
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  // delete software
  $(".delete_software_icon").click(function (e) {
    let $sw_id = $(this).attr("name");
    let $confirm = confirm(lang.delete_software_warning);

    if ($confirm) {
      $.ajax({
        url: "/admin/mod/delete_software.php",
        type: "POST",
        async: true,
        data: {
          sw_id: $sw_id,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.delete_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.delete_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  /*** publsher ***/

  // edit publisher
  $(".save_publisher_icon").click(function (e) {
    let $row = $(this).attr("name");
    let $publisher = $("#edit_publisher_" + $row).val();

    if (!$("#edit_publisher_" + $row).val()) {
      alert(lang.publisher + " " + lang.missing);
    } else {
      $.ajax({
        url: "/admin/mod/update_publisher.php",
        type: "POST",
        async: true,
        data: {
          id: $row,
          publisher: $publisher,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else if (data.status == "already existing") {
            alert(lang.other_publisher_existing);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  // add publisher
  $("#publisher_submit").click(function (e) {
    let $publisher = $("#add_publisher").val();

    if (!$("#add_publisher").val()) {
      $(".invalid-feedback").hide();
      $("#publisher_missing").show();
    } else {
      $.ajax({
        url: "/admin/mod/insert_publisher.php",
        type: "POST",
        async: true,
        data: {
          publisher: $publisher,
        },
        success: function (data) {
          $(".invalid-feedback").hide();
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else if (data.status == "already existing") {
            alert(lang.other_publisher_existing);
          } else {
            location.reload();
          }
        },
        error: function () {
          $(".invalid-feedback").hide();
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  // delete publisher
  $(".delete_publisher_icon").click(function (e) {
    let $publisher_id = $(this).attr("name");
    let $confirm = confirm(lang.delete_publisher_warning);

    if ($confirm) {
      $.ajax({
        url: "/admin/mod/delete_publisher.php",
        type: "POST",
        async: true,
        data: {
          publisher_id: $publisher_id,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.delete_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.delete_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  /*** connections ***/
  // sw multi select
  $("#sw_list_conn").multiSelect({
    keepOrder: true,
    afterSelect: function (value, text) {
      let $sw_values = $("#sw_list_conn").val();
      if ($sw_values.length > 2) {
        alert(lang.too_many_elements);
        this.deselect(value);
        return false;
      }
      let get_val = $("#multiple_value").val();
      let hidden_val = get_val != "" ? get_val + "," : get_val;
      $("#multiple_value").val(hidden_val + "" + value);
    },
    afterDeselect: function (value, text) {
      const getValue = $("#multiple_value").val();
      let newValue = getValue.replace(`${value},`, "");
      newValue = newValue.replace(`,${value}`, "");
      newValue = newValue.replace(value, "");
      $("#multiple_value").val(newValue);
    },
  });

  // save sw connection
  $("#connection_save_button").click(function () {
    let $sw_values = [];
    $sw_values = $("#multiple_value").val().split(",");
    if ($sw_values.length != 2) {
      $(".invalid-feedback").hide();
      $("#id_missing_software").show();
    } else {
      $(".invalid-feedback").hide();

      $.ajax({
        url: "/admin/mod/insert_sw_connection.php",
        type: "POST",
        async: true,
        data: {
          sw: $sw_values,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.saving_not_possible);
        },
      });
    }
  });

  $(".delete_connection_icon").click(function (e) {
    let $entry_id = $(this).attr("name");
    let $confirm = confirm(lang.delete_connection_warning);

    if ($confirm) {
      $.ajax({
        url: "/admin/mod/delete_sw_connection.php",
        type: "POST",
        async: true,
        data: {
          entry_id: $entry_id,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.delete_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.delete_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });
});

(function ($) {
  $.getFormattedDate = function (date) {
    let year = date.getFullYear();
    let month = (1 + date.getMonth()).toString().padStart(2, "0");
    let day = date.getDate().toString().padStart(2, "0");
    return year + "-" + month + "-" + day;
  };
})(jQuery);

function removeTags(str) {
  if (str === null || str === "") return false;
  else str = str.toString();

  // Regular expression to identify HTML tags in
  // the input string. Replacing the identified
  // HTML tag with a null string.
  return str.replace(/(<([^>]+)>)/gi, "");
}

$.fn.showPools = function () {
  $(".vdi-pool").each(function () {
    let $element = $(this);
    let $id = $(this).attr("name");
    let $vdiPool = $(this).attr("data-href");
    let $setup = $("#edit_vdi_pool_setup_" + $id + " option:selected").val();

    $(this).removeClass("vdi-pool");
    $.ajax({
      url: "/admin/mod/get_vdi_testpools.php",
      type: "POST",
      async: true,
      data: {
        poolName: $vdiPool,
        setup: $setup,
      },
      success: function (data) {
        let $content = data["content"];
        $element.html($content);
      },
    });
  });
};
