$(document).ready(function () {
  // checkbox styling
  $(":checkbox").checkboxradio({
    icon: false,
  });
  
  $("#vho_template_list").click(function () {
    location.href = "../upload/vho/vho_template.csv";
  });

  $("#vho_upload_button").click(function () {
    $("#fileloader_vho").click();
  });

  $("#fileloader_vho").change(function (e) {
    let file = $("#fileloader_vho")[0].files[0];
    if (file != undefined) {
      let formData = new FormData();
      formData.append("file", file);
      $.ajax({
        url: "/admin/mod/update_vho_accounts.php",
        type: "POST",
        async: true,
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
          if (data.status == "error") {
            alert(lang.error);
          } else if (data.status == "nothing to do") {
            alert(lang.nothing_to_do);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.error);
        },
      });
    }
    e.preventDefault(); //STOP default action
  });

  $("#show_vho_generator").click(function () {
    $("#vho_generator").show($speed);
  });

  $("#generate_vho_accounts").click(function (e) {
    let $count = $("#vho_count").val();
    let $theme_id = $("#vho_theme option:selected").val();

    if (!$("#vho_count").val()) {
      $(".invalid-feedback").hide();
      $("#number_missing").show();
    } else if (!$("#vho_theme").val()) {
      $(".invalid-feedback").hide();
      $("#theme_missing").show();
    } else {
      $.ajax({
        url: "/admin/mod/insert_vho_accounts.php",
        type: "POST",
        async: true,
        data: {
          count: $count,
          theme_id: $theme_id,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.error);
          } else if (data.status == "nothing to do") {
            alert(lang.nothing_to_do);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.error);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $(".toggle_pw").click(function () {
    let $id = $(this).attr("name");
    let $pw = $("#pw_txt_" + $id).attr("data-value");
    let $pw_subst = "*".repeat(12);
    let $pw_text = $("#pw_txt_" + $id).text();
    if ($pw_text === $pw_subst) {
      $("#pw_txt_" + $id).text($pw);
    } else {
      $("#pw_txt_" + $id).text($pw_subst);
    }
    $("#toggle_pw_" + $id).toggleClass("fa-eye");
    $("#toggle_pw_" + $id).toggleClass("fa-eye-slash");
  });

  $(".mail_icon").click(function () {
    let $id = $(this).attr("name");
    let $exam_id = $("#select_exam_" + $id + " option:selected").val();
    let $student_id = $("#select_student_" + $id + " option:selected").val();
    let $mail = $(this).attr("data-href");

    if ($exam_id != 0 && $student_id != 0) {
      $.ajax({
        url: "/admin/mod/update_vho_account.php",
        type: "POST",
        async: true,
        data: {
          account_id: $id,
          exam_id: $exam_id,
          student_id: $student_id,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.error);
          } else if (data.status == "already_in_use") {
            alert(lang.already_in_use);
          } else {
            // get template content
            $.ajax({
              url: "/admin/mod/get_vho_mail_template.php",
              type: "POST",
              async: true,
              data: {
                account_id: $id,
                exam_id: $exam_id,
                student_id: $student_id,
              },
              success: function (data) {
                // generate e-mail
                const email = $mail;
                const dp = $dp_mail;
                const subject = encodeURIComponent(data["subject"]);
                const content = data["content"].replace(/&/g, "%26");
                let mail =
                  "mailto:" +
                  email +
                  ";" +
                  dp +
                  "?subject=" +
                  subject +
                  "&body=" +
                  content;
                let a = document.createElement("a");
                a.href = mail;
                a.target = "_top";
                document.body.appendChild(a);
                a.click();
                document.body.removeChild(a);
                location.reload();
              },
            });
          }
        },
      });
    } else {
      alert(
        lang.exam + " " + lang.or + " " + lang.student + " " + lang.missing
      );
    }
  });

  $(".select_exam").change(function () {
    let $id = $(this).attr("name");
    let $exam_id = $("#select_exam_" + $id + " option:selected").val();
    $("#select_student_" + $id).prop("disabled", false);
    $.ajax({
      url: "/admin/mod/get_vho_accounts.php",
      type: "POST",
      async: true,
      data: {
        exam_id: $exam_id,
      },
      success: function (data) {
        $("#select_student_" + $id).html(data);
      },
      error: function () {
        alert(lang.error);
      },
    });
  });

  $(".archive_vho_icon").click(function (e) {
    let $account_id = $(this).attr("name");
    let $archived = $(this).attr("id").slice(0, 1);

    $.ajax({
      url: "/admin/mod/archive_vho_account.php",
      type: "POST",
      async: true,
      data: {
        account_id: $account_id,
        archived: $archived,
      },
      success: function (data) {
        if (data.status == "error") {
          alert(lang.archive_not_possible);
        } else {
          location.reload();
        }
      },
      error: function () {
        alert(lang.archive_not_possible);
      },
    });
    e.preventDefault(); //STOP default action
  });

  $(".delete_vho_icon").click(function (e) {
    let $account_id = $(this).attr("name");
    let $confirm = confirm(lang.delete_vho_warning);

    if ($confirm) {
      $.ajax({
        url: "/admin/mod/delete_vho_account.php",
        type: "POST",
        async: true,
        data: {
          account_id: $account_id,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.delete_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.delete_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });
});
