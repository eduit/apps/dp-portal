$(document).ready(function () {
  // checkbox styling
  $(":checkbox").checkboxradio({
    icon: false,
  });

  $("#logsheet").change(function () {
    if ($("#archived").is(":checked")) {
      let rowCount = $("#logsheet tbody tr:visible").length;
      $("#count").html(rowCount + "/");
    }
  });

  $("#add_exam").change(function () {
    let $exam_id = $("#add_exam option:selected").val();
    $("#add_student").prop("disabled", false);
    $.ajax({
      url: "/admin/mod/get_logsheet.php",
      type: "POST",
      async: true,
      data: {
        exam_id: $exam_id,
      },
      success: function (data) {
        $("#add_student").html(data);
      },
      error: function () {
        alert(lang.error);
      },
    });
  });

  $("#add_room").change(function () {
    let $room_id = $("#add_room option:selected").val();
    $.ajax({
      url: "/admin/mod/get_logsheet.php",
      type: "POST",
      async: true,
      data: {
        room_id: $room_id,
      },
      success: function (data) {
        $("#add_computer").html(data);
      },
      error: function () {
        alert(lang.error);
      },
    });
  });

  $("#add_file_upload").click(function () {
    $("#add_file").val("");
    $("#fileloader_logsheet").click();
  });

  $("#fileloader_logsheet").change(function (e) {
    let $file = $("#fileloader_logsheet")[0].files[0];
    if ($file != undefined) {
      let $dir = $(this).attr("data-href");
      let $rename = true;
      let formData = new FormData();
      formData.append("file", $file);
      formData.append("dir", $dir);
      formData.append("filetype", "*");
      formData.append("rename", $rename);

      $.ajax({
        url: "/admin/mod/check_file.php",
        type: "POST",
        async: true,
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
          if (data.status == "already existing" && !$rename) {
            let $overwrite = confirm(lang.file_exists);
            if (!$overwrite) {
              return;
            }
          }
          $.ajax({
            url: "/admin/mod/upload_file.php",
            type: "POST",
            async: true,
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
              if (data.status1 == "already existing") {
                alert(lang.overwriting_file);
                $("#add_file").val(data.link);
                $("#file_missing").text(lang.overwriting_file);
                $("#file_missing").show($speed);
              } else if (data.status1 == "too large") {
                alert(lang.file_too_large);
              } else if (data.status1 == "wrong file") {
                alert(lang.wrong_file);
              } else if (data.status2 == "error") {
                alert(lang.saving_not_possible);
              } else {
                $("#add_file").val(data.link);
                $("#file_missing").text(lang.upload_successful);
                $("#file_missing").css("color", "green");
                $("#file_missing").show($speed);
              }
            },
            error: function () {
              alert(lang.saving_not_possible);
            },
          });
        },
        error: function () {
          alert(lang.saving_not_possible);
        },
      });
    }
    e.preventDefault(); //STOP default action
  });

  $("#incident_submit").click(function (e) {
    let $exam = $("#add_exam option:selected").val();
    let $student = $("#add_student option:selected").val();
    let $room = $("#add_room option:selected").val();
    let $computer = $("#add_computer option:selected").val();
    let $multiple_computers = "f";
    if ($computer == "mc") {
      $multiple_computers = "t";
      $computer = 0;
    }
    let $description = $("#add_description").val();
    let $file = $("#add_file").val();
    let $user = $("#add_user").val();

    if ($exam == 0) {
      $(".invalid-feedback").hide();
      $("#exam_missing").show();
    } else if (!$description) {
      $(".invalid-feedback").hide();
      $("#description_missing").show();
    } else {
      $.ajax({
        url: "/admin/mod/insert_incident.php",
        type: "POST",
        async: true,
        data: {
          exam: $exam,
          student: $student,
          room: $room,
          computer: $computer,
          multiple_computers: $multiple_computers,
          description: $description,
          file: $file,
          user: $user,
        },
        success: function (data) {
          $(".invalid-feedback").hide();
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          $(".invalid-feedback").hide();
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $(".edit_exam").change(function () {
    let $row = $(this).attr("name");
    let $exam_id = $("#edit_exam_" + $row + " option:selected").val();
    $("#edit_student_" + $row).prop("disabled", false);
    $.ajax({
      url: "/admin/mod/get_logsheet.php",
      type: "POST",
      async: true,
      data: {
        exam_id: $exam_id,
      },
      success: function (data) {
        $("#edit_student_" + $row).html(data);
      },
      error: function () {
        alert(lang.error);
      },
    });
  });

  $(".edit_room").change(function () {
    let $row = $(this).attr("name");
    let $room_id = $("#edit_room_" + $row + " option:selected").val();
    $.ajax({
      url: "/admin/mod/get_logsheet.php",
      type: "POST",
      async: true,
      data: {
        room_id: $room_id,
      },
      success: function (data) {
        $("#edit_computer_" + $row).html(data);
      },
      error: function () {
        alert(lang.error);
      },
    });
  });

  $(".edit_file_upload_logsheet").click(function () {
    let $row = $(this).attr("name");
    $("#edit_file_" + $row).val("");
    $("#edit_fileloader_logsheet_" + $row).click();
  });

  $(".edit_fileloader_logsheet").change(function (e) {
    let $row = $(this).attr("name");
    let $file = $("#edit_fileloader_logsheet_" + $row)[0].files[0];
    if ($file != undefined) {
      let $dir = $(this).attr("data-href");
      let $rename = true;
      let formData = new FormData();
      formData.append("file", $file);
      formData.append("dir", $dir);
      formData.append("filetype", "*");
      formData.append("rename", $rename);

      $.ajax({
        url: "/admin/mod/check_file.php",
        type: "POST",
        async: true,
        data: formData,
        processData: false,
        contentType: false,
        success: function (data) {
          if (data.status == "already existing" && !$rename) {
            let $overwrite = confirm(lang.file_exists);
            if (!$overwrite) {
              return;
            }
          }
          $.ajax({
            url: "/admin/mod/upload_file.php",
            type: "POST",
            async: true,
            data: formData,
            processData: false,
            contentType: false,
            success: function (data) {
              if (data.status1 == "already existing") {
                alert(lang.overwriting_file);
                $("#edit_file_" + $row).val(data.link);
                $("#file_missing_" + $row).text(lang.overwriting_file);
                $("#file_missing_" + $row).show($speed);
              } else if (data.status1 == "too large") {
                alert(lang.file_too_large);
              } else if (data.status1 == "wrong file") {
                alert(lang.wrong_file);
              } else if (data.status2 == "error") {
                alert(lang.saving_not_possible);
              } else {
                $("#edit_file_" + $row).val(data.link);
                $("#file_missing_" + $row).text(lang.upload_successful);
                $("#file_missing_" + $row).css("color", "green");
                $("#file_missing_" + $row).show($speed);
              }
            },
            error: function () {
              alert(lang.saving_not_possible);
            },
          });
        },
        error: function () {
          alert(lang.saving_not_possible);
        },
      });
    }
    e.preventDefault(); //STOP default action
  });

  $(".save_incident_icon").click(function (e) {
    let $row = $(this).attr("name");
    let $exam = $("#edit_exam_" + $row).attr("name");
    let $student = $("#edit_student_" + $row + " option:selected").val();
    let $room = $("#edit_room_" + $row + " option:selected").val();
    let $computer = $("#edit_computer_" + $row + " option:selected").val();
    let $multiple_computers = "f";
    if ($computer == "mc") {
      $multiple_computers = "t";
      $computer = 0;
    }
    let $description = $("#edit_description_" + $row).val();
    let $file = $("#edit_file_" + $row).val();
    let $user = $("#edit_user_" + $row).val();

    if ($exam == 0) {
      $(".invalid-feedback").hide();
      $("#exam_missing_" + $row).show();
    } else if (!$description) {
      $(".invalid-feedback").hide();
      $("#description_missing_" + $row).show();
    } else {
      $.ajax({
        url: "/admin/mod/update_incident.php",
        type: "POST",
        async: true,
        data: {
          id: $row,
          exam: $exam,
          student: $student,
          room: $room,
          computer: $computer,
          multiple_computers: $multiple_computers,
          description: $description,
          file: $file,
          user: $user,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $(".delete_incident_icon").click(function (e) {
    let $incident_id = $(this).attr("name");
    let $confirm = confirm(lang.delete_incident_warning);

    if ($confirm) {
      $.ajax({
        url: "/admin/mod/delete_incident.php",
        type: "POST",
        async: true,
        data: {
          incident_id: $incident_id,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.delete_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.delete_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $(".img_dialog").click(function (e) {
    let $img = $(this).attr("href");
    let $size = JSON.parse($(this).attr("data-href").replace(/'/g, '"'));
    $("#dialog").html('<img src="' + $img + '" />');
    $("#dialog").dialog({
      width: $size.width + 70,
      height: $size.height + 70,
    });
    e.preventDefault(); //STOP default action
  });

  $("#add_description")
    .autocomplete({
      source: "/admin/mod/get_template.php",
      minLength: 0,
    })
    .focus(function () {
      $(this).data("uiAutocomplete").search($(this).val());
    });

  $("#logsheet_download").click(function (e) {
    let $archived = $("#archived").is(":checked");
    let $download = true;
    window.location.href =
      "/admin/mod/get_logsheet.php?download=" +
      $download +
      "&archived=" +
      $archived;
  });
});
