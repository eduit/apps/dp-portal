$(document).ready(function () {
  $("#clipboard").click(function (e) {
    e.stopPropagation();
    copyToClipboard($(e.target).attr("name"));

    $(this).after('<i class="message"> ' + lang.copied + "</i>");
    setTimeout(function () {
      $(".message").fadeOut();
    }, 1000);
  });
});

function copyToClipboard(text) {
  let $temp = $("<input>");
  $("body").append($temp);
  $temp.val(text).select();
  navigator.clipboard.writeText(text);
  $temp.remove();
}
