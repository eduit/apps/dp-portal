let handle;

$(document).ready(function () {
  $.fn.loadPage();
  handle = setInterval("location.reload(true);", 120000);
});

$.fn.loadPage = function () {
  if ($("#tablist").length > 0) {
    let $tab = $(".nav-link.active.show").attr("data-href");
    let $pool = $.urlParam("pool");
    let $url = "get_vdi_" + $tab + "_data.php";

    $.ajax({
      url: "/admin/mod/" + $url,
      type: "POST",
      async: true,
      data: {
        tab: $tab,
        pool: $pool,
      },
      beforeSend: function () {
        $("#loader").show();
      },
      success: function (data) {
        $("#horizon_data").html(data["content"]);
      },
      complete: function () {
        $("#loader").hide();
        $.fn.showUsers();
        $.fn.showMachines();
        $.fn.showMachinesState();
        $.fn.showSnapshots();
        $.generalFunctions();
        $("span").tooltip();
        $.fn.vdiFunctions();
      },
      error: function () {
        urlExists($url, function (exists) {
          if (exists) {
            alert(lang.error);
          } else {
            let $html =
              "<h2>" +
              lang.wrong_parameter +
              '</h2><p>&nbsp;</p><i class="fa fa-exclamation fa-fw" style="font-size: 200px; margin-left: 80px;" title="" aria-label="false" name=""></i>';
            $("#horizon_data").html($html);
          }
        });
      },
    });
  }
};

$(window).on("load, resize, scroll", function () {
  if ($("#tablist").length > 0) {
    $.fn.showUsers();
    $.fn.showMachines();
    $.fn.showMachinesState();
    $.fn.showSnapshots();
  }
});

$.fn.showUsers = function () {
  $(".user").each(function () {
    if ($(this).isInViewport()) {
      let $userSID = $(this).attr("name");
      $(this).removeClass("user");
      $.ajax({
        url: "/admin/mod/get_vdi_user.php",
        type: "POST",
        async: true,
        data: {
          user_sid: $userSID,
        },
        success: function (data) {
          if ($userSID) {
            $("." + $userSID).text(data["username"]);
            $("." + $userSID).removeClass($userSID);
          }
        },
      });
    }
  });
};

$.fn.showMachines = function () {
  $(".machine").each(function () {
    if ($(this).isInViewport()) {
      let $machineSID = $(this).attr("name");
      $(this).removeClass("machine");
      $.ajax({
        url: "/admin/mod/get_vdi_machine.php",
        type: "POST",
        async: true,
        data: {
          machine_sid: $machineSID,
        },
        success: function (data) {
          if ($machineSID) {
            $("." + $machineSID).text(data["machinename"]);
            $("." + $machineSID).removeClass($machineSID);
          }
        },
      });
    }
  });
};

$.fn.showMachinesState = function () {
  $(".available").each(function () {
    if ($(this).isInViewport()) {
      let $poolID = $(this).attr("name");
      let $id = $(this).attr("id").replace('avail_', '');
      $(this).removeClass("available");
      $.ajax({
        url: "/admin/mod/get_vdi_machines_state.php",
        type: "POST",
        async: true,
        data: {
          pool_id: $poolID,
        },
        success: function (data) {
          if ($poolID) {
            $("#avail_" + $id).text(data["available"]);
            $("#conn_" + $id).text(data["conn"]);
            $("#conn_" + $id).removeClass("conn");
          }
        },
      });
    }
  });
};

$.fn.showSnapshots = function () {
  $.ajax({
    url: "/admin/mod/get_vdi_snapshot_data.php",
    type: "POST",
    async: true,
    beforeSend: function () {
      $(".snapshot").each(function () {
        $(this).text(lang.loading);
        $(this).css("font-style", "italic");
      });
    },
    success: function (data) {
      $(".snapshot").each(function () {
        let parentID = $(this).attr("data-href");
        let snapshotID = $(this).attr("name");
        $(this).removeClass("snapshot");
        $(this).css("font-style", "");

        let snapshotName;
        let description;
        let valid;
        let expired;
        let undef = true;

        if (data[parentID] !== undefined) {
          if (data[parentID]["snapshots"][snapshotID] !== undefined) {
            undef = false;
            snapshotName = data[parentID]["snapshots"][snapshotID]["name"];
            description =
              data[parentID]["snapshots"][snapshotID]["description"];
            valid = data[parentID]["snapshots"][snapshotID]["valid"];
            expired = data[parentID]["snapshots"][snapshotID]["expired"];
          }
        }

        if (undef) {
          $(this).css("font-style", "");
          snapshotName = lang.deleted;
          description = "";
          valid = false;
          expired = true;
        }

        $("." + snapshotID).text(snapshotName);
        $("." + snapshotID).attr("title", description);
        if (valid) {
          if (expired) {
            $("." + snapshotID).css("font-style", "italic");
          } else {
            $("." + snapshotID).css("font-weight", "bold");
          }
        }
      });
    },
  });
};

$.fn.isInViewport = function () {
  let elementTop = $(this).offset().top;
  let elementBottom = elementTop + $(this).outerHeight();

  let viewportTop = $(window).scrollTop();
  let viewportBottom = viewportTop + $(window).height();

  return elementBottom > viewportTop && elementTop < viewportBottom;
};

$.fn.vdiFunctions = function () {
  // edit desktop pool
  $(".edit_icon").click(function () {
    let $rowID = $(this).attr("data-href");
    let $name = $("#name_" + $rowID).text();
    let $cluster = $name.split("-")[1];
    let $currentParentID = $(
      "#provisioning_settings_1_" + $rowID + " span"
    ).attr("name");
    let $currentSnapshotID = $(
      "#provisioning_settings_2_" + $rowID + " span"
    ).attr("name");
    let $pendingParentID = $(
      "#provisioning_status_data_2_" + $rowID + " span"
    ).attr("name");

    $("#edit_pending_snapshot_" + $rowID).prop("disabled", true);
    $("#edit_pending_snapshot_" + $rowID).html(
      '<option name="' +
        lang.no_snapshot +
        '" value="0" selected>' +
        lang.no_snapshot +
        "</option>"
    );

    let $array = [
      ["current", "parent", $currentParentID, 0],
      ["current", "snapshot", $currentParentID, $currentSnapshotID],
      ["pending", "parent", $pendingParentID, 0],
    ];

    $($array).each(function ($key, $item) {
      let isLastElement = $key == $array.length - 1;
      let $element = $("#edit_" + $item[0] + "_" + $item[1] + "_" + $rowID);
      $.ajax({
        url: "/admin/mod/get_vdi_" + $item[1] + "s.php",
        type: "POST",
        async: true,
        data: {
          mode: $item[0],
          parentID: $item[2],
          snapshotID: $item[3],
          cluster: $cluster,
        },
        beforeSend: function () {
          $element.prop("disabled", true);
        },
        success: function (data) {
          let $content = data["content"];
          $element.html($content);
        },
        complete: function () {
          $element.prop("disabled", false);
          if (isLastElement) {
            $("#save_pool_" + $rowID).show();
          }
        },
        error: function () {
          alert(lang.error);
        },
      });
    });
  });

  // remove all machines from pool
  $(".delete_machines").click(function () {
    let $rowID = $(this).attr("name");
    let $command = $(this).attr("data-href");
    let data = {};
    data["id"] = $("#row_" + $rowID).attr("name");

    $.fn.sendCommand($command, data);
  });

  // assign parent to multiple pools
  let control = false;
  $(".checkbox_pool").change(function () {
    clearInterval(handle);
    let cluster = $(this).attr("data-href");
    let checked = $("input.checkbox_pool:checked").length;
    if (checked) {
      $("#select_multiple_pools").show();
        $(".checkbox_pool").each(function () {
          if ($(this).attr("data-href") != cluster) {
            $(this).prop("disabled", true);
          } else {
            $(this).prop("disabled", false);
          }
        });
        if (!control) {
          $.ajax({
            url: "/admin/mod/get_vdi_parents.php",
            type: "POST",
            async: true,
            data: {
              mode: 0,
              parentID: 0,
              cluster: cluster,
            },
            success: function (data) {
              let $content = data["content"];
              $("#select_parent").html($content);
              control = true;
              $("#select_parent").prop("disabled", false);
              $("#select_parent").trigger("change");
            },
            error: function () {
              alert(lang.error);
            },
          });
        }
    } else {
      $("#select_multiple_pools").hide();
      $(".checkbox_pool").prop("disabled", false);
      $("#select_parent").prop("disabled", true);
      $("#select_snapshot").prop("disabled", true);
      $("#multi_promote").hide();
      control = false;
    }
  });

  $("#select_parent").change(function () {
    let $currentParentID = $(this).val();
    $.ajax({
      url: "/admin/mod/get_vdi_snapshots.php",
      type: "POST",
      async: true,
      data: {
        mode: "current",
        parentID: $currentParentID,
        snapshotID: 0,
      },
      success: function (data) {
        let $content = data["content"];
        $("#select_snapshot").html($content);
        $("#select_snapshot").prop("disabled", false);
        $("#multi_promote").show();
      },
      error: function () {
        alert(lang.error);
      },
    });
  });

  $("#multi_promote").click(function () {
    let $command = $(this).attr("data-href");
    $(".checkbox_pool:checked").each(function () {
      let poolID = $(this).attr("name");
      let parentID = $("#select_parent option:selected").val();
      let snapshotID = $("#select_snapshot option:selected").val();
      let data = {};
      data["desktopPoolID"] = poolID;
      data["currentParentID"] = parentID;
      data["currentSnapshotID"] = snapshotID;
      data["secondaryImage"] = "False";
      $(this).addClass("fa-spinner");
      $.fn.sendCommand($command, data);
      handle = setInterval("location.reload(true);", 120000);
    });
  });

  // change current parent -> get snapshots
  $(".current_parent").change(function () {
    let $rowID = $(this).attr("name");
    let $currentParentID = $(this).val();

    $.ajax({
      url: "/admin/mod/get_vdi_snapshots.php",
      type: "POST",
      async: true,
      data: {
        mode: "current",
        parentID: $currentParentID,
        snapshotID: 0,
      },
      success: function (data) {
        let $content = data["content"];
        $("#edit_current_snapshot_" + $rowID).html($content);
      },
      error: function () {
        alert(lang.error);
      },
    });
  });

  // change pending parent -> get snapshots
  $(".pending_parent").change(function () {
    let $rowID = $(this).attr("name");
    let $pendingParentID = $(this).val();

    if ($pendingParentID != "0") {
      let $currentParentID = $(
        "#provisioning_settings_1_" + $rowID + " span"
      ).attr("name");
      let $currentSnapshotID = $(
        "#provisioning_settings_2_" + $rowID + " span"
      ).attr("name");
      $("#edit_current_parent_" + $rowID)
        .val($currentParentID)
        .prop("disabled", true);
      $("#edit_current_parent_" + $rowID).trigger("change");
      $("#edit_current_snapshot_" + $rowID)
        .val($currentSnapshotID)
        .prop("disabled", true);
    } else {
      $("#edit_current_parent_" + $rowID).prop("disabled", false);
      $("#edit_current_snapshot_" + $rowID).prop("disabled", false);
    }
    $.ajax({
      url: "/admin/mod/get_vdi_snapshots.php",
      type: "POST",
      async: true,
      data: {
        mode: "pending",
        parentID: $pendingParentID,
        snapshotID: 0,
      },
      success: function (data) {
        let $content = "";
        if ($pendingParentID == "0") {
          $content =
            '<option name="' +
            lang.no_snapshot +
            '" value="0" selected>' +
            lang.no_snapshot +
            "</option>";
        }
        $content = $content + data["content"];
        let $element = $("#edit_pending_snapshot_" + $rowID);
        $element.html($content);
        $element.prop("disabled", false);
      },
      error: function () {
        alert(lang.error);
      },
    });
  });

  // save pool
  $(".save_pool").click(function () {
    let $rowID = $(this).attr("name");
    let $command = $(this).attr("data-href");
    let data = {};
    data["desktopPoolID"] = $("#row_" + $rowID).attr("name");
    data["currentParentID"] = $(
      "#edit_current_parent_" + $rowID + " option:selected"
    ).val();
    data["currentSnapshotID"] = $(
      "#edit_current_snapshot_" + $rowID + " option:selected"
    ).val();
    data["pendingParentID"] = $(
      "#edit_pending_parent_" + $rowID + " option:selected"
    ).val();
    data["pendingSnapshotID"] = $(
      "#edit_pending_snapshot_" + $rowID + " option:selected"
    ).val();

    if (data["pendingParentID"] != "0" && data["pendingSnapshotID"] != "0") {
      delete data["currentParentID"];
      delete data["currentSnapshotID"];
      data["secondaryImage"] = "True";
    } else {
      delete data["pendingParentID"];
      delete data["pendingSnapshotID"];
      data["secondaryImage"] = "False";
    }
    $(this).removeClass("save_pool");
    $(this).removeClass("fa-save");
    $(this).addClass("fa-spinner");

    $.fn.sendCommand($command, data);
  });

  // toggle provisioning
  $(".toggle_provisioning").click(function () {
    let $rowID = $(this).attr("name");
    let $command = $(this).attr("data-href");
    let data = {};
    data["desktopPoolID"] = $("#row_" + $rowID).attr("name");
    data["enabled"] = $(this).is(":checked");

    $.fn.sendCommand($command, data);
  });

  // promote secondary snapshot
  $(".promote").click(function () {
    let $rowID = $(this).attr("name");
    let $command = $(this).attr("data-href");
    let data = {};
    data["desktopPoolID"] = $("#row_" + $rowID).attr("name");

    $(this).removeClass("promote");
    $(this).removeClass("fa-fa-clone");
    $(this).addClass("fa-spinner");

    $.fn.sendCommand($command, data);
  });

  // select pool
  $("#select_pools").change(function () {
    let $pool = $("option:selected", this).attr("value");
    $.urlChangeParam("pool", $pool);
    location.reload();
  });

  // reset machine/session
  $(".reset").click(function () {
    let $rowID = $(this).attr("name");
    let $command = $(this).attr("data-href");
    let data = {};
    // machine or session id
    data["id"] = $("#row_" + $rowID).attr("name");

    $.fn.sendCommand($command, data);
  });

  // restart machine/session
  $(".restart").click(function () {
    let $rowID = $(this).attr("name");
    let $command = $(this).attr("data-href");
    let data = {};
    // machine or session id
    data["id"] = $("#row_" + $rowID).attr("name");

    $.fn.sendCommand($command, data);
  });

  // delete machine
  $(".delete").click(function () {
    let $rowID = $(this).attr("name");
    let $command = $(this).attr("data-href");
    let data = {};
    data["machineID"] = $("#row_" + $rowID).attr("name");

    $.fn.sendCommand($command, data);
  });

  // unassign user
  $(".unassign").click(function () {
    let $rowID = $(this).attr("name");
    let $command = $(this).attr("data-href");
    let data = {};
    data["machineID"] = $("#row_" + $rowID).attr("name");
    data["userID"] = $("#user_ids_" + $rowID + " span").attr("name");

    $.fn.sendCommand($command, data);
  });

  // disconnect session
  $(".disconnect").click(function () {
    let $rowID = $(this).attr("name");
    let $command = $(this).attr("data-href");
    let data = {};
    data["sessionID"] = $("#row_" + $rowID).attr("name");

    $.fn.sendCommand($command, data);
  });

  // logoff session
  $(".logoff").click(function () {
    let $rowID = $(this).attr("name");
    let $command = $(this).attr("data-href");
    let data = {};
    data["sessionID"] = $("#row_" + $rowID).attr("name");

    $.fn.sendCommand($command, data);
  });
};

$.fn.sendCommand = function ($command, $data) {
  $.ajax({
    url: "/admin/mod/send_vdi_command.php",
    type: "POST",
    async: true,
    data: {
      command: $command,
      parameters: $data,
    },
    beforeSend: function () {
      $("#loader").show();
    },
    complete: function (data) {
      if (data.length == 0) {
        console.log(data);
      }
      setTimeout(function () {
        $("#horizon_data").empty();
        $.fn.loadPage();
      }, 8000);
    },
  });
};

function urlExists(url, callback) {
  $.ajax({
    type: "HEAD",
    url: url,
    success: function () {
      callback(true);
    },
    error: function () {
      callback(false);
    },
  });
}
