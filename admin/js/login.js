$(document).ready(function () {
    if ($("#id_username").length) {
		$("#id_username").focus();
	}

	// navigation
	$("a[data-key='login']").addClass("active");
	$("a[data-key='login'] span.media-body").addClass("font-weight-bold");
    
    $("#login_form").submit(function (e) {
		let $username = $("#id_username").val();
		let $password = $("#id_password").val();
		let $csrf_token = $("#id_csrf_token").val();
		let $login = false;
		let $login_mode = '';
		let $groups = [];
		const $currentUrl = window.location.href;

		// get groups
		$.ajax({
      		url: "/admin/login/get_ldap_groups.php",
      		type: "POST",
			async: false,
			data: {
				csrf_token: $csrf_token,
			},
			success: function (data) {
				$.each(data, function (key, val) {
					$groups[key] = val;
				});
      		}
		});

		$($groups).each(function (_$key, $group) {
			$.ajax({
				url: "/admin/login/ldap_group_check.php",
				type: "POST",
				async: false,
				data: {
					username: $username,
					group: $group,
				},
				success: function (data) {
					let $user_lastname = data.user_lastname;
					let $user_firstname = data.user_firstname;
					let $user_mail = data.user_mail;
					if(data.status == 'true' && !$login){
						$login = true;
						$.ajax({
							url: "/admin/login/ldap_login.php",
							type: "POST",
							async: false,
							data: {
								username: $username,
								password: $password,
								csrf_token: $csrf_token,
								user_lastname: $user_lastname,
								user_firstname: $user_firstname,
								user_mail: $user_mail,
								login_mode: $login_mode
							},
							success: function (data) {
								if (data.status == "error") {
									alert(lang.login_not_possible);
									location.reload();
								}
								$login_mode = data.login_mode;
							},
							error: function () {
								alert(lang.login_not_possible);
							}
						});
					}
				},
				error: function () {
					alert(lang.login_not_possible);					
				}
			});
		});
		e.preventDefault(); //STOP default action

		if (!$login){
			alert(lang.no_rights);
			location.reload();
		} else if ($currentUrl.includes("login")) {
			location.href = "index.php";
		} else {
			location.href = $currentUrl;
		}
	});
});
