$(document).ready(function () {
  $(".save_theme_icon").click(function (e) {
    let $row = $(this).attr("name");
    let $theme = $("#edit_theme_" + $row).val();
    let $values = $("#edit_values_" + $row).val();
    let $confirm = confirm(lang.save_warning);

    if (!$("#edit_theme_" + $row).val()) {
      alert(lang.theme + " " + lang.missing);
    } else if (!$("#edit_values_" + $row).val()) {
      alert(lang.values + " " + lang.missing);
    } else if ($confirm) {
      $.ajax({
        url: "/admin/mod/update_theme.php",
        type: "POST",
        async: true,
        data: {
          id: $row,
          theme: $theme,
          values: $values,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $("#theme_submit").click(function (e) {
    let $theme = $("#add_theme").val();
    let $values = $("#add_values").val();

    if (!$("#add_theme").val()) {
      $(".invalid-feedback").hide();
      $("#theme_missing").show();
    } else if (!$("#add_values").val()) {
      $(".invalid-feedback").hide();
      $("#values_missing").show();
    } else {
      $.ajax({
        url: "/admin/mod/insert_theme.php",
        type: "POST",
        async: true,
        data: {
          theme: $theme,
          values: $values,
        },
        success: function (data) {
          $(".invalid-feedback").hide();
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          $(".invalid-feedback").hide();
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $(".delete_theme_icon").click(function (e) {
    let $theme_id = $(this).attr("name");
    let $confirm = confirm(lang.delete_theme_warning);

    if ($confirm) {
      $.ajax({
        url: "/admin/mod/delete_theme.php",
        type: "POST",
        async: true,
        data: {
          theme_id: $theme_id,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.delete_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.delete_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });
});
