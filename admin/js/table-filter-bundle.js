(function ($$1) {
	'use strict';

	$$1 = 'default' in $$1 ? $$1['default'] : $$1;

	let FilterMenu = function () {
		function FilterMenu(target, th, column, index, options) {
			this.options = options;
			this.th = th;
			this.column = column;
			this.index = index;
			this.tds = target.find('tbody tr td:nth-child(' + (this.column + 1) + ')').toArray();
		}
		FilterMenu.prototype.initialize = function () {
			this.menu = this.dropdownFilterDropdown();
			this.th.appendChild(this.menu);
			let $trigger = $(this.menu.children[0]);
			let $content = $(this.menu.children[1]);
			let $menu = $(this.menu);
			$trigger.click(function () {
				return $content.toggle();
			});
			$(document).click(function (el) {
				if (!$menu.is(el.target) && $menu.has(el.target).length === 0) {
					$content.hide();
				}
			});
		};
		FilterMenu.prototype.searchToggle = function (value) {
			if (this.selectAllCheckbox instanceof HTMLInputElement) this.selectAllCheckbox.checked = false;
			if (value.length === 0) {
				this.toggleAll(true);
				if (this.selectAllCheckbox instanceof HTMLInputElement) this.selectAllCheckbox.checked = true;
				return;
			}
			this.toggleAll(false);
			this.inputs.filter(function (input) {
				return input.value.toLowerCase().indexOf(value.toLowerCase()) > -1;
			}).forEach(function (input) {
				input.checked = true;
			});
		};
		FilterMenu.prototype.updateSelectAll = function () {
			if (this.selectAllCheckbox instanceof HTMLInputElement) {
				$(this.searchFilter).val('');
				this.selectAllCheckbox.checked = this.inputs.length === this.inputs.filter(function (input) {
					return input.checked;
				}).length;
			}
		};
		FilterMenu.prototype.selectAllUpdate = function (checked) {
			$(this.searchFilter).val('');
			this.toggleAll(checked);
		};
		FilterMenu.prototype.toggleAll = function (checked) {
			for (const element of this.inputs) {
				let input = element;
				if (input instanceof HTMLInputElement) input.checked = checked;
			}
		};
		FilterMenu.prototype.dropdownFilterItem = function (td, self) {
			let value = td.innerText;
			let dropdownFilterItem = document.createElement('div');
			dropdownFilterItem.className = 'dropdown-filter-item';
			let input = document.createElement('input');
			input.type = 'checkbox';
			input.value = value.trim().replace(/ +(?= )/g, '');
			input.setAttribute('checked', 'checked');
			input.className = 'dropdown-filter-menu-item item';
			input.setAttribute('data-column', self.column.toString());
			input.setAttribute('data-index', self.index.toString());
			dropdownFilterItem.appendChild(input);
			dropdownFilterItem.innerHTML = dropdownFilterItem.innerHTML.trim() + ' ' + value;
			return dropdownFilterItem;
		};
		FilterMenu.prototype.dropdownFilterItemSelectAll = function () {
			let value = this.options.captions.select_all;
			let dropdownFilterItemSelectAll = document.createElement('div');
			dropdownFilterItemSelectAll.className = 'dropdown-filter-item';
			let input = document.createElement('input');
			input.type = 'checkbox';
			input.value = this.options.captions.select_all;
			input.setAttribute('checked', 'checked');
			input.className = 'dropdown-filter-menu-item select-all';
			input.setAttribute('data-column', this.column.toString());
			input.setAttribute('data-index', this.index.toString());
			dropdownFilterItemSelectAll.appendChild(input);
			dropdownFilterItemSelectAll.innerHTML = dropdownFilterItemSelectAll.innerHTML + ' ' + value;
			return dropdownFilterItemSelectAll;
		};
		FilterMenu.prototype.dropdownFilterSearch = function () {
			let dropdownFilterItem = document.createElement('div');
			dropdownFilterItem.className = 'dropdown-filter-search';
			let input = document.createElement('input');
			input.type = 'text';
			input.className = 'dropdown-filter-menu-search form-control';
			input.setAttribute('data-column', this.column.toString());
			input.setAttribute('data-index', this.index.toString());
			input.setAttribute('placeholder', this.options.captions.search);
			dropdownFilterItem.appendChild(input);
			return dropdownFilterItem;
		};
		FilterMenu.prototype.dropdownFilterSort = function (direction) {
			let dropdownFilterItem = document.createElement('div');
			dropdownFilterItem.className = 'dropdown-filter-sort';
			let span = document.createElement('span');
			span.className = direction.toLowerCase().split(' ').join('-');
			span.setAttribute('data-column', this.column.toString());
			span.setAttribute('data-index', this.index.toString());
			span.innerText = direction;
			dropdownFilterItem.appendChild(span);
			return dropdownFilterItem;
		};
		FilterMenu.prototype.dropdownFilterContent = function () {
			let _this = this;
			let self = this;
			let dropdownFilterContent = document.createElement('div');
			dropdownFilterContent.className = 'dropdown-filter-content';
			let innerDivs = this.tds.reduce(function (arr, el) {
				let values = arr.map(function (el) {
					return el.innerText.trim();
				});
				if (values.indexOf(el.innerText.trim()) < 0) arr.push(el);
				return arr;
			}, []).sort(function (a, b) {
				let A = a.innerText.toLowerCase();
				let B = b.innerText.toLowerCase();
				if (!isNaN(Number(A)) && !isNaN(Number(B))) {
					if (Number(A) < Number(B)) return -1;
					if (Number(A) > Number(B)) return 1;
				} else {
					if (A < B) return -1;
					if (A > B) return 1;
				}
				return 0;
			}).map(function (td) {
				return _this.dropdownFilterItem(td, self);
			});
			this.inputs = innerDivs.map(function (div) {
				return div.firstElementChild;
			});
			let selectAllCheckboxDiv = this.dropdownFilterItemSelectAll();
			this.selectAllCheckbox = selectAllCheckboxDiv.firstElementChild;
			innerDivs.unshift(selectAllCheckboxDiv);
			let searchFilterDiv = this.dropdownFilterSearch();
			this.searchFilter = searchFilterDiv.firstElementChild;
			let outerDiv = innerDivs.reduce(function (outerDiv, innerDiv) {
				outerDiv.appendChild(innerDiv);
				return outerDiv;
			}, document.createElement('div'));
			outerDiv.className = 'checkbox-container';
			let elements = [];
			if (this.options.sort) elements = elements.concat([this.dropdownFilterSort(this.options.captions.a_to_z), this.dropdownFilterSort(this.options.captions.z_to_a)]);
			if (this.options.search) elements.push(searchFilterDiv);
			return elements.concat(outerDiv).reduce(function (html, el) {
				html.appendChild(el);
				return html;
			}, dropdownFilterContent);
		};
		FilterMenu.prototype.dropdownFilterDropdown = function () {
			let dropdownFilterDropdown = document.createElement('div');
			dropdownFilterDropdown.className = 'dropdown-filter-dropdown';
			let arrow = document.createElement('span');
			arrow.className = 'glyphicon glyphicon-arrow-down dropdown-filter-icon';
			let icon = document.createElement('i');
			icon.className="icon fa fa-filter fa-fw";
			arrow.appendChild(icon);
			dropdownFilterDropdown.appendChild(arrow);
			dropdownFilterDropdown.appendChild(this.dropdownFilterContent());
			if ($(this.th).hasClass('no-sort')) {
				$(dropdownFilterDropdown).find('.dropdown-filter-sort').remove();
			}
			if ($(this.th).hasClass('no-filter')) {
				$(dropdownFilterDropdown).find('.checkbox-container').remove();
			}
			if ($(this.th).hasClass('no-search')) {
				$(dropdownFilterDropdown).find('.dropdown-filter-search').remove();
			}
			if ($(this.th).hasClass('no-values')) {
				$(dropdownFilterDropdown).find('.checkbox-container').remove();
			}
			if ($(this.th).hasClass('slider')) {
				let values = [];
				let column_count = $("#all_files > thead > tr:first > th").length;
				
				$('#all_files tr td:nth-child('+column_count+')').each(function () {
					let value = parseFloat($(this).text());
					values.push(value);
				});
				
				let min = Math.min(...values);
				let max = Math.max(...values);
				
				$(dropdownFilterDropdown).find('.dropdown-filter-content').append(
					'<span id="min-display">'+min+'</span><span id="max-display" style="float: right;">'+max+'</span><div id="time_slider"></div>'
				).ready(function () {
					$('#time_slider').slider({
						min: min,
						max: max,
						step: 0.25,
						slide: function(_event,ui) {
							$('#all_files tbody tr').filter(function(){
								$('tr').show();
								let time = $.trim($(this).find('td').eq(5).text());
								let hours = time.substring(0, 2);
								let min = time.substring(3, 5);
								let sec = time.substring(6, 8);
								let timestamp = Number(sec) + Number(min) * 60 + Number(hours) * 3600;
								let slide_time = Number(ui.value) * 3600;
								return timestamp <= slide_time;
							}).hide();
						}
					});
				});
			}
			return dropdownFilterDropdown;
		};
		return FilterMenu;
	}();

	let FilterCollection = function () {
		function FilterCollection(target, options) {
			this.target = target;
			this.options = options;
			this.ths = target.find('th' + options.columnSelector).toArray();
			this.filterMenus = this.ths.map(function (th, index) {
				let column = $(th).index();
				return new FilterMenu(target, th, column, index, options);
			});
			this.rows = target.find('tbody').find('tr').toArray();
			this.table = target.get(0);
		}
		FilterCollection.prototype.initialize = function () {
			this.filterMenus.forEach(function (filterMenu) {
				filterMenu.initialize();
			});
			this.bindCheckboxes();
			this.bindSelectAllCheckboxes();
			this.bindSort();
			this.bindSearch();
		};
		FilterCollection.prototype.bindCheckboxes = function () {
			let filterMenus = this.filterMenus;
			let rows = this.rows;
			let ths = this.ths;
			let updateRowVisibility = this.updateRowVisibility;
			this.target.find('.dropdown-filter-menu-item.item').change(function () {
				let index = $(this).data('index');
				filterMenus[index].updateSelectAll();
				updateRowVisibility(filterMenus, rows, ths);
			});
		};
		FilterCollection.prototype.bindSelectAllCheckboxes = function () {
			let filterMenus = this.filterMenus;
			let rows = this.rows;
			let ths = this.ths;
			let updateRowVisibility = this.updateRowVisibility;
			this.target.find('.dropdown-filter-menu-item.select-all').change(function () {
				let index = $(this).data('index');
				let value = this.checked;
				filterMenus[index].selectAllUpdate(value);
				updateRowVisibility(filterMenus, rows, ths);
			});
		};
		FilterCollection.prototype.bindSort = function () {
			let filterMenus = this.filterMenus;
			let rows = this.rows;
			let ths = this.ths;
			let sort = this.sort;
			let table = this.table;
			let options = this.options;
			let updateRowVisibility = this.updateRowVisibility;
			this.target.find('.dropdown-filter-sort').click(function () {
				let $sortElement = $(this).find('span');
				let column = $sortElement.data('column');
				let order = $sortElement.attr('class');
				sort(column, order, table, options);
				updateRowVisibility(filterMenus, rows, ths);
			});
		};
		FilterCollection.prototype.bindSearch = function () {
			let filterMenus = this.filterMenus;
			let rows = this.rows;
			let ths = this.ths;
			let updateRowVisibility = this.updateRowVisibility;
			this.target.find('.dropdown-filter-search').keyup(function () {
				let $input = $(this).find('input');
				let index = $input.data('index');
				let value = $input.val();
				filterMenus[index].searchToggle(value);
				updateRowVisibility(filterMenus, rows, ths);
			});
		};
		FilterCollection.prototype.updateRowVisibility = function (filterMenus, rows) {
			let selectedLists = filterMenus.map(function (filterMenu) {
				return {
					column: filterMenu.column,
					selected: filterMenu.inputs.filter(function (input) {
						return input.checked;
					}).map(function (input) {
						return input.value.trim().replace(/ +(?= )/g, '');
					})
				};
			});
			for (const element of rows) {
				let tds = element.children;
				for (const list of selectedLists) {
					let content = tds[list.column].innerText.trim().replace(/ +(?= )/g, '');
					if (list.selected.indexOf(content) === -1) {
						$(element).hide();
						break;
					}
					$(element).show();
				}
			}
		};
		FilterCollection.prototype.sort = function (column, order, table, options) {
			let flip = 1;
			if (order === options.captions.z_to_a.toLowerCase().split(' ').join('-')) flip = -1;
			let tbody = $(table).find('tbody').get(0);
			let rows = $(tbody).find('tr').get();
			rows.sort(function (a, b) {
				let A = a.children[column].innerText.toUpperCase();
				let B = b.children[column].innerText.toUpperCase();
				if (!isNaN(Number(A)) && !isNaN(Number(B))) {
					if (Number(A) < Number(B)) return -1 * flip;
					if (Number(A) > Number(B)) return 1 * flip;
				} else {
					if (A < B) return -1 * flip;
					if (A > B) return 1 * flip;
				}
				return 0;
			});
			for (const element of rows) {
				tbody.appendChild(element);
			}
		};
		return FilterCollection;
	}();

	$$1.fn.excelTableFilter = function (options) {
		let target = this;
		options = $$1.extend({}, $$1.fn.excelTableFilter.options, options);
		if (typeof options.columnSelector === 'undefined') options.columnSelector = '';
		if (typeof options.sort === 'undefined') options.sort = true;
		if (typeof options.search === 'undefined') options.search = true;
		if (typeof options.captions === 'undefined') options.captions = {
			a_to_z:"A "+lang.to+" Z",
			z_to_a:"Z "+lang.to+" A",
			search:lang.search,
			select_all:lang.select_all
		};
		
		let filterCollection = new FilterCollection(target, options);
		filterCollection.initialize();
		return target;
	};
	$$1.fn.excelTableFilter.options = {};
}(jQuery));
