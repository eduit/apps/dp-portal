$(document).ready(function () {
	$("#id_exam").change(function () {
		let $checked = false;
		let rowCount = 0;
		$("#div_notification").hide($speed);
		$(".csv_buttons").hide($speed);
		$("#id_filename").html("");
		$.urlChangeParam('exam', this.value);
		$.ajax({
			url: "/admin/mod/get_exam_students.php",
			type: "POST",
			async: true,
			data: {
				exam_id: this.value
			},
			success: function (data) {
				if (data['content'] == '') {
					$("#existing_list").show($speed);
					$("#fileloader_students").show($speed);
					$("#delete_list").hide($speed);
					$("#remove_computers").hide($speed);
					$("#div_student_list").hide($speed);
					$("#student_list").find("tr:not(:first)").remove();
					$("#div_nothing").show($speed);
				} else {
					$("#student_list").find("tr:not(:first)").remove();
					$('#student_list tbody').append(data['content']);
					$("#existing_list").hide($speed);
					$("#fileloader_students").hide($speed);
					$("#delete_list").show($speed);
					$("#remove_computers").show($speed);
					$("#div_student_list").show($speed);
					$("#div_nothing").hide($speed);
				}

				rowCount = $('#student_list tbody tr:visible').length - 1;
				if (rowCount == '-1') {
					$('#count').html('0');
				} else {
					$('#count').html(rowCount);
				}

				$(".edit_allocation_icon").click(function () {
					let $id = $(this).attr("data-href");
					$(".edit_rows").hide($speed);
					$("#" + $(this).attr("name")).show($speed);
					$checked = $("#edit_allocation_" + $id).is(':checked');
				});

				$(".cancel_icon").click(function () {
					$(".edit_rows").hide($speed);
				});

				$("#cancel_upload").click(function () {
					location.reload();
				});

				$(".save_participant_icon").click(function () {
					let $id = $(this).attr("name");
					let $room_id = $("#edit_room_" + $id).val();
					let $exam_id = $("#id_exam").val();
					let $us_keyboard = $("#edit_us_keyboard_" + $id).is(':checked');
					let $group = $("#edit_group_" + $id).val();
					let $remark = $("#edit_remark_" + $id).val();
					let $url = $("#edit_url_" + $id).val();
					let $computer_id = $("#edit_computer_" + $id).val();
					let $active = $("#edit_allocation_" + $id).is(':checked');
					let $confirm = '';
					if ($active && !$checked) {
						$confirm = confirm(lang.toggle_exams_warning);
					} else {
						$confirm = true;
					}
					if ($confirm) {
						$.ajax({
							url: "/admin/mod/update_exam_student.php",
							type: "POST",
							async: true,
							data: {
								id: $id,
								room_id: $room_id,
								exam_id: $exam_id,
								us_keyboard: $us_keyboard,
								group: $group,
								remark: $remark,
								url: $url,
								computer_id: $computer_id,
								active: $active
							},
							success: function (data) {
								if (data.status1 == "error" || data.status2 == "error") {
									alert(lang.saving_not_possible);
								} else if (data.status2 == "computer busy") {
									alert(lang.computer_busy);
								} else {
									$("#id_exam").trigger("change");
								}
							},
							error: function () {
								alert(lang.error);
							}
						});
					}
				});

				$(".select-room").change(function () {
					let $row = $(this).attr("name");
					$.ajax({
						url: "/admin/mod/get_exam_students.php",
						type: "POST",
						async: true,
						data: {
							exam_id: $("#id_exam").val(),
							room_id: this.value,
							mode: "computers"
						},
						success: function (data) {
							$("#edit_computer_" + $row).html(data['content']);
						},
						error: function () {
							alert(lang.error);
						}
					});
				});
			},
			error: function () {
				alert(lang.error);
			}
		});
	});

	let $exam = $.urlParam('exam');
	if ($exam) {
		$("#id_exam").val($exam).trigger("change");
	}

	$("#fileloader_students").change(function () {
		let file = $("#fileloader_students")[0].files[0];
		let reader = new FileReader();
		if (file['name'] == 'example.csv') {
			alert(lang.example_csv);
			return 0;
		}
		reader.onload = function (e) {
			let csvResult = e.target.result;
			$("#fileloader_students").attr('encoding', jschardet.detect(csvResult).encoding);
			$.show_list_from_file($("#fileloader_students"));
			$("#div_nothing").hide($speed);
		}
		reader.readAsArrayBuffer(file);
	});

	(function ($) {
		$.show_list_from_file = function (file) {
			let regex = /^([a-zA-Z0-9\s_\\.\-:\(\)])+(.csv|.txt)$/;
			let table;
			let row;
			let separator;
			let cells;
			let cell;
			let filename;
			if (regex.test(file.val().toLowerCase())) {
				if (typeof (FileReader) != "undefined") {
					let reader = new FileReader();
					reader.onload = function (e) {
						let rows = e.target.result.split("\n");
						$("#student_list").find("tr").remove();
						for (let i = 0; i < rows.length; i++) {
							if (i == 0) {
								table = $("#student_list thead");
								row = $("<tr />");
								separator = ",";
								cells = rows[i].split(separator);
								if (cells.length == 1) {
									separator = ";";
									cells = rows[i].split(separator);
								}
								if (cells.length > 1) {
									for (let value of cells) {
										cell = $("<th />");
										cell.html(value);
										row.append(cell);
									}
									table.append(row);
								}
							} else {
								table = $("#student_list tbody");
								row = $("<tr id='" + i + "' />");
								cells = rows[i].split(separator);
								if (cells.length > 1) {
									for (let value of cells) {
										cell = $("<td />");
										cell.html(value);
										row.append(cell);
									}
									table.append(row);
								}
							}
						}
						$(".csv_buttons").show($speed);
						$("#div_student_list").show($speed, function () {
							let rowCount = $('#student_list tbody tr:visible').length;
							$('#count').html(rowCount);
						});

						//get filename
						let fullPath = file.val();
						if (fullPath) {
							let startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
							filename = fullPath.substring(startIndex);
							if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
								filename = filename.substring(1);
							}
						}
						$("#id_filename").html(filename);
					}
					reader.readAsText(file[0].files[0], $("#fileloader_students").attr('encoding'));

					$("#student_save_button").one("click", function (e) {
						let file1 = file[0].files[0];
						let $filetype = "csv";
						let errors;

						if ($("#id_exam").val() == null) {
							$(".invalid-feedback").hide();
							$("#id_missing_exam").show();
						} else if (file1 != undefined) {
							$(".invalid-feedback").hide();
							let formData = new FormData();
							let exam_id = $("#id_exam option:selected").val();
							let encoding = $("#fileloader_students").attr('encoding');
							formData.append('file', file1);
							formData.append("filetype", $filetype);

							$.ajax({
								url: "/admin/mod/insert_exam_students.php?exam_id=" + exam_id + "&encoding=" + encoding,
								type: "POST",
								async: true,
								data: formData,
								processData: false,
								contentType: false,
								success: function (data) {
									if (data[0]['final_status_text']) {
										errors = data[0]['final_status_text'];
									} else {
										errors = [];
									}

									let error_text = '';

									errors.every((element, index) => {
										if (index > 1) {
											error_text = error_text + '\n...';
											return false;
										}
										error_text = error_text + '\n' + element;
										return true;
									});

									if (data[0]['file_status'] == "too large") {
										alert(lang.file_too_large);
									} else if (data[0]['file_status'] == "wrong file") {
										alert(lang.wrong_file + " (" + $filetype + ")");
									} else if (data[0]['upload_status'] == "error") {
										alert(lang.saving_not_possible + "\n'" + data[0]['file_status'] + "'");
									} else if (data[0]['csv'] == 0) {
										alert(lang.reading_not_possible);
									} else if (data[0]['final_status'] == "error") {
										alert(lang.error + '\n' + error_text);
									} else {
										$("#div_notification").show($speed);
										$("#notification_text").html(lang.success);
										$(".csv_buttons").hide($speed);
										$("#existing_list").hide($speed);
										$("#delete_list").show($speed);
										setTimeout(function () {
											location.reload();
										}, 2000);
									}
								},
								error: function () {
									alert(lang.saving_not_possible /*+':\n'+errorThrown*/ );
									$(".csv_buttons").hide($speed);
									$("#div_student_list").hide($speed);
									$("#div_nothing").show($speed);
								}
							});
						}
						e.preventDefault(); //STOP default action
					});
				} else {
					alert(lang.no_html5);
				}
			} else {
				alert(lang.valid_csv);
			}
		}
	})(jQuery);

	$("#template_list").click(function () {
		location.href = "../upload/students-csv/example.csv";
	});

	$("#delete_list").click(function () {
		let $confirm = confirm(lang.delete_enrollments_warning);
		if ($confirm) {
			$.ajax({
				url: "/admin/mod/delete_exam_students.php",
				type: "POST",
				async: true,
				data: {
					exam_id: $("#id_exam").val()
				},
				success: function (data) {
					if (data.status == "error") {
						alert(lang.delete_not_possible);
					} else {
						location.reload();
					}
				},
				error: function () {
					alert(lang.delete_not_possible);
				}
			});
		}
	});

	$("#remove_computers").click(function () {
		let $confirm = confirm(lang.delete_computers_warning);
		if ($confirm) {
			$.ajax({
				url: "/admin/mod/delete_exam_computers.php",
				type: "POST",
				async: true,
				data: {
					exam_id: $("#id_exam").val()
				},
				success: function (data) {
					if (data.status == "error") {
						alert(lang.delete_not_possible);
					} else {
						location.reload();
					}
				},
				error: function () {
					alert(lang.delete_not_possible);
				}
			});
		}
	});

	$(document).on("click", ".delete_participant_icon", function () {
		let $row = $(this).attr('name')
		let $confirm = confirm(lang.delete_enrollment_warning);
		if ($confirm) {
			$.ajax({
				url: "/admin/mod/delete_exam_student.php",
				type: "POST",
				async: true,
				data: {
					exam_student_id: $row
				},
				success: function (data) {
					if (data.status == "error") {
						alert(lang.delete_not_possible);
					} else {
						$("#" + $row).hide($speed);
						$(".edit_rows").hide($speed);
					}
				},
				error: function () {
					alert(lang.delete_not_possible);
				}
			});
		}
	});

	$("#student_delete_button").click(function () {
		let $confirm = confirm(lang.delete_list_warning);
		if ($confirm) {
			$(".csv_buttons").hide($speed);
			$("#div_student_list").hide($speed);
			$("#div_nothing").show($speed);
		}
	});

	$(".status_icon").click(function () {
		let dataArr = $(this).data("href").split(',');
		$exam = dataArr[0];
		let $room = dataArr[1];
		let $status = dataArr[2];
		let $button = $(this).attr('name');

		$.ajax({
			url: "/admin/mod/toggle_room_status.php",
			type: "POST",
			async: true,
			data: {
				exam: $exam,
				room: $room,
				active: $status
			},
			success: function (data) {
				if (data.status1 == "error" || data.status2 == "error") {
					alert(lang.saving_not_possible);
				} else {
					if (data.status1 == "success") {
						$(".fa-pause[name*='button_" + $room + "_']").hide();
						$(".fa-play[name*='button_" + $room + "_']").show();
					}
					$(".status_icon[name*='" + $button + "']").toggle();
				}
			},
			error: function () {
				alert(lang.saving_not_possible);
			}
		});
	});
});
