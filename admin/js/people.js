$(document).ready(function () {
  $(".save_people_icon").click(function (e) {
    let $row = $(this).attr("name");
    let $username = $("#edit_username_" + $row).val();
    let $lastname = $("#edit_lastname_" + $row).val();
    let $firstname = $("#edit_firstname_" + $row).val();
    let $confirm = confirm(lang.save_warning);

    if (!$("#edit_lastname_" + $row).val()) {
      alert(lang.lastname + " " + lang.missing);
    } else if (!$("#edit_firstname_" + $row).val()) {
      alert(lang.firstname + " " + lang.missing);
    } else if ($confirm) {
      $.ajax({
        url: "/admin/mod/update_people.php",
        type: "POST",
        async: true,
        data: {
          id: $row,
          username: $username,
          lastname: $lastname,
          firstname: $firstname,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else if (data.status == "already existing") {
            alert(lang.other_people_existing);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $("#people_submit").click(function (e) {
    let $username = $("#add_username").val();
    let $lastname = $("#add_lastname").val();
    let $firstname = $("#add_firstname").val();

    if (!$("#add_username").val()) {
      $(".invalid-feedback").hide();
      $("#username_missing").show();
    } else if (!$("#add_lastname").val()) {
      $(".invalid-feedback").hide();
      $("#lastname_missing").show();
    } else if (!$("#add_firstname").val()) {
      $(".invalid-feedback").hide();
      $("#firstname_missing").show();
    } else {
      $.ajax({
        url: "/admin/mod/insert_people.php",
        type: "POST",
        async: true,
        data: {
          username: $username,
          lastname: $lastname,
          firstname: $firstname,
        },
        success: function (data) {
          $(".invalid-feedback").hide();
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else if (data.status == "already existing") {
            alert(lang.other_people_existing);
          } else {
            location.reload();
          }
        },
        error: function () {
          $(".invalid-feedback").hide();
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $(".delete_people_icon").click(function (e) {
    let $people_id = $(this).attr("name");
    let $confirm = confirm(lang.delete_people_warning);

    if ($confirm) {
      $.ajax({
        url: "/admin/mod/delete_people.php",
        type: "POST",
        async: true,
        data: {
          people_id: $people_id,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.delete_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.delete_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  // LDAP functions
  $.ajax({
    url: "/admin/mod/insert_ldap_people.php",
    type: "POST",
    async: true,
    data: {
      mode: "list",
      group: "ID-EDUC-dp-portal_vho",
    },
    success: function (data) {
      if (data.status == "error") {
        alert(lang.error);
      } else {
        $("#ldap_count").html(data.number);
      }
    },
    error: function (xhr, status, error) {
      if (xhr.status != 0) {
        alert(lang.error);
      }
    },
  });

  $("#add_ldap_people").click(function (e) {
    $.ajax({
      url: "/admin/mod/insert_ldap_people.php",
      type: "POST",
      async: true,
      data: {
        mode: "write",
        group: "ID-EDUC-dp-portal_vho",
      },
      success: function (data) {
        if (data.status == "error") {
          alert(lang.saving_not_possible);
        } else {
          location.reload();
        }
      },
      error: function () {
        alert(lang.saving_not_possible);
      },
    });
    e.preventDefault(); //STOP default action
  });

  $.ajax({
    url: "/admin/mod/update_ldap_people.php",
    type: "POST",
    async: true,
    data: {
      mode: "list",
    },
    success: function (data) {
      if (data.status == "error") {
        alert(lang.error);
      } else {
        $("#ldap_update_count").html(data.number);
      }
    },
    error: function (xhr, status, error) {
      if (xhr.status != 0) {
        alert(lang.error);
      }
    },
  });

  $("#update_ldap_people").click(function (e) {
    $.ajax({
      url: "/admin/mod/update_ldap_people.php",
      type: "POST",
      async: true,
      data: {
        mode: "write",
      },
      success: function (data) {
        if (data.status == "error") {
          alert(lang.saving_not_possible);
        } else {
          location.reload();
        }
      },
      error: function () {
        alert(lang.saving_not_possible);
      },
    });
    e.preventDefault(); //STOP default action
  });
});
