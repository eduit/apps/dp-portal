$(document).ready(function () {
  // checkbox styling
  $(":checkbox").checkboxradio({
    icon: false,
  });

  $("#all_files").change(function () {
    let fullCount = $("#all_files tbody tr").length;
    let rowCount = $("#all_files tbody tr:visible").length;
    $("#count").html(rowCount + "/" + fullCount);
    clearTimeout(reload);
  });
});
