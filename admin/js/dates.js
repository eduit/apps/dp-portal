let $start = 0;
let $action = "inactive";

$(document).ready(function () {
  // checkbox styling
  $(":checkbox").checkboxradio({
    icon: false,
  });

  $("#show_disabled").change(function (e) {
    let $view;
    $("#count").hide();
    $("#exams").hide();
    $start = 0;
    $action = "inactive";
    $("#number").html(0);
    $("#exams").html("");
    if ($(this).is(":checked")) {
      if ($("#show_archive").is(":checked")) {
        $.urlChangeParam("disabled", "true");
        $.urlChangeParam("archive", "true");
        $view = "dates_all";
      } else {
        $.urlChangeParam("disabled", "true");
        $.urlChangeParam("archive", "false");
        $view = "dates_disabled";
      }
    } else if ($("#show_archive").is(":checked")) {
      $.urlChangeParam("disabled", "false");
      $.urlChangeParam("archive", "true");
      $view = "dates_archive";
    } else {
      $.urlChangeParam("disabled", "false");
      $.urlChangeParam("archive", "false");
      $view = "dates";
    }
    $.getDates($view);
    e.preventDefault(); //STOP default action
  });

  $("#show_archive").change(function (e) {
    let $view;
    $("#count").hide();
    $("#exams").hide();
    $start = 0;
    $action = "inactive";
    $("#number").html(0);
    $("#exams").html("");
    if ($(this).is(":checked")) {
      if ($("#show_disabled").is(":checked")) {
        $.urlChangeParam("disabled", "true");
        $.urlChangeParam("archive", "true");
        $view = "dates_all";
      } else {
        $.urlChangeParam("disabled", "false");
        $.urlChangeParam("archive", "true");
        $view = "dates_archive";
      }
    } else if ($("#show_disabled").is(":checked")) {
      $.urlChangeParam("disabled", "true");
      $.urlChangeParam("archive", "false");
      $view = "dates_disabled";
    } else {
      $.urlChangeParam("disabled", "false");
      $.urlChangeParam("archive", "false");
      $view = "dates";
    }
    $.getDates($view);
    e.preventDefault(); //STOP default action
  });

  $("#sharepoint").click(function () {
    location.href = "index.php?admin_mode=site_administration&tab=sharepoint";
  });
});

// Functions
(function ($) {
  $.getDates = function ($view) {
    //get parameters and set checkboxes
    if ($.urlParam("disabled") == "true") {
      $("#show_disabled").prop("checked", true);
    }

    if ($.urlParam("archive") == "true") {
      $("#show_archive").prop("checked", true);
    }

    if ($view == "init") {
      if (
        $("#show_disabled").is(":checked") &&
        $("#show_archive").is(":checked")
      ) {
        $view = "dates_all";
      }
      if (
        $("#show_disabled").is(":checked") &&
        !$("#show_archive").is(":checked")
      ) {
        $view = "dates_disabled";
      }
      if (
        !$("#show_disabled").is(":checked") &&
        $("#show_archive").is(":checked")
      ) {
        $view = "dates_archive";
      }
      if (
        !$("#show_disabled").is(":checked") &&
        !$("#show_archive").is(":checked")
      ) {
        $view = "dates";
      }
    }

    (function ($) {
      $.load_dates = function (limit, start) {
        $.ajax({
          url: "/admin/mod/get_date_list.php",
          type: "POST",
          async: true,
          data: {
            view: $view,
            limit: limit,
            start: start,
          },
          success: function (data) {
            if (data.status == "error") {
              alert(lang.error);
            } else {
              if (data["content"] == "") {
                $action = "active";
              } else {
                $("#exams").append(data["content"]);
                $action = "inactive";
              }
              $("#number").html(
                parseFloat($("#number").html()) + parseFloat(data["number"])
              );
              $.dateFunctions($view);
              $.generalFunctions();
              setTimeout(function () {
                $.ajax({
                  url: "/admin/mod/get_exam_people.php",
                  type: "POST",
                  async: true,
                  success: function (data) {
                    $(".examPeople").each(function () {
                      let $content = data["content"];
                      let $selected = $(this).attr("value");
                      $(this).append($content);
                      if ($selected) {
                        $(this).find("option").each(function () {
                          if ($(this).val() == $selected) {
                            $(this).prop("selected", true);
                          }
                        })
                      }
                    });
                  },
                });
              }, 2000);
            }
          },
          error: function (xhr, status, error) {
            if (xhr.status != 0) {
              alert(lang.error);
            }
          },
        });
      };
    })(jQuery);

    if ($action == "inactive") {
      $action = "active";
      $.load_dates($limit, $start);
    }
    $(window).scroll(function () {
      if (
        $(window).scrollTop() + $(window).height() > $("#exam_list").height() &&
        $action == "inactive"
      ) {
        $action = "active";
        $start = $start + $limit;
        setTimeout(function () {
          $.load_dates($limit, $start);
        }, 1000);
      }
    });
    $("#count").delay(500).show($speed);
    $("#exams").delay(500).show($speed);
  };
})(jQuery);

(function ($) {
  $.dateFunctions = function ($view) {
    $(".toggle_exam_icon").off();
    $(".toggle_exam_icon").click(function (e) {
      let $row = $(this).attr("name");
      let $status = $(this).attr("id").slice(0, 1);

      $.ajax({
        url: "/admin/mod/toggle_exam.php",
        type: "POST",
        async: true,
        data: {
          id: $row,
          disabled: $status,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.toggle_not_possible);
          } else {
            $.ajax({
              url: "/admin/mod/toggle_exam_status.php",
              type: "POST",
              async: true,
              data: {
                exam: $row,
                active: "f",
              },
              success: function (data) {
                if (data.status == "error") {
                  alert(lang.saving_not_possible);
                } else {
                  $("#row_" + $row).hide($speed);
                  $("#number").html(parseFloat($("#number").html()) - 1);
                }
              },
              error: function () {
                alert(lang.saving_not_possible);
              },
            });
          }
        },
        error: function () {
          alert(lang.toggle_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    });

    $(".archive_exam_icon").off();
    $(".archive_exam_icon").click(function (e) {
      let $row = $(this).attr("name");
      let $status = $(this).attr("id").slice(0, 1);
      let $error = false;

      $.ajax({
        url: "/admin/mod/archive_exam.php",
        type: "POST",
        async: true,
        data: {
          id: $row,
          archived: $status,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.archive_not_possible);
          } else {
            $.ajax({
              url: "/admin/mod/delete_exam_students.php",
              type: "POST",
              async: true,
              data: {
                exam_id: $row,
              },
              success: function (data) {
                if (data.status == "error") {
                  alert(lang.delete_not_possible);
                  $error = true;
                }
              },
              error: function () {
                alert(lang.delete_not_possible);
                $error = true;
              },
            });

            $.ajax({
              url: "/admin/mod/toggle_exam_status.php",
              type: "POST",
              async: true,
              data: {
                exam: $row,
                active: "f",
              },
              success: function (data) {
                if (data.status == "error") {
                  alert(lang.saving_not_possible);
                  $error = true;
                } else if (!$error) {
                  $("#row_" + $row).hide($speed);
                  $("#number").html(parseFloat($("#number").html()) - 1);
                }
              },
              error: function () {
                alert(lang.saving_not_possible);
                $error = true;
              },
            });
          }
        },
        error: function () {
          alert(lang.archive_not_possible);
        },
      });

      e.preventDefault(); //STOP default action
    });

    $(".save_date_icon").off();
    $(".save_date_icon").click(function (e) {
      let $row = $(this).attr("name");
      let $date = $("#edit_date_" + $row).val();
      let $exam_name_id = $("#edit_exam_" + $row + " option:selected").val();
      let $type = $("#edit_type_" + $row + " option:selected").val();
      let $lang = $("#edit_lang_" + $row + " option:selected").val();
      let $lecturer = $("#edit_lecturer_" + $row + " option:selected").val();
      let $vdp = $("#edit_vdp_" + $row + " option:selected").val();
      let $sysadmin = $("#edit_sysadmin_" + $row + " option:selected").val();
      let $link = $("#edit_link_" + $row).val();
      let $seb = $("#edit_seb_" + $row).val();
      let $confirm = confirm(lang.save_warning);

      if (!$("#edit_date_" + $row).val()) {
        alert(lang.date + " " + lang.missing);
      } else if (!$("#edit_link_" + $row).val() && $type != 3) {
        alert(lang.url + " " + lang.missing);
      } else if (
        $("#edit_link_" + $row)
          .val()
          .indexOf("http") != 0 &&
        $type != 3
      ) {
        alert(lang.url + " " + lang.wrong);
        $("#edit_link_" + $row).focus();
        $("#edit_link_" + $row).select();
      } else if ($confirm) {
        $.ajax({
          url: "/admin/mod/update_date.php",
          type: "POST",
          async: true,
          data: {
            exam_id: $row,
            date: $date,
            exam_name_id: $exam_name_id,
            type: $type,
            lang: $lang,
            lecturer: $lecturer,
            vdp: $vdp,
            sysadmin: $sysadmin,
            url: $link,
            seb: $seb,
          },
          success: function (data) {
            if (data.status1 == "error" || data.status2 == "error") {
              alert(lang.saving_not_possible);
            } else {
              $start = 0;
              $action = "inactive";
              $("#number").html(0);
              $("#exams").html("");
              $.getDates($view);
            }
          },
          error: function () {
            alert(lang.saving_not_possible);
          },
        });
        e.preventDefault(); //STOP default action
      }
    });

    $(".delete_date_icon").off();
    $(".delete_date_icon").click(function (e) {
      let $exam_id = $(this).attr("name");
      let $confirm = confirm(lang.delete_exam_warning);

      if ($confirm) {
        $.ajax({
          url: "/admin/mod/delete_exam.php",
          type: "POST",
          async: true,
          data: {
            exam_id: $exam_id,
          },
          success: function (data) {
            if (data.status == "error") {
              alert(lang.delete_not_possible);
            } else if (data.status == "enrollments_existing") {
              alert(lang.enrollments_existing);
            } else {
              $("#row_" + $exam_id).hide($speed);
              $("#number").html($("#number").html() - 1);
            }
          },
          error: function () {
            alert(lang.delete_not_possible);
          },
        });
        e.preventDefault(); //STOP default action
      }
    });

    $("#exam_submit").off();
    $("#exam_submit").click(function (e) {
      let $date = $("#add_date").val();
      let $exam = $("#add_exam option:selected").val();
      let $lecturer = $("#add_lecturer option:selected").val();
      let $vdp = $("#add_vdp option:selected").val();
      let $sysadmin = $("#add_sysadmin option:selected").val();
      let $link = $("#add_link").val();
      let $seb = $("#add_seb").val();
      let $type = $("#add_type option:selected").val();
      let $lang = $("#add_lang option:selected").val();

      if (!$("#add_date").val()) {
        $(".invalid-feedback").hide();
        $("#date_missing").show();
      } else if ($("#add_exam").val() == null) {
        $(".invalid-feedback").hide();
        $("#exam_missing").show();
      } else if ($("#add_type").val() == null) {
        $(".invalid-feedback").hide();
        $("#type_missing").show();
      } else if ($("#add_lang").val() == null) {
        $(".invalid-feedback").hide();
        $("#lang_missing").show();
      } else if ($("#add_lecturer").val() == null) {
        $(".invalid-feedback").hide();
        $("#lecturer_missing").show();
      } else if ($("#add_vdp").val() == null) {
        $(".invalid-feedback").hide();
        $("#vdp_missing").show();
      } else if ($("#add_sysadmin").val() == null) {
        $(".invalid-feedback").hide();
        $("#sysadmin_missing").show();
      } else if (!$("#add_link").val() && $type != 3) {
        $(".invalid-feedback").hide();
        $("#link_missing").show();
      } else if ($("#add_link").val().indexOf("http") != 0 && $type != 3) {
        $(".invalid-feedback").hide();
        $("#link_wrong").show();
      } else {
        $.ajax({
          url: "/admin/mod/insert_date.php",
          type: "POST",
          async: true,
          data: {
            date: $date,
            exam: $exam,
            url: $link,
            seb: $seb,
            lecturer: $lecturer,
            vdp: $vdp,
            sysadmin: $sysadmin,
            type: $type,
            lang: $lang,
          },
          success: function (data) {
            $(".invalid-feedback").hide();
            if (data.status1 == "error" || data.status2 == "error") {
              alert(lang.saving_not_possible);
            } else if (data.status1 == "already existing") {
              alert(lang.other_exam_existing);
            } else {
              $start = 0;
              $action = "inactive";
              $("#number").html(0);
              $("#exams").html("");
              $.getDates($view);
            }
          },
          error: function () {
            $(".invalid-feedback").hide();
            alert(lang.saving_not_possible);
          },
        });
        e.preventDefault(); //STOP default action
      }
    });

    $("#add_date").off();
    $("#add_date").change(function () {
      if (!$(this).val()) {
        $("#date_missing").show();
      } else {
        $("#date_missing").hide();
      }
    });

    $("#add_link").off();
    $("#add_link").change(function () {
      if (!$(this).val()) {
        $("#link_missing").show();
      } else {
        $("#link_missing").hide();
      }
    });

    $("#upload_seb_config").off();
    $("#upload_seb_config").click(function () {
      $("#seb_missing").hide($speed);
      $("#add_seb").val("");
      $("#fileloader_dates").click();
    });

    $("#fileloader_dates").off();
    $("#fileloader_dates").change(function (e) {
      let file = $("#fileloader_dates")[0].files[0];
      let $filetype = "seb";
      if (file != undefined) {
        let dir = $(this).attr("data-href");
        let $rename = false;
        let formData = new FormData();
        formData.append("file", file);
        formData.append("dir", dir);
        formData.append("filetype", $filetype);
        formData.append("rename", $rename);

        $.ajax({
          url: "/admin/mod/check_file.php",
          type: "POST",
          async: true,
          data: formData,
          processData: false,
          contentType: false,
          success: function (data) {
            if (data.status == "already existing") {
              let $overwrite = confirm(lang.file_exists);
              if (!$overwrite) {
                return;
              }
            }
            $.ajax({
              url: "/admin/mod/upload_file.php",
              type: "POST",
              async: true,
              data: formData,
              processData: false,
              contentType: false,
              success: function (data) {
                if (data.status1 == "already existing") {
                  alert(lang.overwriting_file);
                  $("#add_seb").val(data.link);
                  $("#seb_missing").text(lang.overwriting_file);
                  $("#seb_missing").show($speed);
                } else if (data.status1 == "too large") {
                  alert(lang.file_too_large);
                } else if (data.status1 == "wrong file") {
                  alert(lang.wrong_file + " (" + $filetype + ")");
                } else if (data.status2 == "error") {
                  alert(lang.saving_not_possible);
                } else {
                  $("#add_seb").val(data.link);
                  $("#seb_missing").text(lang.upload_successful);
                  $("#seb_missing").css("color", "green");
                  $("#seb_missing").show($speed);
                }
              },
              error: function () {
                alert(lang.saving_not_possible);
              },
            });
          },
          error: function () {
            alert(lang.saving_not_possible);
          },
        });
      }
      e.preventDefault(); //STOP default action
    });

    $(".edit_upload").off();
    $(".edit_upload").click(function () {
      $("#fileloader_dates_" + $(this).attr("name")).click();
    });

    $(".edit_fileloader_dates").off();
    $(".edit_fileloader_dates").change(function (e) {
      let file = $("#fileloader_dates_" + $(this).attr("name"))[0].files[0];
      let $filetype = "seb";
      let $name = $(this).attr("name");
      if (file != undefined) {
        let dir = $(this).attr("data-href");
        let $rename = false;
        let formData = new FormData();
        formData.append("file", file);
        formData.append("dir", dir);
        formData.append("filetype", $filetype);
        formData.append("rename", $rename);

        $.ajax({
          url: "/admin/mod/check_file.php",
          type: "POST",
          async: true,
          data: formData,
          processData: false,
          contentType: false,
          success: function (data) {
            if (data.status == "already existing") {
              let $overwrite = confirm(lang.file_exists);
              if (!$overwrite) {
                return;
              }
            }
            $.ajax({
              url: "/admin/mod/upload_file.php",
              type: "POST",
              async: true,
              data: formData,
              processData: false,
              contentType: false,
              success: function (data) {
                if (data.status1 == "already existing") {
                  alert(lang.overwriting_file);
                  $("#edit_seb_" + $name).val(data.link);
                } else if (data.status1 == "too large") {
                  alert(lang.file_too_large);
                } else if (data.status1 == "wrong file") {
                  alert(lang.wrong_file + " (" + $filetype + ")");
                } else if (data.status2 == "error") {
                  alert(lang.saving_not_possible);
                } else {
                  $("#edit_seb_" + $name).val(data.link);
                }
              },
              error: function () {
                alert(lang.saving_not_possible);
              },
            });
          },
          error: function () {
            alert(lang.saving_not_possible);
          },
        });
      }
      e.preventDefault(); //STOP default action
    });

    // moodle backup
    $(".backup_icon").off();
    $(".backup_icon").click(function (e) {
      e.preventDefault(); // STOP default action
      let id = $(this).attr("name");
      let obj = JSON.parse($(this).attr("data-href").replace(/'/g, '"'));
      let exam_link = new URL($("#link_" + id + " a").attr("href"));
      let moodle_url = exam_link.origin;
      let moodle_id = obj.moodle_id;
      let pathname = exam_link.pathname;
      let type;

      if (pathname.startsWith("/mod/quiz")) {
        type = "quiz";
      } else if (pathname.startsWith("/course")) {
        type = "course";
      }

      let exam_nr = obj.exam_nr;
      let lecturer_name = $("#lecturer_" + id + " .lastname").text();
      let lecturer_name_us = lecturer_name.replace(/ /g, "_");
      let exam = escapeHtml($("#exam_" + id).text());
      let date = $("#date_" + id).text();
      let date_reverse = date.split(".").reverse().join(".");
      date_reverse = date_reverse.split(".").join("");
      let user_info = $("#user_info").attr("title");
      let user_firstname = user_info.substring(0, user_info.indexOf(" "));
      let user_lastname = user_info.substring(
        user_info.indexOf(" ") + 1,
        user_info.length
      );
      let user_init = user_firstname.substr(0, 1) + user_lastname.substr(0, 1);
      let filename =
        date_reverse + "_" + lecturer_name_us + "_Backup_" + user_init + ".mbz";

      // start moodle backup
      $.ajax({
        url: "/admin/mod/get_moodle_backup.php",
        type: "POST",
        async: true,
        data: {
          moodle_url: moodle_url,
          id: moodle_id,
          type: type,
        },
        beforeSend: function () {
          $("#loader").show();
        },
        success: function (result) {
          let isError;
          if (result.errorcode) {
            alert(
              lang.moodle_error +
                " " +
                result.exception +
                ' ("' +
                result.errorcode +
                '")'
            );
            isError = true;
            afterDownload(isError);
          } else {
            isError = false;
            let backup_file = result.rel;
            // download file
            downloadFile(backup_file, filename).then(() => {
              afterDownload(isError);
            });
          }
        },
        error: function (error) {
          alert(lang.timeout + " (" + error.status + ")");
          let isError = true;
          afterDownload(isError);
        },
        complete: function () {
          $("#loader").hide();
        },
        timeout: 120000
      });

      function downloadFile(url, filename) {
        return new Promise((resolve, reject) => {
          const download = async (url, filename) => {
            try {
              const data = await fetch(url);
              const blob = await data.blob();
              const objectUrl = URL.createObjectURL(blob);
              const link = document.createElement("a");
              link.setAttribute("href", objectUrl);
              link.setAttribute("download", filename);
              link.style.display = "none";
              document.body.appendChild(link);
              link.click();
              document.body.removeChild(link);
              resolve();
            } catch (e) {
              reject(e);
            }
          };
          download(url, filename);
        });
      }

      function afterDownload(error) {
        let mail = true;
        if (error) {
          mail = confirm(lang.generate_mail);
        }
        if (mail) {
          // generate e-mail
          $.generateMail(
            id,
            exam_nr,
            exam,
            lecturer_name,
            date,
            filename,
            user_firstname
          );
        }
      }
    });

    $(".mail_icon").off();
    $(".mail_icon").click(function (e) {
      let id = $(this).attr("name");
      let obj = JSON.parse($(this).attr("data-href").replace(/'/g, '"'));

      let exam_nr = obj.exam_nr;
      let lecturer_name = $("#lecturer_" + id + " .lastname").text();
      let lecturer_name_us = lecturer_name.replace(/ /g, "_");
      let exam = encodeURIComponent($("#exam_" + id).text());
      let date = $("#date_" + id).text();
      let date_reverse = date.split(".").reverse().join(".");
      date_reverse = date_reverse.split(".").join("");
      let user_info = $("#user_info").attr("title");
      let user_firstname = user_info.substring(0, user_info.indexOf(" "));
      let user_lastname = user_info.substring(
        user_info.indexOf(" ") + 1,
        user_info.length
      );
      let user_init = user_firstname.substr(0, 1) + user_lastname.substr(0, 1);
      let filename = date_reverse + "_" + lecturer_name_us + "_Backup_" + user_init + ".mbz";

      // generate e-mail
      $.generateMail(
        id,
        exam_nr,
        exam,
        lecturer_name,
        date,
        filename,
        user_firstname
      );
      e.preventDefault(); //STOP default action
    });
  };
})(jQuery);

(function ($) {
  $.generateMail = function (id, exam_nr, exam, lecturer_name, date, filename, user_firstname) {
    const email = $dp_mail;
    const subject = "Moodle-Prüfungsbackup #" + exam_nr;
    const emailBody =
      "Hallo zusammen%0D%0A%0D%0ADas Backup der Prüfung «" +
      exam +
      "» (Doz.: " +
      lecturer_name +
      ") vom " +
      date +
      " ist gemacht und auf dem Share abgelegt.%0D%0ADateiname: " +
      filename +
      "%0D%0A%0D%0AGruss " +
      user_firstname;
    document.location =
      "mailto:" + email + "?subject=" + subject + "&body=" + emailBody;

    // archive exam
    let $archive = confirm(lang.archive_exam);
    if ($archive) {
      $("#t_a_" + id).trigger("click");
    }
  };
})(jQuery);
