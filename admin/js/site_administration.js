$(document).ready(function () {
	// file explorer
	if ($("#id_explorer").length) {
		let $folder_param = $.urlParam('folder');
		let $tab_param = $.urlParam("tab");
		let $folder = $("#id_explorer").val();
		
		if ($folder_param && $folder_param != $folder) {
			$("#id_explorer").val($folder_param);
		} else if (!$tab_param) {
			$.urlChangeParam("tab", "explorer");
		}
	}

	$("#id_explorer").change(function () {
		let $folder_param = $.urlParam('folder');
		let $folder = $("#id_explorer").val();

		if ($folder_param != $folder) {
			$.urlChangeParam('folder', $("#id_explorer").val());
			$folder = $("#id_explorer").val();
		}

		const $added_rows = 2;
		const $total_rows = 7;

		$.ajax({
			url: "/admin/mod/get_folder_content.php",
			type: "POST",
			async: true,
			data: {
				folder: $folder,
			},
			success: function (data) {
        if (data.content == "empty") {
          $("#explorer_buttons").hide($speed);
          $("#explorer_data").html(lang.no_files);
          $("#explorer_data").show($speed);
          return 0;
        }

        let table = $("<table></table>");
        let thead = $("<thead></thead>");
        thead.addClass("resizable sticky");
        let tbody = $("<tbody></tbody>");
        let $i = -1;
        let fields;
        do {
          $i++;
          fields = Object.keys(data[$i]);
        } while (Object.keys(data[$i]).length < $total_rows);

        let row = $("<tr></tr>");
        let value;

        // empty content
        $("#explorer_data").empty();

        // add selection header
        row.append(
          $(
            '<th style="width: 40px;"><input title="' +
              lang.select_all +
              '" type="checkbox" name="explorer_data" value="0" class="select_all"></th>'
          )
        );
        thead.append(row);

        // get headers
        $(fields).each(function (key, field) {
          if (key < fields.length - $added_rows) {
            row.append($('<th class="data">' + field + "</th>"));
            thead.append(row);
          }
        });

        // get values
        $(data).each(function (i, rowData) {
          let disabled = "";
          let style = "";
          let title = "";

          // disable checkbox if used in database
          if (rowData[lang.date] != "") {
            disabled = "disabled";
            style = "font-style: italic;";
            title = lang.file_linked;
          }

          // add selection row
          row = $(
            '<tr id="ex_row_' +
              i +
              '" style="' +
              style +
              '" title="' +
              title +
              '"></tr>'
          );
          row.append(
            $(
              '<td><input class="ex_checkbox" type="checkbox" name="' +
                i +
                '" value="0" id="select_' +
                i +
                '"' +
                disabled +
                "></td>"
            )
          );
          tbody.append(row);

          $(fields).each(function (j, field) {
            if (j < fields.length - $added_rows) {
              value = rowData[field];
              let path = rowData["filepath"];
              let is_image = /\.(jpg|jpeg|png|webp|avif|gif|svg)$/.test(path);
              let $class = "";
              let $size = "";
              if (is_image) {
                $class = "img_dialog";
                $size = rowData["size"];
              }
              if (j == 0) {
                value =
                  '<a href="' +
                  path +
                  '" target="_blank" class="' +
                  $class +
                  '" data-href="' +
                  $size +
                  '">' +
                  value +
                  "</a>";
              }
              row.append(
                $(
                  '<td id="' +
                    field +
                    "_" +
                    i +
                    '" class="data">' +
                    value +
                    "</td>"
                )
              );
            }
          });
          tbody.append(row);
        });
        table.append(thead);
        table.append(tbody);
        table.addClass("admintable generaltable");
        table.attr("id", "explorer_data_table");

        // append to table and show div
        $("#explorer_data").append(table);
        $("#explorer_data").show($speed);
        $("#explorer_buttons").show($speed);

        $(".select_all").off();
        $(".select_all").change(function () {
          if ($(this).is(":checked")) {
            $(".ex_checkbox").not(":disabled").prop("checked", true);
          } else {
            $(".ex_checkbox").prop("checked", false);
          }
          $(".ex_checkbox").trigger("change");
        });

        $(".ex_checkbox").off();
        $(".ex_checkbox").change(function () {
          if ($(this).is(":checked")) {
            $("#ex_row_" + $(this).attr("name")).addClass("selected");
          } else {
            $("#ex_row_" + $(this).attr("name")).removeClass("selected");
          }
        });

        $(".img_dialog").off();
        $(".img_dialog").click(function (e) {
          let $img = $(this).attr("href");
          let $size = JSON.parse($(this).attr("data-href").replace(/'/g, '"'));
          $("#dialog").html('<img src="' + $img + '" />');
          $("#dialog").dialog({
            width: $size.width + 70,
            height: $size.height + 70,
          });
          e.preventDefault(); //STOP default action
        });

        // resizable headers
        $("#explorer_data th").resizable({
          handles: "e",
          minWidth: 10,
          maxWith: 400,
        });
      },
			error: function () {
				alert(lang.error);
			}
		});
		
		$("#explorer_delete_button").off();
		$("#explorer_delete_button").click(function (e) {
			if ($(".ex_checkbox:checked").length > 0) {
				let selectedRows = [];
				let $headers = $("th.data");
				$("#explorer_data tr.selected").each(function (index) {
					let $cells = $(this).find("td.data");
					selectedRows[index] = {};
					$cells.each(function (cellIndex) {
						selectedRows[index][$($headers[cellIndex]).html()] = $(this).html();
					});
				});
				let selectedJson = {};
				selectedJson.selectedRows = selectedRows;
				
				let $confirm = confirm(lang.delete_files_warning);

				// delete files
				if ($confirm) {
					$.ajax({
						url: "/admin/mod/delete_files.php",
						type: "POST",
						async: true,
						data: {
							rows: selectedJson,
							folder: $folder
						},
						success: function (data) {
							let $no_error = true;
							$(data).each(function (_key, field) {
								if (field.status == 'error') {
									alert(lang.delete_not_possible);
									$no_error = false;
									return false; // breaks
								}
							});
							if ($no_error) {
								location.reload();
							}
						},
						error: function () {
							alert(lang.error);
						}
					});
					e.preventDefault(); //STOP default action
				}
			} else {
				alert(lang.nothing_checked);
			}
		});
	});

	if (window.location.href.indexOf('explorer') > -1) {
		$("#id_explorer").trigger("change");
	}

	// cleanup
	$("#cleanup_date").change(function () {
		$("#stats").remove();
		if ($(this).val()) {
			$("#cleanup_button").show($speed);
			$("#cleanup_button").trigger("click");
		} else {
			$("#cleanup_button").hide($speed);
		}
	});

	$("#cleanup_button").click(function (e) {
		let $date = $("#cleanup_date").val();
		$.ajax({
			url: "/admin/mod/cleanup_db.php",
			type: "POST",
			async: true,
			data: {
				date: $date,
				mode: 'count'
			},
			success: function (data) {
				if (data[data.length - 1].status == "error") {
					alert(lang.error);
				} else {
					$("#stats").remove();
					let stats = $('<div id="stats"></div>');
					stats.append('<p>');
					let empty = true;
					$(data).each(function (key, field) {
						if (key === data.length - 1) {
							return;
						}
						stats.append(field.name + ': ' + field.content + '<br>');
						if (field.content > 0) {
							empty = false;
						}
					});
					stats.append('</p>');
					if (!empty) {
						stats.append('<p><i id="really_cleanup_button" class="icon fa fa-trash fa-fw"></i></p>');
					}
					$("#cleanup_data").append(stats);
					$("#cleanup_data").show($speed);
					$("#really_cleanup_button").click(function (e) {
						let $confirm = confirm(lang.delete_stats_warning);
						if ($confirm) {
							$.ajax({
								url: "/admin/mod/cleanup_db.php",
								type: "POST",
								async: true,
								data: {
									date: $date,
									mode: 'delete'
								},
								success: function (data) {
									if (data[data.length - 1].status == "error") {
										alert(lang.error);
									} else {
										$("#really_cleanup_button").remove();
										$("#stats").append('<b>' + lang.data_deleted + '</b>');
									}
								},
								error: function () {
									alert(lang.error);
								}
							});
							e.preventDefault(); //STOP default action
						}
					});
				}
			},
			error: function () {
				alert(lang.error);
			}
		});
		e.preventDefault(); //STOP default action
	});

	// computers
	$("#computer_template_list").click(function () {
		location.href = "../upload/computer/computer_template.csv";
	});

	$("#cancel_icon_computers").click(function () {
    	location.reload();
  	});

	$("#computer_upload_button").click(function () {
		$("#fileloader_computers").click();
	});

	$("#fileloader_computers").change(function (e) {
		let file = $("#fileloader_computers")[0].files[0];
		if (file != undefined) {
			let formData = new FormData();
			formData.append('file', file);
			$.ajax({
				url: "/admin/mod/get_computers_csv.php",
				type: "POST",
				async: true,
				data: formData,
				processData: false,
				contentType: false,
				success: function (data) {
					if (data.status == "error") {
						alert(lang.error);
					} else {
						$("#hide_unmodified_icon").addClass('fa-eye-slash').removeClass('fa-eye');
						$("#computer_list").html(data.content);
					}
				},
				error: function () {
					alert(lang.error);
				}
			});
		}
		e.preventDefault(); //STOP default action
	});

	$("#computer_save_button").click(function () {
		let file = $("#fileloader_computers")[0].files[0];
		if (file != undefined) {
			let formData = new FormData();
			formData.append('file', file);
			$.ajax({
				url: "/admin/mod/update_computers.php",
				type: "POST",
				async: true,
				data: formData,
				processData: false,
				contentType: false,
				success: function (data) {
					if (data[0].status == "error") {
						alert(lang.error);
					} else {
						$("#hide_unmodified_icon").addClass('fa-eye-slash').removeClass('fa-eye');
						$("#computer_upload_button").hide($speed);
						$("#computer_save_button").hide($speed);
						$("#div_notification").show($speed);
						$("#notification_text").html(lang.success);
						$("#computer_list").html(data[0].content);
						setTimeout(function () {
							$("#div_notification").hide($speed);
						}, 5000);
					}
				},
				error: function () {
					alert(lang.error);
				}
			});
		}
	});

	$("#hide_unmodified_icon").click(function () {
		if ($(this).hasClass('fa-eye-slash')){
			$(this).addClass('fa-eye').removeClass('fa-eye-slash');
			$(".unmodified").hide();
		}else{
			$(this).addClass('fa-eye-slash').removeClass('fa-eye');
			$(".unmodified").show();
		}
	});

	// template types
	if ($("#id_templates").length) {
		let $type_param = $.urlParam('type');
		let $tab_param = $.urlParam("tab");
		let $type = $("#id_templates").val();
		
		if ($type_param && $type_param != $type) {
			$("#id_templates").val($type_param);
		} else if (!$tab_param) {
			$.urlChangeParam("tab", "templates");
		}
	}

	$("#id_templates").change(function () {
		let $type_param = $.urlParam('type');
		let $type = $("#id_templates").val();

		if ($type_param != $type) {
			$.urlChangeParam('type', $("#id_templates").val());
			location.reload();
		}
	});

	if (window.location.href.indexOf('templates') > -1) {
		$("#id_templates").trigger("change");
	}

	// logsheet templates
	$("#template_submit").click(function (e) {
		let $text = $("#add_text").val();
		let $type = 'logsheet';

		if ($text == 0) {
			$(".invalid-feedback").hide();
			$("#text_missing").show();
		} else {
			$.ajax({
				url: "/admin/mod/insert_template.php",
				type: "POST",
				async: true,
				data: {
					text: $text,
					type: $type
				},
				success: function (data) {
					$(".invalid-feedback").hide();
					if (data.status == "error") {
						alert(lang.saving_not_possible);
					} else {
						location.reload();
					}
				},
				error: function () {
					$(".invalid-feedback").hide();
					alert(lang.saving_not_possible);
				}
			});
			e.preventDefault(); //STOP default action
		}
	});

	$(".save_template_icon").click(function (e) {
		let $row = $(this).attr('name');
		let $text = $("#edit_text_" + $row).val();

		if (!$("#edit_text_" + $row).val()) {
			alert(lang.description + " " + lang.missing);
		} else {
			$.ajax({
				url: "/admin/mod/update_template.php",
				type: "POST",
				async: true,
				data: {
					id: $row,
					text: $text,
				},
				success: function (data) {
					if (data.status == "error") {
						alert(lang.saving_not_possible);
					} else {
						location.reload();
					}
				},
				error: function () {
					alert(lang.saving_not_possible);
				}
			});
			e.preventDefault(); //STOP default action
		}
	});

	$(".delete_template_icon").click(function (e) {
		let $template_id = $(this).attr('name');
		let $confirm = confirm(lang.delete_template_warning);

		if ($confirm) {
			$.ajax({
				url: "/admin/mod/delete_template.php",
				type: "POST",
				async: true,
				data: {
					template_id: $template_id
				},
				success: function (data) {
					if (data.status == "error") {
						alert(lang.delete_not_possible);
					} else {
						location.reload();
					}
				},
				error: function () {
					alert(lang.delete_not_possible);
				}
			});
		}
		e.preventDefault(); //STOP default action
	});

	// vho e-mail template
	$("#vho_mail_save_button").click(function(e){
		let $id = $("#vho_mail_template").attr("name");
		let $text = $("#vho_mail_template").val();

		// write to database
		$.ajax({
			url: "/admin/mod/update_vho_mail_template.php",
			type: "POST",
			async: true,
			data: {
				id: $id,
				text: $text
			},
			success: function (data) {
				if (data.status == "error") {
					alert(lang.saving_not_possible);
				} else {
					alert(lang.success);
				}
			},
			error: function () {
				alert(lang.saving_not_possible);
			}
		});
		e.preventDefault(); //STOP default action
	});

	// sharepoint
	if (window.location.href.indexOf('sharepoint') > -1) {
		$("#sharepoint_data").html(lang.loading);

		let $added_rows = 4;
		let $total_rows = 10;
		$.ajax({
			url: "/admin/mod/get_sharepoint_items.php",
			type: "POST",
			async: true,
			beforeSend: function () {
				$('#loader').show();
			},
			success: function (data) {
        if (data == null) {
          $("#sharepoint_data").html(lang.no_data);
          return 0;
        }

        let table = $("<table></table>");
        let thead = $("<thead></thead>");
        thead.addClass("resizable sticky");
        let tbody = $("<tbody></tbody>");
        let $i = -1;
        let fields;
        do {
          $i++;
          fields = Object.keys(data[$i]);
        } while (Object.keys(data[$i]).length < $total_rows);

        let row = $("<tr></tr>");
        let value;
        let date;

        // empty content
        $("#sharepoint_data").hide($speed);
        $("#sharepoint_data").empty();

        // add selection header
        row.append(
          $(
            '<th style="width: 40px;"><input title="' +
              lang.select_all +
              '" type="checkbox" name="sharepoint_data" value="0" class="select_all"></th>'
          )
        );
        thead.append(row);

        // get headers
        $(fields).each(function (key, field) {
          if (key < fields.length - $added_rows) {
            row.append($('<th class="data">' + field + "</th>"));
            thead.append(row);
          }
        });

        // get values
        $(data).each(function (i, rowData) {
          // only if setup is defined
          if (data[i][lang.type] != null) {
            let disabled = "";
            let lect_style = "";
            let vdp_style = "";
            let sys_style = "";
            let style = "";
            let title = "";
            let disable = false;

            // disable checkbox if already in database
            if (rowData["existing"] == "true") {
              title = lang.other_exam_existing;
              disable = true;
            }
            if (rowData["lecturer_exists"] == "false") {
              title = lang.no_people;
              lect_style = "color: red;";
              disable = true;
            }
            if (rowData["Setup"] === "") {
              title = lang.no_setup;
              disable = true;
            }
            if (
              rowData[lang.language] == null ||
              rowData[lang.language] == ""
            ) {
              title = lang.no_language;
              disable = true;
            }
            if (disable) {
              disabled = "disabled";
              style = "font-style: italic;";
            }

            // add selection row
            row = $(
              '<tr id="sp_row_' +
                i +
                '" style="' +
                style +
                '" title="' +
                title +
                '"></tr>'
            );
            row.append(
              $(
                '<td><input class="checkbox_sp" type="checkbox" name="' +
                  i +
                  '" value="0" id="select_' +
                  i +
                  '"' +
                  disabled +
                  "></td>"
              )
            );
            tbody.append(row);

            $(fields).each(function (j, field) {
              if (j < fields.length - $added_rows) {
                value = rowData[field];
                // empty values
                if (typeof value === "undefined" || value === null) {
                  value = "";
                }
                // examinator
                if (j == 2) {
                  if (lect_style != "") {
                    value =
                      '<span style="' + lect_style + '">' + value + "</span";
                  }
                }
                // vdp
                if (j == 3) {
                  if (vdp_style != "") {
                    value =
                      '<span style="' + vdp_style + '">' + value + "</span";
                  }
                }
                // sysadmin
                if (j == 4) {
                  if (sys_style != "") {
                    value =
                      '<span style="' + sys_style + '">' + value + "</span";
                  }
                }
                // format date
                if (j == 5 && value != "") {
                  date = new Date(value);
                  value = $.getFormattedDate(date);
                }
                row.append(
                  $(
                    '<td id="' +
                      field +
                      "_" +
                      i +
                      '" class="data">' +
                      value +
                      "</td>"
                  )
                );
              }
            });
            tbody.append(row);
          }
        });
        table.append(thead);
        table.append(tbody);
        table.addClass("admintable generaltable");
        table.attr("id", "sharepoint_data_table");

        // append to table and show div
        $("#sharepoint_data").append(table);
        $("#sharepoint_data").show($speed);
        $("#sharepoint_buttons").show($speed);

        // resizable headers
        $("#sharepoint_data th").resizable({
          handles: "e",
          minWidth: 10,
          maxWith: 400,
        });

        // save button
        $("#sharepoint_save_button").click(function (e) {
          if ($(".checkbox_sp:checked").length > 0) {
            let selectedRows = [];
            let $headers = $("th.data");
            $("#sharepoint_data tr.selected").each(function (index) {
              let $cells = $(this).find("td.data");
              selectedRows[index] = {};
              $cells.each(function (cellIndex) {
                selectedRows[index][$($headers[cellIndex]).text()] =
                  $(this).html();
              });
            });
            let selectedJson = {};
            selectedJson.selectedRows = selectedRows;

            // write to database
            $.ajax({
              url: "/admin/mod/insert_sharepoint_items.php",
              type: "POST",
              async: true,
              dataType: "json",
              data: selectedJson,
              success: function (data) {
                let $no_error = true;
                if (!$.trim(data)) {
                  alert(lang.error);
                } else {
                  $(data).each(function (_key, field) {
                    $(field.status).each(function (_key_data, status_data) {
                      if (status_data == "error") {
                        alert(lang.error);
                        $no_error = false;
                        return false; // breaks
                      }
                    });
                  });
                  if ($no_error) {
                    alert(lang.success);
                    $("#sharepoint_data tr.selected").attr(
                      "style",
                      "font-style: italic;"
                    );
                    $(".checkbox_sp:checked").prop("disabled", true);
                    $(".checkbox_sp:checked").prop("checked", false);
                  }
                }
              },
              error: function () {
                alert(lang.saving_not_possible);
              },
            });
          } else {
            alert(lang.nothing_checked);
          }
          e.preventDefault(); //STOP default action
        });

        $("#cancel_icon_sp").click(function () {
          location.reload();
        });

        $(".select_all").off();
        $(".select_all").change(function () {
          if ($(this).is(":checked")) {
            $(".checkbox_sp").not(":disabled").prop("checked", true);
          } else {
            $(".checkbox_sp").prop("checked", false);
          }
          $(".checkbox_sp").trigger("change");
        });

        $(".checkbox_sp").off();
        $(".checkbox_sp").change(function () {
          if ($(this).is(":checked")) {
            $("#sp_row_" + $(this).attr("name")).addClass("selected");
          } else {
            $("#sp_row_" + $(this).attr("name")).removeClass("selected");
          }
        });
      },
			complete: function () {
				$('#loader').hide();
			},
			error: function () {
				alert(lang.error);
			}
		});
	}

	// navigation
	$(".sortable").sortable({
    	placeholder: "ui-state-highlight",
  	});
	$(".sortable").disableSelection();

	$("#navigation_save_button").click(function () {
		let idsInOrder = $("#navigation_order").sortable("toArray");
		let $i = 1;
		jQuery.each(idsInOrder, function (index, item) {
			if ($("#" + item).find("ul").length) {
				idsInOrder = idsInOrder.toSpliced(index+$i, 0 , $("#" + item).find("ul").sortable("toArray"));
				$i++;
			}
    	});
		let jsonString = JSON.stringify(idsInOrder);
		$.ajax({
			url: "/admin/mod/update_navigation.php",
			type: "POST",
			async: true,
			dataType: 'json',
			data: {data : jsonString}, 
			success: function (data) {
				if (data.status == "error") {
					alert(lang.saving_not_possible);
				} else {
					location.reload();
				}
			},
			error: function () {
				alert(lang.saving_not_possible);
			}
		});
	});

	$(".nav_checkbox").click(function (e) {
      let $id = $(this).attr("name");
      let $visible = $(this).is(":checked");
      $.ajax({
        url: "/admin/mod/toggle_navigation.php",
        type: "POST",
        async: true,
        data: {
          id: $id,
          visible: $visible,
        },
        success: function (data) {
          location.reload();
        },
        error: function () {
          alert(lang.toggle_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    });

	// VHO themes
	$(".save_theme_icon").click(function (e) {
    let $row = $(this).attr("name");
    let $theme = $("#edit_theme_" + $row).val();
    let $values = $("#edit_values_" + $row).val();

    if (!$("#edit_theme_" + $row).val()) {
      alert(lang.theme + " " + lang.missing);
    } else if (!$("#edit_values_" + $row).val()) {
      alert(lang.values + " " + lang.missing);
    } else {
      $.ajax({
        url: "/admin/mod/update_vho_theme.php",
        type: "POST",
        async: true,
        data: {
          id: $row,
          theme: $theme,
          values: $values,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else if (data.status == "already existing") {
            alert(lang.other_exam_existing);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $("#theme_submit").click(function (e) {
    let $theme = $("#add_theme").val();
    let $values = $("#add_values").val();

    if (!$("#add_theme").val()) {
      $(".invalid-feedback").hide();
      $("#theme_missing").show();
    } else if (!$("#add_values").val()) {
      $(".invalid-feedback").hide();
      $("#values_missing").show();
    } else {
      $.ajax({
        url: "/admin/mod/insert_vho_theme.php",
        type: "POST",
        async: true,
        data: {
          theme: $theme,
          values: $values,
        },
        success: function (data) {
          $(".invalid-feedback").hide();
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else if (data.status == "already existing") {
            alert(lang.other_exam_existing);
          } else {
            location.reload();
          }
        },
        error: function () {
          $(".invalid-feedback").hide();
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $(".delete_theme_icon").click(function (e) {
    let $theme_id = $(this).attr("name");
    let $confirm = confirm(lang.delete_theme_warning);

    if ($confirm) {
      $.ajax({
        url: "/admin/mod/delete_vho_theme.php",
        type: "POST",
        async: true,
        data: {
          theme_id: $theme_id,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.delete_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.delete_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

	// vdi pools
	$(".save_pool_icon").click(function (e) {
    let $row = $(this).attr("name");
	let $pool_lang = $("#edit_pool_description_lang_" + $row).val();
	let $info_text = $("#edit_info_text_" + $row).val();

    if (!$("#edit_pool_description_" + $row).val()) {
      alert(lang.description + " " + lang.missing);
    } else {
      $.ajax({
        url: "/admin/mod/update_vdi_pool.php",
        type: "POST",
        async: true,
        data: {
          id: $row,
		  pool_lang: $pool_lang,
		  info_text: $info_text,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $("#pool_submit").click(function (e) {
    let $pool_desc = $("#add_pool_description").val();
	let $pool_lang = $("#add_pool_description_lang").val();
	let $info_text = $("#add_info_text").val();

    if (!$("#add_pool_description").val()) {
      $(".invalid-feedback").hide();
      $("#pool_description_missing").show();
    } else {
      $.ajax({
        url: "/admin/mod/insert_vdi_pool.php",
        type: "POST",
        async: true,
        data: {
          pool_desc: $pool_desc,
          pool_lang: $pool_lang,
		  info_text: $info_text,
        },
        success: function (data) {
          $(".invalid-feedback").hide();
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else if (data.status == "already existing") {
            alert(lang.other_entry_existing);
          } else {
            location.reload();
          }
        },
        error: function () {
          $(".invalid-feedback").hide();
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $(".delete_pool_icon").click(function (e) {
    let $pool_id = $(this).attr("name");
    let $confirm = confirm(lang.delete_entry_warning);

    if ($confirm) {
      $.ajax({
        url: "/admin/mod/delete_vdi_pool.php",
        type: "POST",
        async: true,
        data: {
          pool_id: $pool_id,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.delete_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.delete_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

	// status
	$(".save_status_icon").click(function (e) {
    let $row = $(this).attr("name");
    let $status_lang = $("#edit_status_lang_" + $row).val();
	let $color = $("#edit_color_" + $row).val();
	let $standard = $("#edit_standard_" + $row).is(":checked");

    if (!$("#edit_status_" + $row).val()) {
      alert(lang.status + " " + lang.missing);
    } else {
      $.ajax({
        url: "/admin/mod/update_exam_status.php",
        type: "POST",
        async: true,
        data: {
          id: $row,
          status_lang: $status_lang,
		  color: $color,
		  standard: $standard
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $("#status_submit").click(function (e) {
	let $status = $("#add_status").val();
    let $status_lang = $("#add_status").val();
	let $color = $("#add_color").val();
	let $standard = $("#add_standard").is(":checked");

    if (!$("#add_status").val()) {
      $(".invalid-feedback").hide();
      $("#status_missing").show();
    } else {
      $.ajax({
        url: "/admin/mod/insert_exam_status.php",
        type: "POST",
        async: true,
        data: {
          status: $status,
          status_lang: $status_lang,
          color: $color,
		  standard: $standard
        },
        success: function (data) {
          $(".invalid-feedback").hide();
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else if (data.status == "already existing") {
            alert(lang.other_status_existing);
          } else {
            location.reload();
          }
        },
        error: function () {
          $(".invalid-feedback").hide();
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $(".delete_status_icon").click(function (e) {
    let $status_id = $(this).attr("name");
    let $confirm = confirm(lang.delete_status_warning);

    if ($confirm) {
      $.ajax({
        url: "/admin/mod/delete_exam_status.php",
        type: "POST",
        async: true,
        data: {
          status_id: $status_id,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.delete_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.delete_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });
});

// general functions
(function ($) {
	$.getFormattedDate = function (date) {
		let year = date.getFullYear();
		let month = (1 + date.getMonth()).toString().padStart(2, '0');
		let day = date.getDate().toString().padStart(2, '0');
		return day + '.' + month + '.' + year;
	}
})(jQuery);
