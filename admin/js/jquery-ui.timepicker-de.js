/* German initialisation for the jQuery UI time picker plugin. */
/* Written by Flavio Steger. */
(function (factory) {
  if (typeof define === "function" && define.amd) {
    // AMD. Register as an anonymous module.
    define(["../widgets/timepicker"], factory);
  } else {
    // Browser globals
    factory(jQuery.timepicker);
  }
})(function (timepicker) {
  timepicker.regional.de = {
    timeOnlyTitle: "Zeit",
    timeText: "Zeit",
    hourText: "Stunde",
    minuteText: "Minute",
    secondText: "Sekunde",
    millisecText: "Millisekunde",
    microsecText: "Mikrosekunde",
    timezoneText: "Time Zone",
    currentText: "Jetzt",
    closeText: "Schließen",
    timeFormat: "HH:mm",
    amNames: ["AM", "A"],
    pmNames: ["PM", "P"],
    isRTL: false,
  };
  timepicker.setDefaults(timepicker.regional.de);

  return timepicker.regional.de;
});
