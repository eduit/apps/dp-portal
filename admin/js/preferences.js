$(document).ready(function () {
	$(".color_value").mouseover(function () {
		$(this).attr("style", "background: " + $(this).text() + ";");
	}).mouseout(function () {
		$(this).removeAttr("style");
	});

	$(".save_preference_icon").click(function (e) {
		let $row = $(this).attr('name');
		let $pref_name = $("#edit_pref_name_" + $row).val();
		let $pref_value = "";
		if ($("#edit_pref_value_" + $row).is(':checkbox')) {
			$pref_value = $("#edit_pref_value_" + $row).is(':checked');
		} else {
			$pref_value = $("#edit_pref_value_" + $row).val();
			if ($pref_value == "true" || $pref_value == "false") {
				let $confirm = confirm(lang.change_to_checkbox);
				if (!$confirm) {
					return;
				}
			}

		}

		if (!$("#edit_pref_name_" + $row).val()) {
			alert(lang.pref_name + " " + lang.missing);
		} else {
			$.ajax({
				url: "/admin/mod/update_preference.php",
				type: "POST",
				async: true,
				data: {
					id: $row,
					name: $pref_name,
					value: $pref_value
				},
				success: function (data) {
					if (data.status == "error") {
						alert(lang.saving_not_possible);
					} else {
						location.reload();
					}
				},
				error: function () {
					alert(lang.saving_not_possible);
				}
			});
			e.preventDefault(); //STOP default action
		}
	});
});
