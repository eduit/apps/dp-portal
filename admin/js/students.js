$(document).ready(function () {
	$(".save_student_icon").click(function (e) {
		let $row = $(this).attr('name');
		let $reg_nr = $("#edit_reg_nr_" + $row).val();
		let $lastname = $("#edit_lastname_" + $row).val();
		let $firstname = $("#edit_firstname_" + $row).val();

		if (!$("#edit_reg_nr_" + $row).val()) {
			alert(lang.reg_nr + " " + lang.missing);
		} else if (!$("#edit_lastname_" + $row).val()) {
			alert(lang.lastname + " " + lang.missing);
		} else if (!$("#edit_firstname_" + $row).val()) {
			alert(lang.firstname + " " + lang.missing);
		} else {
			$.ajax({
				url: "/admin/mod/update_student.php",
				type: "POST",
				async: true,
				data: {
					id: $row,
					reg_nr: $reg_nr,
					lastname: $lastname,
					firstname: $firstname
				},
				success: function (data) {
					if (data.status == "error") {
						alert(lang.delete_not_possible);
					} else {
						location.reload();
					}
				},
				error: function () {
					alert(lang.saving_not_possible);
				}
			});
			e.preventDefault(); //STOP default action
		}
	});

	$(".delete_student_icon").click(function (e) {
		let $student_id = $(this).attr('name');
		let $confirm = confirm(lang.delete_student_warning);

		if ($confirm) {
			$.ajax({
				url: "/admin/mod/delete_student.php",
				type: "POST",
				async: true,
				data: {
					student_id: $student_id
				},
				success: function (data) {
					if (data.status == "error") {
						alert(lang.delete_not_possible);
					} else {
						location.reload();
					}
				},
				error: function () {
					alert(lang.delete_not_possible);
				}
			});
			e.preventDefault(); //STOP default action
		}
	});
});
