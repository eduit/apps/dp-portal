$(document).ready(function () {
  // checkbox styling
  $("#auto_refresh").checkboxradio({
    icon: false,
  });

  $("#all_computers").change(function () {
    let rowCount = $("#all_computers tbody tr:visible").length;
    $("#count").html(rowCount + "/");
  });

  $("#select_rooms").change(function () {
    let $room = $("option:selected", this).attr("value");
    $.urlChangeParam("room", $room);
    location.reload();
  });

  $(".edit_icon").click(function () {
    let $row_id = $(this).attr("data-href");
    let $examUserID = $("#edit_exam_user_" + $row_id).attr("name");
    $.ajax({
      url: "/admin/mod/get_exam_users.php",
      type: "POST",
      async: true,
      data: {
        examUserID: $examUserID,
      },
      success: function (data) {
        let $content = data["content"];
        $("#edit_exam_user_" + $row_id).html($content);
      },
      error: function () {
        alert(lang.error);
      },
    });
  });

  $("#computer_submit").click(function (e) {
    let $computer = $("#add_computer").val();
    let $ip_address = $("#add_ip_address").val();
    let $room_sector = $("#add_sector option:selected").val();
    let $vdi = $("#add_vdi").is(":checked");
    let $exam_user_id = $("#add_exam_user option:selected").val();

    if (!$("#add_ip_address").val()) {
      $(".invalid-feedback").hide();
      $("#ip_address_missing").show();
    } else if (!$("#add_computer").val()) {
      $(".invalid-feedback").hide();
      $("#computer_missing").show();
    } else {
      $.ajax({
        url: "/admin/mod/insert_computer.php",
        type: "POST",
        async: true,
        data: {
          computer: $computer,
          ip_address: $ip_address,
          room_sector: $room_sector,
          vdi: $vdi,
          exam_user_id: $exam_user_id,
        },
        success: function (data) {
          $(".invalid-feedback").hide();
          if (data.status1 == "error") {
            alert(lang.saving_not_possible);
          } else if (data.status1 == "already existing") {
            alert(lang.other_computer_existing);
          } else {
            location.reload();
          }
        },
        error: function () {
          $(".invalid-feedback").hide();
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $(".save_computer_icon").click(function (e) {
    let $row = $(this).attr("name");
    let $ip_address = $("#edit_ip_address_" + $row).val();
    let $computer = $("#edit_computer_" + $row).val();
    let $vdi = $("#edit_vdi_" + $row).is(":checked");
    let $exam_user_id = $("#edit_exam_user_" + $row + " option:selected").val();

    if (!$("#edit_ip_address_" + $row).val()) {
      alert(lang.current_ip + " " + lang.missing);
    } else if (!$("#edit_computer_" + $row).val()) {
      alert(lang.current_hostname + " " + lang.missing);
    } else {
      $.ajax({
        url: "/admin/mod/update_computer.php",
        type: "POST",
        async: true,
        data: {
          id: $row,
          ip_address: $ip_address,
          computer: $computer,
          vdi: $vdi,
          exam_user_id: $exam_user_id,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.saving_not_possible);
          } else if (data.status == "already existing") {
            alert(lang.other_computer_existing);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.saving_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $(".delete_computer_icon").click(function (e) {
    let $computer_id = $(this).attr("name");
    let $confirm = confirm(lang.delete_computer_warning);

    if ($confirm) {
      $.ajax({
        url: "/admin/mod/delete_computer.php",
        type: "POST",
        async: true,
        data: {
          computer_id: $computer_id,
        },
        success: function (data) {
          if (data.status == "error") {
            alert(lang.delete_not_possible);
          } else {
            location.reload();
          }
        },
        error: function () {
          alert(lang.delete_not_possible);
        },
      });
      e.preventDefault(); //STOP default action
    }
  });

  $("#auto_refresh_label").tooltip();

  $("#auto_refresh").change(function (e) {
    let $room_id = $(this).attr("name");
    let $refresh = $("#auto_refresh").is(":checked");

    $.ajax({
      url: "/admin/mod/toggle_refresh.php",
      type: "POST",
      async: true,
      data: {
        room_id: $room_id,
        refresh: $refresh,
      },
      success: function (data) {
        if (data.status == "error") {
          alert(lang.saving_not_possible);
        }
      },
      error: function () {
        alert(lang.saving_not_possible);
      },
    });
    e.preventDefault(); //STOP default action
  });
});
