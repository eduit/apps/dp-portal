const $speed = "slow";
let start = false;
let timer;
let $dp_mail;
let displayMode;

$(document).ready(function () {
  //***** general *****
  let url_param = $(location).attr("href").split("?");
  let param = url_param[1];

  // check session
  $.checkSession();

  // sticky header
  $(window).on("scroll", function () {
    let backgroundColor = $(".card").css("background-color");
    $(".sticky").css({
      top: $(window).scrollTop() - 130 + "px",
    });
    $(".sticky th").css({
      background: backgroundColor,
    });
  });

  // navigation
  if (param?.includes("nav")) {
    let $nav = $.urlParam("nav");
    if ($nav == "closed") {
      $.fn.toggleNav("closed");
    }
  }

  // toggle navigation
  $("#nav-drawer-button").click(function () {
    if ($("#nav-drawer").hasClass("closed")) {
      $.fn.toggleNav("open");
    } else {
      $.fn.toggleNav("closed");
    }
  });

  // toggle display mode
  $("#toggle_mode").click(function () {
    let $displayMode = $(this).attr("name");
    $.ajax({
      url: "/admin/mod/toggle_mode.php",
      type: "POST",
      data: {
        displayMode: $displayMode,
      },
      success: function () {
        location.reload();
      },
    });
  });
  displayMode = $("#toggle_mode").attr("data-href");

  // get dp_mail from preferences
  $.ajax({
    url: "/admin/mod/get_preference.php",
    type: "POST",
    async: true,
    data: {
      pref: "dp_mail",
    },
    success: function (data) {
      $dp_mail = data.value;
    },
  });

  // load general functions
  $.generalFunctions();
});

// grab function
$(document).on("mousedown mouseup", ".grab, .grabbing", function (event) {
  $(this).toggleClass("grab").toggleClass("grabbing");
});

// Save function
$(document).on("keydown", function (e) {
  // CTRL+S
  if ($(".fa-save").is(":visible")) {
    if (e.ctrlKey && e.which === 83) {
      e.preventDefault();
      $(".fa-save:visible").click();
    }
  }
});

//***** functions *****
(function ($) {
  $.urlParam = function (name) {
    let results = new RegExp("[?&]" + name + "=([^&#]*)").exec(
      window.location.href
    );
    if (results == null) {
      return null;
    } else {
      return results[1] || "";
    }
  };
})(jQuery);

(function ($) {
  $.urlChangeParam = function (name, value) {
    let queryParams = new URLSearchParams(window.location.search);
    queryParams.set(name, value);
    history.replaceState(null, null, "?" + queryParams.toString());

    $(".lang-button").each(function () {
      let url = $(location).attr("pathname") + $(location).attr("search");
      $(this).attr("href", url + "&" + "lang" + "=" + $(this).attr("name"));
    });
  };
})(jQuery);

// check session
(function ($) {
  $.checkSession = function () {
    let $sessionStatus;
    $.ajax({
      url: "/admin/login/check_session.php",
      type: "POST",
      async: true,
      success: function (data) {
        if (typeof data !== "undefined") {
          $sessionStatus = data.status;
          if ($sessionStatus) {
            start = true;
            if (!timer) {
              timer = setInterval(function () {
                $sessionStatus = $.checkSession();
              }, 20000);
            }
          }
          if (!$sessionStatus && start) {
            clearInterval(timer);
            alert(lang.session_expired);
            location.reload();
          }
        }
      },
    });
    return $sessionStatus;
  };
})(jQuery);

(function ($) {
  $.generalFunctions = function () {
    // tooltip
    $("table").tooltip();
    $(".icon").tooltip();

    // resizable headers
    $(".resizable th").resizable({
      handles: "e",
      minWidth: 10,
      maxWith: 400,
    });

    // archived entries
    $("#archived").click(function () {
      let $archived = $("#archived").is(":checked");
      $.urlChangeParam("archived", $archived);
      location.reload();
    });

    // disabled entries
    $("#disabled").click(function () {
      let $disabled = $("#disabled").is(":checked");
      $.urlChangeParam("disabled", $disabled);
      location.reload();
    });

    // progressbar
    $("#progressbar").progressbar({
      value: false,
    });

    // RTE
    $(".rte").richText({
      bold: false,
      italic: false,
      underline: false,
      leftAlign: false,
      centerAlign: false,
      rightAlign: false,
      justify: false,
      ol: false,
      ul: false,
      heading: false,
      fonts: false,
      fontColor: false,
      fontSize: false,
      imageUpload: false,
      fileUpload: false,
      videoEmbed: false,
      backgroundColor: false,
      urls: false,
      table: false,
      removeStyles: false,
      height: 500,
    });

    // edit icon
    $(".edit_icon").off();
    $(".edit_icon").click(function () {
      $(".edit_rows").hide($speed);
      $("#" + $(this).attr("name")).show($speed);
    });

    // cancel icon
    $(".cancel_icon").off();
    $(".cancel_icon").click(function () {
      $(".edit_rows").hide($speed);
    });

    // datepicker
    $(".datepicker").off();
    $(".datepicker").datepicker({
      dateFormat: "yy-mm-dd",
      firstDay: 1,
    });

    // datepicker year
    $(".datepicker_year").off();
    $(".datepicker_year").datepicker({
      dateFormat: "yy-mm-dd",
      firstDay: 1,
      changeYear: true,
    });

    // datetimepicker
    $(".datetimepicker").off();
    $(".datetimepicker").datetimepicker();
  };
})(jQuery);

$.fn.toggleNav = function ($mode) {
  if ($mode == "open") {
    $("#nav-drawer").removeClass("closed");
    $("body").addClass("drawer-open-left");
    $("#nav-drawer-button").attr("aria-expanded", "true");
  } else {
    $("#nav-drawer").addClass("closed");
    $("body").removeClass("drawer-open-left");
    $("#nav-drawer-button").attr("aria-expanded", "false");
  }
  $.urlChangeParam("nav", $mode);
};

(function ($) {
  "use strict";
  // Allows you to trigger a callback when the user presses a certain sequence
  // of keys.
  let plugin = {
    // ## Default options
    options: {
      sequence: [38, 38, 40, 40, 37, 39, 37, 39, 66, 65], // Konami Code
    },
    setup: function (elem, options) {
      $.extend(this.options, options);
      this.index = 0;
      this.elem = elem;
      $(this.elem).on("keydown.easteregg", $.proxy(this.onKeyDown, this));
      return this;
    },
    teardown: function () {
      $(this.elem).off(".easteregg");
      return this;
    },
    onKeyDown: function (evt) {
      let options = this.options;
      let sequence = options.sequence;
      let callback = options.callback;
      if (evt.which === sequence[this.index]) {
        // If the user presses the right key, advance the index...
        this.index += 1;
        if (this.index === sequence.length) {
          // ... until we reach the end of the sequence.
          this.index = 0;
          if (callback != null) {
            callback.call(this.elem);
          }
        }
      } else {
        // If the user presses the wrong key, reset the index.
        this.index = 0;
      }
    },
  };
  // ## Object.create polyfill
  // Covers the main use case, which is creating a new object for which the
  // prototype has been chosen, but doesn't take the second argument into
  // account.
  if (typeof Object.create !== "function") {
    Object.create = function (object) {
      let F = function () {
        /* this function is empty */
      };
      F.prototype = object;
      return new F();
    };
  }
  // ## Pluginize
  // Creates a jQuery plugin based on a defined object.
  $.pluginize = function (name, object) {
    $.fn[name] = function (options) {
      return this.each(function () {
        if (!$(this).data(name)) {
          plugin = Object.create(object).setup(this, options);
          $(this).data(name, plugin);
        }
      });
    };
    return object;
  };
  $.pluginize("easteregg", plugin);
})(jQuery);

function escapeHtml(text) {
  let map = {
    "&": "&amp;",
    "<": "&lt;",
    ">": "&gt;",
    '"': "&quot;",
    "'": "&#039;",
  };

  return text.replace(/[&<>"']/g, function (m) {
    return map[m];
  });
}

$(function () {
  $(document).easteregg({
    sequence: [38, 38, 40, 40, 37, 39, 37, 39, 66, 65],
    callback: function () {
      alert("Are you crazy?");
      $(
        "#page-header-color, #page-header-color-left, #page-header-color-right, .drawer-open-left #page-header-color-nav, #page-site-index #page-header-color-block, .path-mod-forum:target ~ .forumpost::before"
      ).toggleClass("rb_wrapper");
      $("body").toggleClass("easteregg");
      $("#page-header-color, #page, #nav-drawer").toggleClass("tilt");
    },
  });
});
