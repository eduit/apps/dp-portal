<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

session_start();
$loggedin = $_SESSION["loggedin"] ?? 0;
$userVho = $_SESSION["user_vho"] ?? 0;

if ($loggedin || $userVho) {
    //session not expired
    $response_array['status'] = true;
} else {
    //session expired
    $response_array['status'] = false;
}

echo json_encode($response_array);
