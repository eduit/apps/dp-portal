<?php

header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin'] || $_SESSION['user_vho']) {
	$csrf_token = $_GET['csrf_token'];
	if (!empty($csrf_token)) {
		if (hash_equals($_SESSION['csrf_token'], $csrf_token)) {
			$username = urlencode($_GET['username']);

			foreach ($_SESSION as $key => $value) {
				unset($_SESSION[$key]);
			}

			// delete old session data
			session_destroy();
			session_start();
			session_regenerate_id();

			header("Location: ../index.php");
		} else {
			echo "Invalid token";
		}
	} else {
		echo "Logout error";
	}
}
