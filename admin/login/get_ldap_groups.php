<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";

session_start();

$csrf_token = $_POST['csrf_token'] ?? '';

if (!empty($csrf_token) && hash_equals($_SESSION['csrf_token'], $csrf_token)) {
    echo json_encode($login_groups);
}
