<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

$username = $_POST['username'] ?? '';
$password = $_POST['password'] ?? '';
$csrf_token = $_POST['csrf_token'] ?? '';
$user_lastname = $_POST['user_lastname'] ?? '';
$user_firstname = $_POST['user_firstname'] ?? '';
$user_mail = $_POST['user_mail'] ?? '';
$login_mode = $_SESSION['login_mode'] ?? '';

$loggedin = 0;
$user_admin = 0;
$userVho = 0;

// set login mode
foreach ($login_rights as $login_right) {
    if ($login_mode == $login_right[0]) {
        $loggedin = $login_right[1];
        $user_admin = $login_right[2];
        $userVho = $login_right[3];
    }
}

// get IP address
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip_address = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip_address = $_SERVER['REMOTE_ADDR'];
}

if (!empty($csrf_token) && hash_equals($_SESSION['csrf_token'], $csrf_token)) {
    // get login attempts and disabled time from preferences
    $qry = $SELECT_pref . " WHERE pref_name = 'login_attempts'";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $login_attempts = $row['pref_value'];

    $qry = $SELECT_pref . " WHERE pref_name = 'login_disabled'";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $login_disabled = $row['pref_value'];

    //LDAP
    $ldap_user_dn = "cn=$username,ou=users,ou=nethz,ou=id,ou=auth,o=ethz,c=ch";

    if ($connect = ldap_connect($ldap_address, $ldap_port)) {
        //connected
        ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);
        if (isset($_POST['username'])) {
            if ($bind = @ldap_bind($connect, $ldap_user_dn, $password)) {
                // delete old session data
                session_destroy();

                // regenerate sesssion and set attributes
                session_start();
                session_regenerate_id();
                $_SESSION['csrf_token'] = uniqid('', true);
                $_SESSION['username'] = $username;
                $_SESSION['user_lastname'] = $user_lastname;
                $_SESSION['user_firstname'] = $user_firstname;
                $_SESSION['user_mail'] = $user_mail;
                $_SESSION['loggedin'] = $loggedin;
                $_SESSION['user_admin'] = $user_admin;
                $_SESSION['user_vho'] = $userVho;
                $response_array['login_mode'] = $login_mode;
                $response_array['status'] = 'success';
                $login = true;
            } else {
                foreach ($_SESSION as $key => $value) {
                    unset($_SESSION[$key]);
                }
                $response_array['status'] = "error";
                $response_array['error'] = "login";
                $login = false;
            }

            // read and write login attempt to DB
            $qry = $SELECT_login_attempt . "'$ip_address'";
            $res = pg_query($con, $qry);
            $row = pg_fetch_assoc($res);
            $num = pg_num_rows($res);
            if ($num > 0) {
                $attempts = $row['attempts'];
                $last_login = $row['last_login'];
            } else {
                $attempts = 0;
                $last_login = 0;
            }
            $last_time = strtotime($last_login);
            $actual_time = time() - $login_disabled * 60;

            if ($last_time < $actual_time || $login) {
                $attempts = 0;
            } else {
                if ($attempts == $login_attempts) {
                    $attempts = 1;
                } else {
                    $attempts++;
                }
            }

            if ($num > 0) {
                $qry = $UPDATE_login_attempt . " SET ip_address='$ip_address', attempts=$attempts, last_login=NOW()";
                $response_array['action'] = 'update';
            } else {
                $qry = $INSERT_login_attempt . " VALUES ('$ip_address', 1)";
                $response_array['action'] = 'insert';
            }
            if (pg_query($con, $qry)) {
                $response_array['action_status'] = 'success';
            } else {
                $response_array['action_status'] = 'error';
            }
            pg_close($con);
        }
        ldap_close($connect);
    } else {
        $response_array['status'] = "error";
        $response_array['error'] = "connection";
    }
} else {
    $response_array['status'] = 'error';
    $response_array['error'] = "general";
}

echo json_encode($response_array);
