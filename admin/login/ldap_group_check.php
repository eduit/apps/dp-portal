<?php

    header("Content-type: application/json");
    header("Cache-Control: no-cache, no-store, must-revalidate");
    header("Pragma: no-cache");
    header("Expires: 0");


    include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";

    session_start();

    $username = $_POST['username'];
    $group = $_POST['group'];

    // get IP address
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
        $ip_address = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip_address = $_SERVER['REMOTE_ADDR'];
    }

    if ($connect = ldap_connect($ldap_address, $ldap_port)) {
        // connected
        ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);

        if ($bind = ldap_bind($connect, $ldap_dn, $ldap_password)) {
            // logged in
            $group_dn = "cn=$group,ou=custom,ou=groups,ou=nethz,ou=id,ou=auth,o=ethz,c=ch";
            $filter = "(&(objectClass=posixGroup)(memberUid=$username))";
            $justthese = array("ou", "sn", "givenName");
            $sr = ldap_search($connect, $ldap_base_dn, $filter, $justthese);
            $info = ldap_get_entries($connect, $sr);

            // get user info
            $filter = "(|(cn=$username))";
            $justthese = array("ou", "sn", "givenname", "mail");
            $sr = ldap_search($connect, $ldap_base_dn, $filter, $justthese);
            $user_info = ldap_get_entries($connect, $sr);
            $response_array['user_lastname'] = $user_info[0]['sn'][0] ?? '';
            $response_array['user_firstname'] = $user_info[0]['givenname'][0] ?? '';
            $response_array['user_mail'] = $user_info[0]['mail'][0] ?? '';

            if (inArrayR($group_dn, $info)) {
                $response_array['status'] = "true";
                foreach ($login_groups as $login_group) {
                    if ($group == $login_group) {
                        $_SESSION['login_mode'] = $login_group;
                    }
                }
            } else {
                $response_array['status'] = "false";
            }
        } else {
            $response_array['status'] = "error";
            $response_array['error'] = "login";
        }
    } else {
        $response_array['status'] = "error";
        $response_array['error'] = "connection";
    }
    ldap_close($connect);
    echo json_encode($response_array);

    function inArrayR($needle, $haystack, $strict = true)
    {
        foreach ($haystack as $item) {
            if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && inArrayR($needle, $item, $strict))) {
                return true;
            }
        }

        return false;
    }
