<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

$loggedin = $_SESSION['loggedin'] ?? false;
$vho =  $_SESSION['user_vho'] ?? false;

if ($loggedin || $vho) {
    $preference = htmlspecialchars($_POST["pref"]);

    // get variable
    $qry = $SELECT_pref . " WHERE pref_name = '$preference'";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $response_array['preference'] = $preference;
    $response_array['value'] = $row['pref_value'];

    pg_close($con);
    echo json_encode($response_array);
}
