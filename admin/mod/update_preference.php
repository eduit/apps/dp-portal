<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$id = htmlspecialchars($_POST["id"]);
	$name = htmlspecialchars($_POST["name"]);
	$value = htmlspecialchars($_POST["value"]);

	$value = str_replace("%23", "#", $value);

	$qry = $UPDATE_preference . " SET pref_value='$value' WHERE pref_id=$id";

	if (pg_query($con, $qry)) {
		$response_array['status'] = 'success';
	} else {
		$response_array['status'] = 'error';
	}

	pg_close($con);
	echo json_encode($response_array);
}
