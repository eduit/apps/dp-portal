<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $mode = htmlspecialchars($_POST["mode"]);
    $qry = $SELECT_people . " WHERE (people_username = '') IS NOT FALSE";
    $res1 = pg_query($con, $qry);
    $num1 = pg_num_rows($res1);
    $j = 0;
    $response_array['status'] = 'success';

    if ($connect = ldap_connect($ldap_address, $ldap_port)) {
        // connected
        ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);

        if (ldap_bind($connect, $ldap_dn, $ldap_password)) {
            for ($i = 0; $i < $num1; $i++) {
                $row1 = pg_fetch_array($res1);
                $people_id = $row1['id'];
                $lastname = $response_array[$i]['lastname'] = $row1['lastname'];
                $firstname = $response_array[$i]['firstname'] = $row1['firstname'];
                
                // get user info
                $filter = "(&(sn=$lastname)(givenname=$firstname))";
                $justthese = array("cn");
                $sr = ldap_search($connect, $ldap_base_dn, $filter, $justthese);
                $user_info = ldap_get_entries($connect, $sr);
                $username = $response_array[$i]['username'] = $user_info[0]['cn'][0] ?? '';
                if ($username) {
                    if ($mode == "write") {
                        $qry = $UPDATE_people . " SET people_username = '$username' WHERE people_id = '$people_id'";
                        if (pg_query($con, $qry)) {
                            $response_array[$i]['status'] = 'success';
                        } else {
                            $response_array[$i]['status'] = 'error';
                            $response_array['status'] = 'error';
                        }
                    } else {
                        $j++;
                    }
                } else {
                    $response_array[$i]['status'] = 'not found';
                }
            }
            if ($mode == "list") {
                $response_array['number'] = $j;
            }
        }
    }

    ldap_close($connect);
    pg_close($con);
    echo json_encode($response_array);
}
