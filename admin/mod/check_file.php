<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";

session_start();

if ($_SESSION['loggedin'] || $_SESSION['user_vho']) {
	$relative_path = "../../";
	$dir = $_POST['dir'];
	list($name, $ext) = explode('.', basename($_FILES["file"]["name"]));
	$newFilename = $_POST['new_filename'];
	if ($newFilename != "") {
		$target_file = $relative_path . $dir . $newFilename . '.' . $ext;
	} else {
		$target_file = $relative_path . $dir . $name . '.' . $ext;
	}
	$uploadOk = 1;
	$FileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
	$response_array['file'] = $target_file;
	$response_array['filetype'] = $FileType;

	// Check if file already exists
	if (file_exists($target_file)) {
		$response_array['status'] = 'already existing';
	} else {
		$response_array['status'] = 'not existing';
	}
	echo json_encode($response_array);
}
