<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {

	// Include language file
	if (isset($_SESSION['lang'])) {
		$lang = $_SESSION['lang'];
	} else {
		// default
		$qry = $SELECT_lang_default;
		$res = pg_query($con, $qry);
		$row = pg_fetch_assoc($res);
		$lang = $row['lang'];
	}
	include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

	$exam_id = $_POST['exam_id'] ?? 0;
	$roomID = $_POST['room_id'] ?? 0;
	$content = '';
	$info = '';

	if (isset($_POST['mode']) && $_POST['mode'] == "computers") {
		$mode = $_POST['mode'];
		$response_array['content'] = selectComputers(0, $roomID, $exam_id);

		pg_close($con);

		echo json_encode($response_array);
		return;
	}

	$qry = $SELECT_exam_students . " AND tbl_exam_student.form = 'false' AND tbl_exam.exam_id = $exam_id ORDER BY tbl_exam_student.created_at";
	$res = pg_query($con, $qry);
	$num = pg_num_rows($res);

	if ($num != 0) {
		$empty_row = "<tr id=\"empty_row\"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>";
		$content = $empty_row;
	}

	for ($i = 0; $i < $num; $i++) {
		$row = pg_fetch_array($res);
		$exam_student_id = $row['exam_student_id'] ?? 0;
		$student_id = $row['student_id'] ?? 0;
		$lastname = $row['lastname'] ?? '';
		$firstname = $row['firstname'] ?? '';
		$registration = $row['registration'] ?? '';
		$roomID = $row['room_id'] ?? 0;
		$room = $row['room_name'] ?? '';
		$roomDN = $row['room_display_name'] ?? '';
		$us_keyboard = $row['us_keyboard'] ?? 'f';
		$group = $row['group'] ?? '';
		$remark = $row['remark'] ?? '';
		$url = $row['url'] ?? '';
		$computerID = $row['computer_id'] ?? 0;
		$computer = $row['computer'] ?? '';
		$ip_address = $row['ip_address'] ?? '';
		$link = $row['exam_link'] ?? '';
		$active = $row['active'] ?? 'f';
		$accepted = $row['accepted'] ?? 'f';
		$exam_type = $row['exam_type'] ?? '';
		$status = '';
		$info = '';
		$disabled = '';

		if ($us_keyboard == "f") {
			$us_checked = "";
			$us_keyboard = "<i class=\"fa fa-square-o fa-fw false_icon\" title=\"" . _FALSE . "\" aria-label=\"false\" name=\"us_keyboard_" . $exam_student_id . "\"></i>";
		} else {
			$us_checked = "checked";
			$us_keyboard = "<i class=\"fa fa-check-square-o fa-fw true_icon\" title=\"" . _TRUE . "\" aria-label=\"true\" name=\"us_keyboard_" . $exam_student_id . "\"></i>";
		}

		if ($active == "f") {
			$checked = "";
			$status = "";
			$info = "* " . _DISABLED;
			$activation = "<i class=\"fa fa-square-o fa-fw false_icon\" title=\"" . _FALSE . "\" aria-label=\"false\" name=\"allocation_" . $exam_student_id . "\"></i>";
		} else {
			$checked = "checked";
			$activation = "<i class=\"fa fa-check-square-o fa-fw true_icon\" title=\"" . _TRUE . "\" aria-label=\"true\" name=\"allocation_" . $exam_student_id . "\"></i>";
		}

		//show display name if set
		if ($roomDN) {
			$room = $roomDN;
		}

		$edit_buttons = "<i class=\"icon fa fa-edit fa-fw edit_allocation_icon\" title=\"" . _EDIT . "\" aria-label=\"" . _EDIT . "\" name=\"edit_row_" . $exam_student_id . "\" data-href=\"" . $exam_student_id . "\"></i><i class=\"icon fa fa-trash fa-fw delete_participant_icon\" title=\"\" aria-label=\"\" name=\"$exam_student_id\"></i>";
		$accepted_button = "<i class=\"fa fa-lock fa-fw ban_icon\" title=\"" . _ENROLLED . "\" aria-label=\"" . _ENROLLED . "\" name=\"edit_enrolled_" . $exam_student_id . "\"></i>";
		$student = "<tr id=\"$exam_student_id\"><td>$status$lastname</td><td>$firstname</td><td>$registration</td><td>$room</td><td>$us_keyboard</td><td>$group</td><td>$remark</td><td>$url</td><td><span class=\"computer\" title=\"$ip_address\">$computer</span></td><td id=\"allocation_" . $exam_student_id . "\">" . $activation . "</td><td class=\"edit_row\">";
		if ($accepted == "f") {
			$student = $student . $edit_buttons;
		} else {
			$student = $student . $accepted_button;
		}
		$student = $student . "</td></tr>";
		$edit = "<tr id=\"edit_row_$exam_student_id\" class=\"edit_rows hidden\"><td><input type=\"text\" class=\"form-control edit_lastname\" name=\"lastname_" . $exam_student_id . "\" id=\"edit_lastname_" . $exam_student_id . "\" required=\"required\" value=\"$lastname\" readonly></td><td><input type=\"text\" class=\"form-control edit_firstname\" name=\"firstname_" . $exam_student_id . "\" id=\"edit_firstname_" . $exam_student_id . "\" required=\"required\" value=\"$firstname\" readonly></td><td><input type=\"text\" class=\"form-control edit_registration\" name=\"registration_" . $exam_student_id . "\" id=\"edit_registration_" . $exam_student_id . "\" required=\"required\" value=\"$registration\" readonly></td><td><select class=\"select custom-select form-control select-room\" $disabled name=\"" . $exam_student_id . "\" id=\"edit_room_" . $exam_student_id . "\" required=\"required\" value=\"\">" . selectRooms($roomID) . "</select></td><td><input type=\"checkbox\" name=\"us_keyboard_" . $exam_student_id . "\" id=\"edit_us_keyboard_" . $exam_student_id . "\" " . $us_checked . "></td><td><input type=\"text\" class=\"form-control edit_group\" name=\"group_" . $exam_student_id . "\" id=\"edit_group_" . $exam_student_id . "\" required=\"required\" value=\"$group\"></td><td><input type=\"text\" class=\"form-control edit_remark\" name=\"remark_" . $exam_student_id . "\" id=\"edit_remark_" . $exam_student_id . "\" required=\"required\" value=\"$remark\"></td><td><input type=\"text\" class=\"form-control edit_url\" name=\"url_" . $exam_student_id . "\" id=\"edit_url_" . $exam_student_id . "\" required=\"required\" value=\"$url\"></td><td><select class=\"select custom-select form-control select-computer\" $disabled name=\"computer_" . $exam_student_id . "\" id=\"edit_computer_" . $exam_student_id . "\" required=\"required\" value=\"\">" . selectComputers($computerID, $roomID, $exam_id) . "</select></td><td><input type=\"checkbox\" name=\"allocation_" . $exam_student_id . "\" id=\"edit_allocation_" . $exam_student_id . "\" " . $checked . "></td><td><i class=\"icon fa fa-save fa-fw save_participant_icon\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"" . $exam_student_id . "\"></i><i class=\"icon fa fa-times fa-fw cancel_icon\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"" . $exam_student_id . "\"></i></td></tr>";
		$content = $content . $student . $edit;
	}

	$response_array['content'] = $content;
	$response_array['info'] = $info;

	pg_close($con);

	echo json_encode($response_array);
}

function selectRooms($selected)
{
	include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
	include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

	$qry = $SELECT_rooms . " ORDER BY room_name";
	$res1 = pg_query($con, $qry);
	$num1 = pg_num_rows($res1);
	$rooms = '';

	for ($i = 0; $i < $num1; $i++) {
		$row1 = pg_fetch_array($res1);
		$roomID = $row1['room_id'];
		$room = $row1['room_name'];
		$roomDN = $row1['room_display_name'];

		//show display name if set
		if ($roomDN) {
			$room = $roomDN;
		}

		$rooms = $rooms . "<option value=\"" . $roomID . "\" title=\"" . $room . "\"";
		if ($roomID == $selected) {
			$rooms = $rooms . " selected";
		}
		$rooms = $rooms . ">" . $room . "</option>\n";
	}
	return $rooms;
}

function selectComputers($selected, $room, $exam)
{
	include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
	include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

	$qry = $SELECT_computers_listed . " AND tbl_room.room_id = $room AND tbl_computer.computer_id NOT IN(" . $SELECT_exam_computers . "AND tbl_exam.exam_id = $exam AND tbl_computer.computer_id != $selected) ORDER BY computer";
	$res2 = pg_query($con, $qry);
	$num2 = pg_num_rows($res2);

	if ($num2 == 0) {
		$computers = "<option value=\"0\" title=\"" . _NO_COMPUTERS . "\">" . _NO_COMPUTERS . "</option>\n";
	} else {
		$computers = "<option value=\"0\" title=\"" . _NO_COMPUTER . "\">" . _NO_COMPUTER . "</option>\n";
		for ($i = 0; $i < $num2; $i++) {
			$row2 = pg_fetch_array($res2);
			$computerID = $row2['computer_id'];
			$computer = $row2['computer'];

			$computers = $computers . "<option value=\"" . $computerID . "\" title=\"" . $computer . "\"";
			if ($computerID == $selected) {
				$computers = $computers . " selected";
			}
			$computers = $computers . ">" . $computer . "</option>\n";
		}
	}
	return $computers;
}
