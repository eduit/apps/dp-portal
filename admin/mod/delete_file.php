<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$file = $_POST['file'];

	$filepath = $_SERVER['DOCUMENT_ROOT'] . "/" . $csv_dir . $file;

	if (!unlink($filepath)) {
		$response_array['status'] = 'error';
	} else {
		$response_array['status'] = 'success';
	}

	echo json_encode($response_array);
}
