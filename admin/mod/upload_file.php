<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";

session_start();

if ($_SESSION['loggedin'] || $_SESSION['user_vho']) {
	$relative_path = "../../";
	$dir = $_POST['dir'];
	$rename = $_POST['rename'];
	$allowed_filetype = $_POST['filetype'];
	$newFilename = $_POST['new_filename'];
	$link = basename($_FILES["file"]["name"]);
	$target_file = $relative_path . $dir . basename($_FILES["file"]["name"]);
	$uploadOk = 1;
	$fileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
	$response_array['file'] = $target_file;
	$response_array['filetype'] = $fileType;
	$response_array['status1'] = 'success';
	$response_array['status2'] = 'success';

	// Overwrite if file already exists
	list($name, $ext) = explode('.', basename($_FILES["file"]["name"]));
	if (file_exists($target_file) && $newFilename == "") {
		if ($rename == "true") {
			$increment = 0;
			while (file_exists($target_file)) {
				$increment++;
				$renamed = $name . $increment . '.' . $ext;
				$target_file = $relative_path . $dir . $renamed;
			}
			$link = $renamed;
			$response_array['file_renamed'] = $target_file;
			$response_array['status1'] = 'renamed';
		} else {
			unlink($target_file);
			$response_array['status1'] = 'overwritten';
		}
		$response_array['link'] = $link;
	} elseif ($newFilename != "") {
		$renamed = $newFilename . '.' . $ext;
		$target_file = $relative_path . $dir . $renamed;
		$link = $renamed;
		$response_array['file_renamed'] = $target_file;
		$response_array['status1'] = 'renamed';
		if (file_exists($target_file)) {
			unlink($target_file);
			$response_array['status1'] = 'overwritten';
		}
	} else {
		$uploadOk = 1;
	}
	// Check file size
	if ($_FILES["file"]["size"] > 500000) {
		$response_array['status1'] = 'too large';
		$uploadOk = 0;
	}
	// Allow certain file formats
	if ($fileType != $allowed_filetype && $allowed_filetype != "*") {
		$response_array['status1'] = 'wrong file';
		$uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
		$response_array['status2'] = 'error';
	// if everything is ok, try to upload file
	} else {
		$response_array['link'] = $link;
		if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
			$response_array['status2'] = 'success';
		} else {
			$response_array['status2'] = 'error';
		}
	}

	echo json_encode($response_array);
}
