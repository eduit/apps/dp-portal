<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";

session_start();

if ($_SESSION['loggedin']) {
    $id = $_POST["id"];
    $moodle_url = $_POST["moodle_url"];
    $type = $_POST["type"];

    $types = array('quiz', 'course');

    $backup = true;

    if (!in_array($type, $types)) {
        $response_array['errorcode'] = 1;
        $response_array['exception'] = "wrong type";
        $backup = false;
    }

    if ($backup) {
        require_once $_SERVER["DOCUMENT_ROOT"] . "/inc/moodle/MoodleRest.php";

        $server_url = $moodle_url. '/webservice/rest/server.php';
        $rel_url = $moodle_url . '/mod/quiz/accessrule/sebserver/downloadbackup.php?token=' . $moodle_token . '&relativelink=';

        $MoodleRest = new MoodleRest($server_url, $moodle_token);
        $result = $MoodleRest->request('quizaccess_sebserver_backup_course', array('id' => $id, 'backuptype' => $type));

        if ($result["errorcode"] ?? false) {
            $response_array['errorcode'] = $result["errorcode"];
            $response_array['exception'] = $result["exception"];
        } else {
            $backup_file = $result["data"][0]["filelink"];
            $relative_link = $result['data'][0]['relativelink'];
            $response_array['filelink'] = trim($backup_file, "?forcedownload=1");
            $response_array['rel'] = $rel_url . $relative_link;
            
        }
    }
    echo json_encode($response_array);
}
