<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/func.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {

	$exam_id = urlencode($_GET['exam_id']);
	$js_encoding = urlencode($_GET['encoding']);
	$delimiter = "";
	$file = "";
	$allowed_filetype = $_POST['filetype'];

	$e = 0;

	// upload and read csv
	$relative_path = "../../";
	$file = $_SERVER['DOCUMENT_ROOT'] . "/" . $csv_dir . basename($_FILES["file"]["name"]);
	$filename = basename($_FILES["file"]["name"]);
	$target_file = $relative_path . $csv_dir . basename($_FILES["file"]["name"]);
	$uploadOk = 1;
	$fileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));
	$response_array[0]['file'] = $target_file;
	$response_array[0]['filetype'] = $fileType;
	$response_array[0]['file_status'] = 'success';
	$response_array[0]['upload_status'] = 'success';
	$response_array[0]['final_status'] = 'success';
	$response_array[0]['encoding_status'] = "success";
	$error = false;
	$student_id_placeholder = "###replace###";

	// Check if example.csv
	if ($filename == "example.csv") {
		$response_array[0]['file_status'] = 'cannot overwrite example.csv';
		$uploadOk = 0;
	}

	// Check if file already exists
	if (file_exists($target_file) && $uploadOk) {
		unlink($target_file);
		$response_array[0]['file_status'] = 'already existing';
	}
	// Check file size
	if ($_FILES["file"]["size"] > 500000) {
		$response_array[0]['file_status'] = 'too large';
		$uploadOk = 0;
	}
	// Allow certain file formats
	if ($fileType != $allowed_filetype && $allowed_filetype != "*") {
		$response_array['file_status'] = 'wrong file';
		$uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
		$response_array[0]['upload_status'] = 'error';
		$response_array[0]['final_status'] = 'error';
	} else {
		// if everything is ok, try to upload file
		if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
			$response_array[0]['upload_status'] = 'success';
		} else {
			$response_array[0]['upload_status'] = 'error';
			$response_array[0]['final_status'] = 'error';
		}
	}

	// Check and convert encoding
	define('UTF8_BOM', chr(0xEF) . chr(0xBB) . chr(0xBF));
	$str = file_get_contents($file);
	$encoding = mb_detect_encoding($str, 'auto');

	if ($encoding != "UTF-8") {
		file_put_contents($file, mb_convert_encoding($str, 'UTF-8', mb_list_encodings()));
	} else {
		// Check if BOM and remove first three bytes
		$first3 = left($str, 3);
		if ($first3 == UTF8_BOM) {
			$encoding = "UTF-8-BOM";
			$str = substr($str, 3);
			file_put_contents($file, $str);
		}
	}
	$response_array[0]['encoding'] = $encoding;

	// Detect delimiter from first line.
	$handle = fopen($file, "r");
	if ($handle !== false) {
		$delimiter = "";
		$firstLine = fgets($handle);
		if ($firstLine !== false) {
			if (strpos($firstLine, ",") !== false) {
				$delimiter = ",";
			} elseif (strpos($firstLine, ";") !== false) {
				$delimiter = ";";
			} else {
				$delimiter = "";
			}
		}
	}
	$response_array[0]['delimiter'] = $delimiter;

	// compare with data
	$csv = array_map(function ($v) {
		global $delimiter;
		return str_getcsv($v, $delimiter);
	}, file($file));

	// extract headers
	foreach ($csv[0] as $i => $csv_title) {
		$csv_header[$i] = $csv_title;
		$response_array[0]['header'][$i] = $csv_title;
	}

	// combine data with headers
	array_walk($csv, function (&$a) use ($csv) {
		$a = array_combine($csv[0], $a);
	});

	// remove column header
	array_shift($csv);

	$len = count($csv);
	$response_array[0]['csv'] = $len;

	$lastname = array('lastname', 'familienname', 'last_name', 'nachname');
	$firstname = array('firstname', 'vorname', 'first_name');
	$registration = array('registration', 'nummer', 'number', 'matrikelnummer');
	$room = array('room', 'room', 'rooms', 'räume', 'raum');
	$keyboard = array('keyboard', 'keyboard', 'tastatur');
	$group = array('group', 'gruppe', 'group', 'cohort', 'kohorte');
	$remark = array('remark', 'bemerkung', 'remark');
	$url = array('url', 'url', 'prüfungslink', 'exam_link');
	$computer = array('computer', 'computer', 'computername');

	$titles_array = array(
		$lastname,
		$firstname,
		$registration,
		$room,
		$keyboard,
		$group,
		$remark,
		$url,
		$computer
	);

	// generate data from headers
	foreach ($csv as $i => $row) {
		foreach ($titles_array as $titles) {
			foreach ($titles as $title) {
				foreach ($csv_header as $header) {
					if (strtolower($header) == $title) {
						${$titles[0]} = $row[$header];
						$break = true;
					}
				}
				if ($break) {
					break;
				} else {
					${$titles[0]} = 'null';
				}
			}
			$break = false;
		}

		$entry = $i + 1;
		$line = $entry;

		// ******************************** PART 1 *****************************************************

		// Familienname, Vorname, Nummer, Raum dürfen nicht leer sein
		if (empty($lastname) || $lastname == ' ' || $lastname == 'null') {
			$response_array[0]['final_status_text'][$e] = '- student error (Entry #' . $line . ')';
			$e++;
			$error = true;
		}

		if (empty($firstname) || $firstname == ' ' || $firstname == 'null') {
			$response_array[0]['final_status_text'][$e] = '- student error (Entry #' . $line . ')';
			$e++;
			$error = true;
		}

		if (empty($registration) || $registration == ' ' || $registration == 'null') {
			$registration = '00-000-000';
		}

		if (empty($room) || $room == ' ' || $room == 'null') {
			$response_array[0]['final_status_text'][$e] = '- room error (Entry #' . $line . ')';
			$e++;
			$error = true;
		}

		if ($remark == 'null') {
			$remark = '';
		}

		if ($url == 'null') {
			$url = '';
		}

		if ($error) {
			$response_array[0]['final_status'] = 'error';
			echo json_encode($response_array);
			throw new UnexpectedValueException('unexpected status');
		}

		if ($keyboard == "US") {
			$keyboard = "true";
		} else {
			$keyboard = "false";
		}

		$url_clean = $url;
		$url = "<a href=\"" . $url . "\" target=\"_blank\">" . $url . "</a>";
		$student_id = '';
		$computer_id = '';
		$room_id = '';
		$lastname = strval(str_replace("'", "&apos;", $lastname));
		$firstname = strval(str_replace("'", "&apos;", $firstname));
		if ($js_encoding != "UTF-8") {
			$lastname = mb_convert_encoding($lastname, 'UTF-8', mb_list_encodings());
			$firstname = mb_convert_encoding($firstname, 'UTF-8', mb_list_encodings());
		}

		// check if student already exists in database
		$qry = $SELECT_students . " WHERE student_matrikelnr='$registration' AND student_lastname='$lastname' AND student_firstname='$firstname'";
		$res1 = pg_query($con, $qry);
		$row1 = pg_fetch_assoc($res1);
		$num1 = pg_num_rows($res1);
		$student_id = $row1['student_id'];

		$response_array[$entry]['student_name'] = $lastname . " " . $firstname;
		$student_value[$i] = '';

		if (empty($student_id)) {
			if ($i < $len) {
				$student_value[$i] = "('$registration','$lastname','$firstname')";
			}
		} else {
			$response_array[$entry]['student'] = 'already existing';
		}

		// ******************************** PART 2 *****************************************************

		// get room_id from computer
		$qry = $SELECT_room_by_computer . " AND tbl_computer.computer_name='" . $computer . "'";
		$res3 = pg_query($con, $qry);
		$row3 = pg_fetch_assoc($res3);
		$num3 = pg_num_rows($res3);
		if ($num3 > 0) {
			$computer_room_id = $row3['room_id'];
		} else {
			$computer_room_id = '';
		}

		// check if computer already exists in database
		$qry = "SELECT computer_id FROM tbl_computer WHERE computer_name='$computer'";
		$res4 = pg_query($con, $qry);
		$row4 = pg_fetch_assoc($res4);
		$num4 = pg_num_rows($res4);
		if ($num4 > 0) {
			$computer_id = $row4['computer_id'];
		} else {
			$computer_id = '';
		}

		if (!$computer_id) {
			$response_array[$entry]['computer'] = 'none';
			$computer_id = 'null';
		} else {
			$response_array[$entry]['computer'] = $computer_id;
		}

		// remove . from room name
		$room = str_replace(".", "", $room);

		// check if room already exists in database
		$qry = "SELECT room_id FROM tbl_room WHERE room_name='$room'";
		$res5 = pg_query($con, $qry);
		$row5 = pg_fetch_assoc($res5);
		$room_id = $row5['room_id'];

		if (!isset($room_id)) {
			$response_array[$entry]['room'] = 'missing';
			$response_array[0]['final_status'] = 'error';
			$response_array[0]['final_status_text'][$e] = '- room missing (Entry #' . $line . ')';
			$error = true;
			$e++;
		}

		// check if computer exists in room
		if ($computer_room_id != $room_id && $computer_room_id) {
			$response_array[$entry]['room'] = 'wrong room ' . $computer_room_id . ' <> ' . $room_id;
			$response_array[0]['final_status'] = 'error';
			$response_array[0]['final_status_text'][$e] = '- wrong room or computer (Entry #' . $line . ')';
			$error = true;
			echo json_encode($response_array);
			throw new UnexpectedValueException('wrong room or computer');
		}

		$active = "false";

		// generate insert statements
		if (empty($student_id)) {
			$exam_student_value[$i] = "($room_id, $student_id_placeholder, $computer_id, $group, $keyboard, '$remark', '$url_clean', $exam_id, $active, 'false')";
		} else {
			$exam_student_value[$i] = "($room_id, $student_id, $computer_id, $group, $keyboard, '$remark', '$url_clean', $exam_id, $active, 'false')";
		}
	}

	// write to database

	if (!$error) {
		foreach ($csv as $i => $row) {
			$entry = $i + 1;

			if ($student_value[$i]) {
				$qry = $INSERT_student . " VALUES $student_value[$i] RETURNING student_id";
				if ($res2 = pg_query($con, $qry)) {
					$row2 = pg_fetch_row($res2);
					$student_id = $row2['0'];
					$exam_student_value[$i] = str_replace($student_id_placeholder, $student_id, $exam_student_value[$i]);
				} else {
					$response_array[0]['final_status'] = 'error';
				}
			}
			if ($exam_student_value[$i]) {
				$qry = $INSERT_exam_student_2 . " VALUES $exam_student_value[$i]";
				if (pg_query($con, $qry)) {
					$response_array[$entry]['exam_student'] = 'success';
				} else {
					$response_array[$entry]['exam_student'] = 'error';
					$response_array[0]['final_status'] = 'error';
				}
			}
		}
	}

	pg_close($con);

	echo json_encode($response_array);
}
