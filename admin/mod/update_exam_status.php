<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$id = $_POST['id'];
	$status_lang = htmlspecialchars($_POST['status_lang']);
	$color = htmlspecialchars($_POST['color']);
	$standard = $_POST['standard'];

	// Include language
	if (isset($_SESSION['lang'])) {
		$lang = $_SESSION['lang'];
		$qry = $SELECT_language . " WHERE lang_code='$lang'";
		$res = pg_query($con, $qry);
		$row = pg_fetch_assoc($res);
		$lang_id = $row['lang_id'];
	} else {
		// default
		$qry = $SELECT_lang_default;
		$res = pg_query($con, $qry);
		$row = pg_fetch_assoc($res);
		$lang_id = $row['lang_id'];
	}

	$response_array['status'] = 'success';

	$qry = $UPDATE_status_lang . " SET status='$status_lang' WHERE status_id=$id AND lang_id=$lang_id";

	if (pg_query($con, $qry)) {
		$response_array['status1'] = 'success';
	} else {
		$response_array['status1'] = 'error';
		$response_array['status'] = 'error';
	}

	$qry = $UPDATE_exam_status . " SET color='$color', standard='$standard' WHERE status_id=$id";

	if (pg_query($con, $qry)) {
		$response_array['status2'] = 'success';
	} else {
		$response_array['status2'] = 'error';
		$response_array['status'] = 'error';
	}

	pg_close($con);
	echo json_encode($response_array);
}
