<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$id = $_POST['id'];
	$theme = htmlspecialchars($_POST['theme']);
	$values = htmlspecialchars($_POST['values']);

	$qry = $SELECT_VHO_themes . " WHERE theme_name='$theme' AND theme_values='$values'";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$num = pg_num_rows($res);
	if ($num > 0) {
		$themeID = $row['theme_id'];
	} else {
		$themeID = '';
	}

	if (!$themeID || $themeID == $id) {
		$qry = $UPDATE_VHO_theme . " SET theme_name='$theme', theme_values='$values' WHERE theme_id=$id";

		if (pg_query($con, $qry)) {
			$response_array['status'] = 'success';
		} else {
			$response_array['status'] = 'error';
		}
	} else {
		$response_array['status'] = 'already existing';
	}

	pg_close($con);
	echo json_encode($response_array);
}
