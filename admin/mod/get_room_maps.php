<?php

// Get rooms with no map
$qry = $SELECT_pref . " WHERE pref_name = 'no_room_maps'";
$res = pg_query($con, $qry);
$row = pg_fetch_assoc($res);
$no_room_maps = $row['pref_value'];
$noshow = explode("; ", $no_room_maps);

$qry = $SELECT_rooms . " ORDER BY room_name";
$res1 = pg_query($con, $qry);
$num1 = pg_num_rows($res1);

if (isset($_GET['room'])) {
    $room = htmlspecialchars($_GET['room']);
} else {
    $room = "";
}

$selected = false;
$selectRooms = "<select id=\"select_rooms\" class=\"select custom-select form-control\" style=\"width: 120px;\">";
for ($i = 0; $i < $num1; $i++) {
    $row1 = pg_fetch_array($res1);
    $roomID = $row1['room_id'];
    $roomName = $row1['room_name'];

    if (!in_array($roomName, $noshow)) {
        if (!$room && $i == 0) {
            $room = $roomName;
        }

        if ($row1['room_display_name']) {
            $roomDN = $row1['room_display_name'];
        } else {
            $roomDN = $row1['room_name'];
        }
        $option = "<option name=\"$roomName\" value=\"$roomID\"";

        if (($room == $roomName || empty($room)) && !$selected) {
            $option = $option . " selected>" . $roomDN;
            $roomIDselected = $roomID;
            $roomSelected = $roomName;
            $selected = true;
        } else {
            $option = $option . ">" . $roomDN;
        }
        $option = $option . "</option>";
        $selectRooms = $selectRooms . $option;
    }
}
$selectRooms = $selectRooms . "</select><br><br>";

echo _ROOM . ": " . $selectRooms;

echo "<div id=\"room_map\">";

getRoomMap($roomSelected);

echo "</div>";

echo "<div id=\"room_list\">";

$header =   "<th style=\"width: 30%;\">" . _LASTNAME . "<br /></th>
            <th style=\"width: 30%;\">" . _FIRSTNAME . "<br /></th>
            <th style=\"width: 25%;\">" . _REGISTRATION . "<br /></th>
            <th style=\"width: 15%;\">" . _VDI . "<br /></th>";

$qry = $SELECT_exam_students_by_room . $roomIDselected . " AND archive = 'f' AND disabled = 'f' AND tbl_exam_student.student_id <> 1 ORDER BY lastname";
$res = pg_query($con, $qry);
$num = pg_num_rows($res);

if ($num > 0) {
    $students = '';
    for ($i = 0; $i < $num; $i++) {
        $row = pg_fetch_array($res);
        $exam_student_id = $row['exam_student_id'] ?? 0;
        $student_id = $row['student_id'] ?? 0;
        $lastname = $row['lastname'] ?? '';
        $firstname = $row['firstname'] ?? '';
        $registration = $row['registration'] ?? '';
        $computer = $row['computer'] ?? '';
        $exam_user_id =  $row['exam_user_id'] ?? '';
        $vdi = $row['vdi'] ?? '';

        if ($vdi == "t") {
            $vdiText = "<i class=\"fa fa-check-square-o fa-fw true_icon\" title=\"" . _TRUE . "\" aria-label=\"true\"></i>";
            $vdiText = $vdiText . "<span style=\"opacity: 0;\">" . _TRUE . "</span>";
            
            $qry = $SELECT_computer_by_VDI_user . $exam_user_id;
            $res2 = pg_query($con, $qry);
            $num2 = pg_num_rows($res2);
            if ($num2 > 0) {
                $row2 = pg_fetch_assoc($res2);
                $computer = $row2['computer_name'];
            }
        } else {
            $vdiText = "<i class=\"fa fa-square-o fa-fw false_icon\" title=\"" . _FALSE . "\" aria-label=\"false\"></i>";
            $vdiText = $vdiText . "<span style=\"opacity: 0;\">" . _FALSE . "</span>";
        }

        $students = $students . "<tr id=\"$exam_student_id\" name=\"$computer\" class=\"student\"><td>$lastname</td><td>$firstname</td><td>$registration</td><td>$vdiText</td></tr>";
    }

    echo "<div id=\"div_student_list\">";
    echo "<h3>" . _STUDENT_LIST . "</h3>";
    echo "<p><span id=\"count\">$num</span> " . _COUNT . "</p>";
    echo "<table id=\"student_list\" class=\"admintable generaltable\" style=\"width: 100%;\">
        <thead class=\"resizable sticky\">$header</thead>
        <tbody class=\"resizable\">$students</tbody>
    </table>
    </div>";
} else {
    echo "<div id=\"div_nothing\"><h3>" . _NO_ALLOCATION_TITLE . "</h3></div>";
}
echo "</div>";

function getRoomMap($room)
{
    include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
    include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

    $roomLink = str_replace(" ", "_", strtolower($room));
    $file = "../img/rooms/$roomLink.svg";

    if (file_exists($file)) {
        echo file_get_contents($file);
    } else {
        echo "<h2>" . _WRONG_PARAMETER . "</h2>
				<p>&nbsp;</p>
				<i class=\"fa fa-exclamation fa-fw\" style=\"font-size: 200px; margin-left: 80px;\" title=\"\" aria-label=\"false\" name=\"\"></i>";
    }
}
