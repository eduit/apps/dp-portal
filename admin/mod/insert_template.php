<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $text = htmlspecialchars($_POST["text"]);
    $type = $_POST["type"];

    // Include language
    if (isset($_SESSION['lang'])) {
        $lang = $_SESSION['lang'];
        $qry = $SELECT_language . " WHERE lang_code='$lang'";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $lang_id = $row['lang_id'];
    } else {
        // default
        $qry = $SELECT_lang_default;
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $lang_id = $row['lang_id'];
    }

    $qry = $INSERT_template . " VALUES ('$text', $lang_id, '$type')";

    if (pg_query($con, $qry)) {
        $response_array['status'] = 'success';
    } else {
        $response_array['status'] = 'error';
    }

    pg_close($con);
    echo json_encode($response_array);
}
