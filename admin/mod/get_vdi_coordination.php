<?php

// Include language
if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else {
    // default
    $qry = $SELECT_lang_default;
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $lang = $row['lang'];
}

switch ($tab) {
    case "exams":
        if ($_SESSION['loggedin'] || $_SESSION['user_vho']) {

            // archive
            if (isset($_GET['archived']) && ($_GET['archived'] == "true" || $_GET['archived'] == "false")) {
                $archived = $_GET['archived'];
            } else {
                $archived = "false";
            }

            // sort
            $sortBy = array("exam_date" => _EXAM_DATE, "exam_name" => _EXAM_2, "testing_from" => _TESTING . " " . _FROM, "testing_to" => _TESTING . " " . _TO, "status_id" => _STATUS, "deadline" => _DEADLINE);

            if (isset($_GET['sort']) && array_key_exists($_GET['sort'], $sortBy)) {
                $sort = $_GET['sort'];
            } else {
                $sort = array_key_first($sortBy);
            }
            if (isset($_GET['order']) && ($_GET['order'] == "ASC" || $_GET['order'] == "DESC")) {
                $order = $_GET['order'];
            } else {
                $order = "ASC";
            }

            // get vdi-exam entries
            $qry = $SELECT_vdi_exam . " WHERE tbl_exam.archive = $archived AND t_lang1.lang_code = '$lang' AND t_lang2.lang_code = '$lang' ORDER BY $sort $order";
            $res1 = pg_query($con, $qry);
            $num1 = pg_num_rows($res1);

            // get user
            $user = $_SESSION['username'];
            echo "<p id=\"username\" class=\"username hidden\">$user</p>";

            // archive
            echo "<p><label id=\"archived_label\" title=\"" . _ARCHIVED . "\"><input class=\"button\" name=\"archived\" type=\"checkbox\" name=\"\" value=\"0\" id=\"archived\" ";
            if ($archived == "true") {
                echo "checked";
            }
            echo "> " . _ARCHIVED . "</label> ";

            // import
            if ($_SESSION['loggedin']) {
                $qry = "SELECT exam_id FROM tbl_exam
                    WHERE tbl_exam.exam_id NOT IN (SELECT exam_id FROM tbl_vdi_exam)
                    AND tbl_exam.type_id=2
                    AND tbl_exam.disabled = false
                    AND tbl_exam.archive = false
                    AND tbl_exam.exam_date > CURRENT_DATE";
                $res2 = pg_query($con, $qry);
                $num2 = pg_num_rows($res2);

                echo "<label id=\"import_label\" title=\"" . _IMPORT . "\"><input class=\"button\" name=\"import\" type=\"checkbox\" name=\"\" value=\"0\" id=\"import\"> " . _IMPORT . " ($num2)</label>";
            }
            echo "</p>";

            // sort
            echo "<p>" . _SORT_BY . ": <select id=\"sort\" class=\"select custom-select form-control\" name=\"sort\" style=\"width: 400px;\">";

            foreach ($sortBy as $key => $value) {
                $selected = "";
                if ($key == $sort) {
                    $selected = "selected";
                }
                echo "<option value=\"$key\" $selected>$value</option>";
            }
            echo "</select> <label id=\"desc_label\" title=\"" . _DESC . "\"><input type=\"checkbox\" name=\"$order\" value=\"0\" id=\"order\" ";
            if ($order == "DESC") {
                echo "checked";
            }
            echo "> " . _DESC . "</label></p>";

            // table
            if ($_SESSION['loggedin']) {
                echo "<p>" . $num1 . " " . _COUNT . "</p>";
            }

            // width
            $width = "9%";

            echo "<table id=\"vdi_exams\" class=\"admintable generaltable\" style=\"width: 100%;\">
					<thead class=\"resizable sticky\">
					<tr>
                        <th style=\"width: $width;\">" . _EXAM . "</th>
                        <th style=\"width: $width;\">" . _CONFIGURATION . "</th>
                        <th style=\"width: $width;\">" . _TESTING . " (" . _FROM . " " . _TO . ")</th>
                        <th style=\"width: $width;\">" . _SELF_TESTING . "</th>
                        <th style=\"width: $width;\">" . _SETUP . "</th>
                        <th style=\"width: $width;\">" . _VDI_POOL . "</th>
                        <th style=\"width: $width;\">" . _TESTING_USER . "</th>
                        <th style=\"width: $width;\">" . _REMARKS . "</th>
                        <th style=\"width: $width;\">" . _DEADLINE . " " . _SETUP . "</th>
                        <th style=\"width: $width;\">" . _STATUS . "</th>";
            if ($_SESSION['loggedin']) {
                echo "<th style=\"width: 10%;\">" . _EDIT . "</th>";
            }
            echo "</tr>
                </thead>
                <tbody class=\"resizable\">";

            if ($_SESSION['loggedin'] && $archived == "false") {
                echo "<tr id=\"add_row\">";
                echo "<td><select class=\"select custom-select form-control\" name=\"exam\" id=\"add_exam\" required=\"required\">" . selectExams(0) . "</select><br>
                        <span id=\"exam_missing\" class=\"invalid-feedback\">" . _EXAM . " " . _MISSING . "</span></td>";
                echo "<td><select class=\"select custom-select form-control\" name=\"config\" id=\"add_config\" required=\"required\" disabled><option name=\"" . _NO_CONFIG . "\" value=\"0\" disabled selected>" . _NO_CONFIG . "</option></select><br>
                        <span id=\"config_missing\" class=\"invalid-feedback\">" . _CONFIGURATION . " " . _MISSING . "</span></td>";
                echo "<td>";
                echo "<input type=\"text\" class=\"form-control testing_date datetimepicker\" name=\"testing_from\" id=\"add_testing_from\" value=\"\" maxlength=\"" . MAXLENGTH . "\"> ";
                echo "<input type=\"text\" class=\"form-control testing_date datetimepicker\" name=\"testing_to\" id=\"add_testing_to\" value=\"\" maxlength=\"" . MAXLENGTH . "\">";
                echo "<br><span id=\"testing_date_missing\" class=\"invalid-feedback\">" . _TESTING_DATE . " " . _MISSING . "</span></td>";
                echo "<td><input type=\"checkbox\" name=\"self_testing\" id=\"add_self_testing\" value=\"\"></td>";
                echo "<td><select class=\"select custom-select form-control\" name=\"vdi_pool_setup\" id=\"add_vdi_pool_setup\" style=\"width: 100%;\" disabled>" . selectPoolSetup(1) . "</select></td>";
                echo "<td><select class=\"select custom-select form-control vdi-pool\" name=\"1\" data-href=\"1\" id=\"add_vdi_pool\" style=\"width: 100%;\" disabled></select></td>";
                echo "<td><ul id=\"testing_people\" class=\"people\" style=\"width: 100%; overflow: hidden;\"></ul></td>";
                echo "<td><input type=\"text\" class=\"form-control\" name=\"remarks\" id=\"add_remarks\" value=\"\" maxlength=\"" . MAXLENGTH . "\"></td>";
                echo "<td><input type=\"text\" class=\"form-control datepicker\" name=\"deadline\" id=\"add_deadline\" value=\"\" maxlength=\"" . MAXLENGTH . "\"></td>";
                echo "<td><select class=\"select custom-select form-control\" name=\"status\" id=\"add_status\" style=\"width: 100%;\" disabled>" . selectStatus(1, 1) . "</select></td>";
                echo "<td><i id=\"vdi_exam_submit\" class=\"icon fa fa-plus fa-fw add_icon\" title=\"" . _ADD . "\" aria-label=\"" . _ADD . "\" name=\"add_vdi_exam\"></i></td>";
                echo "</tr>";
            }

            for ($i = 0; $i < $num1; $i++) {
                $row1 = pg_fetch_array($res1);
                $id = $row1['id'];
                $examID = $row1['exam_id'];
                $examDate = date_format(date_create($row1['exam_date']), 'Y-m-d');
                $examNameID = $row1['exam_name_id'];
                $examNr = $row1['exam_nr'];
                $examName = $row1['exam_name'];
                $semester = $row1['semester'];
                $lecturer = $row1['lecturer_lastname'];
                $configID = $row1['config_id'];
                $configUserID = $row1['config_user_id'];
                $configUsername = $row1['config_username'];
                $configFirstname = $row1['config_firstname'];
                $configLastname = $row1['config_lastname'];
                $configRemarks = $row1['config_remarks'];
                $configFiles = $row1['config_files'];
                $configFilepath = $row1['config_filepath'];
                // testing from
                $testingFromRaw = date_create($row1['testing_from'] ?? '');
                $testingFromDate = $testingFromRaw ? date_format($testingFromRaw, 'Y-m-d') : '';
                $testingFromDateTime = $testingFromRaw ? date_format($testingFromRaw, 'Y-m-d H:i') : '';
                $testingFromTime = $testingFromRaw ? date_format($testingFromRaw, 'H:i') : '';
                // testing to
                $testingToRaw = date_create($row1['testing_to'] ?? '');
                $testingToDate = $testingToRaw ? date_format($testingToRaw, 'Y-m-d') : '';
                $testingToDateTime = $testingToRaw ? date_format($testingToRaw, 'Y-m-d H:i') : '';
                $testingToTime = $testingToRaw ? date_format($testingToRaw, 'H:i') : '';
                // testing range
                $testingRange = '';
                if ($testingFromDate && $testingToDate) {
                    if ($testingFromDate === $testingToDate) {
                        $testingRange = "$testingFromDate<br>$testingFromTime - $testingToTime";
                    } else {
                        $testingRange = "$testingFromDate - $testingToDate";
                    }
                }
                $selfTesting = $row1['self_testing'];
                $vdiPoolID = $row1['pool_id'];
                $vdiPoolSetup = $row1['vdi_pool_setup'];
                $vdiPool = $row1['vdi_pool'];
                $remarks = $row1['remarks'];
                $deadline = $row1['deadline'];
                $statusID = $row1['status_id'];
                $status = $row1['status'];
                $bgColor = $row1['color'];
                $txtColor = getTextColour($bgColor);
                $file_link = "https://$_SERVER[HTTP_HOST]/" . $vdi_dir . $configFilepath;
                $file_check = file_exists($_SERVER["DOCUMENT_ROOT"] . "/" . $vdi_dir . $configFilepath);

                // red if internal information exists
                $qry = $SELECT_vdi_info . " WHERE vdi_exam_id = $id LIMIT 1";
                $res = pg_query($con, $qry);
                $row = pg_fetch_assoc($res);
                $info = strip_tags(html_entity_decode($row['info']));
                if ($info) {
                    $css = "style=\"color: red;\"";
                } else {
                    $css = "";
                }

                if ($vdiPoolID == 1) {
                    $hidden = "hidden";
                } else {
                    $hidden = "";
                }

                // config users
                $configUsers = "";
                if ($configID) {
                    $qry = $SELECT_config_people . " AND tbl_config_people.config_id = $configID ORDER BY tbl_people.people_username";
                    $res3 = pg_query($con, $qry);
                    $num3 = pg_num_rows($res3);
                    $continue = false;
                    $users[] = $configUsername;
                    $configUsers = "<li id=\"$configUserID\">$configUsername</li>\n";
                    for ($j = 0; $j < $num3; $j++) {
                        $row3 = pg_fetch_array($res3);
                        $peopleID = $row3['people_id'];
                        $username = $row3['username'];
                        $configUsers = $configUsers . "<li id=\"$peopleID\">$username</li>\n";
                        $users[] = $username;
                    }
                    if (in_array($_SESSION['username'], $users) || !$_SESSION['user_vho']) {
                        $continue = true;
                    }
                    unset($users);
                } else {
                    if (!$_SESSION['user_vho']) {
                        $configUsers = "";
                        $continue = true;
                    } else {
                        $continue = false;
                    }
                }

                if ($continue) {
                    echo "<tr id=\"row_$id\">
                            <td>$examName ($examNr) | $lecturer | $examDate</td>
                            <td>";
                    if ($configID) {
                        echo "<span title=\"$configRemarks\">$examName ($semester)</span>";
                    }
                    echo "</td>
                            <td>$testingRange</td>
                            <td>";
                    if ($selfTesting == "t") {
                        echo "<i class=\"fa fa-check-square-o fa-fw true_icon\" title=\"" . _TRUE . "\" aria-label=\"true\" name=\"\"></i>";
                    } else {
                        echo "<i class=\"fa fa-square-o fa-fw false_icon\" title=\"" . _FALSE . "\" aria-label=\"false\" name=\"\"></i>";
                    }
                    echo "</td>
                            <td>$vdiPoolSetup</td>
                            <td>$vdiPool</td>
                            <td><ul>$configUsers</ul></td>
                            <td>$remarks</td>
                            <td>$deadline</td>
                            <td style=\"background-color: $bgColor; color: $txtColor; font-weight: bold;\">$status</td>";

                    if ($_SESSION['loggedin']) {
                        echo "<td class=\"edit_row\">";
                        // internal information
                        echo "<i class=\"icon fa fa-info-circle fa-fw info_icon\" id=\"info_$id\"  $css title=\"" . _INTERNAL_INFORMATION . "\" aria-label=\"" . _INTERNAL_INFORMATION . "\" name=\"$id\"></i>";
                        echo "<i class=\"icon fa fa-edit fa-fw edit_icon\" title=\"" . _EDIT . "\" aria-label=\"" . _EDIT . "\" name=\"edit_row_$id\"></i>";
                        // setup editable
                        if ($statusID <= 2) {
                            echo "<i class=\"icon fa fa-trash fa-fw delete_exam_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" name=\"$id\"></i>";
                        }
                        // reminder sent
                        if ($statusID <= 2) {
                            echo "<i class=\"icon fa fa-bell fa-fw status_icon\" title=\"" . _REMINDER . "\" aria-label=\"" . _REMINDER . "\" name=\"$id\" data-href=\"2\"></i>";
                        }
                        // setup confirmed
                        if ($statusID <= 2) {
                            echo "<i class=\"icon fa fa-check fa-fw status_icon\" title=\"" . _CONFIRMED . "\" aria-label=\"" . _CONFIRMED . "\" name=\"$id\" data-href=\"3\"></i>";
                        }
                        // testing env to prepare
                        if ($statusID == 3) {
                            echo "<i class=\"icon fa fa-thumbs-o-up fa-fw status_icon\" title=\"" . _READY_TO_PREPARE . "\" aria-label=\"" . _READY_TO_PREPARE . "\" name=\"$id\" data-href=\"5\"></i>";
                        }
                        // testing env prepared
                        if ($statusID == 5) {
                            echo "<i class=\"icon fa fa-gift fa-fw status_icon\" title=\"" . _CREATED . "\" aria-label=\"" . _CREATED . "\" name=\"$id\" data-href=\"6\"></i>";
                        }
                        // testing successful or not
                        if ($statusID == 6 || $statusID == 8) {
                            if ($vdiPoolID == 1) {
                                $next = 10;
                            } else {
                                $next = 7;
                            }
                            echo "<i class=\"icon fa fa-thumbs-up fa-fw status_icon\" title=\"" . _SUCCESS . "\" aria-label=\"" . _SUCCESS . "\" name=\"$id\" data-href=\"$next\"></i>";
                            echo "<i class=\"icon fa fa-thumbs-down fa-fw status_icon\" title=\"" . _FAIL . "\" aria-label=\"" . _FAIL . "\" name=\"$id\" data-href=\"8\"></i>";
                        }
                        // ready to finalize
                        if ($vdiPoolID != 1 && $statusID == 7) {
                            echo "<i class=\"icon fa fa-flag-o fa-fw status_icon\" title=\"" . _READY_TO_FINALIZE . "\" aria-label=\"" . _READY_TO_FINALIZE . "\" name=\"$id\" data-href=\"9\"></i>";
                        }
                        // setup finalized
                        if ($vdiPoolID != 1 && $statusID == 9) {
                            echo "<i class=\"icon fa fa-flag-checkered fa-fw status_icon\" title=\"" . _FINALIZED . "\" aria-label=\"" . _FINALIZED . "\" name=\"$id\" data-href=\"10\"></i>";
                        }
                        // download
                        if ($configFilepath != "" && $file_check) {
                            echo "<i class=\"icon fa fa-download fa-fw download_icon\" title=\"" . _DOWNLOAD . "\" aria-label=\"" . _DOWNLOAD . "\" href=\"$file_link\"></i>";
                        }
                        // ics
                        echo "<i class=\"icon fa fa-calendar fa-fw calendar_icon\" title=\"" . _ICS_FILE . "\" aria-label=\"" . _ICS_FILE . "\" name=\"$id\"></i>";
                        echo "</td></tr>";
                        // edit row
                        echo "<tr id=\"edit_row_$id\" class=\"edit_rows hidden\">";
                        echo "<td>$examName ($examNr) | $lecturer | $examDate</td>";
                        echo "<td><select class=\"select custom-select form-control edit_config\" name=\"$id\" id=\"edit_config_$id\" required=\"required\">" . selectConfigs($configID, $examNameID) . "</select></td>";
                        echo "<td><input type=\"text\" class=\"form-control testing_date datetimepicker\" name=\"testing_from\" id=\"edit_testing_from_$id\" value=\"$testingFromDateTime\" maxlength=\"" . MAXLENGTH . "\"> ";
                        echo "<input type=\"text\" class=\"form-control testing_date datetimepicker\" name=\"testing_to\" id=\"edit_testing_to_$id\" value=\"$testingToDateTime\" maxlength=\"" . MAXLENGTH . "\">";
                        echo "<br><span id=\"testing_date_missing_$id\" class=\"invalid-feedback\">" . _TESTING_DATE . " " . _MISSING . "</span></td>";
                        if ($selfTesting == "t") {
                            $checked = "checked";
                        } else {
                            $checked = "";
                        }
                        echo "<td><input type=\"checkbox\" name=\"self_testing\" id=\"edit_self_testing_$id\" value=\"\" $checked></td>";
                        echo "<td><select class=\"select custom-select form-control vdi_pool_setup\" name=\"$id\" id=\"edit_vdi_pool_setup_$id\" style=\"width: 100%;\">" . selectPoolSetup($vdiPoolID) . "</select></td>";
                        if ($vdiPoolID == 1) {
                            $disabled = "disabled";
                        } else {
                            $disabled = "";
                        }
                        echo "<td><select id=\"edit_vdi_pool_$id\" class=\"select custom-select form-control vdi-pool\" name=\"$id\" data-href=\"$vdiPool\" style=\"width: 100%;\" $disabled></select></td>
                            <td><ul id=\"testing_people_$id\" class=\"people\" style=\"width: 100%; overflow: hidden;\">$configUsers</ul></td>
                            <td><input type=\"text\" class=\"form-control\" name=\"remarks\" id=\"edit_remarks_$id\" value=\"$remarks\" maxlength=\"" . MAXLENGTH . "\"></td>
                            <td><input type=\"text\" class=\"form-control datepicker\" name=\"deadline\" id=\"edit_deadline_$id\" value=\"$deadline\" maxlength=\"" . MAXLENGTH . "\"></td>
                            <td><select class=\"select custom-select form-control\" name=\"status\" id=\"edit_status_$id\" style=\"width: 100%;\">" . selectStatus($statusID, $vdiPoolID) . " </select></td>";
                        echo "<td class=\"edit_row\">";
                        echo "<i class=\"icon fa fa-save fa-fw save_exam_icon\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"$id\"></i>";
                        echo "<i class=\"icon fa fa-times fa-fw cancel_icon\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"$id\"></i>";
                        echo "</td>";
                    }
                }
                echo "</tr>";
            }
            echo "</tbody></table>";

            echo "<div id=\"dialog\" class=\"hidden\" title=\"" . _INTERNAL_INFORMATION . "\"></div>";
        }
        break;

    case "configuration":
        if ($_SESSION['loggedin'] || $_SESSION['user_vho']) {
            
            $date = date("Y-m-d");
            $username = $_SESSION['username'];
            $firstname = $_SESSION['user_firstname'];
            $lastname = $_SESSION['user_lastname'];

            echo "<div class=\"form-group fitem row hidden\">
                <div class=\"col-md-2\">
                    <label class=\"col-form-label\" for=\"id_exam\">ID: </label>
                </div>
                <div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
                    <input type=\"text\" class=\"form-control\" id=\"config_id\" value=\"\" style=\"width: 400px;\">
                </div>
            </div>";
            echo "<div class=\"form-group fitem row\">
                <div class=\"col-md-2\">
                    <label class=\"col-form-label\" for=\"id_exam\">" . _EXAM . ": </label>
                </div>
                <div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
                    <select class=\"select custom-select form-control\" name=\"exam_name\" id=\"id_exam_name\" required=\"required\" style=\"width: 400px;\">" . selectExamNames() . "</select>
                    <div class=\"form-control-feedback invalid-feedback\" id=\"id_missing_exam_name\">" . _EXAM . " " . _MISSING . "</div>
                </div>
            </div>";
            echo "<div class=\"form-group fitem row\">
                <div class=\"col-md-2\">
                    <label class=\"col-form-label\" for=\"id_exam\">" . _SEMESTER . ": </label>
                </div>
                <div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">";
            
            echo "<input type=\"text\" class=\"form-control\" id=\"semester\" value=\"" . getSemester($date) . "\" style=\"width: 400px;\" readonly>";
            echo "</div>
            </div>";

            echo "<div class=\"form-group fitem row\">
                <div class=\"col-md-2\">
                    <label id=\"label_software_list\" class=\"col-form-label\" for=\"sw_list_config\">" . _SOFTWARE . ": </label>
                </div>
                <div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">";

            echo "<select id=\"sw_list_config\" multiple=\"multiple\">" . selectSoftware() . "</select>";
            echo "<div class=\"form-control-feedback invalid-feedback\" id=\"id_missing_software\">" . _SOFTWARE . " " . _MISSING . "</div>";
            echo "</div>
            </div>";

            echo "<div class=\"form-group fitem row\">
                <div class=\"col-md-2\">
                    <label id=\"label_files\" class=\"col-form-label\" for=\"files\">" . _FILES . ": </label>
                </div>
                <div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">";
            echo "<input type=\"checkbox\" name=\"files\" id=\"files\" value=\"\" title=\"" . _FILE_INFO . "\">";
            echo "<i id=\"id_file_upload\" class=\"icon fa fa-upload fa-fw upload_icon hidden\" style=\"margin-left: 20px;\" title=\"" . _UPLOAD . "\" aria-label=\"" . _UPLOAD . "\" name=\"0\"></i> ";
            echo "<input type=\"file\" id=\"fileloader_vdi\" class=\"fileloader hidden\" name=\"0\" title=\"" . _UPLOAD . "\" data-href=\"$vdi_dir\" />";
            echo "<input type=\"text\" class=\"form-control\" name=\"files_0\" id=\"id_files\" value=\"\" maxlength=\"" . 2 * MAXLENGTH . "\" style=\"display: none; margin-left: 140px; height: 28px; width: 200px;\" readonly>";
            echo "<div class=\"form-control-feedback invalid-feedback\" id=\"id_old_files\">" . _FILES_OLD . "</div>";
            echo "<div class=\"form-control-feedback invalid-feedback\" id=\"id_missing_file\">" . _FILE . " " . _MISSING . "</div>";
            echo "</div>
            </div>";

            echo "<div class=\"form-group fitem row\">
                <div class=\"col-md-2\">
                    <label id=\"label_remarks\" class=\"col-form-label\" for=\"remarks\">" . _REMARKS . ": </label>
                </div>
                <div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">";
            echo "<textarea id=\"remarks\" class=\"form-control\" name=\"remarks\" maxlength=\"300\" rows=\"4\" cols=\"50\" style=\"width: 400px;\"></textarea>";
            echo "</div>
            </div>";

            echo "<div class=\"form-group fitem row\">
                <div class=\"col-md-2\">
                    <label class=\"col-form-label\" for=\"add_config_people\">" . _PEOPLE . ": </label>
                </div>
                <div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
                    <select class=\"select custom-select form-control select_people\" name=\"config_people\" id=\"add_config_people\" style=\"width: 400px;\">" . selectPeople($username) . "</select>
                </div>
            </div>";
            echo "<div class=\"form-group fitem row\">
                <div class=\"col-md-2\">
                </div>
                <div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
                    <ul id=\"config_people\" class=\"people\">
                        <li id=\"config_user_empty\" name=\"$username\">$lastname $firstname ($username)</li>
                    </ul>
                </div>
            </div>";

            echo "<div class=\"form-group fitem row\">
                <div class=\"col-md-2 config_buttons\">
                    <i id=\"config_save_button\" class=\"icon fa fa-save fa-fw\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\"></i>
                </div>
            </div>";

            $qry = $SELECT_exam_config . " ORDER BY tbl_exam_name.exam_name";
            $res1 = pg_query($con, $qry);
            $num1 = pg_num_rows($res1);

            echo "<div id=\"div_config_list\" class=\"\">
                <h3>" . _CONFIG_LIST . " (" . _CLICK_TO_LOAD . ")</h3>
                <p><span style=\"color: red\">*</span> " . _DEADLINE . " " . _EXPIRED . "</p>";

            echo "<table id=\"config_list\" class=\"admintable generaltable\" style=\"width: 100%;\">
                    <thead class=\"resizable sticky\">
                        <tr>
                        <th style=\"width: 20%;\">" . _EXAM . "<br /></th>
                        <th style=\"width: 10%;\">" . _SEMESTER . "<br /></th>
                        <th style=\"width: 10%;\">" . _SOFTWARE . "<br /></th>
                        <th style=\"width: 10%;\">" . _FILES . "</th>
                        <th style=\"width: 10%;\">" . _REMARKS . "<br /></th>
                        <th style=\"width: 10%;\">" . _USER . "<br /></th>
                        <th style=\"width: 10%;\">" . _UPDATED . "<br /></th>
                        <th style=\"width: 10%;\">" . _CONFIRMED . "<br /></th>
                        <th style=\"width: 10%;\">" . _EDIT . "<br /></th>
                        </tr>
                    </thead>
                    <tbody>";

            for ($i = 0; $i < $num1; $i++) {
                $row1 = pg_fetch_array($res1);
                $username = $row1['username'];
                $lastname = $row1['lastname'];
                $firstname = $row1['firstname'];
                $configID = $row1['id'];
                $continue = false;
                $users[] = $username;

                // people
                $qry = $SELECT_config_people . " AND tbl_config_people.config_id = $configID ORDER BY people_username";
                $res2 = pg_query($con, $qry);
                $num2 = pg_num_rows($res2);
                $people = "";
                for ($j = 0; $j < $num2; $j++) {
                    $row2 = pg_fetch_array($res2);
                    $people_id = $row2['people_id'];
                    $selected_username = $row2['username'];
                    $selected_lastname = $row2['lastname'];
                    $selected_firstname = $row2['firstname'];
                    $people = $people . "<li id=\"$people_id\" class=\"selected_people\">$selected_lastname $selected_firstname ($selected_username)</li>";
                    $users[] = $selected_username;
                }
                if (in_array($_SESSION['username'], $users) || !$_SESSION['user_vho']) {
                    $continue = true;
                }
                unset($users);

                if ($continue) {
                    $examNameID = $row1['exam_name_id'];
                    $examName = $row1['exam_name'];
                    $examNumber = $row1['exam_nr'];
                    $semester = $row1['semester'];
                    $files = $row1['files'];
                    $filepath = $row1['filepath'];
                    $file_link = "https://$_SERVER[HTTP_HOST]/" . $vdi_dir . $filepath;
                    $file_check = file_exists($_SERVER["DOCUMENT_ROOT"] . "/" . $vdi_dir . $filepath);
                    $remarks = $row1['remarks'];
                    $people_id = $row1['people_id'];
                    $updated = date_format(date_create($row1['updated']), 'd.m.Y H:i:s');
                    $confirmed = $row1['confirmed'];

                    $qry = $SELECT_exam_deadline . $configID . " LIMIT 1";
                    $res = pg_query($con, $qry);
                    $row = pg_fetch_assoc($res);
                    $num = pg_num_rows($res);

                    $expired = "";
                    if ($num > 0) {
                        $deadline = $row['deadline'];
                        $deadline = date('Y-m-d', strtotime($deadline));
                        $response_array['deadline'] = $deadline;
                        $today = date("Y-m-d");
                        if ($deadline < $today) {
                            $expired = "<span style=\"color: red\">*</span>";
                        }
                    }

                    echo "<tr id=\"config_$configID\" class=\"clickable config\" name=\"$configID\">
                            <td id=\"" . $examNameID . "_" . $semester . "\" name=\"$examNameID\" class=\"exam\">$expired $examName ($examNumber)</td>
                            <td class=\"semester\">$semester</td>
                            <td class=\"software\"><ul>";

                    // software
                    $qry = $SELECT_config_sw . " AND tbl_config_sw.config_id = $configID ORDER BY sw_name";
                    $res3 = pg_query($con, $qry);
                    $num3 = pg_num_rows($res3);

                    for ($k = 0; $k < $num3; $k++) {
                        $row3 = pg_fetch_array($res3);
                        $swName = $row3['sw_name'];
                        echo "<li>$swName</li>";
                    }
                    echo
                    "</ul></td>
                    <td class=\"files\">";
                    if ($files == "t") {
                        echo "<i class=\"fa fa-check-square-o fa-fw true_icon\" title=\"" . _TRUE . "\" aria-label=\"true\" name=\"$filepath\"></i>";
                    } else {
                        echo "<i class=\"fa fa-square-o fa-fw false_icon\" title=\"" . _FALSE . "\" aria-label=\"false\" name=\"\"></i>";
                    }
                    echo "</td>
                    <td class=\"remarks\">$remarks</td>
                    <td class=\"people\"><ul><li id=\"$people_id\" class=\"config_user\">$lastname $firstname ($username)</li>";
                    echo $people;
                    echo "</ul></td>
                    <td>$updated</td>
                    <td>";
                    if ($confirmed == "t") {
                        echo "<i class=\"fa fa-check-square-o fa-fw true_icon\" title=\"" . _TRUE . "\" aria-label=\"true\" name=\"$configID\"></i>";
                    } else {
                        echo "<i class=\"fa fa-square-o fa-fw false_icon\" title=\"" . _FALSE . "\" aria-label=\"false\" name=\"$configID\"></i>";
                    }
                echo "</td>";
                echo "<td>";
                echo "<i class=\"icon fa fa-check fa-fw confirm_icon\" title=\"" . _CONFIRM . "\" aria-label=\"" . _CONFIRM . "\" name=\"$configID\"></i>";
                echo "<i class=\"icon fa fa-trash fa-fw delete_config_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" name=\"$configID\"></i>";
                    if ($filepath != "" && $files == "t" && $file_check) {
                        echo "<i class=\"icon fa fa-download fa-fw download_icon\" title=\"" . _DOWNLOAD . "\" aria-label=\"" . _DOWNLOAD . "\" href=\"$file_link\"></i>";
                    }
                    echo "</td>
                    </tr>";
                }
            }
            echo "</tbody>
                </table>";
            echo "</div>";
        }
        break;

    case "software":
        if ($_SESSION['loggedin'] || $_SESSION['user_vho']) {
            if ($_SESSION['user_vho']) {
                $visible = " AND tbl_vdi_software.sw_visible = 't'";
            } else {
                $visible = "";
            }
            $qry = $SELECT_software . $visible . " ORDER BY sw_name;";
            $res1 = pg_query($con, $qry);
            $num1 = pg_num_rows($res1);

            echo "<p>" . $num1 . " " . _COUNT . "</p>";

            // width
            if ($_SESSION['loggedin']) {
                $width = "20%";
            } else {
                $width = "25%";
            }

            echo "<table id=\"software\" class=\"admintable generaltable\" style=\"width: 100%;\">
					<thead class=\"resizable sticky\">
					<tr>
						<th style=\"width: $width;\">" . _SOFTWARE . "</th>
						<th style=\"width: $width;\">" . _PUBLISHER . "</th>
                        <th style=\"width: $width;\">" . _VERSION . "</th>
                        <th style=\"width: $width;\">" . _REMARK . "</th>";
                    if ($_SESSION['loggedin']) {
                        echo "<th style=\"width: 10%;\">" . _VISIBLE . "</th>";
						echo "<th style=\"width: 10%;\">" . _EDIT . "</th>";
                    }
					echo "</tr>
					    </thead>
                        <tbody class=\"resizable\">";

            if ($_SESSION['loggedin']) {
                echo "<tr id=\"add_row\">
                        <td><input type=\"text\" class=\"form-control\" name=\"software\" id=\"add_software\" required=\"required\" value=\"\" maxlength=\"" . MAXLENGTH . "\">
                        <span id=\"software_missing\" class=\"invalid-feedback\">" . _SOFTWARE . " " . _MISSING . "</span></td>
                        <td><select class=\"select custom-select form-control select-publisher\" name=\"publisher\" id=\"add_publisher\" required=\"required\">" . selectPublishers(0) . "</select></td>
                        <td><input type=\"text\" class=\"form-control\" name=\"version\" id=\"add_version\" required=\"required\" value=\"\" maxlength=\"" . MAXLENGTH . "\">
                        <span id=\"version_missing\" class=\"invalid-feedback\">" . _VERSION . " " . _MISSING . "</span></td>
                        <td><input type=\"text\" class=\"form-control\" name=\"remarks\" id=\"add_remarks\" value=\"\" maxlength=\"" . MAXLENGTH . "\">
                        <td><input type=\"checkbox\" name=\"visible\" id=\"add_visible\" checked></td>
                        <td><i id=\"software_submit\" class=\"icon fa fa-plus fa-fw add_icon\" title=\"" . _ADD . "\" aria-label=\"" . _ADD . "\" name=\"add_software\"></i></td>
                        </tr>";
            }

            for (
                $i = 0;
                $i < $num1;
                $i++
            ) {
                $row1 = pg_fetch_array($res1);
                $id = $row1['id'];
                $software = $row1['software'];
                $publisherID = $row1['publisher_id'];
                $publisher = $row1['publisher'];
                $version = $row1['version'];
                $remarks = $row1['remarks'];
                $visible = $row1['visible'];

                echo "<tr id=\"row_$id\">
						<td id=\"software_$id\">$software</td>
						<td id=\"publisher_$id\">$publisher</td>
                        <td id=\"version_$id\">$version</td>
                        <td id=\"remarks_$id\">$remarks</td>";
                if ($_SESSION['loggedin']) {
                        echo "<td id=\"visible_$id\">";
                    if ($visible == "t") {
                        echo "<i class=\"fa fa-check-square-o fa-fw true_icon\" title=\"" . _TRUE . "\" aria-label=\"true\" name=\"visible_$id\"></i>";
                        echo "<span style=\"opacity: 0;\">" . _TRUE . "</span>";
                    } else {
                        echo "<i class=\"fa fa-square-o fa-fw false_icon\" title=\"" . _FALSE . "\" aria-label=\"false\" name=\"visible_$id\"></i>";
                        echo "<span style=\"opacity: 0;\">" . _FALSE . "</span>";
                    }
                    echo "</td>";
                
                    echo "<td class=\"edit_row\"><i class=\"icon fa fa-edit fa-fw edit_icon\" title=\"" . _EDIT . "\" aria-label=\"" . _EDIT . "\" name=\"edit_row_$id\"></i><i class=\"icon fa fa-trash fa-fw delete_software_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" name=\"$id\"></i></td>
						</tr>";
                    echo "<tr id=\"edit_row_$id\" class=\"edit_rows hidden\">
                            <td><input type=\"text\" class=\"form-control\" name=\"software_$id\" id=\"edit_software_$id\" required=\"required\" value=\"$software\" maxlength=\"" . MAXLENGTH . "\"></td>
                            <td><select class=\"select custom-select form-control select-publisher\" name=\"publisher\" id=\"edit_publisher_$id\" required=\"required\">" . selectPublishers($publisherID) . "</select></td>
                            <td><input type=\"text\" class=\"form-control\" name=\"version_$id\" id=\"edit_version_$id\" required=\"required\" value=\"$version\" maxlength=\"" . MAXLENGTH . "\"></td>
                            <td><input type=\"text\" class=\"form-control\" name=\"remarks_$id\" id=\"edit_remarks_$id\" value=\"$remarks\" maxlength=\"" . MAXLENGTH . "\"></td>
                            <td><input type=\"checkbox\" name=\"visible_$id\" id=\"edit_visible_$id\"";
                    if ($visible == "t") {
                        echo " checked";
                    }
                    echo "></td><td>";
                    echo "<i class=\"icon fa fa-save fa-fw save_software_icon\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"$id\"></i>";
                    echo "<i class=\"icon fa fa-times fa-fw cancel_icon\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"$id\"></i>
                            </td>";
                }
                echo "</tr>";
            }
            echo "</tbody></table>";
        }
        break;

    case "publisher":
        if ($_SESSION['loggedin']) {
            $qry = $SELECT_publisher . " ORDER BY publisher_name;";
            $res1 = pg_query($con, $qry);
            $num1 = pg_num_rows($res1);

            echo "<p>" . $num1 . " " . _COUNT . "</p>";

            echo "<table id=\"publisher\" class=\"admintable generaltable\" style=\"width: 100%;\">
					<thead class=\"resizable sticky\">
					<tr>
						<th style=\"width: 80%;\">" . _PUBLISHER . "</th>
						<th style=\"width: 20%;\">" . _EDIT . "</th>
					</tr>
					</thead><tbody class=\"resizable\">";

            echo "<tr id=\"add_row\">
					<td><input type=\"text\" class=\"form-control\" name=\"publisher\" id=\"add_publisher\" required=\"required\" value=\"\" maxlength=\"" . MAXLENGTH . "\">
					<span id=\"publisher_missing\" class=\"invalid-feedback\">" . _PUBLISHER . " " . _MISSING . "</span></td>
					<td><i id=\"publisher_submit\" class=\"icon fa fa-plus fa-fw add_icon\" title=\"" . _ADD . "\" aria-label=\"" . _ADD . "\" name=\"add_publisher\"></i></td>
					</tr>";

            for ($i = 0; $i < $num1; $i++) {

                $row1 = pg_fetch_array($res1);
                $id = $row1['publisher_id'];
                $publisher = $row1['publisher_name'];

                echo "<tr id=\"row_$id\">
						<td id=\"publisher_$id\">$publisher</td>
						<td class=\"edit_row\"><i class=\"icon fa fa-edit fa-fw edit_icon\" title=\"" . _EDIT . "\" aria-label=\"" . _EDIT . "\" name=\"edit_row_$id\"></i><i class=\"icon fa fa-trash fa-fw delete_publisher_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" name=\"$id\"></i></td>
						</tr>";
                echo "<tr id=\"edit_row_$id\" class=\"edit_rows hidden\">
						<td><input type=\"text\" class=\"form-control\" name=\"publisher_$id\" id=\"edit_publisher_$id\" required=\"required\" value=\"$publisher\" maxlength=\"" . MAXLENGTH . "\"></td>
						<td>";
                echo "<i class=\"icon fa fa-save fa-fw save_publisher_icon\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"$id\"></i>";
                echo "<i class=\"icon fa fa-times fa-fw cancel_icon\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"$id\"></i>
						</td>
						</tr>";
            }
            echo "</tbody></table>";
        }
        break;

    case "sw_connections":
        if ($_SESSION['loggedin']) {
            echo "<div class=\"form-group fitem row\">
                <div class=\"col-md-2\"><label id=\"label_software_list\" class=\"col-form-label\" for=\"sw_list_conn\">" . _SOFTWARE . ": </label></div>
                <div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">";
            echo "<select id=\"sw_list_conn\" multiple=\"multiple\">" . selectSoftware() . "</select>";
            echo "<input type=\"hidden\" name=\"multiple_value\" id=\"multiple_value\" />";
            echo "<div class=\"form-control-feedback invalid-feedback\" id=\"id_missing_software\">" . _SOFTWARE . " " . _MISSING . "</div><br>";
            echo "</div>
            </div>";
            echo "<div class=\"form-group fitem row\">
                <div class=\"col-md-2\"></div>
                <div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
                <div class=\"form-control-feedback\" id=\"id_order\">* " . _CONSIDER_ORDER . "</div>
                </div>
            </div>";

            echo "<div class=\"form-group fitem row\">
                <div class=\"col-md-2 connection_buttons\">
                    <i id=\"connection_save_button\" class=\"icon fa fa-save fa-fw\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\"></i>
                </div>
            </div>";

            // show connections
            $qry = $SELECT_sw_connections;
            $res1 = pg_query($con, $qry);
            $num1 = pg_num_rows($res1);

            echo "<p>" . $num1 . " " . _COUNT . "</p>";

            echo "<table id=\"sw_connections\" class=\"admintable generaltable\" style=\"width: 100%;\">
                    <thead class=\"resizable sticky\">
                    <tr>
                        <th style=\"width: 80%;\">" . _SOFTWARE . "</th>
                        <th style=\"width: 20%;\">" . _DELETE . "</th>
                    </tr>
                    </thead><tbody class=\"resizable\">";

            for ($i = 0; $i < $num1; $i++) {
                $row1 = pg_fetch_array($res1);
                $id = $row1['entry_id'];

                $qry = $SELECT_sw_conn_entry . $id . " ORDER BY tbl_sw_connection.conn_id";
                $res2 = pg_query($con, $qry);
                $num2 = pg_num_rows($res2);
                $swValues = "";

                for ($j = 0; $j < $num2; $j++) {
                    $row2 = pg_fetch_array($res2);
                    $swID = $row2['sw_id'];
                    $swName = $row2['sw_name'];
                    $swValues .= "<span id=\"sw_id_$swID\">$swName</span> => ";
                }

                $swValues = substr($swValues, 0, strlen($swValues) - 3);

                echo "<tr id=\"row_$id\">
                        <td id=\"sw_values_$id\">$swValues</td>
                        <td class=\"edit_row\"><i class=\"icon fa fa-trash fa-fw delete_connection_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" id=\"delete_row_$id\" name=\"$id\"></i></td>
                        </tr>";

                $swValues = "";
            }
            echo "</tbody></table>";
        }
        break;

    default:
        echo "<h2>" . _WRONG_PARAMETER . "</h2>
				<p>&nbsp;</p>
				<i class=\"fa fa-exclamation fa-fw\" style=\"font-size: 200px; margin-left: 80px;\" title=\"\" aria-label=\"false\" name=\"\"></i>";
        break;
}

function selectExams($examID)
{
    include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
    include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

    $qry = $SELECT_dates_light . " AND
                    tbl_exam.disabled = false AND
                    tbl_exam.archive = false
                    ORDER BY tbl_exam_name.exam_name, tbl_exam.exam_date DESC";
    $res1 = pg_query($con, $qry);
    $num1 = pg_num_rows($res1);

    if ($examID == 0) {
        $selected = "selected";
    } else {
        $selected = "";
    }

    $exams = "<option name=\"" . _NO_EXAM_2 . "\" value=\"0\" disabled $selected>" . _NO_EXAM_2 . "</option>\n";

    for ($i = 0; $i < $num1; $i++) {
        $row1 = pg_fetch_array($res1);
        $id = $row1['id'];
        $examNameID = $row1['exam_name_id'];
        $examNr = $row1['exam_nr'];
        $examName = $row1['exam_name'];
        $lecturer = $row1['lecturer_lastname'];
        $date = date_format(date_create($row1['date']), 'Y-m-d');

        if ($examID == $id) {
            $selected = "selected";
        } else {
            $selected = "";
        }
        $exams = $exams . "<option name=\"$examNameID\" value=\"$id\" data-href=\"$date\" $selected>$examName ($examNr) | $lecturer | $date</option>\n";
    }
    return $exams;
}

function selectConfigs($configID, $examNameID)
{
    include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
    include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

    $qry = $SELECT_exam_config . " AND tbl_exam_config.exam_name_id = $examNameID ORDER BY tbl_exam_name.exam_name, tbl_exam_config.updated_at DESC";
    $res1 = pg_query($con, $qry);
    $num1 = pg_num_rows($res1);

    $configs = "";

    for ($i = 0; $i < $num1; $i++) {
        $row1 = pg_fetch_array($res1);
        $id = $row1['id'];
        $examName = $row1['exam_name'];
        $semester = $row1['semester'];
        $files = $row1['files'];
        $date = date("Y-m-d");
        $semesterNow = getSemester($date);

        if ($semester == $semesterNow) {
            if ($configID == $id) {
                $selected = "selected";
            } else {
                $selected = "";
            }
            $configs = $configs . "<option value=\"$id\" data-href=\"$files\" $selected>$examName ($semester)</option>\n";
        }
    }
    return $configs;
}

function selectPoolSetup($poolID)
{
    include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
    include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

    // Include language
    if (isset($_SESSION['lang'])) {
        $lang = $_SESSION['lang'];
    } else {
        // default
        $qry = $SELECT_lang_default;
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $lang = $row['lang'];
    }

    $qry = $SELECT_vdi_pools . " AND tbl_language.lang_code = '$lang' ORDER BY tbl_vdi_pool.pool_id";
    $res1 = pg_query($con, $qry);
    $num1 = pg_num_rows($res1);

    $poolSetups = "";

    for ($i = 0; $i < $num1; $i++) {
        $row1 = pg_fetch_array($res1);
        $id = $row1['pool_id'];
        $name = $row1['description_lang'];

        if ($poolID == $id) {
            $selected = "selected";
        } else {
            $selected = "";
        }

        $poolSetups = $poolSetups . "<option value=\"$id\" $selected>$name</option>\n";
    }
    return $poolSetups;
}

function selectStatus($statusID, $vdiPoolID)
{
    include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
    include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

    // Include language
    if (isset($_SESSION['lang'])) {
        $lang = $_SESSION['lang'];
    } else {
        // default
        $qry = $SELECT_lang_default;
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $lang = $row['lang'];
    }

    $qry = $SELECT_exam_status . " AND tbl_language.lang_code = '$lang' ORDER BY tbl_exam_status.status_id";
    $res1 = pg_query($con, $qry);
    $num1 = pg_num_rows($res1);

    $qry = $SELECT_status_inactive;
    $res = pg_query($con, $qry);
    while ($row = pg_fetch_array($res)) {
        $inactive[] = $row['status_id'];
    }

    $status = "";

    for ($i = 0; $i < $num1; $i++) {
        $row1 = pg_fetch_array($res1);
        $id = $row1['status_id'];
        $name = $row1['status_lang'];

        if ($statusID == $id) {
            $selected = "selected";
        } else {
            $selected = "";
        }
        if ($vdiPoolID > 1 || !in_array($id, $inactive)) {
            $status = $status . "<option value=\"$id\" $selected>$name</option>\n";
        }
    }
    return $status;
}

function selectPublishers($publisherID)
{
    include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
    include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

    $qry = $SELECT_publisher;
    $res1 = pg_query($con, $qry);
    $num1 = pg_num_rows($res1);
    $publishers = "";

    for ($i = 0; $i < $num1; $i++) {
        $row1 = pg_fetch_array($res1);
        $id = $row1['publisher_id'];
        $publisher = $row1['publisher_name'];

        $publishers = $publishers . "<option value=\"$id\" title=\"$publisher\"";
        if ($publisherID == $id) {
            $publishers = $publishers . " selected";
        }
        $publishers = $publishers . ">$publisher</option>\n";
    }
    return $publishers;
}

function selectExamNames()
{
    include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
    include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

    $qry = $SELECT_exam_name;
    $res1 = pg_query($con, $qry);
    $num1 = pg_num_rows($res1);

    $examNames =  "<option name=\"" . _NO_EXAM . "\" value=\"0\" disabled selected>" . _NO_EXAM . "</option>\n";

    for ($i = 0; $i < $num1; $i++) {
        $row1 = pg_fetch_array($res1);
        $id = $row1['exam_name_id'];
        $examNumber = $row1['exam_number'];
        $examName = $row1['exam_name'];
        $examNames = $examNames . "<option value=\"$id\" name=\"$examNumber\">$examName ($examNumber)</option>\n";
    }
    return $examNames;
}

function selectSoftware()
{
    include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
    include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

    $qry = $SELECT_software . " AND tbl_vdi_software.sw_visible = 't' ORDER BY tbl_vdi_software.sw_name";
    $res1 = pg_query($con, $qry);
    $num1 = pg_num_rows($res1);

    $softwares = "";

    for ($i = 0; $i < $num1; $i++) {
        $row1 = pg_fetch_array($res1);
        $id = $row1['id'];
        $software = $row1['software'];
        $softwares = $softwares . "<option value=\"$id\">$software</option>\n";
    }
    return $softwares;
}

function selectPeople($user)
{
    include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
    include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

    $qry = $SELECT_people . " WHERE tbl_people.people_username IS NOT NULL ORDER BY tbl_people.people_lastname, tbl_people.people_firstname";
    $res1 = pg_query($con, $qry);
    $num1 = pg_num_rows($res1);

    $people = "<option name=\"" . _NO_PEOPLE . "\" value=\"0\" disabled selected>" . _NO_PEOPLE . "</option>\n";

    for ($i = 0; $i < $num1; $i++) {
        $row1 = pg_fetch_array($res1);
        $id = $row1['id'];
        $username = $row1['username'];
        $lastname = $row1['lastname'];
        $firstname = $row1['firstname'];

        if ($user == $username) {
            $disabled = "disabled";
        } else {
            $disabled = "";
        }
        $people = $people . "<option value=\"$id\" $disabled>$lastname $firstname ($username)</option>\n";
    }
    return $people;
}
