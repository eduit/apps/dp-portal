<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $pool_desc = htmlspecialchars($_POST["pool_desc"]);
    $pool_lang = htmlspecialchars($_POST["pool_lang"]);
    $info_text = htmlspecialchars($_POST["info_text"]);

    // Include language
    if (isset($_SESSION['lang'])) {
        $lang = $_SESSION['lang'];
        $qry = $SELECT_language . " WHERE lang_code='$lang'";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $lang_id = $row['lang_id'];
    } else {
        // default
        $qry = $SELECT_lang_default;
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $lang_id = $row['lang_id'];
    }

    $qry = "SELECT pool_id FROM tbl_vdi_pool WHERE pool_description='$pool_desc'";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $num = pg_num_rows($res);
    if ($num > 0) {
        $pool_id = $row['pool_id'];
    } else {
        $pool_id = '';
    }

    if (!$pool_id) {
        $response_array['status'] = 'success';

        $qry = $INSERT_vdi_pool . " VALUES ('$pool_desc') RETURNING pool_id";
        if ($res = pg_query($con, $qry)) {
            $response_array['status1'] = 'success';
            $row = pg_fetch_row($res);
            $pool_id = $row['0'];

            $qry = $SELECT_language;
            $res = pg_query($con, $qry);
            $num = pg_num_rows($res);

            for ($i = 0; $i < $num; $i++) {
                $row = pg_fetch_array($res);
                $lang_id_2 = $row['lang_id'];

                if ($lang_id_2 == $lang_id) {
                    $pool_lang_2 = $pool_lang;
                    $info_text_2 = $info_text;
                } else {
                    $pool_lang_2 = "empty";
                    $info_text_2 = "empty";
                }

                $qry = $INSERT_pool_lang . " VALUES ($pool_id, '$pool_lang_2', '$info_text_2', $lang_id_2)";
                if (pg_query($con, $qry)) {
                    $response_array['status2'][$i] = 'success';
                } else {
                    $response_array['status2'][$i] = 'error';
                    $response_array['status'] = 'error';
                }
            }
        } else {
            $response_array['status1'] = 'error';
            $response_array['status'] = 'error';
        }
    } else {
        $response_array['status'] = 'already existing';
    }

    pg_close($con);
    echo json_encode($response_array);
}
