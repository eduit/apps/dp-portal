<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['user_vho'] || $_SESSION['loggedin']) {
    $incident_id = $_POST["id"];
    $exam = $_POST["exam"];
    $student = $_POST["student"];
    $room = $_POST["room"];
    if ($student == 0) {
        $student = "null";
    }
    $computer = $_POST["computer"];
    if ($computer == 0) {
        $computer = "null";
    }
    $multiple_computers = $_POST["multiple_computers"];
    $description = htmlspecialchars($_POST["description"]);
    $file = $_POST["file"];
    $user = $_POST["user"];

    // update database
    $qry = $UPDATE_incident . " SET student_id=$student, room_id=$room, exam_id=$exam, computer_id=$computer, multiple_computers='$multiple_computers', description='$description', reported_by='$user', file='$file' WHERE incident_id=$incident_id";

    if (pg_query($con, $qry)) {
        $response_array['status'] = 'success';
    } else {
        $response_array['status'] = 'error';
    }

    pg_close($con);
    echo json_encode($response_array);
}
