<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin'] || $_SESSION['user_vho']) {

    // get sender e-mail address
    $qry = $SELECT_pref . " WHERE pref_name = 'no_reply'";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $no_reply = $row['pref_value'];

    $to = $_POST["to"];
    $subject = $_POST["subject"];
    $message = $_POST["message"];
    $headers = "From: " . $no_reply;

    $success = mail($to, $subject, $message, $headers);
    $response_array['send'] = $success;
    if ($success) {
        $response_array['status'] = "success";
    } else {
        $response_array['status'] = "error";
    }
    
    pg_close($con);
    echo json_encode($response_array);
}
