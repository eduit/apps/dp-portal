<?php

// Prepare checkboxes for dates
$checkboxes = "<p><label><input type=\"checkbox\" name=\"\" value=\"0\" id=\"show_disabled\"> " . _HIDDEN . "</label>
            <label><input type=\"checkbox\" name=\"\" value=\"0\" id=\"show_archive\"> " . _ARCHIVED . "</label>
            <button id=\"sharepoint\" class=\"ui-button ui-widget ui-corner-all\">" . _SHAREPOINT . "</button></p>";
echo $checkboxes;
echo "<div id=\"exam_list\">
        <p id=\"count\" class=\"hidden\"><span id=\"number\">0</span> " . _COUNT . "</p>
        <table id=\"exams\" class=\"admintable generaltable hidden\" style=\"width: 100%;\"></table>
        </div>";
echo "<script>
            $.getDates('init');
        </script>";
