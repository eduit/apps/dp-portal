<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['user_vho']) {

    // get environment
    $qry = $SELECT_pref . " WHERE pref_name = 'environment'";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $env = $row['pref_value'];

    // get website title
    $qry = $SELECT_pref . " WHERE pref_name = 'website_title'";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $website_title = $row['pref_value'];

    // get sender e-mail address
    $qry = $SELECT_pref . " WHERE pref_name = 'no_reply'";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $no_reply = $row['pref_value'];

    // get dp e-mail address
    $qry = $SELECT_pref . " WHERE pref_name = 'dp_mail'";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $dp_mail = $row['pref_value'];

    // get vho alert
    $qry = $SELECT_pref . " WHERE pref_name = 'vho_alert'";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $vho_alert = $row['pref_value'];

    // set parameters
    $account_id = $_POST["account_id"];
    $exam_id = $_POST["exam_id"];
    $student_id = $_POST["student_id"];
    $user = $_SESSION['username'];

    // update database
    $qry = $UPDATE_VHO_account . " SET exam_id=$exam_id, student_id=$student_id, in_use='t', used_by='$user' WHERE account_id=$account_id AND in_use='f'";

    if ($res = pg_query($con, $qry)) {
        $num = pg_affected_rows($res);
        if ($num > 0) {
            $response_array['status'] = 'success';
        } else {
            $response_array['status'] = 'already_in_use';
        }
    } else {
        $response_array['status'] = 'error';
    }

    // check accounts left
    $qry = $SELECT_VHO_accounts . " WHERE in_use = false AND archive = false";
    $res = pg_query($con, $qry);
    $num = pg_num_rows($res);

    $response_array['accounts_left'] = $num;

    // send mail to op
    if ($num < $vho_alert) {
        $to = $dp_mail;
        $subject = "VHO-Accounts bald aufgebraucht";
        $message = "Es sind nur noch $num VHO-Accounts im $website_title ($env) verfuegbar.";
        $headers = "From: " . $no_reply;

        $success = mail($to, $subject, $message, $headers);
        $response_array['send'] = $success;
        if ($success) {
            $response_array['mail'] = "success";
        } else {
            $response_array['mail'] = "error";
        }
    }

    pg_close($con);
    echo json_encode($response_array);
}
