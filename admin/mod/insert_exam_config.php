<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/func.php";

session_start();

if ($_SESSION['user_vho'] || $_SESSION['loggedin']) {
    $configID = htmlspecialchars($_POST["config_id"]);
    $examNameID = htmlspecialchars($_POST["exam_name_id"]);
    $semester = htmlspecialchars($_POST["semester"]);
    $swList = $_POST["sw"] ?? [];
    $files = $_POST["files"];
    $filepath = $_POST["filepath"];
    $remarks = htmlspecialchars($_POST["remarks"]);
    $username = htmlspecialchars($_POST["username"]);
    $users = $_POST["users"] ?? [];

    $qry = $SELECT_people_id . " WHERE people_username='$username'";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $num = pg_num_rows($res);

    if ($num > 0) {
        $peopleID = $row['people_id'];
    } else {
        $peopleID = '';
    }

    $check1 = false;

    if ($configID) {
        $qry = "SELECT semester FROM tbl_exam_config WHERE config_id=$configID LIMIT 1";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $num = pg_num_rows($res);
        if ($num > 0) {
            $semesterDB = $row['semester'];
            if ($semester == $semesterDB) {
                $check1 = true;
            } else {
                $qry = "SELECT config_id FROM tbl_exam_config WHERE exam_name_id=$examNameID AND people_id=$peopleID AND semester='$semester' LIMIT 1";
                $res = pg_query($con, $qry);
                $row = pg_fetch_assoc($res);
                $num = pg_num_rows($res);
                if ($num > 0) {
                    $check1 = true;
                    $configID = $row['config_id'];
                }
            }
        }
    }

    $i = 1;

    if (!$check1 || !$configID) {
        $qry = $INSERT_exam_config . " VALUES ($examNameID, '$semester', '$files', '$filepath', '$remarks', $peopleID) RETURNING config_id";
        $response_array['function'] = 'insert_new';
        if ($res = pg_query($con, $qry)) {
            $response_array['status' . $i] = 'success';
            $row = pg_fetch_row($res);
            $configID = $row['0'];
        } else {
            $response_array['status' . $i] = 'error';
            $response_array['status'] = 'error';
        }
    } else {
        $check2 = false;
        
        $qry = $SELECT_exam_deadline . $configID . " LIMIT 1";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $num = pg_num_rows($res);
        
        if ($num > 0) {
            $deadline = $row['deadline'];
            $deadline = date('Y-m-d', strtotime($deadline));
            $response_array['deadline'] = $deadline;
            $today = date("Y-m-d");
            $check2 = $deadline > $today;
        } else {
            $check2 = true;
        }

        if ($check2) {
            $qry = $UPDATE_exam_config . " SET files='$files', filepath='$filepath', config_remarks='$remarks' WHERE config_id = $configID;";
            $response_array['function'] = 'update_existing';
            if (pg_query($con, $qry)) {
                $response_array['status' . $i] = 'success';
            } else {
                $response_array['status' . $i] = 'error';
                $response_array['status'] = 'error';
            }

            $qry = $DELETE_config_sw . " WHERE config_id = $configID;";
            $i++;

            if (pg_query($con, $qry)) {
                $response_array['status' . $i] = 'success';
            } else {
                $response_array['status' . $i] = 'error';
                $response_array['status'] = 'error';
            }

            $qry = $DELETE_config_people . " WHERE config_id = $configID;";
            $i++;

            if (pg_query($con, $qry)) {
                $response_array['status' . $i] = 'success';
            } else {
                $response_array['status' . $i] = 'error';
                $response_array['status'] = 'error';
            }
        } else {
            $response_array['status'] = 'deadline expired';
            $configID = '';
        }
    }

    if ($configID) {
        $swValues  = '';
        $userValues  = '';
        
        foreach ($swList as $sw) {
            $swValues  .= "($configID, $sw),";
        }
        $swValues  = substr($swValues , 0, -1);
        
        $qry = $INSERT_config_sw . " VALUES $swValues ";
        $i++;

        if (pg_query($con, $qry)) {
            $response_array['status' . $i] = 'success';
        } else {
            $response_array['status' . $i] = 'error';
            $response_array['status'] = 'error';
        }

        foreach ($users as $user) {
            $userValues  .= "($configID, $user),";
        }
        $userValues  = substr($userValues , 0, -1);

        if ($userValues ) {
            $qry = $INSERT_config_people . " VALUES $userValues ";
            $i++;

            if (pg_query($con, $qry)) {
                $response_array['status' . $i] = 'success';
            } else {
                $response_array['status' . $i] = 'error';
                $response_array['status'] = 'error';
            }
        }
    }

    pg_close($con);
    echo json_encode($response_array);
}
