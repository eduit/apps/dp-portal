<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$exam_id = $_POST["exam_id"];
	$date = date_create($_POST["date"]);
	$date = date_format($date, "Y-m-d");
	$exam_name_id = $_POST["exam_name_id"];
	$type = $_POST["type"];
	$lang = $_POST["lang"];
	$lecturer = $_POST["lecturer"];
	$vdp = $_POST["vdp"];
	$sysadmin = $_POST["sysadmin"];
	$url = $_POST["url"];
	$seb = $_POST["seb"];

	$qry = $UPDATE_exam . " SET exam_date='$date', exam_name_id='$exam_name_id', type_id='$type', lang_id='$lang', exam_link='$url', exam_seb='$seb' WHERE exam_id=$exam_id";

	if (pg_query($con, $qry)) {
		$response_array['status1'] = 'success';
	} else {
		$response_array['status1'] = 'error';
	}

	$queries[1] = $INSERT_exam_people . " VALUES ($exam_id, $lecturer, 1) ON CONFLICT (exam_id, function_id) DO UPDATE SET people_id = $lecturer";
	$queries[2] = $INSERT_exam_people . " VALUES ($exam_id, $vdp, 3) ON CONFLICT (exam_id, function_id) DO UPDATE SET people_id = $vdp";
	$queries[3] = $INSERT_exam_people . " VALUES ($exam_id, $sysadmin, 4) ON CONFLICT (exam_id, function_id) DO UPDATE SET people_id = $sysadmin";

	foreach ($queries as $key => $qry) {
		if (pg_query($con, $qry)) {
			$response_array[$key]['status'] = 'success';
		} else {
			$response_array[$key]['status'] = 'error';
		}
	}

	pg_close($con);
	echo json_encode($response_array);
}
