<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

$poolID = $_POST['id'];

// Include language file
if (isset($_SESSION['lang'])) {
    $lang_code = $_SESSION['lang'];
} else {
    $lang_code = "de";
}

if ($_SESSION['loggedin'] || $_SESSION['user_vho']) {
    // get language_id
    $qry = $SELECT_language . " WHERE lang_code = '$lang_code'";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $lang_id = $row['lang_id'];

    // get info_text
    $qry = $SELECT_pool_lang_info_text . " WHERE tbl_pool_lang.lang_id=$lang_id AND tbl_pool_lang.pool_id=$poolID LIMIT 1";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $num = pg_num_rows($res);
    $info_text = $row['info_text'];

    if ($num > 0) {
        $response_array['info_text'] = $info_text;
    } else {
        $response_array['status'] = "error";
    }

    pg_close($con);

    echo json_encode($response_array);
}
