<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin'] || $_SESSION['user_vho']) {
    // set parameters
    $account_id = $_POST["account_id"];
    $exam_id = $_POST["exam_id"];
    $student_id = $_POST["student_id"];

    // get account data
    $qry = $SELECT_VHO_accounts . " WHERE account_id = $account_id";
    $res1 = pg_query($con, $qry);
    $row1 = pg_fetch_assoc($res1);
    $account_username = $row1['username'];
    $account_pw = $row1['password'];

    // get exam data
    $qry = $SELECT_exam_name_by_id . $exam_id;
    $res2 = pg_query($con, $qry);
    $row2 = pg_fetch_assoc($res2);
    $num2 = pg_num_rows($res2);
    $exam_nr = $row2['exam_nr'];
    $exam = $row2['exam_name'];
    $exam_date = $row2['exam_date'];

    // get student data
    $qry = $SELECT_students . " WHERE student_id=$student_id";
    $res3 = pg_query($con, $qry);
    $row3 = pg_fetch_assoc($res3);
    $student_lastname = $row3['student_lastname'];
    $student_firstname = $row3['student_firstname'];
    $student_registraiton = $row3['student_matrikelnr'];

    // get user data
    $username = $_SESSION['user_firstname'] . " " . $_SESSION['user_lastname'];

    // set subject text
    $subject = "Exam ###_LK_NUMBER_### ###_LK_TITLE_###: Temporary Account ###_STUDENT_PRENAME_### ###_STUDENT_NAME_###";

    // get template text
    $qry = $SELECT_templates . " AND type='vho-mail' ORDER BY lang_id";
    $res4 = pg_query($con, $qry);
    $num4 = pg_num_rows($res4);
    $content = '';

    for ($i = 0; $i < $num4; $i++) {
        $row4 = pg_fetch_array($res4);
        $lang_id = $row4['lang_id'];
        $text = $row4['text'];

        $content = $content . $text;

        if ($i < $num4-1) {
            $content = $content . "<br><br>";
        }

        // replace template fields
        $array = [
            "<br>" => "%0D%0A",
            "###_LK_NUMBER_###" => $exam_nr,
            "###_LK_TITLE_###" => $exam,
            "###_DATE_###" => $exam_date,
            "###_STUDENT_PRENAME_###" => $student_firstname,
            "###_STUDENT_NAME_###" => $student_lastname,
            "###_REGISTRATION_###" => $student_registraiton,
            "###_USER_NAME_###" => $username
        ];

        foreach ($array as $key => $value) {
            $subject = str_replace($key, $value, $subject);
            $content = str_replace($key, $value, htmlspecialchars_decode($content));
        }
    }

    pg_close($con);

    $response_array['exam'] = $exam;
    $response_array['student'] = $student_lastname . " " . $student_firstname;
    $response_array['subject'] = strip_tags(htmlspecialchars_decode($subject));
    $response_array['content'] = strip_tags($content);

    echo json_encode($response_array);
}
