<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $theme = htmlspecialchars($_POST["theme"]);
    $values = htmlspecialchars($_POST["values"]);

    $qry = "SELECT theme_id FROM tbl_vho_themes WHERE theme_name='$theme' AND theme_values='$values'";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $num = pg_num_rows($res);
    if ($num > 0) {
        $theme_id = $row['theme_id'];
    } else {
        $theme_id = '';
    }

    if (!$theme_id) {
        $qry = $INSERT_VHO_theme . " VALUES ('$theme', '$values')";

        if (pg_query($con, $qry)) {
            $response_array['status'] = 'success';
        } else {
            $response_array['status'] = 'error';
        }
    } else {
        $response_array['status'] = 'already existing';
    }

    pg_close($con);
    echo json_encode($response_array);
}
