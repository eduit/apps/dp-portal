<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/func.php";

// Include language file
if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else {
    // default
    $qry = $SELECT_lang_default;
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $lang = $row['lang'];
}
include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

session_start();

if ($_SESSION['loggedin']) {
    $tabSelected = $_POST['tab'] ?? '';
    $function = str_replace(' ', '_', strtolower($tabSelected));

    // start caching
    $cachefile = cacheStart();

    $content = '';
    $entries = '';
    $headerEntries = '';
    $num = 0;

    // get parents
    $horizonScript = $_SERVER["DOCUMENT_ROOT"] . "/inc/horizon/get_parents.py";
    $json = shell_exec("python3 " . $horizonScript);
    $parents = json_decode($json, true);
    $columns = array('name');
    $sortby = $columns[0];
    $keyValues = array_column($parents, $sortby);
    array_multisort($keyValues, SORT_NATURAL, $parents);

    // headers
    $columns = array(
        ['name', _SNAPSHOT],
        ['description', _DESCRIPTION],
        ['created_timestamp', _CREATED]
    );
    $headerEntries = $headerEntries . "<th>" . _PARENT . "<br /></th>";
    foreach ($columns as $i => $header) {
        $headerEntries = $headerEntries . "<th>$header[1]<br /></th>";
        $cells[$i] = $header[0];
    }

    foreach ($parents as $item) {
        $parentID = $item['id'];
        $parent = $item['name'];

        // get snapshots
        $horizonScript = $_SERVER["DOCUMENT_ROOT"] . "/inc/horizon/get_snapshots.py";
        $json = shell_exec("python3 " . $horizonScript . " " . $parentID);
        $snapshots = json_decode($json, true);
        $sortby = $columns[2][0];
        $keyValues = array_column($snapshots, $sortby);
        array_multisort($keyValues, SORT_ASC, $snapshots);
        $num = $num + count($snapshots) ?? 0;

        foreach ($snapshots as $key => $item) {
            $id = $item['id'];
            $name = $item['name'];
            $date = substr($name, 0, 10);
            $now = new DateTime('today midnight');
            $css = "";
            if (isValidDate(($date))) {
                $valid = true;
                $date_formatted = date_create($date);
                if ($now > $date_formatted) {
                    $css = "style=\"font-style: italic\"";
                } else {
                    $css = "style=\"font-weight: bold\"";
                }
            } else {
                $valid = false;
            }
            $entries = $entries . "<tr id=\"" . $id . "\">";
            $entries = $entries . "<td id=\"parent_" . $key . "\">" . $parent . "</td>";
            foreach ($cells as $cell) {
                $entries = $entries . "<td id=\"" . $cell . "_" . $key . "\">";
                $entry = $item[$cell] ?? null;
                if ($cell == 'created_timestamp') {
                    $seconds = $entry / 1000;
                    $entry = date('d.m.Y H:i:s', $seconds);
                }
                if ($cell == 'name' && $valid) {
                    $entry = "<span $css>$entry</span>";
                }
                $entries = $entries . $entry;
                $entries = $entries . "</td>";
            }
            $entries = $entries . "</tr>";
        }
    }

    $content = $content . "<p>" . $num . " " . _COUNT . "</p>";
    $content = $content . "<table id=\"$function-entries\" class=\"admintable generaltable\" style=\"width: 100%;\"><thead class=\"resizable sticky\"><tr>";
    $content = $content . $headerEntries;
    $content = $content . "</tr></thead><tbody class=\"resizable\">";
    $content = $content . $entries;
    $content = $content . "</tbody></table>";
    $content = $content .  "<script>
        $(function() {
            $(\"#$function-entries\").excelTableFilter();
            let rowCount = $(\"#all_subscriptions tbody tr:visible\").length;
            $(\"#count\").html(rowCount+\"/\");
            if (/chrom(e|ium)/.test(navigator.userAgent.toLowerCase())) {
                $(\".dropdown-filter-content\").parent().parent(\"th\").addClass(\"drp_menu\");
            }
        });
    </script>";

    $response_array['content'] = $content;
    echo json_encode($response_array);

    // stop caching
    cacheEnd($cachefile);
}
