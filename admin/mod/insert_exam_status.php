<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $status = htmlspecialchars($_POST["status"]);
    $status_lang = htmlspecialchars($_POST["status_lang"]);
    $color = htmlspecialchars($_POST["color"]);
    $standard = $_POST["standard"];

    // Include language
    if (isset($_SESSION['lang'])) {
        $lang = $_SESSION['lang'];
        $qry = $SELECT_language . " WHERE lang_code='$lang'";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $lang_id = $row['lang_id'];
    } else {
        // default
        $qry = $SELECT_lang_default;
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $lang_id = $row['lang_id'];
    }

    $qry = "SELECT status_id FROM tbl_exam_status WHERE status='$status'";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $num = pg_num_rows($res);
    if ($num > 0) {
        $status_id = $row['status_id'];
    } else {
        $status_id = '';
    }

    if (!$status_id) {
        $response_array['status'] = 'success';

        $qry = $INSERT_exam_status . " VALUES ('$status', '$color', '$standard') RETURNING status_id";
        if ($res = pg_query($con, $qry)) {
            $response_array['status1'] = 'success';
            $row = pg_fetch_row($res);
            $status_id = $row['0'];

            $qry = $SELECT_language;
            $res = pg_query($con, $qry);
            $num = pg_num_rows($res);

            for ($i = 0; $i < $num; $i++) {
                $row = pg_fetch_array($res);
                $lang_id_2 = $row['lang_id'];

                if ($lang_id_2 == $lang_id) {
                    $status_lang_2 = $status_lang;
                } else {
                    $status_lang_2 = "empty";
                }

                $qry = $INSERT_status_lang . " VALUES ($status_id, '$status_lang_2', $lang_id_2)";
                if (pg_query($con, $qry)) {
                    $response_array['status2'][$i] = 'success';
                } else {
                    $response_array['status2'][$i] = 'error';
                    $response_array['status'] = 'error';
                }
            }
        } else {
            $response_array['status1'] = 'error';
            $response_array['status'] = 'error';
        }
    } else {
        $response_array['status'] = 'already existing';
    }

    pg_close($con);
    echo json_encode($response_array);
}
