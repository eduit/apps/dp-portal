<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin'] || $_SESSION['user_vho']) {
    $qry = $SELECT_sw_connections;
    $res1 = pg_query($con, $qry);
    $num1 = pg_num_rows($res1);

    for ($i = 0; $i < $num1; $i++) {
        $row1 = pg_fetch_array($res1);
        $id = $row1['entry_id'];

        $qry = $SELECT_sw_conn_entry . $id . " ORDER BY tbl_sw_connection.conn_id";
        $res2 = pg_query($con, $qry);
        $num2 = pg_num_rows($res2);

        for ($j = 0; $j < $num2; $j++) {
            $row2 = pg_fetch_array($res2);
            $swID = $row2['sw_id'];
            $swName = $row2['sw_name'];
            $response_array[$id][$j]['id'] = $swID;
            $response_array[$id][$j]['name'] = $swName;
        }
    }

    pg_close($con);
    echo json_encode($response_array);
}
