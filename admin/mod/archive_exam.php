<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$id = $_POST["id"];
	$archived = $_POST["archived"];

	if ($archived == 't') {
		$archived = 'true';
	} else {
		$archived = 'false';
	}

	$qry = $UPDATE_exam . " SET archive=$archived, disabled='false' WHERE exam_id=$id";

	if (pg_query($con, $qry)) {
		$response_array['status'] = 'success';
	} else {
		$response_array['status'] = 'error';
	}

	pg_close($con);
	echo json_encode($response_array);
}
