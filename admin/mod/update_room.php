<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $roomID = $_POST["room_id"];
    $room = htmlspecialchars($_POST["room"]);
    $roomDN = htmlspecialchars($_POST["room_display_name"]);
    $refresh = $_POST["refresh"];

    // update database
    $qry = $UPDATE_room . " SET room_name='$room', room_display_name='$roomDN', auto_refresh='$refresh' WHERE room_id=$roomID";

    if (pg_query($con, $qry)) {
        $response_array['status'] = 'success';
    } else {
        $response_array['status'] = 'error';
    }

    pg_close($con);
    echo json_encode($response_array);
}
