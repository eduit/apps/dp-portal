<?php

if ($_SESSION['loggedin']) {
    getRoomData($tab);
}

function getRoomData($tab)
{
    include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
    include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

    switch ($tab) {
        case 'rooms':
            $qry = $SELECT_rooms . " ORDER BY tbl_room.room_name";
            $res = pg_query($con, $qry);
            $num = pg_num_rows($res);

            echo "<p>" . $num . " " . _COUNT . "</p>";

            echo "<table id=\"rooms\" class=\"admintable generaltable\" style=\"width: 100%;\">
					<thead class=\"resizable sticky\">
					<tr>
						<th style=\"width: 25%;\">" . _ROOM . "</th>
						<th style=\"width: 25%;\">" . _ROOM_DISPLAY_NAME . "</th>
						<th style=\"width: 25%;\">" . _REFRESH . "</th>
                        <th style=\"width: 25%;\">" . _EDIT . "</th>
					</tr>
					</thead><tbody class=\"resizable\">";

            echo "<tr id=\"add_row\">
					<td><input type=\"text\" class=\"form-control\" name=\"room\" id=\"add_room\" required=\"required\" value=\"\" maxlength=\"" . MAXLENGTH . "\"><span id=\"room_missing\" class=\"invalid-feedback\">" . _ROOM . " " . _MISSING . "</span></td>
					<td><input type=\"text\" class=\"form-control\" name=\"room_diplay_name\" id=\"add_room_display_name\" value=\"\" maxlength=\"" . MAXLENGTH . "\"></td>
                    <td><input type=\"checkbox\" name=\"refresh\" id=\"add_refresh\" value=\"\"></td>
					<td><i id=\"room_submit\" class=\"icon fa fa-plus fa-fw add_icon\" title=\"" . _ADD . "\" aria-label=\"" . _ADD . "\" name=\"add_room\"></i></td>
                </tr>";

            for ($i = 0; $i < $num; $i++) {

                $row = pg_fetch_array($res);
                $id = $row['room_id'];
                $room = $row['room_name'];
                $roomDN = $row['room_display_name'];
                $refresh = $row['auto_refresh'];

                echo "<tr id=\"row_" . $id . "\">
						<td id=\"room_" . $id . "\">" . $room . "</td>
						<td id=\"room_display_name_" . $id . "\">" . $roomDN . "</td>";
                echo "<td id=\"refresh_" . $id . "\">";
                if ($refresh == "t") {
                    echo "<i class=\"fa fa-check-square-o fa-fw true_icon\" title=\"" . _TRUE . "\" aria-label=\"true\" name=\"refresh_" . $id . "\"></i>";
                    echo "<span style=\"opacity: 0;\">" . _TRUE . "</span>";
                } else {
                    echo "<i class=\"fa fa-square-o fa-fw false_icon\" title=\"" . _FALSE . "\" aria-label=\"false\" name=\"refresh_" . $id . "\"></i>";
                    echo "<span style=\"opacity: 0;\">" . _FALSE . "</span>";
                }
                echo "</td>";
                echo "<td><i class=\"icon fa fa-edit fa-fw edit_icon\" title=\"" . _EDIT . "\" aria-label=\"" . _EDIT . "\" name=\"edit_row_" . $id . "\"></i><i class=\"icon fa fa-trash fa-fw delete_room_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" name=\"" . $id . "\"></i></td>
						</tr>";
                echo "<tr id=\"edit_row_" . $id . "\" class=\"edit_rows hidden\">
						<td><input type=\"text\" class=\"form-control\" name=\"room_" . $id . "\" id=\"edit_room_" . $id . "\" required=\"required\" value=\"" . $room . "\" maxlength=\"" . MAXLENGTH . "\"></td>
						<td><input type=\"text\" class=\"form-control\" name=\"room_display_name_" . $id . "\" id=\"edit_room_display_name_" . $id . "\" value=\"" . $roomDN . "\" maxlength=\"" . MAXLENGTH . "\"></td>
                        <td><input type=\"checkbox\" name=\"refresh_" . $id . "\" id=\"edit_refresh_" . $id . "\"";
                if ($refresh == "t") {
                    echo " checked";
                }
                echo "></td><td>";
				echo "<i class=\"icon fa fa-save fa-fw save_room_icon\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"" . $id . "\"></i>";
                echo "<i class=\"icon fa fa-times fa-fw cancel_icon\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"" . $id . "\"></i>
						</td>
						</tr>";
            }
            echo "</tbody></table>";
            break;

        case 'sectors':
            $qry = $SELECT_sectors;
            $res = pg_query($con, $qry);
            $num = pg_num_rows($res);

            echo "<p>" . $num . " " . _COUNT . "</p>";

            echo "<table id=\"sectors\" class=\"admintable generaltable\" style=\"width: 100%;\">
					<thead class=\"resizable sticky\">
					<tr>
						<th style=\"width: 75%;\">" . _SECTOR . "</th>
                        <th style=\"width: 25%;\">" . _EDIT . "</th>
					</tr>
					</thead><tbody class=\"resizable\">";

            echo "<tr id=\"add_row\">
					<td><input type=\"text\" class=\"form-control\" name=\"sector\" id=\"add_sector\" required=\"required\" value=\"\" maxlength=\"" . MAXLENGTH . "\"><span id=\"sector_missing\" class=\"invalid-feedback\">" . _SECTOR . " " . _MISSING . "</span></td>
					<td><i id=\"sector_submit\" class=\"icon fa fa-plus fa-fw add_icon\" title=\"" . _ADD . "\" aria-label=\"" . _ADD . "\" name=\"add_sector\"></i></td>
                </tr>";

            for ($i = 0; $i < $num; $i++) {

                $row = pg_fetch_array($res);
                $id = $row['sector_id'];
                $sector = $row['sector_name'];

                echo "<tr id=\"row_" . $id . "\">
						<td id=\"sector_" . $id . "\">" . $sector . "</td>";
                echo "<td><i class=\"icon fa fa-edit fa-fw edit_icon\" title=\"" . _EDIT . "\" aria-label=\"" . _EDIT . "\" name=\"edit_row_" . $id . "\"></i><i class=\"icon fa fa-trash fa-fw delete_sector_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" name=\"" . $id . "\"></i></td>
						</tr>";
                echo "<tr id=\"edit_row_" . $id . "\" class=\"edit_rows hidden\">
						<td><input type=\"text\" class=\"form-control\" name=\"sector_" . $id . "\" id=\"edit_sector_" . $id . "\" required=\"required\" value=\"" . $sector . "\" maxlength=\"" . MAXLENGTH . "\"></td>
						<td>";
					echo "<i class=\"icon fa fa-save fa-fw save_sector_icon\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"" . $id . "\"></i>";
                    echo "<i class=\"icon fa fa-times fa-fw cancel_icon\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"" . $id . "\"></i>
						</td>
						</tr>";
            }
            echo "</tbody></table>";
            break;
        case 'rooms_sectors':
            $qry = $SELECT_room_sector;
            $res = pg_query($con, $qry);
            $num = pg_num_rows($res);

            echo "<p>" . $num . " " . _COUNT . "</p>";

            echo "<table id=\"room_sector\" class=\"admintable generaltable\" style=\"width: 100%;\">
					<thead class=\"resizable sticky\">
					<tr>
						<th style=\"width: 25%;\">" . _ROOM . "</th>
                        <th style=\"width: 25%;\">" . _SECTOR . "</th>
                        <th style=\"width: 25%;\">" . _SECTOR_DISPLAY_NAME . "</th>
                        <th style=\"width: 25%;\">" . _EDIT . "</th>
					</tr>
					</thead><tbody class=\"resizable\">";

            echo "<tr id=\"add_row\">
                    <td><select class=\"select custom-select form-control select-room\" name=\"room\" id=\"add_room\">" . selectRooms(0) . "</select><br><span id=\"room_missing\" class=\"invalid-feedback\">" . _ROOM . " " . _MISSING . "</span></td>
                    <td><select class=\"select custom-select form-control select-sector\" name=\"sector\" id=\"add_sector\">" . selectSectors(0) . "</select><br><span id=\"sector_missing\" class=\"invalid-feedback\">" . _SECTOR . " " . _MISSING . "</span></td>
					<td><input type=\"text\" class=\"form-control\" name=\"sector_display_name\" id=\"add_sector_display_name\" value=\"\" maxlength=\"" . MAXLENGTH . "\"></td>
					<td><i id=\"room_sector_submit\" class=\"icon fa fa-plus fa-fw add_icon\" title=\"" . _ADD . "\" aria-label=\"" . _ADD . "\" name=\"add_room_sector\"></i></td>
                </tr>";

            for ($i = 0; $i < $num; $i++) {

                $row = pg_fetch_array($res);
                $id = $row['room_sector_id'];
                $roomID = $row['room_id'];
                $roomName = $row['room_name'];
                $sectorID = $row['sector_id'];
                $sectorName = $row['sector_name'];
                $sectorDN = $row['sector_display_name'];

                echo "<tr id=\"row_" . $id . "\">
                        <td id=\"room_" . $id . "\">" . $roomName . "</td>
						<td id=\"sector_" . $id . "\">" . $sectorName . "</td>
                        <td id=\"sector_display_name_" . $id . "\">" . $sectorDN . "</td>";
                echo "<td>";
                echo "<i class=\"icon fa fa-edit fa-fw edit_icon\" title=\"" . _EDIT . "\" aria-label=\"" . _EDIT . "\" name=\"edit_row_" . $id . "\"></i>";
                echo "<i class=\"icon fa fa-trash fa-fw delete_room_sector_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" name=\"" . $id . "\"></i>
                        </td></tr>";
                echo "<tr id=\"edit_row_" . $id . "\" class=\"edit_rows hidden\">
                        <td><select class=\"select custom-select form-control select-room\" name=\"room\" id=\"edit_room_$id\">" . selectRooms($roomID) . "</select></td>
                        <td><select class=\"select custom-select form-control select-sector\" name=\"sector\" id=\"edit_sector_$id\">" . SelectSectors($sectorID) . "</select></td>
						<td><input type=\"text\" class=\"form-control\" name=\"sector_display_name_" . $id . "\" id=\"edit_sector_display_name_" . $id . "\" value=\"" . $sectorDN . "\" maxlength=\"" . MAXLENGTH . "\"></td>
						<td>";
				echo "<i class=\"icon fa fa-save fa-fw save_room_sector_icon\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"" . $id . "\"></i>";
                echo "<i class=\"icon fa fa-times fa-fw cancel_icon\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"" . $id . "\"></i>
						</td>
						</tr>";
            }
            echo "</tbody></table>";

            break;
        case 'vdi_users':
            $qry = $SELECT_exam_users . " WHERE exam_user_id > 0 ORDER BY exam_username";
            $res = pg_query($con, $qry);
            $num = pg_num_rows($res);

            echo "<p>" . $num . " " . _COUNT . "</p>";

            echo "<table id=\"exam_users\" class=\"admintable generaltable\" style=\"width: 100%;\">
					<thead class=\"resizable sticky\">
					<tr>
						<th style=\"width: 40%;\">" . _USER . "</th>
						<th style=\"width: 40%;\">" . _SID . "</th>
                        <th style=\"width: 20%;\">" . _EDIT . "</th>
					</tr>
					</thead><tbody class=\"resizable\">";

            echo "<tr id=\"add_row\">
					<td><input type=\"text\" class=\"form-control\" name=\"username\" id=\"add_username\" required=\"required\" value=\"\" maxlength=\"" . MAXLENGTH . "\"><span id=\"username_missing\" class=\"invalid-feedback\">" . _USER . " " . _MISSING . "</span></td>
					<td><input type=\"text\" class=\"form-control\" name=\"sid\" id=\"add_sid\" value=\"\" maxlength=\"" . MAXLENGTH . "\"></td>
					<td><i id=\"exam_user_submit\" class=\"icon fa fa-plus fa-fw add_icon\" title=\"" . _ADD . "\" aria-label=\"" . _ADD . "\" name=\"add_exam_user\"></i></td>
                </tr>";

            for ($i = 0; $i < $num; $i++) {

                $row = pg_fetch_array($res);
                $id = $row['exam_user_id'];
                $username = $row['exam_username'];
                $sid = $row['exam_user_sid'];

                echo "<tr id=\"row_" . $id . "\">
						<td id=\"exam_username_" . $id . "\">" . $username . "</td>
						<td id=\"exam_user_sid_" . $id . "\">" . $sid . "</td>";
                echo "<td><i class=\"icon fa fa-edit fa-fw edit_icon\" title=\"" . _EDIT . "\" aria-label=\"" . _EDIT . "\" name=\"edit_row_" . $id . "\"></i><i class=\"icon fa fa-trash fa-fw delete_exam_user_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" name=\"" . $id . "\"></i></td>
						</tr>";
                echo "<tr id=\"edit_row_" . $id . "\" class=\"edit_rows hidden\">
						<td><input type=\"text\" class=\"form-control\" name=\"exam_user_" . $id . "\" id=\"edit_exam_user_" . $id . "\" required=\"required\" value=\"" . $username . "\" maxlength=\"" . MAXLENGTH . "\"></td>
						<td><input type=\"text\" class=\"form-control\" name=\"exam_user_sid_" . $id . "\" id=\"edit_exam_user_sid_" . $id . "\" value=\"" . $sid . "\" maxlength=\"" . MAXLENGTH . "\"></td>
                        <td>";
				echo "<i class=\"icon fa fa-save fa-fw save_exam_user_icon\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"" . $id . "\"></i>";
                echo "<i class=\"icon fa fa-times fa-fw cancel_icon\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"" . $id . "\"></i>
						</td>
						</tr>";
            }
            echo "</tbody></table>";
            break;
        default:
            echo "<h2>" . _WRONG_PARAMETER . "</h2>
				<p>&nbsp;</p>
				<i class=\"fa fa-exclamation fa-fw\" style=\"font-size: 200px; margin-left: 80px;\" title=\"\" aria-label=\"false\" name=\"\"></i>";
            break;
    }
}

function selectSectors($selected)
{
    include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
    include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

    $qry = $SELECT_sectors . " ORDER BY sector_name";
    $res = pg_query($con, $qry);
    $num = pg_num_rows($res);
    $sectors = "<option name=\"" . _NO_SECTOR . "\" value=\"0\" disabled selected>" . _NO_SECTOR . "</option>\n";

    for ($i = 0; $i < $num; $i++) {
        $row = pg_fetch_array($res);
        $sectorID = $row['sector_id'];
        $sector = $row['sector_name'];

        $sectors = $sectors . "<option value=\"" . $sectorID . "\" title=\"" . $sector . "\"";
        if ($sectorID == $selected) {
            $sectors = $sectors . " selected";
        }
        $sectors = $sectors . ">" . $sector . "</option>\n";
    }
    return $sectors;
}

function selectRooms($selected)
{
    include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
    include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

    $qry = $SELECT_rooms . " ORDER BY room_name";
    $res = pg_query($con, $qry);
    $num = pg_num_rows($res);
    $rooms = "<option name=\"" . _NO_ROOM . "\" value=\"0\" disabled selected>" . _NO_ROOM . "</option>\n";

    for ($i = 0; $i < $num; $i++) {
        $row = pg_fetch_array($res);
        $roomID = $row['room_id'];
        $room = $row['room_name'];

        $rooms = $rooms . "<option value=\"" . $roomID . "\" title=\"" . $room . "\"";
        if ($roomID == $selected) {
            $rooms = $rooms . " selected";
        }
        $rooms = $rooms . ">" . $room . "</option>\n";
    }
    return $rooms;
}
