<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

// include language file
if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else {
    // default
    $qry = $SELECT_lang_default;
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $lang = $row['lang'];
}
include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

if ($_SESSION['loggedin']) {
    $tabSelected = $_POST['tab'] ?? '';
    $function = str_replace(' ', '_', strtolower($tabSelected));
    $content = '';

    // get desktop pools
    $horizonScript = $_SERVER["DOCUMENT_ROOT"] . "/inc/horizon/get_desktop_pools.py";
    $json = shell_exec("python3 " . $horizonScript);
    $desktopPools = json_decode($json, true);

    // get parents
    $horizonScript = $_SERVER["DOCUMENT_ROOT"] . "/inc/horizon/get_parents.py";
    $json = shell_exec("python3 " . $horizonScript);
    $parents = json_decode($json, true);

    // golden_image, snapshot_1, state_1, secondary_image, snapshot_2, state_2
    $columns = array(
        ['name', _POOLNAME],
        ['enable_provisioning', _ENABLED],
        ['machines', _VDI_AVAILABLE],
        ['machines_connected', _VDI_CONNECTED],
        ['pattern_naming_settings_1', _VDI_MAX],
        ['provisioning_settings_1', _PARENT],
        ['provisioning_settings_2', _SNAPSHOT],
        ['provisioning_status_data_1', _STATUS],
        ['provisioning_status_data_2', _SECONDARY . " " . _PARENT],
        ['provisioning_status_data_3', _SECONDARY . " " . _SNAPSHOT],
        ['provisioning_status_data_4', _STATUS]
    );
    $entries = $desktopPools;
    $functions = array(
        ['edit', 'edit', 0],
        ['delete_machines', 'trash', 'delete_machines_from_pool']
    );

    $num = count($entries) ?? 0;
    $sortby = $columns[0][0];
    $keyValues = array_column($entries, $sortby);
    array_multisort($keyValues, SORT_ASC, $entries);

    if ($function == "pool") {
        $content = $content . "<p>";
        $content = $content . "<span id=\"select_multiple_pools\" class=\"hidden\">";
        $content = $content . "<select class=\"select custom-select form-control parent\" name=\"\" id=\"select_parent\" style=\"width: 180px;\"></select>&nbsp;&nbsp;&nbsp;";
        $content = $content . "<select class=\"select custom-select form-control snapshot\" id=\"select_snapshot\" disabled  style=\"width: 180px;\"></select>";
        $content = $content . "&nbsp;&nbsp;&nbsp;";
        $content = $content . "<i id=\"multi_promote\" class=\"icon fa fa-clone fa-fw hidden\" title=\"" . _PROMOTE . "\" aria-label=\"" . _PROMOTE . "\" name=\"0\" data-href=\"deploy_desktop_image\"></i>";
        $content = $content . "</span></p>";
    }

    $content = $content . "<p>" . $num . " " . _COUNT . "</p>";
    $content = $content . "<table id=\"$function-entries\" class=\"admintable generaltable\" style=\"width: 100%;\">
    <thead class=\"resizable sticky\">
        <tr>
            <th style=\"width: 40px;\"></th>";
    foreach ($columns as $i => $header) {
        $content = $content . "<th>$header[1]<br /></th>";
        $cells[$i] = $header[0];
    }
    if ($_SESSION['user_admin']) {
        $content = $content . "<th class=\"no-filter\">" . _EDIT . "<br /></th>";
    }
    $content = $content . "</tr>
    </thead>
    <tbody class=\"resizable\">";

    if ($_SESSION['user_admin']) {
        $content = $content . "<tr id=\"empty_row\">";
        for ($i = 0; $i <= count($columns)+1; $i++) {
            $content = $content . "<td></td>";
        }
        $content = $content . "</tr>";
    }

    foreach ($entries as $key => $item) {
        $user = false;
        $itemID = $item['id'];
        $name = $item['name'];
        $cluster = explode("-", $name)[1];
        $content = $content . "<tr id=\"row_" . $key . "\" name=\"$itemID\">";
        $content = $content . "<td><input class=\"checkbox_pool\" type=\"checkbox\" name=\"" . $itemID . "\" value=\"0\" id=\"select_" . $key . "\" data-href=\"$cluster\"></td>";
        foreach ($cells as $cell) {
            $content = $content . "<td id=\"" . $cell . "_" . $key . "\">";
            switch ($cell) {
                case 'name':
                    $name = $item[$cell] ?? 0;
                    if ($name) {
                        $content = $content . $name;
                    }
                    break;
                case 'desktop_pool_id':
                    $id = array_search($item[$cell], array_column($desktopPools, 'id'));
                    $desktopPoolID = $desktopPools[$id]['id'];
                    $desktopPool = $desktopPools[$id]['name'];
                    $content = $content . "<span class=\"desktop_pool\" name=\"$desktopPoolID\">$desktopPool</span>";
                    break;
                    // provisioning enabled
                case 'enable_provisioning':
                    $enabled = $item[$cell] ?? 0;
                    $content = $content . "<input class=\"toggle_provisioning\" type=\"checkbox\" name=\"" . $key . "\" id=\"edit_enabled_" . $key . "\" data-href=\"toggle_provisioning\"";
                    if ($enabled) {
                        $content = $content .  " checked";
                    }
                    $content = $content . ">";
                    break;
                    // max number of machines
                case 'pattern_naming_settings_1':
                    $cell = substr($cell, 0, strlen($cell) - 2);
                    $maxNum = $item[$cell]['max_number_of_machines'] ?? 0;
                    if ($maxNum) {
                        $content = $content . $maxNum;
                    }
                    break;
                    // golden image parent
                case 'provisioning_settings_1':
                    $cell = substr($cell, 0, strlen($cell) - 2);
                    $currentVM = $item[$cell]['parent_vm_id'] ?? 0;
                    if ($currentVM) {
                        $currentParentID = array_search($currentVM, array_column($parents, 'id'));
                        $currentParent = $parents[$currentParentID]['name'];
                        $content = $content . "<span class=\"parent $currentVM\" name=\"$currentVM\">$currentParent</span>";
                    }
                    break;
                    // golden image snapshot
                case 'provisioning_settings_2':
                    $cell = substr($cell, 0, strlen($cell) - 2);
                    $currentSnapshotID = $item[$cell]['base_snapshot_id'] ?? 0;
                    if ($currentSnapshotID) {
                        $content = $content . "<span class=\"snapshot $currentSnapshotID\" name=\"$currentSnapshotID\" data-href=\"$currentVM\"></span>";
                    }
                    break;
                    // golden image state
                case 'provisioning_status_data_1':
                    $cell = substr($cell, 0, strlen($cell) - 2);
                    $currentState = $item[$cell]['instant_clone_current_image_state'] ?? 0;
                    if ($currentState) {
                        $currentState = str_replace('_', ' ', ucfirst(strtolower($currentState)));
                        $content = $content . $currentState;
                    }
                    break;
                    // secondary image parent
                case 'provisioning_status_data_2':
                    $cell = substr($cell, 0, strlen($cell) - 2);
                    $pendingVM = $item[$cell]['instant_clone_pending_image_parent_vm_id'] ?? 0;
                    if ($pendingVM) {
                        $pendingParentID = array_search($pendingVM, array_column($parents, 'id'));
                        $pendingParent = $parents[$pendingParentID]['name'];
                        $content = $content . "<span class=\"parent $pendingVM\" name=\"$pendingVM\">$pendingParent</span>";
                    }
                    break;
                    // secondary image snapshot
                case 'provisioning_status_data_3':
                    $cell = substr($cell, 0, strlen($cell) - 2);
                    $pendingSnapshotID = $item[$cell]['instant_clone_pending_image_snapshot_id'] ?? 0;
                    if ($pendingSnapshotID) {
                        $content = $content . "<span class=\"snapshot $pendingSnapshotID\" name=\"$pendingSnapshotID\" data-href=\"$pendingVM\"></span>";
                    }
                    break;
                    // secondary image state & progress
                case 'provisioning_status_data_4':
                    $cell = substr($cell, 0, strlen($cell) - 2);
                    $pendingState = $item[$cell]['instant_clone_pending_image_state'] ?? 0;
                    $progress = $item[$cell]['instant_clone_pending_image_progress'] ?? 0;
                    if ($pendingState) {
                        $pendingState = str_replace('_', ' ', ucfirst(strtolower($pendingState)));
                        $content = $content . $pendingState;
                        if ($progress) {
                            $content = $content . " (" . $progress . "%)";
                        }
                    } else {
                        $pendingState = '';
                    }
                    break;
                case 'machines':
                    $machineCount = 0;
                    $content = $content . "<span id=\"avail_$key\" class=\"available\" name=\"$itemID\">$machineCount</span>";
                    break;
                case 'machines_connected':
                    $machinesConnected = 0;
                    $content = $content . "<span id=\"conn_$key\" class=\"conn\" name=\"$itemID\">$machinesConnected</span>";
                    break;
                default:
                    $defaultEntry = $item[$cell] ?? 0;
                    if ($defaultEntry) {
                        $content = $content . $defaultEntry;
                    }
            }
            $content = $content . "</td>";
        }
        if ($_SESSION['user_admin']) {
            $content = $content . "<td class=\"edit_row\">";
            foreach ($functions as $entry) {
                $entryName = $entry[0] ?? '';
                $icon = $entry[1] ?? '';
                $command = $entry[2] ?? '';
                $const = "_" . strtoupper($entryName);
                if (defined($const)) {
                    $const = constant($const);
                } else {
                    $const = "_UNDEFINED";
                }
                $continue = true;
                if ($entryName == 'edit' && (!str_contains($currentState, 'Ready') || $pendingState != '')) {
                    $continue = false;
                }
                if ($entryName == 'edit' && str_contains($pendingState, 'Ready held')) {
                    $content = $content . "<i class=\"icon fa fa-clone fa-fw promote " . $function . "\" title=\"" . _PROMOTE . "\" aria-label=\"" . _PROMOTE . "\" name=\"" . $key . "\" data-href=\"promote_pending_image\"></i>";
                    $continue = false;
                }
                if ($entryName == 'edit' && $continue) {
                    $content = $content . "<i class=\"icon fa fa-$icon fa-fw " . $entryName . "_icon " . $function . "\" title=\"" . $const . "\" aria-label=\"" . $const . "\" name=\"edit_row_" . $key . "\" data-href=\"" . $key . "\"></i>";
                    $continue = false;
                }
                if (str_contains(strtolower($name), 'watch')) {
                    $continue = false;
                }
                if ($continue) {
                    $content = $content . "<i class=\"icon fa fa-$icon fa-fw " . $entryName . " " . $function . "\" title=\"" . $const . "\" aria-label=\"" . $const . "\" name=\"" . $key . "\" data-href=\"$command\"></i>";
                }
            }
            $content = $content . "</td>";
        }
        $content = $content . "</tr>";
        if ($_SESSION['user_admin']) {
            $content = $content . "<tr id=\"edit_row_" . $key . "\" class=\"edit_rows hidden\">
                        <td></td>
                        <td><input type=\"text\" class=\"form-control edit_pool\" name=\"pool_" . $key . "\" id=\"edit_pool_" . $key . "\" disabled value=\"" . $name . "\"></td>
                        <td></td>
                        <td><input type=\"text\" class=\"form-control edit_vdi\" name=\"vdi_" . $key . "\" id=\"edit_vdi_" . $key . "\" disabled value=\"" . $machineCount . "\"></td>
                        <td><input type=\"text\" class=\"form-control edit_vdi_conn\" name=\"vdi_conn_" . $key . "\" id=\"edit_vdi_conn_" . $key . "\" disabled value=\"" . $machinesConnected . "\"></td>
                        <td><input type=\"text\" class=\"form-control edit_vdi_max\" name=\"vdi_max_" . $key . "\" id=\"edit_vdi_max_" . $key . "\" disabled value=\"" . $maxNum . "\"></td>
                        <td><select class=\"select custom-select form-control current_parent\" name=\"$key\" id=\"edit_current_parent_" . $key . "\" required disabled></select></td>
                        <td><select class=\"select custom-select form-control current_snapshot\" id=\"edit_current_snapshot_" . $key . "\" disabled></select></td>
                        <td></td>
                        <td><select class=\"select custom-select form-control pending_parent\" name=\"$key\" id=\"edit_pending_parent_" . $key . "\" required disabled></select></td>
                        <td><select class=\"select custom-select form-control pending_snapshot\" id=\"edit_pending_snapshot_" . $key . "\" disabled></select></td>
                        <td></td>
                        <td>";
            $content = $content . "<i id=\"save_pool_$key\" class=\"icon fa fa-save fa-fw save_pool hidden\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"" . $key . "\" data-href=\"deploy_desktop_image\"></i>";
            $content = $content . "<i class=\"icon fa fa-times fa-fw cancel_icon\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"" . $key . "\"></i></td>
                        </tr>";
        }
    }
    $content = $content . "</tbody>
    </table>";

    $response_array['content'] = $content;
    echo json_encode($response_array);
}
