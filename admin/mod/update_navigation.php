<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/func.php";

session_start();

if ($_SESSION['loggedin']) {
    $entries = json_decode(stripslashes($_POST['data']));
    $group = 0;
    $queries = array();
    $sort1 = 0;
    $i = 0;

    foreach ($entries as $entry) {
        if (is_array($entry)) {
            $sort2= 0;
            foreach($entry as $subentry) {
                $subid = $subentry;
                $sort2++;
                $queries[$i] = $UPDATE_navigation . " SET nav_group=$group, nav_sort=$sort2 WHERE nav_id=$subid;";
                $i++;
            }
        } elseif (str_contains($entry, 'group_')) {
            $group = right($entry, strlen($entry) - 6);
        } else {
            $id = $entry;
            $queries[$i] = $UPDATE_navigation . " SET nav_group=$group, nav_sort=$sort1 WHERE nav_id=$id;";
            $i++;
            $sort1++;
        }
    }

    $qry = "";

    foreach ($queries as $query) {
        $qry = $qry . " " . $query;
    }

    if (pg_query($con, $qry)) {
        $response_array['status'] = 'success';
    } else {
        $response_array['status'] = 'error';
    }

    pg_close($con);
    echo json_encode($response_array);
}
