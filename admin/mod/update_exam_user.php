<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $userID = $_POST["user_id"];
    $username = htmlspecialchars($_POST["username"]);
    $sid = htmlspecialchars($_POST["sid"]);

    // update database
    $qry = $UPDATE_exam_user . " SET exam_username='$username', exam_user_sid='$sid' WHERE exam_user_id=$userID";

    if (pg_query($con, $qry)) {
        $response_array['status'] = 'success';
    } else {
        $response_array['status'] = 'error';
    }

    pg_close($con);
    echo json_encode($response_array);
}
