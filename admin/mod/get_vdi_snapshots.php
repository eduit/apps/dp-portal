<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

// Include language file
if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else {
    // default
    $qry = $SELECT_lang_default;
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $lang = $row['lang'];
}
include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

if ($_SESSION['loggedin']) {
    $parentID = $_POST['parentID'] ?? 0;
    $selected = $_POST['snapshotID'] ?? 0;

    if ($parentID != '0') {
        // get snapshots
        $horizonScript = $_SERVER["DOCUMENT_ROOT"] . "/inc/horizon/get_snapshots.py";
        $json = shell_exec("python3 " . $horizonScript . " " . $parentID);
        $snapshots = json_decode($json, true);
        $columns = array('name');
        $sortby = $columns[0];
        $keyValues = array_column($snapshots, $sortby);
        array_multisort($keyValues, SORT_NATURAL, $snapshots);

        $content = '';

        foreach ($snapshots as $snapshot) {
            $snapshotID = $snapshot['id'];
            $snapshotName = $snapshot['name'];

            $content = $content . "<option value=\"" . $snapshotID . "\" title=\"" . $snapshotName . "\"";
            if ($snapshotID == $selected) {
                $content = $content . " selected";
            }
            $content = $content . ">" . $snapshotName . "</option>\n";
        }
    } else {
        $content = '';
    }
    
    $response_array["content"] = $content;
    echo json_encode($response_array);
}
