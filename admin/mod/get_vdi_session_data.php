<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

// Include language file
if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else {
    // default
    $qry = $SELECT_lang_default;
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $lang = $row['lang'];
}
include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

if ($_SESSION['loggedin']) {
    $tabSelected = $_POST['tab'] ?? '';
    $function = str_replace(' ', '_', strtolower($tabSelected));
    $content = '';

    // get desktop pools
    $horizonScript = $_SERVER["DOCUMENT_ROOT"] . "/inc/horizon/get_desktop_pools.py";
    $json = shell_exec("python3 " . $horizonScript);
    $desktopPools = json_decode($json, true);

    // get sessions
    $horizonScript = $_SERVER["DOCUMENT_ROOT"] . "/inc/horizon/get_sessions.py";
    $json = shell_exec("python3 " . $horizonScript);
    $sessions = json_decode($json, true);

    $columns = array(
        ['user_id', _USER],
        ['desktop_pool_id', _POOLNAME],
        ['machine_id', _VDI],
        ['client_data', _COMPUTER],
        ['session_state', _STATUS]
    );
    $entries = $sessions;
    $functions = array(
        ['reset', 'undo', 'reset_session_machine'],
        ['restart', 'refresh', 'restart_session_machine'],
        ['disconnect', 'times', 'disconnect_session'],
        ['logoff', 'sign-out', 'logoff_session']
    );

    $num = count($entries) ?? 0;
    $sortby = $columns[1][0];
    $keyValues = array_column($entries, $sortby);
    array_multisort($keyValues, SORT_ASC, $entries);

    $content = $content . "<p>" . $num . " " . _COUNT . "</p>";

    $content = $content . "<table id=\"$function-entries\" class=\"admintable generaltable\" style=\"width: 100%;\">
    <thead class=\"resizable sticky\">
        <tr>";
    foreach ($columns as $i => $header) {
        $content = $content . "<th>$header[1]<br /></th>";
        $cells[$i] = $header[0];
    }
    if ($_SESSION['user_admin']) {
        $content = $content . "<th class=\"no-filter\">" . _EDIT . "<br /></th>";
    }
    $content = $content . "</tr>
    </thead>
    <tbody class=\"resizable\">";

    foreach ($entries as $key => $item) {
        $user = false;
        $itemID = $item['id'];
        $content = $content . "<tr id=\"row_" . $key . "\" name=\"$itemID\">";
        foreach ($cells as $cell) {
            $content = $content . "<td id=\"" . $cell . "_" . $key . "\">";
            switch ($cell) {
                case 'desktop_pool_id':
                    $id = array_search($item[$cell], array_column($desktopPools, 'id'));
                    $desktopPoolID = $desktopPools[$id]['id'];
                    $desktopPool = $desktopPools[$id]['name'];
                    $content = $content . "<span class=\"desktop_pool\" name=\"$desktopPoolID\">$desktopPool</span>";
                    break;
                case 'user_id':
                    $userID = $item[$cell];
                    $content = $content . "<span class=\"user $userID\" name=\"$userID\"></span>";
                    break;
                case 'machine_id':
                    $machineID = $item[$cell];
                    $content = $content . "<span class=\"machine $machineID\" name=\"$machineID\"></span>";
                    break;
                case 'client_data':
                    $content = $content . $item[$cell]['name'];
                    break;
                case 'session_state':
                    $sessionState = ucfirst(strtolower($item[$cell]));
                    $content = $content . $sessionState;
                    break;
                default:
                    $defaultEntry = $item[$cell] ?? 0;
                    if ($defaultEntry) {
                        $content = $content . $defaultEntry;
                    }
            }
            $content = $content . "</td>";
        }
        if ($_SESSION['user_admin']) {
            $content = $content . "<td class=\"edit_row\">";
            foreach ($functions as $entry) {
                $entryName = $entry[0];
                $icon = $entry[1];
                $command = $entry[2];
                $const = "_" . strtoupper($entryName);
                if (defined($const)) {
                    $const = constant($const);
                } else {
                    $const = "_UNDEFINED";
                }
                $continue = true;
                if ($entryName == 'unassign' && !$user) {
                    $continue = false;
                }
                if ($entryName == "disconnect" && str_contains($sessionState, 'Disconnected')) {
                    $continue = false;
                }
                if ($continue) {
                    $content = $content . "<i class=\"icon fa fa-$icon fa-fw " . $entryName . " " . $function . "\" title=\"" . $const . "\" aria-label=\"" . $const . "\" name=\"" . $key . "\" data-href=\"$command\"></i>";
                }
            }
            $content = $content . "</td>";
        }
        $content = $content . "</tr>";
    }
    $content = $content . "</tbody>
    </table>";
    
    $content = $content .  "<script>
        $(function() {
            $(\"#$function-entries\").excelTableFilter();
            let rowCount = $(\"#all_subscriptions tbody tr:visible\").length;
            $(\"#count\").html(rowCount+\"/\");
            if (/chrom(e|ium)/.test(navigator.userAgent.toLowerCase())) {
                $(\".dropdown-filter-content\").parent().parent(\"th\").addClass(\"drp_menu\");
            }
        });
    </script>";

    $response_array['content'] = $content;
    echo json_encode($response_array);
}
