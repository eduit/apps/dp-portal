<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $selected = $_POST['examUserID'];

    $qry = $SELECT_exam_users;
    $res1 = pg_query($con, $qry);
    $num1 = pg_num_rows($res1);
    $examUsers = '';

    for ($i = 0; $i < $num1; $i++) {
        $row1 = pg_fetch_array($res1);
        $examUserID = $row1['exam_user_id'];
        $examUser = $row1['exam_username'];

        $examUsers = $examUsers . "<option value=\"" . $examUserID . "\" title=\"" . $examUser . "\"";
        if ($examUserID == $selected) {
            $examUsers = $examUsers . " selected";
        }
        $examUsers = $examUsers . ">" . $examUser . "</option>\n";
    }

    $response_array["content"] = $examUsers;
    pg_close($con);

    echo json_encode($response_array);
}
