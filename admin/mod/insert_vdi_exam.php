<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $examID = htmlspecialchars($_POST["examID"]);
    $configID = htmlspecialchars($_POST["configID"]);
    $testingFrom = date_format(date_create($_POST["testingFrom"]), "Y-m-d H:i");
    $testingTo = date_format(date_create($_POST["testingTo"]), "Y-m-d H:i");
    $selfTesting = htmlspecialchars($_POST["selfTesting"]);
    $vdiPoolID = htmlspecialchars($_POST["vdiPoolID"]);
    $vdiPool = htmlspecialchars($_POST["vdiPool"]);
    $remarks = htmlspecialchars($_POST["remarks"]);
    $deadline = date_format(date_create($_POST["deadline"]), "Y-m-d");
    $statusID = htmlspecialchars($_POST["statusID"]);
    $creator = htmlspecialchars($_POST["creator"]);
    $infoText = htmlspecialchars($_POST["info_text"] ?? "");

    $qry = "SELECT vdi_exam_id FROM tbl_vdi_exam WHERE exam_id = $examID";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $num = pg_num_rows($res);
    if ($num > 0) {
        $vdi_exam_id = $row['vdi_exam_id'];
    } else {
        $vdi_exam_id = '';
    }

    if (!$vdi_exam_id) {
        $qry = $INSERT_vdi_exam . " VALUES ($examID, $configID, '$testingFrom', '$testingTo', '$selfTesting', $vdiPoolID, '$vdiPool', '$remarks', '$deadline', $statusID, '$creator', '$infoText') RETURNING vdi_exam_id";

        if ($res = pg_query($con, $qry)) {
            $response_array['status1'] = 'success';
            $row = pg_fetch_row($res);
            $vdiExamID = $row['0'];
        } else {
            $response_array['status1'] = 'error';
            $vdiExamID = false;
        }
    } else {
        $response_array['status1'] = 'already existing';
    }

    pg_close($con);
    echo json_encode($response_array);
}
