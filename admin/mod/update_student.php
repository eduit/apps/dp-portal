<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$id = htmlspecialchars($_POST["id"]);
	$reg_nr = htmlspecialchars($_POST["reg_nr"]);
	$lastname = htmlspecialchars($_POST["lastname"]);
	$firstname = htmlspecialchars($_POST["firstname"]);

	//remove - and '
	$reg_nr = str_replace("-", "", $reg_nr);
	$lastname = str_replace("'", "", $lastname);
	$firstname = str_replace("'", "", $firstname);

	$qry = $UPDATE_student . " SET student_matrikelnr='$reg_nr', student_lastname='$lastname', student_firstname='$firstname' WHERE student_id=$id";

	if (pg_query($con, $qry)) {
		$response_array['status'] = 'success';
	} else {
		$response_array['status'] = 'error';
	}

	pg_close($con);
	echo json_encode($response_array);
}
