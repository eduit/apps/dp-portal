<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {

    // get user
    $user = $_SESSION['username'];

    // get standard pool preference
    $pref_qry = $SELECT_pref . " WHERE pref_name = 'vdi_standard_pool'";
    $res = pg_query($con, $pref_qry);
    $row = pg_fetch_assoc($res);
    $vdiStandardPool = $row['pref_value'];

    $qry = "SELECT exam_id, pool_id
        FROM tbl_exam
        WHERE tbl_exam.exam_id NOT IN (SELECT exam_id FROM tbl_vdi_exam)
        AND tbl_exam.type_id=2
        AND tbl_exam.disabled = false
        AND tbl_exam.archive = false
        AND tbl_exam.exam_date > CURRENT_DATE";
    $res = pg_query($con, $qry);
    $num = pg_num_rows($res);

    if ($num > 0) {
        $values = '';
        for ($i = 0; $i < $num; $i++) {
            $row = pg_fetch_array($res);
            $examID = $row['exam_id'];
            $vdiPoolID = $row['pool_id'];
            if ($vdiPoolID == 1 || $vdiPoolID == null) {
                $vdiPoolID = 1;
                $vdiPool = $vdiStandardPool;
            } else {
                $vdiPool = substr($vdiStandardPool, 0, -1) . "2";
            }
            $values .= "($examID, $vdiPoolID, '$vdiPool', 1, '$user'),";
        }

        $qry = $INSERT_vdi_exams . " VALUES " . substr($values, 0, -1);
        
        if ($res = pg_query($con, $qry)) {
            $response_array['status'] = 'success';
        } else {
            $response_array['status'] = 'error';
        }
    } else {
        $response_array['status'] = 'nothing to do';
    }

    pg_close($con);
    echo json_encode($response_array);
}
