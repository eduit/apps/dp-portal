<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $roomID = $_POST["room_id"];
    $sectorID = $_POST["sector_id"];
    $sectorDN = htmlspecialchars($_POST["sector_display_name"]);

    $qry = $INSERT_room_sector . " VALUES ($roomID, $sectorID, '$sectorDN')";

    if (pg_query($con, $qry)) {
        $response_array['status'] = 'success';
    } else {
        $response_array['status'] = 'error';
    }

    pg_close($con);
    echo json_encode($response_array);
}
