<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

session_start();

if ($_SESSION['loggedin']) {
    $command = $_POST['command'];
    $parameters = $_POST['parameters'];
    $arguments = '';

    foreach($parameters as $parameter){
        $arguments = $arguments . ' ' . $parameter;
    }

    $horizonScript = $_SERVER["DOCUMENT_ROOT"] . "/inc/horizon/$command.py";
    $response = shell_exec("python3 " . $horizonScript . " " . $arguments);
    
    echo $response;
}
