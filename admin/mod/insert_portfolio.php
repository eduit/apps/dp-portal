<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$exam_nr = htmlspecialchars($_POST["exam_nr"]);
	$exam_name = htmlspecialchars($_POST["exam_name"]);

	$qry = "SELECT exam_name_id FROM tbl_exam_name WHERE exam_nr='$1' AND exam_name='$2'";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$num = pg_num_rows($res);
	if ($num > 0) {
		$exam_name_id = $row['exam_name_id'];
	} else {
		$exam_name_id = '';
	}

	if (!$exam_name_id) {
		$qry = $INSERT_exam_name . " VALUES ('$exam_nr','$exam_name')";

		if (pg_query($con, $qry)) {
			$response_array['status'] = 'success';
		} else {
			$response_array['status'] = 'error';
		}
	} else {
		$response_array['status'] = 'already existing';
	}

	pg_close($con);
	echo json_encode($response_array);
}
