<?php

$qry = $SELECT_rooms . " ORDER BY room_name";
$res1 = pg_query($con, $qry);
$num1 = pg_num_rows($res1);

if (isset($_GET['room'])) {
    $room = htmlspecialchars($_GET['room']);
} else {
    $room = "";
}

$selectRooms = "<select id=\"select_rooms\" class=\"select custom-select form-control\" style=\"width: 120px;\">";
for ($i = 0; $i < $num1; $i++) {
    $row1 = pg_fetch_array($res1);
    $roomID = $row1['room_id'];
    $roomName = $row1['room_name'];
    if ($row1['room_display_name']) {
        $roomDN = $row1['room_display_name'];
    } else {
        $roomDN = $row1['room_name'];
    }
    $option = "<option value=\"$roomName\"";

    if ($room == $roomName || empty($room)) {
        $option = $option . " selected>" . $roomDN;
        $room = $roomName;
        $roomIDselected = $roomID;
    } else {
        $option = $option . ">" . $roomDN;
    }
    $option = $option . "</option>";
    $selectRooms = $selectRooms . $option;
}
if ($room == "other") {
    $option = "<option value=\"other\" selected>" . _OTHER . "</option>";
    $roomIDselected = 0;
} else {
    $option = "<option value=\"other\">" . _OTHER . "</option>";
}
$selectRooms = $selectRooms . $option;
$selectRooms = $selectRooms . "</select><br><br>";

echo _ROOM . ": " . $selectRooms;

getComputerData($roomIDselected, $room);

function getComputerData($roomIDselected, $room)
{
    include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
    include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

    // Get preferences
    $qry = $SELECT_prefs;
    $res = pg_query($con, $qry);
    $num = pg_num_rows($res);
    for ($i = 0; $i < $num; $i++) {
        $row = pg_fetch_array($res);
        if ($row['pref_name'] == "editable_rooms") {
            $editableRooms = $row['pref_value'];
        }
        if ($row['pref_name'] == "number_of_computers") {
            $numberOfComputers = $row['pref_value'];
        }
    }

    $editable = false;
    $editableRooms = explode("; ", $editableRooms);

    if (isset($_GET['room'])) {
        $room = htmlspecialchars($_GET['room']);
    }

    if (in_array($room, $editableRooms)) {
        $editable = true;
        $rowSize = 100 / 6;

        //get room_id
        $qry = $SELECT_room_by_name . "'$room'";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $roomID = $row['room_id'];

        //get sector_id
        $qry = $SELECT_sector . "$roomID LIMIT 1";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $sectorID = $row['sector_id'];
        if ($sectorID == null) {
            $sectorID = "0";
        }
    } else {
        $rowSize = 100 / 5;
    }

    $qry = $SELECT_rooms_plus_empty;
    $res1 = pg_query($con, $qry);
    $roomExists = false;

    if (empty($room)) {
        $row1 = pg_fetch_assoc($res1);
        $roomID = $row1['room_id'];
        $room = $row1['room_name'];
        $roomExists = true;
    } else {
        $num1 = pg_num_rows($res1);
        for ($i = 0; $i < $num1; $i++) {
            $row1 = pg_fetch_array($res1);
            $roomName = $row1['room_name'];
            if ($room == $roomName){
                $roomExists = true;
            }
        }
    }
    
    if ($roomExists) {
        $qry = $SELECT_computers . " AND tbl_room.room_name='$room' ORDER BY ip_address LIMIT " . $numberOfComputers;
        $res2 = pg_query($con, $qry);
        $num2 = pg_num_rows($res2);
        $row2 = pg_fetch_assoc($res2);
        $roomID = $row2['room_id'] ?? $roomIDselected;
        $autoRefresh = $row2['auto_refresh'] ?? 't';
        pg_result_seek($res2, 0);

        $checkbox = "<p><label id=\"auto_refresh_label\" title=\"" . _AUTO_REFRESH . "\"><input name=\"$roomID\" type=\"checkbox\" value=\"0\" id=\"auto_refresh\" ";
        if ($autoRefresh == 't') {
            $checkbox = $checkbox . " checked";
        }
        $checkbox = $checkbox . "> " . _REFRESH . "</label></p>";
        echo $checkbox;

        echo "<p><span id=\"count\"></span>" . $num2 . " " . _COUNT . "</p>";

        $nofilter = "class=\"no-filter\"";

        echo "<table id=\"all_computers\" class=\"admintable generaltable\" style=\"width: 100%;\">
        <thead class=\"resizable sticky\">
            <tr>
                <th ";
        if ($editable) {
            echo $nofilter;
        }
        echo " style=\"width: $rowSize%;\">" . _IP_ADDRESS . "<br /></th>
                <th ";
        if ($editable) {
            echo $nofilter;
        }
        echo " style=\"width: $rowSize%;\">" . _HOSTNAME . "<br /></th>
                <th ";
        if ($editable) {
            echo $nofilter;
        }
        echo " style=\"width: $rowSize%;\">" . _SECTOR . "<br /></th>";
        echo "<th ";
        if ($editable) {
            echo $nofilter;
        }
        echo " style=\"width: $rowSize%;\">" . _VDI . "<br /></th>";
        echo "<th ";
        if ($editable) {
            echo $nofilter;
        }
        echo " style=\"width: $rowSize%;\">" . _VDI . " " . _USER . "<br /></th>";
        if ($editable) {
            echo "<th class=\"no-filter\" style=\"width: $rowSize%;\">" . _EDIT . "<br /></th>";
        }
        echo "
            </tr>
        </thead>
        <tbody class=\"resizable\">";
        if ($editable) {
            echo "<tr id=\"add_row\" class=\"add_rows\">
                <td><input type=\"text\" class=\"form-control\" name=\"ip\" id=\"add_ip_address\" required=\"required\" maxlength=\"" . MAXLENGTH . "\">
                    <span id=\"ip_address_missing\" class=\"invalid-feedback\">" . _IP_ADDRESS . " " . _MISSING . "</span>
                </td>
                <td><input type=\"text\" class=\"form-control\" name=\"computer\" id=\"add_computer\" required=\"required\" maxlength=\"" . MAXLENGTH . "\">
                    <span id=\"computer_missing\" class=\"invalid-feedback\">" . _COMPUTER . " " . _MISSING . "</span>
                </td>
                <td><select class=\"select custom-select form-control select-sector\" name=\"sector\" id=\"add_sector\" required=\"required\" disabled>" . selectSectors($sectorID, $roomID) . "</select></td>
                <td><input type=\"checkbox\" name=\"vdi\" id=\"add_vdi\"></td>
                <td><select class=\"select custom-select form-control select-exam_user\" name=\"exam_user\" id=\"add_exam_user\" required=\"required\">" . selectExamUsers("") . "</select></td>
                <td><i id=\"computer_submit\" class=\"icon fa fa-plus fa-fw add_icon\" title=\"" . _ADD . "\" aria-label=\"" . _ADD . "\" name=\"add_computer\"></i></td>";
            echo "
            </tr>";
        }
        for ($i = 0; $i < $num2; $i++) {
            $row3 = pg_fetch_array($res2);
            if ($row3['sector_display_name']) {
                $sectorName = $row3['sector_display_name'];
            } else {
                $sectorName = $row3['sector_name'];
            }
            $computerID = $row3['computer_id'];
            $ipAddress = $row3['ip_address'];
            $computer = $row3['computer'];
            $vdi = $row3['vdi'];
            $examUserID = $row3['exam_user_id'];
            $examUser = $row3['exam_user'];
            echo "<tr id=\" row_" . $computerID . "\">
                <td id=\"ip_address_" . $computerID . "\">" . $ipAddress . "</td>
                <td id=\"computername_" . $computerID . "\">" . $computer . "</td>
                <td id=\"sector_" . $computerID . "\">" . $sectorName . "</td>";
            echo "<td id=\"vdi_" . $computerID . "\">";
            if ($vdi == "t") {
                echo "<i class=\"fa fa-check-square-o fa-fw true_icon\" title=\"" . _TRUE . "\" aria-label=\"true\" name=\"vdi_" . $computerID . "\"></i>";
                echo "<span style=\"opacity: 0;\">" . _TRUE . "</span>";
            } else {
                echo "<i class=\"fa fa-square-o fa-fw false_icon\" title=\"" . _FALSE . "\" aria-label=\"false\" name=\"vdi_" . $computerID . "\"></i>";
                echo "<span style=\"opacity: 0;\">" . _FALSE . "</span>";
            }
            echo "</td>";
            echo "<td id=\"exam_user_" . $computerID . "\">" . $examUser . "</td>";

            if ($editable) {
                echo "<td id=\"edit_" . $computerID . "\" class=\"edit_row\">";
                echo "<i class=\"icon fa fa-edit fa-fw edit_icon\" title=\"" . _EDIT . "\" aria-label=\"" . _EDIT . "\" name=\"edit_row_" . $computerID . "\" data-href=\"$computerID\"></i>";
                echo "<i class=\"icon fa fa-trash fa-fw delete_computer_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" name=\"" . $computerID . "\"></i>
                </td>";
            }
            echo "</tr>";

            //edit row
            if ($editable) {
                echo "<tr id=\"edit_row_" . $computerID . "\" class=\"edit_rows hidden\">
                    <td><input type=\"text\" class=\"form-control\" name=\"ip_" . $computerID . "\" id=\"edit_ip_address_" . $computerID . "\" required=\"required\" value=\"" . $ipAddress . "\" maxlength=\"" . MAXLENGTH . "\"></td>
                    <td><input type=\"text\" class=\"form-control\" name=\"computer_" . $computerID . "\" id=\"edit_computer_" . $computerID . "\" required=\"required\" value=\"" . $computer . "\" maxlength=\"" . MAXLENGTH . "\"></td>
                    <td>" . $sectorName . "</td>
                    <td><input type=\"checkbox\" name=\"vdi_" . $computerID . "\" id=\"edit_vdi_" . $computerID . "\"";
                if ($vdi == "t") {
                    echo " checked";
                }
                echo "></td>
                                <td><select class=\"select custom-select form-control\" name=\"$examUserID\" id=\"edit_exam_user_" . $computerID . "\" required=\"required\"></select></td>
                    <td>";
                echo "<i class=\"icon fa fa-save fa-fw save_computer_icon\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"" . $computerID . "\"></i>";
                echo "<i class=\"icon fa fa-times fa-fw cancel_icon\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"" . $computerID . "\"></i>
                    </td>";
                echo "
                </tr>";
            }
        }

        echo "</tbody></table>";
        if (!$editable) {
            echo "<script>
            $(function() {
                $(\"#all_computers\").excelTableFilter();
                let rowCount = $(\"#all_computers tbody tr:visible\").length;
                    if ($(\"#add_row\").is(\":visible\")){
                        rowCount--;
                    }
                    $(\"#count\").html(rowCount+\"/\");

                    if (/chrom(e|ium)/.test(navigator.userAgent.toLowerCase())) {
                        $(\".dropdown-filter-content\").parent().parent(\"th\").addClass(\"drp_menu\");
                    }
                });
            </script>";
        }
    } else  {
            echo "<h2>" . _WRONG_PARAMETER . "</h2>
                    <p>&nbsp;</p>
                    <i class=\"fa fa-exclamation fa-fw\" style=\"font-size: 200px; margin-left: 80px;\" title=\"\" aria-label=\"false\" name=\"\"></i>";
    }
}

function selectSectors($selected, $roomID)
{
    include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
    include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

    $qry = $SELECT_sector . $roomID;
    $res1 = pg_query($con, $qry);
    $num1 = pg_num_rows($res1);
    $sectors = "";

    for ($i = 0; $i < $num1; $i++) {
        $row1 = pg_fetch_array($res1);
        $sectorID = $row1['sector_id'];
        $sector = $row1['sector_name'];

        $sectors = $sectors . "<option value=\"" . $sectorID . "\" title=\"" . $sector . "\"";
        if ($sectorID == $selected) {
            $sectors = $sectors . " selected";
        }
        $sectors = $sectors . ">" . $sector . "</option>\n";
    }
    return $sectors;
}

function selectExamUsers($selected)
{
    include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
    include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

    $qry = $SELECT_exam_users;
    $res1 = pg_query($con, $qry);
    $num1 = pg_num_rows($res1);
    $examUsers = '';

    for ($i = 0; $i < $num1; $i++) {
        $row1 = pg_fetch_array($res1);
        $examUserID = $row1['exam_user_id'];
        $examUser = $row1['exam_username'];

        $examUsers = $examUsers . "<option value=\"" . $examUserID . "\" title=\"" . $examUser . "\"";
        if ($examUserID == $selected) {
            $examUsers = $examUsers . " selected";
        }
        $examUsers = $examUsers . ">" . $examUser . "</option>\n";
    }
    return $examUsers;
}
