<?php

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin'] || $_SESSION['user_vho']) {
    if (isset($_GET['id']) && is_numeric($_GET['id'])) {
        $id = $_GET['id'];
    } else {
        $id = 0;
    }
    $qry = $SELECT_vdi_exam . " WHERE tbl_vdi_exam.vdi_exam_id = $id";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $nr = $row['exam_nr'];
    $name = $row['exam_name'];
    $lecturer = $row['lecturer_lastname'] . " " . $row['lecturer_firstname'];
    $pool = $row['vdi_pool'];
    $description = "Testing: $name ($nr)\nLecturer: $lecturer\nNEW Manual: https://unlimited.ethz.ch/spaces/ManualsEduIT/pages/263842379/Testing+Exams+with+Additional+Software\nPool: $pool";
    $testingFromRaw = date_create($row['testing_from']);
    $testingFrom = date_format($testingFromRaw, "Y-m-d\TH:i:s\Z");
    $start = $testingFrom;
    $testingToRaw = date_create($row['testing_to']);
    $testingTo = date_format($testingToRaw, "Y-m-d\TH:i:s\Z");
    $end = $testingTo;
    $location = "Zoom call";
    $data = "BEGIN:VCALENDAR\nVERSION:2.0\nMETHOD:PUBLISH\nBEGIN:VEVENT\nDTSTART:" . $start . "\nDTEND:" . $end . "\nLOCATION:" . $location . "\nTRANSP: OPAQUE\nSEQUENCE:0\nUID:\nDTSTAMP:" . date("Y-m-d\TH:i:s\Z") . "\nSUMMARY:" . $name . "\nDESCRIPTION:" . $description . "\nPRIORITY:1\nCLASS:PUBLIC\nBEGIN:VALARM\nTRIGGER:-PT10080M\nACTION:DISPLAY\nDESCRIPTION:Reminder\nEND:VALARM\nEND:VEVENT\nEND:VCALENDAR\n";
    $filename = $nr . ".ics";

    header("Content-type:text/calendar");
    header('Content-Disposition: attachment; filename="' . $filename);
    Header('Content-Length: ' . strlen($data));
    Header('Connection: close');

    // write file
    $output = fopen('php://output', 'w');
    fwrite($output, $data);
    fclose($output);
}
