<?php

if (isset($_POST["exam_id"])) {
    $exam_selected = $_POST["exam_id"];
} else {
    $exam_selected = "";
}

if ($exam_selected) {
    getSelect("students", $exam_selected);
} else {
    if ($_SESSION['user_vho']) {
        echo "<h3>Info</h3><p>" . _VHO_INFO_1 . "</p>";
        echo "<p>" . _VHO_INFO_2 . "</p><br>";
    }
    
    if (isset($_GET['archived']) && ($_GET['archived'] == "true" || $_GET['archived'] == "false")) {
        $archived = $_GET['archived'];
    } else {
        $archived = "false";
    }

    echo "<p><label id=\"archived_label\" title=\"" . _ARCHIVED . "\"><input name=\"archived\" type=\"checkbox\" name=\"\" value=\"0\" id=\"archived\" ";
    if ($archived == "true") {
        echo "checked";
    }
    echo "> " . _ARCHIVED . "</label>";
    //echo " <button id=\"show_vho_generator\" class=\"ui-button ui-widget ui-corner-all\">" . _GENERATE_VHO . "</button>";
    
    echo "</p>\n";

    echo "<div id=\"vho_generator\" class=\"hidden\">";
    echo "<div class=\"pull-xs-left\"><input type=\"text\" id=\"vho_count\" class=\"form-control\" placeholder=\"" . _COUNT . "\" style=\"width: 180px; display: inline;\" />";
    echo "<span id=\"number_missing\" class=\"invalid-feedback\">" . _NUMBER . " " . _MISSING . "</span>&nbsp;</div>";
    echo "<div class=\"pull-xs-left\"><select class=\"select custom-select\" id=\"vho_theme\" style=\"width: 180px; vertical-align: baseline;\">";
    getSelect("themes", 0);
    echo "</select><span id=\"theme_missing\" class=\"invalid-feedback\">" . _THEME . " " . _MISSING . "</span>&nbsp;</div>";
    echo "<div class=\"pull-xs-left\"> <i id=\"generate_vho_accounts\" class=\"icon fa fa-rocket fa-fw\" title=\"" . _GENERATE_VHO . "\" aria-label=\"" . _GENERATE_VHO . "\"></i>";
    echo "</div></div>\n";
    echo "<div class=\"cleaner\">&nbsp;</div>\n";

    /* upload */
    if (!$_SESSION['user_vho']) {
        echo "<i id=\"vho_template_list\" class=\"icon fa fa-list-alt fa-fw\" title=\"" . _TEMPLATE . "\" aria-label=\"" . _TEMPLATE . "\"></i>";
        echo "<i id=\"vho_upload_button\" class=\"icon fa fa-upload fa-fw\" title=\"" . _UPLOAD . "\" aria-label=\"" . _UPLOAD . "\"></i>";
        echo "<input type=\"file\" accept=\".csv\" id=\"fileloader_vho_accounts\" class=\"btn hidden\" name=\"fileToUpload\" title=\"" . _VHO_ACCOUNTS . "\" />";
        echo "<input type=\"file\" accept=\".csv\" id=\"fileloader_vho\" class=\"btn hidden\" name=\"fileToUpload\" title=\"" . _VHO_ACCOUNTS . "\" />";
        echo "<div id=\"div_notification\" class=\"hidden\"><p id=\"notification_text\"></p></div>";
        echo "<br><br>";
    }

    /*account list */
    $user = "";
    $sql = "";
    if ($_SESSION['loggedin']) {
        $sql = " WHERE";
    } else {
        if ($archived == "false") {
            $moreSQL = "OR used_by IS NULL";
        } else {
            $moreSQL = "";
        }
        $user = $_SESSION['username'];
        $sql = " WHERE (used_by = '$user' $moreSQL) AND";
    }
    $qry = $SELECT_VHO_accounts . $sql . " archive='$archived' ORDER BY username";

    $res1 = pg_query($con, $qry);
    $num1 = pg_num_rows($res1);

    echo "<p>" . $num1 . " " . _COUNT . "</p>";

    echo "<table id=\"vho_accounts\" class=\"admintable generaltable\" style=\"width: 100%;\">
        <thead class=\"resizable sticky\">
        <tr>
            <th style=\"width: 10%;\">" . _USERNAME . "<br /></th>
            <th style=\"width: 10%;\" class=\"no-filter\">" . _PASSWORD . "<br /></th>
            <th style=\"width: 5%;\">" . _IN_USE . "<br /></th>
            <th style=\"width: 20%;\">" . _EXAM . "<br /></th>
            <th style=\"width: 30%;\">" . _STUDENT . "<br /></th>
            <th style=\"width: 15%;\">" . _USED_BY . "<br /></th>
            <th style=\"width: 10%;\" class=\"no-filter\">" . _EDIT . "<br /></th>";
    echo "</tr></thead><tbody class=\"resizable\">";

    for ($i = 0; $i < $num1; $i++) {

        $row1 = pg_fetch_array($res1);
        $id = $row1['account_id'];
        $username = $row1['username'];
        // decryption
        $key = 'mS3c8X0mJPtfPzyalwpkkimEUfVRS29L';
        $ciphertext = $row1['password'];
        $c = base64_decode($ciphertext);
        $ivlen = openssl_cipher_iv_length($cipher = 'AES-128-CBC');
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len = 32);
        $ciphertext_raw = substr($c, $ivlen + $sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options = OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary = true);
        if (hash_equals($hmac, $calcmac))
        {
            $password = $original_plaintext;
        } else {
            $password = '';
        }
        $password_subst = str_repeat('*', 12);
        $in_use = $row1['in_use'];
        $exam_id = $row1['exam_id'];
        $student_id = $row1['student_id'];
        $user = $row1['used_by'];
        $archive = $row1['archive'];
        $exam = '';
        $student = '';

        // get exam
        if ($exam_id) {
            $qry = $SELECT_exam_name_by_id . $exam_id;
            $res2 = pg_query($con, $qry);
            $row2 = pg_fetch_assoc($res2);
            $num2 = pg_num_rows($res2);
            if ($num2 > 0) {
                $exam = $row2['exam_name'] . " (" . $row2['exam_nr'] . ")";
            }
        }

        // get student
        if ($student_id) {
            $qry = $SELECT_students . " WHERE student_id=$student_id";
            $res3 = pg_query($con, $qry);
            $row3 = pg_fetch_assoc($res3);
            $num3 = pg_num_rows($res3);
            if ($num3 > 0) {
                $student = $row3['student_lastname'] . " " . $row3['student_firstname'] . " (" . $row3['student_matrikelnr'] . ")";
            }
        }

        if (($in_use == "f" && $_SESSION['user_vho']) || $_SESSION['loggedin'] || $_SESSION['username'] == $user) {
            echo "<tr id=\"row_" . $id . "\">
                    <td id=\"username_" . $id . "\">" . $username . "</td>
                    <td id=\"password_" . $id . "\">";
            if ($in_use == "t" || $_SESSION['loggedin']) {
                echo "<i id=\"toggle_pw_$id\" class=\"icon fa fa-eye fa-fw toggle_pw\" title=\"" . _TOGGLE . "\" aria-label=\"" . _TOGGLE . "\" name=\"$id\"></i>";
            }
            echo "<span id=pw_txt_" . $id;
            if ($in_use == "t" || $_SESSION['loggedin']) {
                echo " data-value=\"$password\">" . $password_subst . "</span> ";
            }
            echo "</td><td id=\"in_use_" . $id . "\">";
            if ($in_use == "t") {
                echo "<i class=\"fa fa-check-square-o fa-fw true_icon\" title=\"" . _TRUE . "\" aria-label=\"true\"></i>";
                echo "<span style=\"opacity: 0;\">" . _TRUE . "</span>";
            } else {
                echo "<i class=\"fa fa-square-o fa-fw false_icon\" title=\"" . _FALSE . "\" aria-label=\"false\"></i>";
                echo "<span style=\"opacity: 0;\">" . _FALSE . "</span>";
            }
            echo "</td>
                <td id=\"exam_" . $id . "\">";
            if (!$exam && !$_SESSION['loggedin']) {
                echo "<select class=\"select custom-select form-control select_exam\" name=\"$id\" id=\"select_exam_$id\" style=\"max-width: 400px;\">";
                getSelect("dates", 0);
                echo "</select>";
            } else {
                echo $exam;
            }
            echo "</td>
                <td id=\"student_" . $id . "\">";
            if (!$student && !$_SESSION['loggedin']) {
                echo "<select class=\"select custom-select form-control select_student\" name=\"$id\" id=\"select_student_$id\" disabled style=\"max-width: 400px;\">";
                echo "<option name=\"" . _NO_STUDENT . "\" value=\"0\" disabled selected>" . _NO_STUDENT . "</option>\n";
                echo "</select>";
            } else {
                echo $student;
            }
            echo "</td>";
            echo "<td id=\"user_" . $id . "\">" . $user . "</td>";
            echo "<td id=\"edit_" . $id . "\" class=\"edit_row\">";
            if ($_SESSION['user_vho'] && $in_use == "f") {
                echo "<i class=\"fa fa-envelope fa-fw mail_icon clickable\" title=\"E-Mail\" name=\"" . $id . "\" aria-label=\"true\" data-href=\"" . $_SESSION['user_mail'] . "\"></i>";
            }
            if ($_SESSION['loggedin'] || $in_use == "t") {
                if ($archive == "f") {
                    echo "<i id=\"t_a_" . $id . "\" class=\"icon fa fa-archive fa-fw archive_vho_icon\" title=\"" . _ARCHIVE . "\" aria-label=\"" . _ARCHIVE . "\" name=\"" . $id . "\"></i>";
                } else {
                    echo "<i id=\"f_a_" . $id . "\" class=\"icon fa fa-undo fa-fw archive_vho_icon\" title=\"" . _RESTORE . "\" aria-label=\"" . _RESTORE . "\" name=\"" . $id . "\"></i>";
                }
            }
            if ($_SESSION['loggedin']) {
                echo "<i class=\"icon fa fa-trash fa-fw delete_vho_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" name=\"" . $id . "\"></i>";
            }
            echo "</td>";
            echo "</tr>";
        }
    }
    echo "</tbody></table>";
    echo "<script>
            $(function() {
                $(\"#vho_accounts\").excelTableFilter();
                let rowCount = $(\"#all_computers tbody tr:visible\").length;
                if ($(\"#add_row\").is(\":visible\")){
                    rowCount--;
                }
                $(\"#count\").html(rowCount+\"/\");

                if (/chrom(e|ium)/.test(navigator.userAgent.toLowerCase())) {
                    $(\".dropdown-filter-content\").parent().parent(\"th\").addClass(\"drp_menu\");
                }
            });
        </script>";
}

function getSelect($mode, $examID)
{
    include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
    include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

    session_start();

    // Include language file
    if (isset($_SESSION['lang'])) {
        $lang = $_SESSION['lang'];
    } else {
        // default
        $qry = $SELECT_lang_default;
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $lang = $row['lang'];
    }
    include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

    switch ($mode) {
        case "dates":
            $qry = $SELECT_dates_light . " AND
							  tbl_exam.disabled = false AND
							  tbl_exam.archive = false
							ORDER BY tbl_exam.exam_date, tbl_exam_name.exam_name";
            $res = pg_query($con, $qry);
            $num = pg_num_rows($res);

            echo "<option name=\"" . _NO_EXAM_2 . "\" value=\"0\" disabled selected>" . _NO_EXAM_2 . "</option>\n";

            for ($i = 0; $i < $num; $i++) {
                $row = pg_fetch_array($res);
                $id = $row['id'];
                $examName = $row['exam_name'];
                $lastname = $row['lecturer_lastname'];
                $date = date_create($row['date']);

                echo "<option name=\"" . $examName . "\" value=\"" . $id . "\"";
                echo ">" . $examName . " | " . $lastname . " | " . date_format($date, 'Y-m-d') . "</option>\n";
            }
            break;

        case "students":
            $qry = $SELECT_exam_students_by_exam_id . " AND tbl_exam.exam_id = $examID GROUP BY tbl_student.student_id, tbl_exam.exam_id ORDER BY student_lastname";
            $res = pg_query($con, $qry);
            $num = pg_num_rows($res);

            echo "<option name=\"" . _NO_STUDENT . "\" value=\"0\" disabled selected>" . _NO_STUDENT . "</option>\n";

            for ($i = 0; $i < $num; $i++) {
                $row = pg_fetch_array($res);
                $id = $row['student_id'];
                $studentName = $row['lastname'] . " " . $row['firstname'];

                echo "<option name=\"" . $studentName . "\" value=\"" . $id . "\"";
                echo ">" . $studentName . "</option>\n";
            }
            break;

        case "themes":
            $qry = $SELECT_VHO_themes;
            $res = pg_query($con, $qry);
            $num = pg_num_rows($res);

            echo "<option name=\"" . _NO_THEME . "\" value=\"0\" disabled selected>" . _NO_THEME . "</option>\n";

            for ($i = 0; $i < $num; $i++) {
                $row = pg_fetch_array($res);
                $id = $row['theme_id'];
                $themeName = $row['theme_name'];

                echo "<option name=\"" . $themeName . "\" value=\"" . $id . "\"";
                echo ">" . $themeName . "</option>\n";
            }
            break;

        default:
            echo "<option name=\"" . _NO_DATA . "\" value=\"0\" disabled selected>" . _NO_DATA . "</option>\n";
            break;
    }
}
