<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

// Include language file
if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else {
    // default
    $qry = $SELECT_lang_default;
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $lang = $row['lang'];
}
include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

session_start();

if ($_SESSION['loggedin']) {
    $room_style = "";
    $room_sector_style = "";
    $exam_user_style = "";

    $file = $_FILES['file']['tmp_name'];

    // Detect delimiter from first line.
    $handle = fopen($file, "r");
    if ($handle !== false) {
        $delimiter = "";
        $firstLine = fgets($handle);
        if ($firstLine !== false) {
            if (strpos($firstLine, ",") !== false) {
                $delimiter = ",";
            } elseif (strpos($firstLine, ";") !== false) {
                $delimiter = ";";
            } else {
                $delimiter = "";
            }
        }
    }
    $response_array[0]['delimiter'] = $delimiter;

    // compare with data
    $csv = array_map(function ($v) {
        global $delimiter;
        return str_getcsv($v, $delimiter);
    }, file($file));

    $table_header = "<thead><tr>";

    // extract headers
    foreach ($csv[0] as $i => $title) {
        $csv_header[$i] = $title;
        $response_array[0]['header'][$i] = $title;
        $table_header = $table_header . "<th>" . constant(strtoupper("_" . $title)) . "</th>";
    }

    $table_header = $table_header . "</tr></thead>";

    // combine data with headers
    array_walk($csv, function (&$a) use ($csv) {
        $a = array_combine($csv[0], $a);
    });

    // remove column header
    array_shift($csv);

    $len = count($csv);
    $response_array[0]['csv_lines'] = $len;

    $table_body = "<tbody>";

    // process data
    foreach ($csv as $i => $csv_row) {
        $entry = $i + 1;
        $error = false;

        $ip_address = $csv_row['ip_address'];
        $computer = $csv_row['computer'];
        $room = $csv_row['room'];
        $sector = $csv_row['sector'];
        $exam_user = $csv_row['exam_user'];

        // check if exists
        $qry = $SELECT_computers . " AND computer_name = '$computer' AND ip_address = '$ip_address'";
        $res = pg_query($con, $qry);
        $num = pg_num_rows($res);
        $row = pg_fetch_assoc($res);

        if ($num > 0){
            $computer_id = $row['computer_id'];
            $ip_address_db = $row['ip_address'];
            $computer_db = $row['computer'];
            $room_db = $row['room_name'];
            $sector_db = $row['sector_name'];
            $exam_user_db = $row['exam_user'];
        } else {
            $computer_id = '';
            $ip_address_db = '';
            $computer_db = '';
            $room_db = '';
            $sector_db = '';
            $exam_user_db = '';
        }

        $error_style = "style=\"color: red; font-weight: bold;\"";
        $error_text = "";

        // get room_id
        $qry = $SELECT_room_by_name . "'$room'";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $room_id = $row['room_id'];
        if ($room_id == null) {
            $room_id = "0";
            $error = true;
            $error_text = _ROOM;
            $room_style = $error_style;
        }

        // get room_sector_id
        $qry = $SELECT_sector . "$room_id AND tbl_sector.sector_name = '$sector' LIMIT 1";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $room_sector_id = $row['room_sector_id'];
        if ($room_sector_id == null) {
            $room_sector_id = "0";
            $error = true;
            if ($error_text) {
                $error_text = $error_text . ", " .  _ROOM . "/" . _SECTOR;
            } else {
                $error_text = _ROOM . "/" . _SECTOR;
            }
            $room_sector_style = $error_style;
        }

        //get exam_user_id
        $qry = $SELECT_exam_user_id . "'" . $exam_user . "'";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $exam_user_id = $row['exam_user_id'];
        if ($exam_user_id == null) {
            $exam_user_id = "0";
            $error = true;
            if ($error_text) {
                $error_text = $error_text . ", " . _EXAM_USER;
            } else {
                $error_text = _EXAM_USER;
            }
            $exam_user_style = $error_style;
        }

        $same = true;
        if ($ip_address != $ip_address_db) {
            $same = false;
            $same_ip = false;
        } else {
            $same_ip = true;
        }
        if ($computer != $computer_db) {
            $same = false;
        }
        if ($room != $room_db) {
            $same = false;
        }
        if ($sector != $sector_db) {
            $same = false;
        }
        if ($exam_user != $exam_user_db) {
            $same = false;
        }

        if ($num > 0 && $same) {
            $mode = "nothing_to_do";
        }

        if ($num == 1 && !$same) {
            $mode = "update";
        }

        if ($num == 0) {
            $qry = $SELECT_computers . " AND tbl_computer.computer_name = '$computer'";
            $res = pg_query($con, $qry);
            $num = pg_num_rows($res);
            $row = pg_fetch_assoc($res);

            if ($num > 0) {
                $mode = "update";
                $computer_id = $row['computer_id'];
            } else {
                $qry = $SELECT_computers . " AND tbl_computer.ip_address = '$ip_address'";
                $res = pg_query($con, $qry);
                $num = pg_num_rows($res);
                $row = pg_fetch_assoc($res);
                $computer_id = $row['computer_id'];

                if ($num > 0) {
                    $mode = "update";
                } else {
                    $mode = "new_entry";
                }
            }
        }

        if ($error) {
            $mode = "error";
        }

        switch ($mode) {
            case "error":
                $style = "style=\"text-decoration: line-through;\"";
                $title = "title=\"" . _ERROR . ": $error_text\"";
                $class = "class=\"error\"";
                $response_array[$entry]['function'] = 'error';
                $response_array[$entry]['status'] = 'error';
                $response_array[0]['status'] = 'error';
                break;

            case "nothing_to_do":
                $style = "style=\"font-style: italic;\"";
                $title = "title=\"" . _EXISTING_COMPUTER . "\"";
                $class = "class=\"unmodified\"";
                $response_array[$entry]['function'] = 'nothing to do';
                $response_array[$entry]['status'] = 'success';
                break;

            case "update":
                $style = "style=\"\"";
                $title = "title=\"" . _UPDATED . "\"";
                $class = "class=\"updated\"";

                //update database
                $response_array[$entry]['function'] = 'update';
                if ($same_ip){
                    $queries[0] = $UPDATE_computer . " SET computer_name='$computer', room_sector_id=$room_sector_id, exam_user_id=$exam_user_id WHERE computer_id=$computer_id;";
                } else {
                    $queries[0] = $UPDATE_computer . " SET computer_name='$computer', room_sector_id=$room_sector_id, exam_user_id=$exam_user_id WHERE ip_address='$ip_address';";
                }

                foreach ($queries as $query) {
                    if (pg_query($con, $query)) {
                        $response_array[$entry]['status'] = 'success';
                    } else {
                        $response_array[$entry]['status'] = 'error';
                        $response_array[0]['status'] = 'error';
                    }
                }
                break;

            case "new_entry":
                $style = "style=\"font-style: bold;\"";
                $title = "title=\"" . _NEW . "\"";
                $class = "class=\"new\"";

                //insert into database
                $response_array[$entry]['function'] = 'insert';
                $qry = $INSERT_computer . " VALUES ('$computer','$ip_address',$room_sector_id,'f','$exam_user_id')";

                if (pg_query($con, $qry)) {
                    $response_array[$entry]['status'] = 'success';
                } else {
                    $response_array[$entry]['status'] = 'error';
                    $response_array[0]['status'] = 'error';
                }
                break;

            default:
                break;
        }

        $table_body = $table_body . "<tr $class $style $title><td>$ip_address</td><td>$computer</td><td $room_style>$room</td><td $room_sector_style>$sector</td><td $exam_user_style>$exam_user</td></tr>";

        $room_style = "";
        $room_sector_style = "";
        $exam_user_style = "";
    }

    $table_body = $table_body . "</tbody>";

    $response_array[0]['content'] = $table_header . $table_body;

    pg_close($con);
    echo json_encode($response_array);
}
