<?php

if ($_SESSION['loggedin']) {
    // Get preferences
    $qry = $SELECT_prefs;
    $res = pg_query($con, $qry);
    $num = pg_num_rows($res);
    for ($i = 0; $i < $num; $i++) {
        $row = pg_fetch_array($res);
        if ($row['pref_name'] == "vdi_provisioning") {
            $enabled = $row['pref_value'];
        }
    }
    if ($enabled == "true") {
        echo "<div id=\"horizon_data\"></div>";
    } else {
        echo "<h2>" . _NO_VDI_PROVISIONING . "</h2>
				<p>&nbsp;</p>
				<i class=\"fa fa-rocket fa-fw\" style=\"font-size: 200px; margin-left: 80px;\" title=\"\" aria-label=\"false\" name=\"\"></i>";
    }
}
