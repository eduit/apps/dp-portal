<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin'] || $_SESSION['user_vho']) {
    $configID = $_POST["config_id"];

    $response_array['status'] = 'success';
    

    $qry = $DELETE_config_sw . " WHERE config_id = $configID";
    $i = 1;
    if (pg_send_query($con, $qry)) {
        $res = pg_get_result($con);
        if ($res) {
            $state = pg_result_error_field($res, PGSQL_DIAG_SQLSTATE);
            if ($state == 0) {
                $response_array['status' . $i] = 'success';
            } else {
                $response_array['status' . $i] = 'error';
                $response_array['status'] = 'error';
            }
        }
    }

    $qry = $DELETE_config_people . " WHERE config_id = $configID";
    $i++;
    if (pg_send_query($con, $qry)) {
        $res = pg_get_result($con);
        if ($res) {
            $state = pg_result_error_field($res, PGSQL_DIAG_SQLSTATE);
            if ($state == 0) {
                $response_array['status' . $i] = 'success';
            } else {
                $response_array['status' . $i] = 'error';
                $response_array['status'] = 'error';
            }
        }
    }

    $qry = $DELETE_exam_config . " WHERE config_id = $configID";
    $i++;

    if (pg_send_query($con, $qry)) {
        $res = pg_get_result($con);
        if ($res) {
            $state = pg_result_error_field($res, PGSQL_DIAG_SQLSTATE);
            if ($state == 0) {
                $response_array['status' . $i] = 'success';
            } else {
                $response_array['status' . $i] = 'error';
                $response_array['status'] = 'error';
            }
        }
    }

    pg_close($con);
    echo json_encode($response_array);
}
