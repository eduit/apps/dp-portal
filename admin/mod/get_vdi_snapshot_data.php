<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/func.php";

session_start();

if ($_SESSION['loggedin']) {

    // start caching
    $cachefile = cacheStart();

    // get parents
    $horizonScript = $_SERVER["DOCUMENT_ROOT"] . "/inc/horizon/get_parents.py";
    $json = shell_exec("python3 " . $horizonScript);
    $parents = json_decode($json, true);
    $columns = array('name');
    $sortby = $columns[0];
    $keyValues = array_column($parents, $sortby);
    array_multisort($keyValues, SORT_NATURAL, $parents);

    foreach ($parents as $parent) {
        $parentID = $parent['id'];
        $content[$parentID]['name'] = $parent['name'];

        // get snapshots
        $horizonScript = $_SERVER["DOCUMENT_ROOT"] . "/inc/horizon/get_snapshots.py";
        $json = shell_exec("python3 " . $horizonScript . " " . $parent['id']);
        $snapshots = json_decode($json, true);
        $sortby = 'created_timestamp';
        $keyValues = array_column($snapshots, $sortby);
        array_multisort($keyValues, SORT_ASC, $snapshots);

        foreach ($snapshots as $snapshot) {
            $snapshotID = $snapshot['id'];
            $content[$parentID]['snapshots'][$snapshotID]['name'] = $snapshot['name'];
            $content[$parentID]['snapshots'][$snapshotID]['description'] = $snapshot['description'] ?? '';
            if (isset($snapshot['created_timestamp'])) {
                $seconds = $snapshot['created_timestamp'] / 1000;
                $content[$parentID]['snapshots'][$snapshotID]['date_created'] = date('d.m.Y H:i:s', $seconds);
            } else {
                $content[$parentID]['snapshots'][$snapshotID]['date_created'] = '';
            }
            $date = substr($snapshot['name'], 0, 10);
            $now = new DateTime('today midnight');
            $expired = false;
            if (isValidDate(($date))) {
                $valid = true;
                $date_formatted = date_create($date);
                if ($now > $date_formatted) {
                    $expired = true;
                }
            } else {
                $valid = false;
            }
            $content[$parentID]['snapshots'][$snapshotID]['valid'] = $valid;
            $content[$parentID]['snapshots'][$snapshotID]['expired'] = $expired;
        }
    }
    echo json_encode($content);

    // stop caching
    cacheEnd($cachefile);
}
