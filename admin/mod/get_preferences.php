<?php

$query = $SELECT_prefs;
$result = pg_query($con, $query);
$rowCount = pg_num_rows($result);

echo "<p>{$rowCount} " . _COUNT . "</p>";

if ($_SESSION['user_admin']) {
    $width = 45;
} else {
    $width = 50;
}

echo "<table id=\"preferences\" class=\"admintable generaltable\" style=\"width: 100%;\">
        <thead class=\"resizable sticky\">
        <tr>
            <th style=\"width: {$width}%;\">" . _PREF . "</th>
            <th style=\"width: {$width}%;\">" . _VALUE . "</th>";
            
if ($_SESSION['user_admin']) {
    echo "<th style=\"width: " . 100 - 2 * $width . "%;\">" . _EDIT . "</th>";
}
echo "</tr>
        </thead>
        <tbody class=\"resizable\">";
if ($_SESSION['user_admin']) {
    echo "<tr id=\"empty_row\">
            <td></td>
            <td></td>
            <td></td>
        </tr>";
}

for ($i = 0; $i < $rowCount; $i++) {

    $row = pg_fetch_array($result);
    $preferenceId = $row['pref_id'];
    $preferenceName = $row['pref_name'];
    $preferenceValue = htmlspecialchars($row['pref_value'] ?? '');
    $preferenceDescription = $row['pref_desc'];

    echo "<tr id=\"row_{$preferenceId}\">
            <td id=\"pref_name_{$preferenceId}\" class=\"preference\" title=\"{$preferenceDescription}\">{$preferenceName}</td>
            <td id=\"pref_value_{$preferenceId}\" ";
    if (substr($preferenceValue, 0, 1) === "#") {
        echo "name=\"{$preferenceValue}\" class=\"color_value\"";
    }
    echo ">";
    if ($preferenceValue == "true") {
        echo "<i class=\"fa fa-check-square-o fa-fw true_icon\" title=\"" . _TRUE . "\" aria-label=\"true\"></i>";
    } elseif ($preferenceValue == "false") {
        echo "<i class=\"fa fa-square-o fa-fw false_icon\" title=\"" . _FALSE . "\" aria-label=\"false\"></i>";
    } else {
        echo $preferenceValue;
    }
    echo "</td>";
    if ($_SESSION['user_admin']) {
        echo "<td><i class=\"icon fa fa-edit fa-fw edit_icon\" title=\"" . _EDIT . "\" aria-label=\"" . _EDIT . "\" name=\"edit_row_{$preferenceId}\"></i></td>";
    }
    echo "</tr>";

    if ($_SESSION['user_admin']) {
        echo "<tr id=\"edit_row_{$preferenceId}\" class=\"edit_rows hidden\">
                <td><input readonly type=\"text\" class=\"form-control\" name=\"pref_name_{$preferenceId}\" id=\"edit_pref_name_{$preferenceId}\" required=\"required\" value=\"{$preferenceName}\" maxlength=\"" . MAXLENGTH . "\"></td>
                <td>";
        if ($preferenceValue == "true") {
            echo "<input type=\"checkbox\" name=\"pref_value_{$preferenceId}\" id=\"edit_pref_value_{$preferenceId}\" checked>";
        } elseif ($preferenceValue == "false") {
            echo "<input type=\"checkbox\" name=\"pref_value_{$preferenceId}\" id=\"edit_pref_value_{$preferenceId}\">";
        } else {
            echo "<input type=\"text\" class=\"form-control\" name=\"pref_value_{$preferenceId}\" id=\"edit_pref_value_{$preferenceId}\" required=\"required\" value=\"{$preferenceValue}\" maxlength=\"" . MAXLENGTH . "\">";
        }
        echo "</td>
                <td>";
        echo "<i class=\"icon fa fa-save fa-fw save_preference_icon\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"{$preferenceId}\"></i>";
        echo "<i class=\"icon fa fa-times fa-fw cancel_icon\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"{$preferenceId}\"></i>
                </td>
            </tr>";
    }
}
echo "</tbody></table>";
