<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $poolID = $_POST["pool_id"];

    // get ad machinename from Horizon API
    $horizonScript = $_SERVER["DOCUMENT_ROOT"] . "/inc/horizon/get_machines_state.py";
    $json = shell_exec("python3 " . $horizonScript . " " . $poolID);

    echo $json;
}
