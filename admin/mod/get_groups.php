<?php

if ($_SESSION['loggedin']) {
    if ($tab == "vdi_testing") {
        $group = $vdi_testing;
    } else {
        $group = ${'login_' . $tab};
    }
    getGroupData($group);
}

function getGroupData($group)
{
    include $_SERVER["DOCUMENT_ROOT"] . VARPHP;

    if ($connect = ldap_connect($ldap_address, $ldap_port)) {
        // connected
        ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);

        if (ldap_bind($connect, $ldap_dn, $ldap_password)) {
            
            // get group info
            $filter = "(&(objectclass=posixgroup)(cn=$group))";
            $sr = ldap_search($connect, $ldap_base_dn, $filter);
            $info = ldap_get_entries($connect, $sr);
            $members = $info[0]["memberuid"] ?? 0;
            $num = $members["count"] ?? 0;
            $obj = $info[0] ?? [];
            
            if ($num) {
                array_shift($members);
                asort($members);
            }

            if ($obj) {
                echo "<h3>Info</h3>";
                echo "<ul>";
                echo "<li><b>IAM " . _LINK . "</b>: <a href=\"https://www.password.ethz.ch\" target=\"_blank\">www.password.ethz.ch</a></li>";
                echo "<li><b>LDAP-" . _GROUP . "</b>: <span id=\"group\">$group<span> <i id=\"clipboard\" class=\"fa fa-clipboard clickable\" name=\"$group\" title=\"" . _CLIPBOARD . "\" aria-hidden=\"true\"></i></li>";
                echo "</ul>";
                echo "<br>";
                
                echo "<p>" . $num . " " . _COUNT . "</p>";
                echo "<table id=\"$group-users\" class=\"admintable generaltable\" style=\"width: 100%;\">
                            <thead class=\"resizable sticky\">
                            <tr>
                            <th style=\"width: 20%;\">" . _USERNAME . "<br /></th>
                            <th style=\"width: 40%;\">" . _LASTNAME . "<br /></th>
                            <th style=\"width: 40%;\">" . _FIRSTNAME . "<br /></th>
                            </tr>
                            </thead><tbody class=\"resizable\">";
                if ($num) {
                    foreach ($members as $key => $member) {
                        // get user info
                        $filter = "(|(cn=$member))";
                        $justthese = array("ou", "sn", "givenname", "mail");
                        $sr = ldap_search($connect, $ldap_base_dn, $filter, $justthese);
                        $userInfo = ldap_get_entries($connect, $sr);
                        $lastname = $userInfo[0]['sn'][0] ?? '';
                        $firstname = $userInfo[0]['givenname'][0] ?? '';
                        echo "<tr id=\"row_" . $key . "\">
                                    <td id=\"username_" . $key . "\">$member</td>
                                    <td id=\"lastname_" . $key . "\">$lastname</td>
                                    <td id=\"firstname_" . $key . "\">$firstname</td>";
                        echo "</tr>";
                    }
                }
                echo "</tbody></table>";
                echo "<script>
                        $(function() {
                            $(\"#$group-users\").excelTableFilter();
                            let rowCount = $(\"#all_computers tbody tr:visible\").length;
                            if ($(\"#add_row\").is(\":visible\")){
                                rowCount--;
                            }
                            $(\"#count\").html(rowCount+\"/\");

                            if (/chrom(e|ium)/.test(navigator.userAgent.toLowerCase())) {
                                $(\".dropdown-filter-content\").parent().parent(\"th\").addClass(\"drp_menu\");
                            }
                        });
                    </script>";
            } else {
                echo "<h2>" . _WRONG_PARAMETER . " " . _OR . " " . _GROUP . " " . _NOT_FOUND . "</h2>
				<p>&nbsp;</p>
				<i class=\"fa fa-exclamation fa-fw\" style=\"font-size: 200px; margin-left: 80px;\" title=\"\" aria-label=\"false\" name=\"\"></i>";
            }
        }
    }
    ldap_close($connect);
}
