<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $swList = $_POST["sw"] ?? [];

    // get last entry
    $qry = "SELECT entry_id FROM tbl_sw_connection ORDER BY entry_id DESC LIMIT 1";
    $result = pg_query($con, $qry);
    $row = pg_fetch_assoc($result);
    $entryID = $row['entry_id'];
    $newEntryID = $entryID + 1;
    $swValues = '';

    // insert
    foreach ($swList as $sw) {
        $swValues .= "($newEntryID, $sw),";
    }
    $swValues = substr($swValues, 0, -1);

    $qry = $INSERT_sw_connection . " VALUES $swValues";

    if (pg_query($con, $qry)) {
        $response_array['status'] = 'success';
    } else {
        $response_array['status'] = 'error';
    }

    pg_close($con);
    echo json_encode($response_array);
}
