<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    // POST parameters
    $view = htmlspecialchars($_POST["view"]);
    $limit = htmlspecialchars($_POST["limit"]);
    $start = htmlspecialchars($_POST["start"]);

    // validate parameters
    if (!is_numeric($limit)) {
        $limit = 20;
    }
    if (!is_numeric($start)) {
        $start = 0;
    }

    // include language file
    if (isset($_SESSION['lang'])) {
        $lang = $_SESSION['lang'];
    } else {
        // default
        $qry = $SELECT_lang_default;
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $lang = $row['lang'];
    }
    include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_$lang.php";

    // limit and offset
    $limit_offset = " LIMIT $limit OFFSET $start;";

    switch ($view) {
        case "dates":
            $form = 1;
            $qry = $SELECT_dates .
                " AND
							  tbl_exam.disabled = false AND
							  tbl_exam.archive = false
							ORDER BY tbl_exam.exam_date, tbl_exam_name.exam_name" . $limit_offset;
            break;

        case "dates_all":
            $form = 0;
            $qry = $SELECT_dates .
                " AND
							  (tbl_exam.disabled = true OR
							  tbl_exam.archive = true)
							ORDER BY tbl_exam.exam_date DESC, tbl_exam_name.exam_name" . $limit_offset;
            break;

        case "dates_disabled":
            $form = 0;
            $qry = $SELECT_dates .
                " AND
							  tbl_exam.disabled = true AND
							  tbl_exam.archive = false
							ORDER BY tbl_exam.exam_date DESC, tbl_exam_name.exam_name" . $limit_offset;
            break;

        case "dates_archive":
            $form = 0;
            $qry = $SELECT_dates .
                " AND
							  tbl_exam.disabled = false AND
							  tbl_exam.archive = true
							ORDER BY tbl_exam.exam_date DESC, tbl_exam_name.exam_name" . $limit_offset;
            break;

        default:
            break;
    }

    $data = getData("exam_list", $start, $form, $qry, '');
    $response_array['content'] = $data["content"];
    $response_array['number'] = $data["num"];
    $response_array['form'] = $form;
    
    echo json_encode($response_array);
}

function getData($mode, $start, $form, $qry, $people)
{
    include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
    include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";
    include_once $_SERVER["DOCUMENT_ROOT"] . "/inc/func.php";

    // init variables
    $num = 0;
    $maxlength200 = 200;
    global $content;
    global $typeID;
    global $langID;
    global $examNameID;
    global $lecturerID;
    global $vdpID;
    global $sysadminID;

    // get user
    $user = $_SESSION['user_lastname'] . " " . $_SESSION['user_firstname'];

    // get moodle backup preference
    $pref_qry = $SELECT_pref . " WHERE pref_name = 'moodle_backup'";
    $res = pg_query($con, $pref_qry);
    $row = pg_fetch_assoc($res);
    $moodle_backup = $row['pref_value'];
    $response_array['moodle_backup'] = $moodle_backup;

    // get exam color for own exams
    $pref_qry = $SELECT_pref . " WHERE pref_name = 'exam_color'";
    $res = pg_query($con, $pref_qry);
    $row = pg_fetch_assoc($res);
    $examColor = $row['pref_value'];

    if (isset($_SESSION['displayMode'])) {
        $displayMode = $_SESSION['displayMode'];
        if ($displayMode == "dark") {
            $examColor = invertColor($examColor);
        }
    }

    switch ($mode) {
        case "exam_list":
            $res = pg_query($con, $qry);
            $num = pg_num_rows($res);

            $thead = "<thead class=\"resizable sticky\">
                    <tr>
                    <th style=\"width: 8%;\">" . _DATE . "</th>
                    <th style=\"width: 18%;\">" . _EXAM . "</th>
                    <th style=\"width: 10%;\">" . _LECTURER . "</th>
                    <th style=\"width: 9%;\">" . _VDP . "</th>
                    <th style=\"width: 9%;\">" . _SYSADMIN . "</th>
                    <th style=\"width: 12%;\">" . _TYPE . "</th>
                    <th style=\"width: 5%;\">" . _LANGUAGE . "</th>
                    <th style=\"width: 5%;\">" . _LINK . "</th>
                    <th style=\"width: 9%;\">" . _SEB . "</th>
                    <th style=\"width: 4%;\"></th>
                    <th style=\"width: 11%;\">" . _EDIT . "</th>
                    </tr>
                    </thead><tbody class=\"resizable\">";

            if ($num) {
                if ($start == 0) {
                    $content = $content . $thead;

                    if ($form == 1) {
                        getData("new_exam", $start, $form, $qry, '');
                    } else {
                        $content = $content . "<tr id=\"empty_row\">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            </tr>";
                    }
                }

                for ($i = 0; $i < $num; $i++) {
                    $row = pg_fetch_array($res);
                    $dateRaw = $row['date'];
                    $date = date_create($dateRaw);
                    $date = date_format($date, 'Y-m-d');
                    $semester = getSemester($dateRaw);
                    $id = $row['id'];
                    $examNumber = $row['number'];
                    $examName = $row['exam_name'];
                    $lecturerID = $row['lecturer_id'];
                    $lecturerLastname = $row['lecturer_lastname'];
                    $lecturerFirstname = $row['lecturer_firstname'];
                    $vdpID = $row['vdp_id'];
                    $vdpLastname = $row['vdp_lastname'];
                    $vdpFirstname = $row['vdp_firstname'];
                    $vdpName = $vdpLastname . " " . $vdpFirstname;
                    $sysadminID = $row['sys_id'];
                    $sysadminLastname = $row['sys_lastname'];
                    $sysadminFirstname = $row['sys_firstname'];
                    $sysadminName = $sysadminLastname . " " . $sysadminFirstname;
                    $typeID = $row['type_id'];
                    $type = $row['type'];
                    $langID = $row['lang_id'];
                    $langCode = $row['lang_code'];
                    $language = $row['language'];
                    $link = $row['link'];
                    $seb = $row['seb'];
                    $seb_link = "https://$_SERVER[HTTP_HOST]/" . $seb_dir . $seb;
                    $archived = $row['archived'];
                    $disabled = $row['disabled'];
                    $examNameID = $row['exam_name_id'];
                    $strLength = 15;
                    $style = '';

                    // check if enrollments
                    $qry = $SELECT_enrollments_by_exam . $id;
                    $res1 = pg_query($con, $qry);
                    $num1 = pg_num_rows($res1);
                    if ($num1) {
                        $delete = false;
                    } else {
                        $delete = true;
                    }

                    // get moodle course id
                    $urlComponents = parse_url($link);
                    parse_str($urlComponents['query'] ?? '', $params);
                    $courseID = $params['id'] ?? 0;

                    if (str_contains($type, 'VDI')) {
                        $class = "VDIexam";
                    } else {
                        $class = "";
                    }

                    // set row color if own exam
                    if ($user == $vdpName || $user == $sysadminName) {
                        $style = "style=\"background-color: $examColor\"";
                    }

                    $content = $content . "<tr id=\"row_$id\" class=\"$class\" name=\"$id\" $style>
                            <td id=\"date_$id\">$date ($semester)</td>
                            <td id=\"exam_$id\">$examName ($examNumber)</td>
                            <td id=\"lecturer_$id\"><span class=\"lastname\">$lecturerLastname</span> <span class=\"firstname\">$lecturerFirstname</span></td>
                            <td id=\"vdp_$id\"><span class=\"lastname\">$vdpLastname</span> <span class=\"firstname\">$vdpFirstname</span></td>
                            <td id=\"sysadmin_$id\"><span class=\"lastname\">$sysadminLastname</span> <span class=\"firstname\">$sysadminFirstname</span></td>
                            <td id=\"type_$id\">$type</td>
                            <td id=\"lang_$id\" title=\"$language\">$langCode</td>
                            <td id=\"link_$id\"><a href=\"$link\" target=\"_blank\">";
                    if (strlen($link) > $strLength) {
                        $content = $content . "...";
                    }
                    $content = $content . substr($link, -$strLength) .
                        "</a></td>
                            <td id=\"seb_$id\"><a href=\"$seb_link\" target=\"_blank\">";
                    if (strlen($seb) > $strLength) {
                        $content = $content . "...";
                    }
                    $content = $content . substr($seb, -$strLength) . "</a></td><td></td><td class=\"edit_row\">";
                    if ($archived == "f") {
                        $content = $content . "<i class=\"icon fa fa-edit fa-fw edit_icon\" title=\"" . _EDIT . "\" aria-label=\"" . _EDIT . "\" name=\"edit_row_$id\"></i>";

                        if ($disabled != "f") {
                            $content = $content . "<i id=\"f_$id\" class=\"icon fa fa-eye fa-fw toggle_exam_icon\" title=\"" . _SHOW . "\" aria-label=\"" . _SHOW . "\" name=\"$id\"></i>";
                        } else {
                            $content = $content . "<i id=\"t_$id\" class=\"icon fa fa-eye-slash fa-fw toggle_exam_icon\" title=\"" . _HIDE . "\" aria-label=\"" . _HIDE . "\" name=\"$id\"></i>";
                        }
                    }

                    if ($archived != "f") {
                        $content = $content . "<i id=\"f_a_$id\" class=\"icon fa fa-undo fa-fw archive_exam_icon\" title=\"" . _RESTORE . "\" aria-label=\"" . _RESTORE . "\" name=\"$id\"></i>";
                    } else {
                        $content = $content . "<i id=\"t_a_$id\" class=\"icon fa fa-archive fa-fw archive_exam_icon\" title=\"" . _ARCHIVE . "\" aria-label=\"" . _ARCHIVE . "\" name=\"$id\"></i>";
                        if ($delete) {
                            $content = $content . "<i class=\"icon fa fa-trash fa-fw delete_date_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" name=\"$id\"></i>";
                        }
                        if ($courseID) {
                            if ($moodle_backup == "true") {
                                $content = $content . "<i id=\"backup_$id\" class=\"icon fa fa-file-archive-o fa-fw backup_icon\" title=\"" . _BACKUP . "\" aria-label=\"" . _BACKUP . "\" name=\"$id\" data-href=\"{'moodle_id':$courseID, 'exam_nr':'" . $examNumber . "'}\"></i></td></tr>";
                            } else {
                                $content = $content . "<i id=\"mail_$id\" class=\"icon fa fa-envelope fa-fw mail_icon\" title=\"" . _BACKUP . " (" . _EMAIL . ")\" aria-label=\"" . _BACKUP . " (" . _EMAIL . ")\" name=\"$id\" data-href=\"{'moodle_id':$courseID, 'exam_nr':'$examNumber'}\"></i></td></tr>";
                            }
                        }
                    }

                    // edit row
                    $content = $content . "<tr id=\"edit_row_$id\" class=\"edit_rows hidden\">";
                    // date
                    $content = $content . "<td><input type=\"text\" class=\"form-control datepicker edit_date\" name=\"date_$id\" id=\"edit_date_$id\" required=\"required\" value=\"$date\"></td>";
                    // exam
                    $content = $content . "<td><select class=\"select custom-select form-control select-exam\" name=\"exam_$id\" id=\"edit_exam_$id\" required=\"required\" value=\"\">";
                    getData('exam_select', $start, $form, $qry, '');
                    $content = $content . "</select></td>";
                    // lecturer
                    $content = $content . "<td><select class=\"select custom-select form-control select-lecturer examPeople\" name=\"lecturer_$id\" id=\"edit_lecturer_$id\" required=\"required\" value=\"$lecturerID\">";
                    getData("people_select", $start, $form, $qry, 'lecturer');
                    $content = $content . "</select></td>";
                    // VDP
                    $content = $content . "<td><select class=\"select custom-select form-control select-vdp examPeople\" name=\"vdp_$id\" id=\"edit_vdp_$id\" required=\"required\" value=\"$vdpID\">";
                    getData("people_select", $start, $form, $qry, 'vdp');
                    $content = $content . "</select></td>";
                    // sysadmin
                    $content = $content . "<td><select class=\"select custom-select form-control select-sysadmin examPeople\" name=\"sysadmin_$id\" id=\"edit_sysadmin_$id\" required=\"required\" value=\"$sysadminID\">";
                    getData("people_select", $start, $form, $qry, 'sysadmin');
                    $content = $content . "</select></td>";
                    // type
                    $content = $content . "<td><select class=\"select custom-select form-control select-type\" name=\"type_$id\" id=\"edit_type_$id\" required=\"required\" value=\"\">";
                    getData('exam_type_select', $start, $form, $qry, '');
                    $content = $content . "</select></td>";
                    // language
                    $content = $content . "<td><select class=\"select custom-select form-control select-lang\" name=\"lang_$id\" id=\"edit_lang_$id\" required=\"required\" value=\"\">";
                    getData('lang_select', $start, $form, $qry, '');
                    $content = $content . "</select></td>";
                    // links
                    $content = $content . "<td><input type=\"text\" class=\"form-control edit_link\" name=\"link_$id\" id=\"edit_link_$id\" required=\"required\" value=\"$link\" maxlength=\"$maxlength200\"></td>
                            <td><input type=\"text\" class=\"form-control edit_seb\" name=\"seb_$id\" id=\"edit_seb_$id\" required=\"required\" value=\"$seb\" maxlength=\"$maxlength200\" readonly></td>
                            <td class=\"overflow_visible\"><i id=\"edit_upload_seb_config_$id\" class=\"icon fa fa-upload fa-fw upload_icon edit_upload\" title=\"" . _UPLOAD_SEB . "\" aria-label=\"" . _UPLOAD_SEB . "\" name=\"$id\"></i>
                            <input type=\"file\" accept=\".seb\" id=\"fileloader_dates_$id\" class=\"edit_fileloader_dates hidden\" name=\"$id\" title=\"" . _UPLOAD_SEB . "\" data-href=\"$seb_dir\" /></td>
                            <td>";
                    $content = $content . "<i class=\"icon fa fa-save fa-fw save_date_icon\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"$id\"></i>";
                    $content = $content . "<i class=\"icon fa fa-times fa-fw cancel_icon\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"$id\"></i>
                            </td></tr>";
                }

                $content = $content . "</tbody></table>";
            } else {
                if ($form == 1 && $start == 0) {
                    $content = $content . $thead;
                    getData("new_exam", $start, $form, $qry, '');
                    $content = $content . "</tbody></table>";
                } else {
                    $content = '';
                }
            }
            break;

        case "new_exam":
            $content = $content . "<tr id=\"add_row\">";
            // date
            $content = $content . "<td><input type=\"text\" class=\"form-control datepicker\" name=\"date\" id=\"add_date\" required=\"required\" value=\"\">
                    <span id=\"date_missing\" class=\"invalid-feedback\">" . _DATE . " " . _MISSING . "</span></td>";
            // exam
            $content = $content . "<td><select class=\"select custom-select form-control\" name=\"exam\" id=\"add_exam\" required=\"required\">";
            $examNameID = '';
            getData("exam_select", $start, $form, $qry, '');
            $content = $content . "</select>
                    <br><span id=\"exam_missing\" class=\"invalid-feedback\">" . _EXAM . " " . _MISSING . "</span></td>";
            // lecturer
            $content = $content . "<td><select class=\"select custom-select form-control examPeople\"  name=\"lecturer\" id=\"add_lecturer\" required=\"required\" value=\"\">";
            getData("people_select", $start, $form, $qry, 'lecturer');
            $content = $content . "</select>
                    <br><span id=\"lecturer_missing\" class=\"invalid-feedback\">" . _LECTURER . " " . _MISSING . "</span></td>";
            // VDP
            $content = $content . "<td><select class=\"select custom-select form-control examPeople\"  name=\"vdp\" id=\"add_vdp\" required=\"required\" value=\"\">";
            getData("people_select", $start, $form, $qry, 'vdp');
            $content = $content . "</select>
                    <br><span id=\"vdp_missing\" class=\"invalid-feedback\">" . _VDP . " " . _MISSING . "</span></td>";
            // sysadmin
            $content = $content . "<td><select class=\"select custom-select form-control examPeople\"  name=\"sysadmin\" id=\"add_sysadmin\" required=\"required\" value=\"\">";
            getData("people_select", $start, $form, $qry, 'sysadmin');
            $content = $content . "</select>
                    <br><span id=\"sysadmin_missing\" class=\"invalid-feedback\">" . _SYSADMIN . " " . _MISSING . "</span></td>";
            // type
            $content = $content . "<td><select class=\"select custom-select form-control\"  name=\"type\" id=\"add_type\" required=\"required\" value=\"\">";
            $typeID = '';
            getData("exam_type_select", $start, $form, $qry, '');
            $content = $content . "</select>
                    <br><span id=\"type_missing\" class=\"invalid-feedback\">" . _TYPE . " " . _MISSING . "</span></td>";
            // language
            $content = $content . "<td><select class=\"select custom-select form-control\"  name=\"lang\" id=\"add_lang\" required=\"required\" value=\"\">";
            $langID = '';
            getData("lang_select", $start, $form, $qry, '');
            $content = $content . "</select>
                    <br><span id=\"lang_missing\" class=\"invalid-feedback\">" . _LANGUAGE . " " . _MISSING . "</span></td>";
            // links
            $content = $content . "<td><input type=\"text\" class=\"form-control\" name=\"link\" id=\"add_link\" required=\"required\" value=\"\" maxlength=\"$maxlength200\">
                    <span id=\"link_missing\" class=\"invalid-feedback\">" . _LINK . " " . _MISSING . "</span><span id=\"link_wrong\" class=\"invalid-feedback\">" . _LINK . " " . _WRONG . "</span></td>
                    <td><input type=\"text\" class=\"form-control\" name=\"seb\" id=\"add_seb\" required=\"required\" value=\"\" maxlength=\"$maxlength200\" readonly>
                    <span id=\"seb_missing\" class=\"invalid-feedback\">" . _SEB . " " . _MISSING . "</span><span id=\"seb_wrong\" class=\"invalid-feedback\">" . _SEB . " " . _WRONG . "</span></td>
                    <td><i id=\"upload_seb_config\" class=\"icon fa fa-upload fa-fw upload_icon\" title=\"" . _UPLOAD_SEB . "\" aria-label=\"" . _UPLOAD_SEB . "\" name=\"upload_seb_config\"></i>
                    <input type=\"file\" accept=\".seb\" id=\"fileloader_dates\" name=\"fileToUpload\" title=\"" . _UPLOAD_SEB . "\" class=\"hidden\" data-href=\"$seb_dir\" /></td>
                    <td><i id=\"exam_submit\" class=\"icon fa fa-plus fa-fw add_icon\" title=\"" . _ADD . "\" aria-label=\"" . _ADD . "\" name=\"add_date\"></i></td></tr>";
            break;

        case "exam_select":
            $qry = $SELECT_exam_name;
            $res = pg_query($con, $qry);
            $num = pg_num_rows($res);

            $content = $content . "<option title=\"" . _NO_EXAM_2 . "\" value=\"0\" disabled ";
            if (!$examNameID) {
                $content = $content . " selected";
            }
            $content = $content . ">" . _NO_EXAM_2 . "</option>\n";

            for ($i = 0; $i < $num; $i++) {
                $row = pg_fetch_array($res);
                $id = $row['exam_name_id'];
                $examNumber = $row['exam_number'];
                $examName = $row['exam_name'];

                $content = $content . "<option value=\"$id\" title=\"$examNumber\"";
                if ($id == $examNameID) {
                    $content = $content . " selected";
                }
                $content = $content . ">$examName ($examNumber)</option>\n";
            }
            break;

        case "exam_type_select":
            $qry = $SELECT_exam_type . " ORDER BY type_name";
            $res = pg_query($con, $qry);
            $num = pg_num_rows($res);

            $content = $content . "<option title=\"" . _NO_TYPE . "\" value=\"0\" disabled ";
            if (!$typeID) {
                $content = $content . " selected";
            }
            $content = $content . ">" . _NO_TYPE . "</option>\n";

            for ($i = 0; $i < $num; $i++) {
                $row = pg_fetch_array($res);
                $id = $row['type_id'];
                $type = $row['type'];

                $content = $content . "<option value=\"$id\" title=\"$type\"";
                if ($id == $typeID) {
                    $content = $content . " selected";
                }
                $content = $content . ">$type</option>\n";
            }
            break;

        case "people_select":
            $const = "_NO_" . strtoupper($people);
            $title = constant($const);
            $content = $content . "<option title=\"$title\" value=\"0\" disabled selected>$title</option>\n";
            break;

        case "lang_select":
            $qry = $SELECT_language;
            $res = pg_query($con, $qry);
            $num = pg_num_rows($res);

            for ($i = 0; $i < $num; $i++) {

                $row = pg_fetch_array($res);
                $id = $row["lang_id"];
                $langCode = $row['lang_code'];

                $content = $content . "<option value=\"$id\"";
                if ($id == $langID) {
                    $content = $content . " selected";
                }
                $content = $content . ">$langCode</option>\n";
            }
            break;

        default:
            break;
    }
    return array("content" => $content, "num" => $num);
}
