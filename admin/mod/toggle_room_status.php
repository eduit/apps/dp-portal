<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$exam = $_POST["exam"];
	$room = $_POST["room"];
	$active = $_POST["active"];

	if ($active == 't') {
		$active = 'true';
		$qry = $UPDATE_exam_student . " SET active='false' WHERE room_id=$room";

		if (pg_query($con, $qry)) {
			$response_array['status1'] = 'success';
		} else {
			$response_array['status1'] = 'error';
		}
	} else {
		$active = 'false';
		$response_array['status1'] = 'nothing to do';
	}

	$qry = $UPDATE_exam_student . " SET active=$active WHERE exam_id=$exam AND room_id=$room";

	if (pg_query($con, $qry)) {
		$response_array['status2'] = 'success';
	} else {
		$response_array['status2'] = 'error';
	}

	pg_close($con);
	echo json_encode($response_array);
}
