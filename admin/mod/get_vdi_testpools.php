<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $vdiPool = $_POST["poolName"] ?? "";
    $setup = $_POST["setup"] ?? 1;

    // get desktop pools
    $horizonScript = $_SERVER["DOCUMENT_ROOT"] . "/inc/horizon/get_desktop_pools.py";
    $json = shell_exec("python3 " . $horizonScript);
    $desktopPools = json_decode($json, true);

    $qry = $SELECT_pref . " WHERE pref_name = 'vdi_testpools'";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $testPools = $row['pref_value'];

    $pool = "";
    $vdiPools = "";
    $first = true;

    foreach ($desktopPools as $pool) {
        $poolID = $pool['id'];
        $poolName = $pool['name'];

        if (str_contains($poolName, $testPools)) {
            if ($first && $setup > 1) {
                $disabled = "disabled";
            } else {
                $disabled = "";
            }
            
            $option = "<option value=\"$poolID\"";

            if ($vdiPool == $poolName || empty($pool)) {
                $option = $option . " selected>" . $poolName;
                $pool = $poolName;
            } else {
                $option = $option . $disabled . ">" . $poolName;
            }
            $option = $option . "</option>";
            $vdiPools = $vdiPools . $option;
            $first = false;
        }
    }

    $response_array['content'] = $vdiPools;

    pg_close($con);
    echo json_encode($response_array);
}
