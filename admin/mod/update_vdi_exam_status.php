<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

$hash = "$2y$10$4NzC9rr5MaViahTZH6XKdetPizfBMmUVhL0932g.QRnbt/uaDQ72u";
$response_array = [];
$check = true;

if (!empty($_GET['key'])) {
    $check = password_verify($_GET['key'], $hash);
} else {
    $response_array = "authentication key is missing";
    $check = false;
}

if ($check) {
    $qry = "UPDATE tbl_vdi_exam SET status_id = 4 WHERE status_id < 3 AND deadline < current_date";
    if (pg_query($con, $qry)) {
        $response_array['status'] = 'success';
    } else {
        $response_array['status'] = 'error';
    }
    pg_close($con);
}

echo json_encode($response_array);
