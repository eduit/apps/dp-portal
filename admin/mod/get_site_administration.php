<?php

if ($_SESSION['loggedin']) {
	$forbidden = false;

	// get user permissions from DB
	$qry = $SELECT_navigation . " WHERE nav_element='$tab'";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$navUser = $row['nav_user'] ?? 'f';
	if ($navUser == 't' || $_SESSION['user_admin']) {
		$navUser = true;
	} else {
		$navUser = false;
	}

	if ($navUser) {
		getAdminData($tab);
	} else {
		getAdminData(false);
	}
}

function getAdminData($tab)
{
	include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
	include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

	// Include language
	if (isset($_SESSION['lang'])) {
		$lang = $_SESSION['lang'];
	} else {
		// default
		$qry = $SELECT_lang_default;
		$res = pg_query($con, $qry);
		$row = pg_fetch_assoc($res);
		$lang = $row['lang'];
	}

	$dark = "";
	$displayMode = "";
	if (isset($_SESSION['displayMode'])) {
		$displayMode = $_SESSION['displayMode'];
		if ($displayMode == "dark") {
			$dark = "_dark";
		}
	}

	switch ($tab) {
		case "explorer":
			echo "<div class=\"form-group fitem row\">
						<div class=\"col-md-2 admin_task\" id=\"explorer_buttons\">
							<i id=\"explorer_delete_button\" class=\"icon fa fa-trash fa-fw\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\"></i>
						</div>
						<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
						</div>
					</div>";
			$folders = array_diff(scandir($upload_dir), array('..', '.'));
			$select = '';
			foreach ($folders as $folder) {
				$select = $select . "<option value=\"$folder\">$folder</option>";
			}
			echo "<div id=\"explorer\" class=\"admin_task\">
					<div class=\"form-group fitem row\">
						<div class=\"col-md-2\">
							<label class=\"col-form-label\" for=\"id_explorer\">" . _FOLDERS . ": </label>
						</div>
						<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
							<span data-fieldtype=\"text\">
								<select class=\"custom-select form-control\" name=\"explorer\" id=\"id_explorer\" required=\"required\" style=\"width: 400px;\">
									$select
								</select>
							</span>
						</div>
					</div>
				</div>";
			echo "<div id=\"explorer_data\" class=\"admin_task\"></div>";
			echo "<div id=\"dialog\" class=\"hidden\" title=\"" . _FILE_PHOTO . "\"></div>";
			break;

		case "cleanup":
			echo "<div id=\"cleanup_data\" class=\"admin_task\">
					<p>" . _CLEANUP_TEXT . "</p>
					<div class=\"form-group fitem row\">
						<div class=\"col-md-2\">
							<label class=\"col-form-label\" for=\"cleanup_date\">" . _DATE . ": </label>
						</div>
						<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
							<span data-fieldtype=\"text\">
								<input type=\"text\" class=\"form-control datepicker_year\" name=\"date\" id=\"cleanup_date\" required=\"required\" value=\"\" style=\"width: 400px;\">
							</span>&nbsp;&nbsp;<i id=\"cleanup_button\" class=\"fa fa-recycle hidden icon\" aria-hidden=\"true\" title=\"" . _CLEANUP . "\"></i></div>
							<div class=\"form-control-feedback invalid-feedback\" id=\"id_missing_date\">" . _DATE . " " . _MISSING . "</div>
						</div>
					</div>";
			break;

		case "computers":
			echo "<div class=\"form-group fitem row\">
						<div class=\"col-md-2 admin_task\" id=\"computer_buttons\">";
					echo "<i id=\"computer_template_list\" class=\"icon fa fa-list-alt fa-fw\" title=\"" . _TEMPLATE . "\" aria-label=\"" . _TEMPLATE . "\"></i>";
					echo "<i id=\"computer_upload_button\" class=\"icon fa fa-upload fa-fw\" title=\"" . _UPLOAD_COMPUTERS . "\" aria-label=\"" . _UPLOAD_COMPUTERS . "\"></i>";
					echo "<input type=\"file\" accept=\".csv\" id=\"fileloader_computers\" class=\"btn hidden\" name=\"fileToUpload\" title=\"" . _COMPUTERS . "\" />";
					echo "<i id=\"computer_save_button\" class=\"icon fa fa-save fa-fw\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\"></i>";
					echo "<i id=\"hide_unmodified_icon\" class=\"icon fa fa-eye-slash fa-fw\" title=\"" . _HIDE_EXISTING . "\" aria-label=\"" . _HIDE_EXISTING . "\" name=\"\"></i>";
					echo "<i id=\"cancel_icon_computers\" class=\"icon fa fa-times fa-fw\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"\"></i>
						</div>
						<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
						</div>
					</div>";
			echo "<div id=\"div_notification\" class=\"hidden\"><p id=\"notification_text\"></p></div>";
			echo "<div id=\"computer_data\" class=\"admin_task\"><table id=\"computer_list\" class=\"admintable generaltable\" style=\"width: 100%;\"></table></div>";
			break;

		case "sharepoint":
			echo "<div class=\"form-group fitem row\">
						<div class=\"col-md-2 admin_task\" id=\"sharepoint_buttons\">";
					echo "<i id=\"sharepoint_save_button\" class=\"icon fa fa-save fa-fw\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\"></i>";
					echo "<i id=\"cancel_icon_sp\" class=\"icon fa fa-times fa-fw\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"\"></i>
						</div>
						<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
						</div>
					</div>";
			echo "<div id=\"sharepoint_data\" class=\"admin_task\"></div>";
			break;

		case "templates":
			// select template types
			$qry = $SELECT_template_types;
			$res1 = pg_query($con, $qry);
			$num1 = pg_num_rows($res1);

			$select = "";

			for ($i = 0; $i < $num1; $i++) {
				$row1 = pg_fetch_array($res1);
				$type = $row1['type'];
				$select = $select . "<option value=\"$type\">$type</option>";
			}
			echo "<div id=\"templates\" class=\"admin_task\">
					<div class=\"form-group fitem row\">
						<div class=\"col-md-2\">
							<label class=\"col-form-label\" for=\"id_templates\">" . _TEMPLATES . ": </label>
						</div>
						<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
							<span data-fieldtype=\"text\">
								<select class=\"custom-select form-control\" name=\"templates\" id=\"id_templates\" required=\"required\" style=\"width: 400px;\">
									$select
								</select>
							</span>
						</div>
					</div>
				</div>";

			$type = $_GET['type'] ?? "";
			getTemplates($type);
			break;

		case "navigation":
			echo "<div class=\"form-group fitem row\">
						<div class=\"col-md-2 admin_task\" id=\"navigation_buttons\">
							<p><i id=\"navigation_save_button\" class=\"icon fa fa-save fa-fw\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\"></i><br>
							<span style=\"color: red\">*</span> " . _VHO . "<br><span style=\"color: red\">**</span> " . _ADMIN . "</p>
						</div>
						<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
						</div>
					</div>";
					
			$navLevel = 0;
			$navGroupOld = 0;

			$qry = $SELECT_navigation . " WHERE nav_level=$navLevel ORDER BY nav_group, nav_sort";
			$res1 = pg_query($con, $qry);
			$num1 = pg_num_rows($res1);

			echo "<ul id=\"navigation_order\" class=\"sortable\">";
			for ($i = 0; $i < $num1; $i++) {
				$row1 = pg_fetch_array($res1);
				$navGroup = $row1['nav_group'];
				if ($navGroup != $navGroupOld) {
					echo "<li id=\"group_$navGroup\" class=\"grab\"><hr></li>";
				}
				$navGroupOld = $navGroup;
				$navID = $row1['nav_id'];
				$navElementRaw = $row1['nav_element'];
				$navElement = constant(strtoupper("_" . $navElementRaw));
				$navIcon = $row1['nav_icon'];
				$navVHO = $row1['nav_vho'];
				if ($navVHO == "t") {
					$navVHO = "<span style=\"color: red\">*</span>";
				} else {
					$navVHO = "";
				}
				$navUser = $row1['nav_user'];
				if ($navUser == "f") {
					$navUser = "<span style=\"color: red\">**</span>";
				} else {
					$navUser = "";
				}
				$navVisible = $row1['nav_visible'];
				if ($navVisible == "f") {
					$navVisible = "";
				} else {
					$navVisible = "checked ";
				}
				if ($navElementRaw == "site_administration") {
					$navVisible = "checked disabled";
				}
				echo "<li id=\"$navID\" class=\"grab ui-state-default\"><i class=\"icon fa fa-$navIcon fa-fw \" aria-hidden=\"true\"></i> <b>$navElement</b> $navVHO $navUser<input id=\"check_$navID\" name=\"$navID\" class=\"nav_checkbox\" type=\"checkbox\" $navVisible/>";
				$navLevel = 1;

				$qry = $SELECT_navigation . " WHERE nav_level=$navLevel AND nav_parent_id=$navID ORDER BY nav_sort";
				$res2 = pg_query($con, $qry);
				$num2 = pg_num_rows($res2);
				if($num2) {
					echo "<ul class=\"sortable second\">";
					for ($j = 0; $j < $num2; $j++) {
						$row2 = pg_fetch_array($res2);
						$navID = $row2['nav_id'];
						$navElementRaw = $row2['nav_element'];
						$navElement = constant(strtoupper("_" . $navElementRaw));
						$navIcon = $row2['nav_icon'];
						$navVHO = $row2['nav_vho'];
						if ($navVHO == "t") {
							$navVHO = "<span style=\"color: red\">*</span>";
						} else {
							$navVHO = "";
						}
						$navUser = $row2['nav_user'];
						if ($navUser == "f") {
							$navUser = "<span style=\"color: red\">**</span>";
						} else {
							$navUser = "";
						}
						$navVisible = $row2['nav_visible'];
						if ($navVisible == "f") {
							$navVisible = "";
						} else {
							$navVisible = "checked ";
						}
						if ($navElementRaw == "navigation") {
							$navVisible = "checked disabled";
						}
						echo "<li id=\"$navID\" class=\"grab ui-state-default$dark\"><i class=\"icon fa fa-$navIcon fa-fw \" aria-hidden=\"true\"></i> $navElement $navVHO $navUser<input id=\"check_$navID\" name=\"$navID\" class=\"nav_checkbox\" type=\"checkbox\" $navVisible/></li>";
					}
					echo "</ul>";
				}
				echo "</li>";
			}
			echo "</ul>";
			break;

		case "vho_themes":
			$qry = $SELECT_VHO_themes . " ORDER BY theme_name";
			$res1 = pg_query($con, $qry);
			$num1 = pg_num_rows($res1);

			echo "<p" . $num1 . " " . _COUNT . "</p>";

			echo "<table id=\"vho_themes\" class=\"admintable generaltable\" style=\"width: 100%;\">
					<thead class=\"resizable sticky\">
					<tr>
						<th style=\"width: 20%;\">" . _THEME . "</th>
						<th style=\"width: 60%;\">" . _VALUES . "</th>
						<th style=\"width: 20%;\">" . _EDIT . "</th>
					</tr>
					</thead><tbody class=\"resizable\">";

			echo "<tr id=\"add_row\">
					<td><input type=\"text\" class=\"form-control\" name=\"theme\" id=\"add_theme\" required=\"required\" value=\"\" maxlength=\"" . MAXLENGTH . "\">
					<span id=\"theme_missing\" class=\"invalid-feedback\">" . _THEME . " " . _MISSING . "</span></td>
					<td><input type=\"text\" class=\"form-control\" name=\"values\" id=\"add_values\" required=\"required\" value=\"\" maxlength=\"" . MAXLENGTH . "\">
					<span id=\"values_missing\" class=\"invalid-feedback\">" . _VALUES . " " . _MISSING . "</span></td>
					<td><i id=\"theme_submit\" class=\"icon fa fa-plus fa-fw add_icon\" title=\"" . _ADD . "\" aria-label=\"" . _ADD . "\" name=\"add_theme\"></i></td>
					</tr>";

			for ($i = 0; $i < $num1; $i++) {

				$row1 = pg_fetch_array($res1);
				$id = $row1['theme_id'];
				$theme = $row1['theme_name'];
				$values = $row1['theme_values'];

				echo "<tr id=\"row_$id\">
						<td id=\"theme_$id\">$theme</td>
						<td id=\"values_$id\" class=\"clip\">$values</td>
						<td class=\"edit_row\"><i class=\"icon fa fa-edit fa-fw edit_icon\" title=\"" . _EDIT . "\" aria-label=\"" . _EDIT . "\" name=\"edit_row_$id\"></i><i class=\"icon fa fa-trash fa-fw delete_theme_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" name=\"$id\"></i></td>
						</tr>";
				echo "<tr id=\"edit_row_$id\" class=\"edit_rows hidden\">
						<td><input type=\"text\" class=\"form-control\" name=\"theme_$id\" id=\"edit_theme_$id\" required=\"required\" value=\"$theme\" maxlength=\"" . MAXLENGTH . "\"></td>
						<td><input type=\"text\" class=\"form-control\" name=\"values_$id\" id=\"edit_values_$id\" required=\"required\" value=\"$values\" maxlength=\"" . MAXLENGTH . "\"></td>
						<td>";
				echo "<i class=\"icon fa fa-save fa-fw save_theme_icon\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"$id\"></i>";
				echo "<i class=\"icon fa fa-times fa-fw cancel_icon\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"$id\"></i>
						</td>
						</tr>";
			}
			echo "</tbody></table>";
			break;

		case "vdi_pools":
			$qry = $SELECT_vdi_pools . " AND tbl_language.lang_code = '$lang' ORDER BY tbl_vdi_pool.pool_id;";
			$res1 = pg_query($con, $qry);
			$num1 = pg_num_rows($res1);

			echo "<p>" . $num1 . " " . _COUNT . "</p>";

			echo "<table id=\"vdi_pools\" class=\"admintable generaltable\" style=\"width: 100%;\">
					<thead class=\"resizable sticky\">
					<tr>
						<th style=\"width: 20%;\">" . _POOL . "</th>
						<th style=\"width: 20%;\">" . _DESCRIPTION . "</th>
						<th style=\"width: 40%;\">" . _INTERNAL_INFORMATION . "</th>
						<th style=\"width: 20%;\">" . _EDIT . "</th>
					</tr>
					</thead><tbody class=\"resizable\">";

			echo "<tr id=\"add_row\">
					<td><input type=\"text\" class=\"form-control\" name=\"pool_description\" id=\"add_pool_description\" required=\"required\" value=\"\" maxlength=\"" . MAXLENGTH . "\">
					<span id=\"pool_description_missing\" class=\"invalid-feedback\">" . _DESCRIPTION . " " . _MISSING . "</span></td>
					<td><input type=\"text\" class=\"form-control\" name=\"pool_description_lang\" id=\"add_pool_description_lang\" required=\"required\" value=\"\" maxlength=\"" . MAXLENGTH . "\">
					<span id=\"pool_description_lang_missing\" class=\"invalid-feedback\">" . _DESCRIPTION . " " . _MISSING . "</span></td>
					<td><input type=\"text\" class=\"form-control\" name=\"pool_info_text\" id=\"add_info_text\" required=\"required\" value=\"\"></td>
					<td><i id=\"pool_submit\" class=\"icon fa fa-plus fa-fw add_icon\" title=\"" . _ADD . "\" aria-label=\"" . _ADD . "\" name=\"add_pool_description\"></i></td>
					</tr>";

			for ($i = 0; $i < $num1; $i++) {

				$row1 = pg_fetch_array($res1);
				$id = $row1['pool_id'];
				$pool = $row1['description'];
				$pool_lang = $row1['description_lang'];
				$info_text = $row1['info_text'];

				echo "<tr id=\"row_$id\">
						<td id=\"pool_$id\">$pool</td>
						<td id=\"pool_lang_$id\">$pool_lang</td>
						<td id=\"info_text_$id\">$info_text</td>
						<td class=\"edit_row\"><i class=\"icon fa fa-edit fa-fw edit_icon\" title=\"" . _EDIT . "\" aria-label=\"" . _EDIT . "\" name=\"edit_row_$id\"></i><i class=\"icon fa fa-trash fa-fw delete_pool_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" name=\"$id\"></i></td>
						</tr>";
				echo "<tr id=\"edit_row_$id\" class=\"edit_rows hidden\">
						<td><input type=\"text\" class=\"form-control\" name=\"pool_$id\" id=\"edit_pool_description_$id\" required=\"required\" value=\"$pool\" maxlength=\"" . MAXLENGTH . "\" readonly></td>
						<td><input type=\"text\" class=\"form-control\" name=\"pool_lang_$id\" id=\"edit_pool_description_lang_$id\" required=\"required\" value=\"$pool_lang\" maxlength=\"" . MAXLENGTH . "\"></td>
						<td><input type=\"text\" class=\"form-control\" name=\"info_text_$id\" id=\"edit_info_text_$id\" required=\"required\" value=\"$info_text\"></td>
						<td>";
				echo "<i class=\"icon fa fa-save fa-fw save_pool_icon\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"$id\"></i>";
				echo "<i class=\"icon fa fa-times fa-fw cancel_icon\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"$id\"></i>
						</td>
						</tr>";
			}
			echo "</tbody></table>";
			break;

		case "status":
			$qry = $SELECT_exam_status . " AND tbl_language.lang_code = '$lang' ORDER BY tbl_exam_status.status_id;";
			$res1 = pg_query($con, $qry);
			$num1 = pg_num_rows($res1);

			echo "<p>" . $num1 . " " . _COUNT . "</p>";

			echo "<table id=\"status\" class=\"admintable generaltable\" style=\"width: 100%;\">
					<thead class=\"resizable sticky\">
					<tr>
						<th style=\"width: 20%;\">" . _STATUS_CODE . "</th>
						<th style=\"width: 40%;\">" . _STATUS . "</th>
						<th style=\"width: 20%;\">" . _COLOR . "</th>
						<th style=\"width: 10%;\">" . _STANDARD . "</th>
						<th style=\"width: 10%;\">" . _EDIT . "</th>
					</tr>
					</thead><tbody class=\"resizable\">";

			echo "<tr id=\"add_row\">
					<td><input type=\"text\" class=\"form-control\" name=\"status\" id=\"add_status\" required=\"required\" value=\"\" maxlength=\"" . MAXLENGTH . "\"></td>
					<td>
						<input type=\"text\" class=\"form-control\" name=\"status_lang\" id=\"add_status_lang\" required=\"required\" value=\"\" maxlength=\"" . MAXLENGTH . "\">
						<span id=\"status_missing\" class=\"invalid-feedback\">" . _STATUS . " " . _MISSING . "</span>
					</td>
					<td><input type=\"text\" class=\"form-control\" name=\"color\" id=\"add_color\" value=\"\" maxlength=\"" . MAXLENGTH . "\"></td>
					<td><input type=\"checkbox\" name=\"standard\" id=\"add_standard\"></td>
					<td><i id=\"status_submit\" class=\"icon fa fa-plus fa-fw add_icon\" title=\"" . _ADD . "\" aria-label=\"" . _ADD . "\" name=\"add_status\"></i></td>
				</tr>";

			for ($i = 0; $i < $num1; $i++) {

				$row1 = pg_fetch_array($res1);
				$id = $row1['status_id'];
				$status = $row1['status'];
				$status_lang = $row1['status_lang'];
				$bgColor = $row1['color'];
				$txtColor = getTextColour($bgColor);
				$standard = $row1['standard'];

				echo "<tr id=\"row_$id\">
						<td id=\"status_code_$id\">$status</td>
						<td id=\"status_$id\">$status_lang</td>
						<td id=\"color_$id\" style=\"background-color: $bgColor; color: $txtColor; font-weight: bold;\">$bgColor</td>
						<td id=\"standard_$id\">";
				if ($standard == "t") {
					echo "<i class=\"fa fa-check-square-o fa-fw true_icon\" title=\"" . _TRUE . "\" aria-label=\"true\" name=\"standard_" . $id . "\"></i>";
					echo "<span style=\"opacity: 0;\">" . _TRUE . "</span>";
				} else {
					echo "<i class=\"fa fa-square-o fa-fw false_icon\" title=\"" . _FALSE . "\" aria-label=\"false\" name=\"standard_" . $id . "\"></i>";
					echo "<span style=\"opacity: 0;\">" . _FALSE . "</span>";
				}
				echo "</td>
						<td class=\"edit_row\"><i class=\"icon fa fa-edit fa-fw edit_icon\" title=\"" . _EDIT . "\" aria-label=\"" . _EDIT . "\" name=\"edit_row_$id\"></i><i class=\"icon fa fa-trash fa-fw delete_status_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" name=\"$id\"></i></td>
						</tr>";
				echo "<tr id=\"edit_row_$id\" class=\"edit_rows hidden\">
						<td><input type=\"text\" class=\"form-control\" name=\"status_$id\" id=\"edit_status_$id\" required=\"required\" value=\"$status\" maxlength=\"" . MAXLENGTH . "\" readonly></td>
						<td><input type=\"text\" class=\"form-control\" name=\"status_lang_$id\" id=\"edit_status_lang_$id\" required=\"required\" value=\"$status_lang\" maxlength=\"" . MAXLENGTH . "\"></td>
						<td><input type=\"text\" class=\"form-control\" name=\"color_$id\" id=\"edit_color_$id\" value=\"$bgColor\" maxlength=\"" . MAXLENGTH . "\"></td>
						<td><input type=\"checkbox\" class=\"form-control\" name=\"standard_$id\" id=\"edit_standard_$id\"";
				if ($standard == "t") {
					echo " checked";
				}
				echo "></td>
						<td>";
				echo "<i class=\"icon fa fa-save fa-fw save_status_icon\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"$id\"></i>";
				echo "<i class=\"icon fa fa-times fa-fw cancel_icon\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"$id\"></i>
						</td>
						</tr>";
			}
			echo "</tbody></table>";
			break;

		default:
				echo "<h2>" . _WRONG_PARAMETER . "</h2>
				<p>&nbsp;</p>
				<i class=\"fa fa-exclamation fa-fw\" style=\"font-size: 200px; margin-left: 80px;\" title=\"\" aria-label=\"false\" name=\"\"></i>";
			break;
	}
}

function getTemplates($type)
{
	include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
	include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

	// Include language
	if (isset($_SESSION['lang'])) {
		$lang = $_SESSION['lang'];
	} else {
		// default
		$qry = $SELECT_lang_default;
		$res = pg_query($con, $qry);
		$row = pg_fetch_assoc($res);
		$lang = $row['lang'];
	}

	if ($type == "vho-mail") {
		$qry = $SELECT_templates . " AND type = 'vho-mail' AND lang_code = '$lang' ORDER BY lang_id";
		$res2 = pg_query($con, $qry);
		$row2 = pg_fetch_assoc($res2);
		$id = $row2["template_id"];
		$text = $row2["text"];

		echo "<div class=\"form-group fitem row\">
								<div class=\"col-md-2 admin_task\" id=\"vho_mail_template_buttons\">
									<i id=\"vho_mail_save_button\" class=\"icon fa fa-save fa-fw\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\"></i>
								</div>
								<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
								</div>
							</div>";

		echo "<div><p><textarea id=\"vho_mail_template\" class=\"rte\" name=\"$id\">$text</textarea></p></div>";
		echo "</div>";
	} else {
		echo "<div id=\"template_data\" class=\"admin_task\">";

		$qry = $SELECT_templates . " AND tbl_language.lang_code='$lang' AND tbl_template.type='$type' ORDER BY text";
		$res1 = pg_query($con, $qry);
		$num1 = pg_num_rows($res1);

		echo "<p>" . $num1 . " " . _COUNT . "</p>";

		echo "<table id=\"templates\" class=\"admintable generaltable\" style=\"width: 100%;\">
							<thead class=\"resizable sticky\">
							<tr>
								<th style=\"width: 80%;\">" . _DESCRIPTION . "</th>
								<th style=\"width: 10%;\">" . _EDIT . "</th>
							</tr>
							</thead><tbody class=\"resizable\">";
		// add row
		echo "<tr id=\"add_row\">
							<td><input type=\"text\" class=\"form-control\" name=\"text\" id=\"add_text\" required=\"required\" value=\"\" maxlength=\"" . MAXLENGTH . "\">
							<span id=\"text_missing\" class=\"invalid-feedback\">" . _DESCRIPTION . " " . _MISSING . "</span></td>";
		echo "<td><i id=\"template_submit\" class=\"icon fa fa-plus fa-fw add_icon\" title=\"" . _ADD . "\" aria-label=\"" . _ADD . "\" name=\"add_template\"></i></td></tr>";
		// add row

		for ($i = 0; $i < $num1; $i++) {
			$row1 = pg_fetch_array($res1);
			$id = $row1['template_id'];
			$text = $row1['text'];

			echo "<tr id=\"tl_row_$id\">
								<td id=\"text_$id\">$text</td>
								<td class=\"edit_row\"><i class=\"icon fa fa-edit fa-fw edit_icon\" title=\"" . _EDIT . "\" aria-label=\"" . _EDIT . "\" name=\"edit_row_$id\"></i><i class=\"icon fa fa-trash fa-fw delete_template_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" name=\"$id\"></i></td>
								</tr>";
			echo "<tr id=\"edit_row_$id\" class=\"edit_rows hidden\">
								<td><input type=\"text\" class=\"form-control\" name=\"text_$id\" id=\"edit_text_$id\" required=\"required\" value=\"$text\" maxlength=\"" . MAXLENGTH . "\"></td>";
			echo "<td>";
			echo "<i class=\"icon fa fa-save fa-fw save_template_icon\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"$id\"></i>";
			echo "<i class=\"icon fa fa-times fa-fw cancel_icon\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"$id\"></i></td></tr>";
		}
		echo "</tbody></table>";

		echo "</div>";
	}
}
