<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$date = date_create($_POST["date"]);
	$date = date_format($date, "Y-m-d");
	$exam_name_id = $_POST["exam"];
	$url = $_POST["url"];
	$seb = $_POST["seb"];
	$lecturer = $_POST["lecturer"];
	$vdp = $_POST["vdp"];
	$sysadmin = $_POST["sysadmin"];
	$type = $_POST["type"];
	$lang = $_POST["lang"];
	$function_id = false;

	$qry = "SELECT exam_id FROM tbl_exam WHERE exam_date='$date' AND exam_name_id=$exam_name_id";
	$res = pg_query($con, $qry);
	$num = pg_num_rows($res);
	if ($num > 0) {
		$row = pg_fetch_assoc($res);
		$exam_id = $row['exam_id'];

		$qry = "SELECT function_id FROM tbl_exam_people WHERE exam_id=$exam_id AND function_id=1";
		$res = pg_query($con, $qry);
		$num = pg_num_rows($res);
		if ($num > 0) {
			$row = pg_fetch_assoc($res);
			$function_id = $row['function_id'];
		}
	}

	if (!$function_id) {
		$qry = $INSERT_exam . " VALUES ('$date', '$url', '$seb', $exam_name_id, $type, $lang, NULL)";
		if (pg_query($con, $qry)) {
			$response_array['status1'] = 'success';
		} else {
			$response_array['status1'] = 'error';
		}

		$qry = "SELECT exam_id FROM tbl_exam WHERE exam_date='$date' AND exam_name_id=$exam_name_id";
		$res = pg_query($con, $qry);
		$row = pg_fetch_assoc($res);
		$exam_id = $row['exam_id'];

		$queries[1] = $INSERT_exam_people . " VALUES ($exam_id, $lecturer, 1)";
		$queries[2] = $INSERT_exam_people . " VALUES ($exam_id, $vdp, 3)";
		$queries[3] = $INSERT_exam_people . " VALUES ($exam_id, $sysadmin, 4)";

		foreach ($queries as $key => $qry) {
			if (pg_query($con, $qry)) {
				$response_array[$key]['status'] = 'success';
			} else {
				$response_array[$key]['status'] = 'error';
			}
		}
	} else {
		$response_array['status1'] = 'already existing';
	}

	pg_close($con);
	echo json_encode($response_array);
}
