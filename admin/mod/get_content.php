<?php

// constants
define("VARPHP", "/inc/var.php");
define("DBPHP", "/inc/db.php");
define("FUNC", "/inc/func.php");
define("MAXLENGTH", 100);

function getData($adminMode)
{
	include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
	include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

	$forbidden = false;

	if (empty($_SESSION['loggedin'])) {
		$loggedin = 0;
	} else {
		$loggedin = $_SESSION['loggedin'];
	}

	if (empty($_SESSION['user_vho'])) {
		$userVHO = 0;
	} else {
		$userVHO = $_SESSION['user_vho'];
	}

	// get user permissions from DB
	$qry = $SELECT_navigation . " WHERE nav_level = 0 AND nav_element='$adminMode' LIMIT 1";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$navVHO = $row['nav_vho'] ?? 'f';
	if ($navVHO == 't') {
		$navVHO = true;
	} else {
		$navVHO = false;
	}

	$continue = false;

	if ($loggedin || ($userVHO && $navVHO) || $adminMode == 'about') {
		$continue = true;
	}

	if ($continue) {
		if ($adminMode == 'about') {
			echo _ABOUT_TEXT;
		} else {
			$link = $_SERVER["DOCUMENT_ROOT"] . "/admin/mod/get_$adminMode.php";
			if (file_exists($link)) {
				// tabs from navigation
				$qry = $SELECT_navigation . " WHERE nav_element = '$adminMode'";
				$res = pg_query($con, $qry);
				$row = pg_fetch_assoc($res);
				$navParentID = $row['nav_id'] ?? 0;

				if (!$_SESSION['user_admin']) {
					$admin = "AND nav_user = 't'";
				} else {
					$admin = "";
				}

				if ($_SESSION['user_vho']) {
					$vho = "AND nav_vho = 't'";
				} else {
					$vho = "";
				}

				$qry = $SELECT_navigation . " WHERE nav_visible = 't' AND nav_parent_id = $navParentID $admin $vho ORDER BY nav_sort";
				$res = pg_query($con, $qry);
				$num = pg_num_rows($res);

				// set tab
				$tab = urlencode($_GET['tab'] ?? '');
				$first = true;

				if ($num > 0) {
					$content = '';
					$content = "<ul class=\"nav nav-tabs\" role=\"tablist\" id=\"tablist\">";
					
					for ($j = 0; $j < $num; $j++) {
						$row = pg_fetch_array($res);
						$key = $row['nav_element'];
						$icon = $row['nav_icon'];
						$value = constant(strtoupper("_" . $key));

						if(!$tab && $first) {
							$tab = $key;
							$first = false;
						}

						$content = $content .  "<li class=\"nav-item\">";
						if ($tab == $key) {
							$active = "active show";
							$selected = "true";
						} else {
							$active = '';
							$selected = "false";
						}
						$content = $content .  "<a class=\"nav-link $active\" href=\"" . paramChanger("tab", "$key") . "\" data-toggle=\"tab\" role=\"tab\" aria-selected=\"$selected\" data-href=\"$key\"><i class=\"icon fa fa-$icon fa-fw\" aria-hidden=\"true\"></i> $value</a>";
						$content = $content .  "</li>";

						$first = false;
					}
					$content = $content . "</ul><br><br>";

					if ($num > 1) {
						echo $content;
					}
				}
				
				// content
				include_once $link;
			} else {
				$forbidden = true;
			}
		}
	} else {
		$forbidden = true;
	}
	if ($forbidden) {
		echo "<h2>" . _WRONG_PARAMETER . "</h2>
				<p>&nbsp;</p>
				<i class=\"fa fa-exclamation fa-fw\" style=\"font-size: 200px; margin-left: 80px;\" title=\"\" aria-label=\"false\" name=\"\"></i>";
	}
}

function paramChanger($param, $page)
{
	// get parameters
	$query = $_GET;
	// replace parameter(s)
	$query[$param] = $page;
	// rebuild url
	$queryResult = http_build_query($query);

	return $_SERVER['PHP_SELF'] . "?" . $queryResult;
}
