<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $machineSID = $_POST["machine_sid"];

    // get ad machinename from Horizon API
    $horizonScript = $_SERVER["DOCUMENT_ROOT"] . "/inc/horizon/get_machine_name.py";
    $machinename = shell_exec("python3 " . $horizonScript . " " . $machineSID);
    $machinename = str_replace(PHP_EOL, '', $machinename);
    
    $response_array['sid'] = $machineSID;
    $response_array['machinename'] = $machinename;

    echo json_encode($response_array);
}
