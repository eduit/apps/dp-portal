<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$id = htmlspecialchars($_POST["id"]);
	$room_id = htmlspecialchars($_POST["room_id"]);
	$exam_id = htmlspecialchars($_POST["exam_id"]);
	$us_keyboard = htmlspecialchars($_POST["us_keyboard"]);
	$group = htmlspecialchars($_POST["group"]);
	$remark = $_POST["remark"];
	$url = $_POST["url"];
	$computer_id = htmlspecialchars($_POST["computer_id"]);
	$active = $_POST["active"];

	if ($group == '') {
		$group = 'null';
	}

	if ($computer_id == '0') {
		$computer_id = 'null';
	}

	//toggle other rooms
	$qry = $UPDATE_exam_student . " SET active='false' WHERE room_id=$room_id AND exam_id != $exam_id";

	if (pg_query($con, $qry)) {
		$response_array['status1'] = 'success';
	} else {
		$response_array['status1'] = 'error';
	}


	$qry = "SELECT exam_student_id FROM tbl_exam_student WHERE exam_id=$exam_id AND computer_id=$computer_id AND accepted=false";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$num = pg_num_rows($res);
	if ($num > 0) {
		$exam_student_id = $row['exam_student_id'];
	} else {
		$exam_student_id = '';
	}

	if (!$exam_student_id || $exam_student_id == $id) {
		$qry = $UPDATE_exam_student . " SET room_id=$room_id, computer_id=$computer_id, accepted='f', exam_group=$group, us_keyboard='$us_keyboard', remark='$remark', url='$url', active='$active' WHERE exam_student_id=$id";
		if (pg_query($con, $qry)) {
			$response_array['status2'] = 'success';
		} else {
			$response_array['status2'] = 'error';
		}
	} else {
		$response_array['status2'] = 'computer busy';
	}

	pg_close($con);
	echo json_encode($response_array);
}
