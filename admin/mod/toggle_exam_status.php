<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$exam = $_POST["exam"];
	$active = $_POST["active"];

	if ($active == 't') {
		$active = 'true';
	} else {
		$active = 'false';
	}

	$qry = $UPDATE_exam_student . " SET active=$active WHERE exam_id=$exam";

	if (pg_query($con, $qry)) {
		$response_array['status'] = 'success';
	} else {
		$response_array['status'] = 'error';
	}

	pg_close($con);
	echo json_encode($response_array);
}
