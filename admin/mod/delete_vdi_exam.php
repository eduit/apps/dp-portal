<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin'] || $_SESSION['user_vho']) {
    $vdiExamID = $_POST["examID"];

    $qry = "SELECT exam_id FROM tbl_vdi_exam WHERE vdi_exam_id = $vdiExamID";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $examID = $row['exam_id'];

    $qry = $DELETE_vdi_exam . " WHERE vdi_exam_id = $vdiExamID";

    if (pg_send_query($con, $qry)) {
        $res = pg_get_result($con);
        if ($res) {
            $state = pg_result_error_field($res, PGSQL_DIAG_SQLSTATE);
            if ($state == 0) {
                $response_array['status1'] = 'success';
            } else {
                $response_array['status1'] = 'error';
            }
        }
    }

    $qry = $DELETE_exam_people . " WHERE exam_id = $examID AND function_id = 2";

    if (pg_send_query($con, $qry)) {
        $res = pg_get_result($con);
        if ($res) {
            $state = pg_result_error_field($res, PGSQL_DIAG_SQLSTATE);
            if ($state == 0) {
                $response_array['status2'] = 'success';
            } else {
                $response_array['status2'] = 'error';
            }
        }
    }

    pg_close($con);
    echo json_encode($response_array);
}
