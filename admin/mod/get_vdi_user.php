<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $userSID = $_POST["user_sid"];

    $qry = $SELECT_exam_users . " WHERE exam_user_sid = '$userSID'";
    $res = pg_query($con, $qry);
    $num = pg_num_rows($res);

    if ($num > 0) {
        $row = pg_fetch_assoc($res);
        $userID = $row['exam_user_id'];
        $username = $row['exam_username'];
    } else {
        // get ad username from Horizon API
        $horizonScript = $_SERVER["DOCUMENT_ROOT"] . "/inc/horizon/get_ad_username.py";
        $username = shell_exec("python3 " . $horizonScript . " " . $userSID);
        $username = str_replace(PHP_EOL, '', $username);

        $qry = $SELECT_exam_user_id . "'$username'";
        $res = pg_query($con, $qry);
        $num = pg_num_rows($res);

        if ($num > 0) {
            $row = pg_fetch_assoc($res);
            $userID = $row['exam_user_id'];
            $qry = $UPDATE_exam_user . " SET exam_user_sid = '$userSID' WHERE exam_user_id = $userID";
            if (pg_query($con, $qry)) {
                $response_array['update'] = 'success';
            } else {
                $response_array['update'] = 'error';
            }
        } else {
            $response_array['update'] = 'nothing to do';
        }
    }

    $response_array['sid'] = $userSID;
    $response_array['username'] = $username;

    pg_close($con);
    echo json_encode($response_array);
}
