<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin'] || $_SESSION['user_vho']) {
	$account_id = $_POST["account_id"];
	$archived = $_POST["archived"];

	if ($archived == 't') {
		$archived = 'true';
	} else {
		$archived = 'false';
	}

	$qry = $UPDATE_VHO_account . " SET archive=$archived WHERE account_id=$account_id";

	if (pg_query($con, $qry)) {
		$response_array['status'] = 'success';
	} else {
		$response_array['status'] = 'error';
	}

	pg_close($con);
	echo json_encode($response_array);
}
