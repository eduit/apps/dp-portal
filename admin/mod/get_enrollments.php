<?php

// Get preferences
$qry = $SELECT_prefs;
$res = pg_query($con, $qry);
$num = pg_num_rows($res);
for ($i = 0; $i < $num; $i++) {
    $row = pg_fetch_array($res);
    if ($row['pref_name'] == "number_of_enrollments") {
        $enrollments = $row['pref_value'];
    }
}

$checkboxes = '<p>';

// disabled
if (isset($_GET['disabled']) && ($_GET['disabled'] == "true" || $_GET['disabled'] == "false")) {
    $disabled = $_GET['disabled'];
} else {
    $disabled = "false";
}
if ($disabled != "true" && $disabled != "false") {
    $disabled = "false";
}
$checkboxes = $checkboxes . "<label id=\" disabled_label\" title=\"" . _HIDDEN . "\"><input name=\"disabled\" type=\"checkbox\" name=\"\" value=\"0\" id=\"disabled\" ";
if ($disabled == "true") {
    $checkboxes = $checkboxes . "checked";
}
$checkboxes = $checkboxes . "> " . _HIDDEN . "</label>&nbsp;";

// archived
if (isset($_GET['archived']) && ($_GET['archived'] == "true" || $_GET['archived'] == "false")) {
    $archived = $_GET['archived'];
} else {
    $archived = "false";
}
if ($archived != "true" && $archived != "false") {
    $archived = "false";
}
$checkboxes = $checkboxes . "<label id=\" archived_label\" title=\"" . _ARCHIVED . "\"><input name=\"archived\" type=\"checkbox\" name=\"\" value=\"0\" id=\"archived\" ";
if ($archived == "true") {
    $checkboxes = $checkboxes . "checked";
}
$checkboxes = $checkboxes . "> " . _ARCHIVED . "</label>";
$checkboxes = $checkboxes . "</p>";

echo $checkboxes;

// checkbox styling
echo "<script>$(\":checkbox\").checkboxradio({icon: false,});</script>";

$qry = $SELECT_all_subscriptions . "tbl_exam.archive=$archived AND tbl_exam.disabled=$disabled ORDER BY tbl_exam_student.updated_at DESC LIMIT $enrollments";
$res1 = pg_query($con, $qry);
$num1 = pg_num_rows($res1);
$str_length = 30;
echo "<p><span id=\" count\"></span>" . $num1 . " " . _COUNT . "</p>";

echo "<table id=\"all_subscriptions\" class=\"admintable generaltable\" style=\"width: 100%;\">
        <thead class=\"resizable sticky\">
            <tr>
                <th style=\"width: 5%;\">" . _DATE . "<br /></th>
                <th style=\"width: 10%;\">" . _EXAM_NR . "<br /></th>
                <th style=\"width: 10%;\">" . _EXAM . "<br /></th>
                <th style=\"width: 10%;\">" . _REGISTRATION . "<br /></th>
                <th style=\"width: 10%;\">" . _LASTNAME . "<br /></th>
                <th style=\"width: 10%;\">" . _FIRSTNAME . "<br /></th>
                <th style=\"width: 7%;\">" . _HOSTNAME . "<br /></th>
                <th style=\"width: 8%;\">" . _IP_ADDRESS . "<br /></th>
                <th style=\"width: 5%;\">" . _FORM . "<br /></th>
                <th style=\"width: 10%;\">" . _CREATED . "<br /></th>
                <th style=\"width: 10%;\">" . _UPDATED . "<br /></th>
                <th style=\"width: 5%;\" class=\"no-filter\">" . _DELETE . "<br /></th>
            </tr>
        </thead>
        <tbody class=\"resizable\">";
for ($i = 0; $i < $num1; $i++) {
    $row1 = pg_fetch_array($res1);
    $exam_date = date_create($row1['exam_date']);
    $created = date_create($row1['created']);
    $updated = date_create($row1['updated']);
    $vdi_name = $row1['vdi_name'];
    if ($vdi_name) {
        $computer_name = $vdi_name . " (VDI)";
    } else {
        $computer_name = $row1['computer_name'];
    }
    $student_id = $row1['student_id'];
    $exam_nr = $row1['exam_nr'];
    $exam_name = $row1['exam_name'];
    $student_registration = $row1['student_registration'];
    $student_lastname = $row1['student_lastname'];
    $student_firstname = $row1['student_firstname'];
    $ip_address = $row1['ip_address'];
    $form = $row1['form'];
    echo "<tr id=\" row_" . $student_id . "\">
                <td>" . date_format($exam_date, 'Y-m-d') . "</td>
                <td>$exam_nr</td>
                <td>$exam_name</td>
                <td>$student_registration</td>
                <td>";
    echo left($student_lastname, $str_length);
    if (strlen($student_lastname) > $str_length) {
        echo "...";
    }
    echo "</td>
                <td>";
    echo left($student_firstname, $str_length);
    if (strlen($student_firstname) > $str_length) {
        echo "...";
    }
    echo "</td>
                <td>" . $computer_name . "</td>
                <td>" . $ip_address . "</td>";

    echo "<td>";
    if ($form == "t") {
        echo "<i class=\"fa fa-check-square-o fa-fw true_icon\" title=\"" . _TRUE . "\" aria-label=\"true\"></i>";
        echo "<span style=\"opacity: 0;\">" . _TRUE . "</span>";
    } else {
        echo "<i class=\"fa fa-square-o fa-fw false_icon\" title=\"" . _FALSE . "\" aria-label=\"false\"></i>";
        echo "<span style=\"opacity: 0;\">" . _FALSE . "</span>";
    }
    echo "</td>";

    echo "<td>" . date_format($created, 'Y-m-d H:i:s') . "</td>
                <td>" . date_format($updated, 'Y-m-d H:i:s') . "</td>
                <td>";
    if ($archived == "false") {
        echo "<i class=\"icon fa fa-trash fa-fw delete_enrollment_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" name=\"" . $student_id . "\"></i>";
    } else {
        echo "<i class=\"fa fa-lock fa-fw ban_icon\" title=\"" . _ARCHIVED . "\" aria-label=\"" . _ARCHIVED . "\" name=\"archive_row_" . $student_id . "\"></i>";
    }
    echo "</td>
                </tr>";
}

echo "</tbody>
    </table>";

echo "<script>
        $(function() {
            $(\"#all_subscriptions\").excelTableFilter();
            let rowCount = $(\"#all_subscriptions tbody tr:visible\").length;
            $(\"#count\").html(rowCount+\"/\");
            if (/chrom(e|ium)/.test(navigator.userAgent.toLowerCase())) {
                $(\".dropdown-filter-content\").parent().parent(\"th\").addClass(\"drp_menu\");
            }
        });
    </script>";
