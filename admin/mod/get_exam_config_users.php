<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

// Include language file
if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else {
    // default
    $qry = $SELECT_lang_default;
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $lang = $row['lang'];
}
include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

if ($_SESSION['loggedin'] || $_SESSION['user_vho']) {
    $configID = $_POST['configID'] ?? 0;

    $qry = $SELECT_exam_config  . " AND tbl_exam_config.config_id = $configID";
    $res = pg_query($con, $qry);
    $row = pg_fetch_array($res);
    $peopleID = $row['people_id'];
    $peopleUsername = $row['username'];

    $users = "<li id=\"$peopleID\">$peopleUsername</li>\n";

    $qry = $SELECT_config_people . " AND tbl_config_people.config_id = $configID ORDER BY tbl_people.people_username";
    $res = pg_query($con, $qry);
    $num = pg_num_rows($res);

    for ($i = 0; $i < $num; $i++) {
        $row = pg_fetch_array($res);
        $id = $row['people_id'];
        $username = $row['username'];
        $users = $users . "<li id=\"$id\">$username</li>\n";
    }

    $response_array["content"] = $users;

    echo json_encode($response_array);
}
