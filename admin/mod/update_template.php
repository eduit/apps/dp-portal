<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $templateID = $_POST["id"];
    $text = htmlspecialchars($_POST["text"]);

    // Include language
    if (isset($_SESSION['lang'])) {
        $lang = $_SESSION['lang'];
        $qry = $SELECT_language . " WHERE lang_code='$lang'";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $langID = $row['lang_id'];
    } else {
        // default
        $qry = $SELECT_lang_default;
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $langID = $row['lang_id'];
    }

    // update database
    $qry = $UPDATE_template . " SET text='$text', lang_id=$langID WHERE template_id=$templateID";

    if (pg_query($con, $qry)) {
        $response_array['status'] = 'success';
    } else {
        $response_array['status'] = 'error';
    }

    pg_close($con);
    echo json_encode($response_array);
}
