<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$exam_id = $_POST['exam_id'];

	$qry = $SELECT_exam_students . " AND tbl_exam.exam_id = $exam_id";
	$res = pg_query($con, $qry);
	$num = pg_num_rows($res);

	for ($i = 0; $i < $num; $i++) {
		$row = pg_fetch_array($res);
		if (empty($exam_student_ids)) {
			$exam_student_ids = $row['exam_student_id'];
		} else {
			$exam_student_ids = $exam_student_ids . "," . $row['exam_student_id'];
		}
	}

	if (!empty($exam_student_ids)) {
		$qry = $DELETE_exam_student . " WHERE exam_student_id IN ($exam_student_ids) AND tbl_exam_student.accepted = false";
		if (pg_send_query($con, $qry)) {
			$res = pg_get_result($con);
			if ($res) {
				$state = pg_result_error_field($res, PGSQL_DIAG_SQLSTATE);
				if ($state == 0) {
					$response_array['status'] = 'success';
				} else {
					$response_array['status'] = 'error';
				}
			}
		}
	} else {
		$response_array['status'] = 'success';
	}

	pg_close($con);
	echo json_encode($response_array);
}
