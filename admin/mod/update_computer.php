<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$id = htmlspecialchars($_POST["id"]);
	$ip_address = htmlspecialchars($_POST["ip_address"]);
	$computer = htmlspecialchars($_POST["computer"]);
	$vdi = htmlspecialchars($_POST["vdi"]);
	$exam_user_id = htmlspecialchars($_POST["exam_user_id"]);

	$qry = $UPDATE_computer . " SET computer_name='$computer', ip_address='$ip_address', vdi='$vdi', exam_user_id='$exam_user_id' WHERE computer_id=$id";

	if (pg_query($con, $qry)) {
		$response_array['status'] = 'success';
	} else {
		$error = pg_last_error($con);
		// you need to adapt this regex
		if (preg_match('/duplicate/i', $error)) {
			$response_array['status'] = 'already existing';
		} else {
			$response_array['status'] = 'error';
		}
	}

	pg_close($con);
	echo json_encode($response_array);
}
