<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$id = $_POST["id"];
	$software = htmlspecialchars($_POST["software"]);
	$pubslisher_id = $_POST["publisher_id"];
	$version = htmlspecialchars($_POST["version"]);
	$remarks = htmlspecialchars($_POST["remarks"]);
	$visible = htmlspecialchars($_POST["visible"]);

	$qry = $SELECT_software . " AND sw_name='$software' AND tbl_vdi_software.publisher_id=$pubslisher_id AND sw_version='$version'";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$num = pg_num_rows($res);
	if ($num > 0) {
		$sw_id = $row['id'];
	} else {
		$sw_id = '';
	}

	if (!$sw_id || $sw_id == $id) {
		$qry = $UPDATE_software . " SET sw_name='$software', publisher_id=$pubslisher_id, sw_version='$version', sw_remarks='$remarks', sw_visible='$visible' WHERE sw_id=$id";
		if (pg_query($con, $qry)) {
			$response_array['status'] = 'success';
		} else {
			$response_array['status'] = 'error';
		}
	} else {
		$response_array['status'] = 'already existing';
	}

	pg_close($con);
	echo json_encode($response_array);
}
