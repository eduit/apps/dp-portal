<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$publisher = htmlspecialchars($_POST['publisher']);

	$qry = "SELECT publisher_id FROM tbl_publisher WHERE publisher_name='$publisher'";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$num = pg_num_rows($res);
	if ($num > 0) {
		$publisher_id = $row['publisher_id'];
	} else {
		$publisher_id = '';
	}

	if (!$publisher_id) {
		$qry = $INSERT_publisher . " VALUES ('$publisher')";
		if (pg_query($con, $qry)) {
			$response_array['status'] = 'success';
		} else {
			$response_array['status'] = 'error';
		}
	} else {
		$response_array['status'] = 'already existing';
	}

	pg_close($con);
	echo json_encode($response_array);
}
