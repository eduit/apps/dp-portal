<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin'] || $_SESSION['user_vho']) {
    $function = $_POST["function"];
    $id = $_POST["id"];
    
    switch ($function) {
        case "save":
            $configID = htmlspecialchars($_POST["configID"] ?? "null");
            $testingFrom = date_format(date_create($_POST["testingFrom"]), "Y-m-d H:i");
            $testingTo = date_format(date_create($_POST["testingTo"]), "Y-m-d H:i");
            $selfTesting = htmlspecialchars($_POST["selfTesting"]);
            $vdiPoolID = htmlspecialchars($_POST["vdiPoolID"]);
            $vdiPool = htmlspecialchars($_POST["vdiPool"]);
            $remarks = htmlspecialchars($_POST["remarks"]);
            $deadline = date_format(date_create($_POST["deadline"]), "Y-m-d");
            $statusID = htmlspecialchars($_POST["statusID"]);
            $infoText = htmlspecialchars($_POST["info_text"]);

            $qry = "SELECT internal_info FROM tbl_vdi_exam WHERE vdi_exam_id = $id";
            $res = pg_query($con, $qry);
            $row = pg_fetch_assoc($res);
            $num = pg_num_rows($res);
            if ($num > 0) {
                $info_text = strip_tags(html_entity_decode($row['internal_info']));
                if (empty($info_text)) {
                    $internal_info = ", internal_info='$infoText'";
                } else {
                    $internal_info = "";
                }
            }

            $qry = $UPDATE_vdi_exam . " SET config_id=$configID, testing_from='$testingFrom', testing_to='$testingTo', self_testing='$selfTesting', pool_id=$vdiPoolID, vdi_pool='$vdiPool', remarks='$remarks', deadline='$deadline', status_id=$statusID $internal_info WHERE vdi_exam_id=$id";

            if (pg_query($con, $qry)) {
                $response_array['status1'] = 'success';
            } else {
                $response_array['status1'] = 'error';
            }
            break;

        case "status":
            $statusID = $_POST["status_id"];

            $qry = $UPDATE_vdi_exam . " SET status_id=$statusID WHERE vdi_exam_id=$id";
            
            if (pg_query($con, $qry)) {
                $response_array['status2'] = 'success';
            } else {
                $response_array['status2'] = 'error';
            }

            break;

        default:
            $response_array['status'] = 'error';
            break;

    }

    pg_close($con);
    echo json_encode($response_array);
}
