<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

// Include language file
if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else {
    // default
    $qry = $SELECT_lang_default;
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $lang = $row['lang'];
}
include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

if ($_SESSION['loggedin']) {
    $tabSelected = $_POST['tab'] ?? '';
    $function = str_replace(' ', '_', strtolower($tabSelected));
    $content = '';

    // get desktop pools
    $horizonScript = $_SERVER["DOCUMENT_ROOT"] . "/inc/horizon/get_desktop_pools.py";
    $json = shell_exec("python3 " . $horizonScript);
    $desktopPools = json_decode($json, true);

    if (isset($_POST['pool'])) {
        $pool = htmlspecialchars($_POST['pool']);
    } else {
        $pool = "";
    }

    $response_array['pool'] = $pool;

    $selectPools = "<select id=\"select_pools\" class=\"select custom-select form-control\" style=\"width: 220px;\">";
    foreach ($desktopPools as $key => $item) {
        $poolID = $item['id'];
        $poolName = $item['name'];
        $option = "<option value=\"$poolID\"";

        if ($pool == $poolID || empty($pool)) {
            $option = $option . " selected>" . $poolName;
            $pool = $poolName;
            $poolIDselected = $poolID;
        } else {
            $option = $option . ">" . $poolName;
        }
        $option = $option . "</option>";
        $selectPools = $selectPools . $option;

        $response_array[$key]['poolID'] = $poolID;
    }
    $selectPools = $selectPools . "</select><br><br>";

    $content = $content . _POOLNAME . ": " . $selectPools;

    // get machines
    $horizonScript = $_SERVER["DOCUMENT_ROOT"] . "/inc/horizon/get_machines.py " . $poolIDselected;
    $json = shell_exec("python3 " . $horizonScript);
    $machines = json_decode($json, true);

    // get sessions
    $horizonScript = $_SERVER["DOCUMENT_ROOT"] . "/inc/horizon/get_sessions.py";
    $json = shell_exec("python3 " . $horizonScript);
    $sessions = json_decode($json, true);

    $columns = array(
        ['name', _VDI],
        ['desktop_pool_id', _POOLNAME],
        ['user_ids', _USER],
        ['state', _STATUS],
    );
    $entries = $machines;
    $functions = array(
        ['reset', 'undo', 'reset_machine'],
        ['restart', 'refresh', 'restart_machine'],
        ['delete', 'trash', 'delete_machine'],
        ['unassign', 'chain-broken', 'unassign_user']
    );

    $num = count($entries) ?? 0;
    $sortby = $columns[0][0];
    $keyValues = array_column($entries, $sortby);
    array_multisort($keyValues, SORT_ASC, $entries);

    $content = $content . "<p>" . $num . " " . _COUNT . "</p>";

    $content = $content . "<table id=\"$function-entries\" class=\"admintable generaltable\" style=\"width: 100%;\">
    <thead class=\"resizable sticky\">
        <tr>";
    foreach ($columns as $i => $header) {
        $content = $content . "<th>$header[1]<br /></th>";
        $cells[$i] = $header[0];
    }
    if ($_SESSION['user_admin']) {
        $content = $content . "<th class=\"no-filter\">" . _EDIT . "<br /></th>";
    }
    $content = $content . "</tr>
    </thead>
    <tbody class=\"resizable\">";

    foreach ($entries as $key => $item) {
        $user = false;
        $itemID = $item['id'];
        $content = $content . "<tr id=\"row_" . $key . "\" name=\"$itemID\">";
        foreach ($cells as $cell) {
            $content = $content . "<td id=\"" . $cell . "_" . $key . "\">";
            switch ($cell) {
                case 'name':
                    $name = $item[$cell] ?? 0;
                    if ($name) {
                        $content = $content . $name;
                    }
                    break;
                case 'desktop_pool_id':
                    $id = array_search($item[$cell], array_column($desktopPools, 'id'));
                    $desktopPoolID = $desktopPools[$id]['id'];
                    $desktopPool = $desktopPools[$id]['name'];
                    $enabled = $desktopPools[$id]['enable_provisioning'];
                    if ($enabled) {
                        $checkbox = "<i class=\"fa fa-check-square-o fa-fw true_icon\" title=\"" . _ENABLED . "\" aria-label=\"true\"></i>";
                    } else {
                        $checkbox = "<i class=\"fa fa-square-o fa-fw false_icon\" title=\"" . _DISABLED . "\" aria-label=\"false\"></i>";
                    }
                    $content = $content . "$checkbox <span class=\"desktop_pool\" name=\"$desktopPoolID\">$desktopPool</span>";
                    break;
                case 'user_ids':
                    $userID = $item[$cell][0] ?? 0;
                    if ($userID) {
                        $content = $content . "<span class=\"user $userID\" name=\"$userID\"></span>";
                        $user = true;
                    } else {
                        if (str_contains(strtolower($item['state']), 'connected')) {
                            //get user from session
                            $sessionID = $item['session_id'];
                            $id = array_search($sessionID, array_column($sessions, 'id'));
                            $userID = $sessions[$id]['user_id'];
                            $content = $content . "<span class=\"user $userID\" name=\"$userID\"></span>";
                            $user = true;
                        } else {
                            $user = false;
                        }
                    }
                    break;
                case 'state':
                    $sessionState = ucfirst(strtolower($item[$cell]));
                    $content = $content . $sessionState;
                    break;
                default:
                    $defaultEntry = $item[$cell] ?? 0;
                    if ($defaultEntry) {
                        $content = $content . $defaultEntry;
                    }
            }
            $content = $content . "</td>";
        }
        if ($_SESSION['user_admin']) {
            $content = $content . "<td class=\"edit_row\">";
            foreach ($functions as $entry) {
                $entryName = $entry[0];
                $icon = $entry[1];
                $command = $entry[2];
                $const = "_" . strtoupper($entryName);
                if (defined($const)) {
                    $const = constant($const);
                } else {
                    $const = "_UNDEFINED";
                }
                $continue = true;
                if ($entryName == 'unassign' && !$user) {
                    $continue = false;
                }
                if (str_contains($sessionState, 'Provisioning') || str_contains($sessionState, 'Deleting') || str_contains($sessionState, 'Maintenance')) {
                    $continue = false;
                }
                if (str_contains($sessionState, 'Publishing') || str_contains($sessionState, 'Agent_unreachable')) {
                    $continue = false;
                }
                if ($entryName == "disconnect" && str_contains($sessionState, 'Disconnected')) {
                    $continue = false;
                }
                if (str_contains(strtolower($name), 'watch')) {
                    $continue = false;
                }
                if ($continue) {
                    $content = $content . "<i class=\"icon fa fa-$icon fa-fw " . $entryName . " " . $function . "\" title=\"" . $const . "\" aria-label=\"" . $const . "\" name=\"" . $key . "\" data-href=\"$command\"></i>";
                }
            }
            $content = $content . "</td>";
        }
        $content = $content . "</tr>";
    }
    $content = $content . "</tbody>
    </table>";
    $content = $content .  "<script>
        $(function() {
            $(\"#$function-entries\").excelTableFilter();
            let rowCount = $(\"#all_subscriptions tbody tr:visible\").length;
            $(\"#count\").html(rowCount+\"/\");
            if (/chrom(e|ium)/.test(navigator.userAgent.toLowerCase())) {
                $(\".dropdown-filter-content\").parent().parent(\"th\").addClass(\"drp_menu\");
            }
        });
    </script>";

    $response_array['content'] = $content;
    echo json_encode($response_array);
}
