<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

// Include language file
if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else {
    // default
    $qry = $SELECT_lang_default;
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $lang = $row['lang'];
}
include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

$n = 0;

if ($_SESSION['loggedin']) {
    $error = false;
    if (isset($_POST['rows'])) {
        $data = $_POST['rows']['selectedRows'];
        $folder = $upload_dir . "/" . htmlspecialchars($_POST["folder"]);
    } else {
        $status[$n] = "no data";
        $n++;
        echo json_encode($response_array);
        throw new UnexpectedValueException('no data');
    }

    foreach ($data as $i => $item) {
        $file = strip_tags($item[_FILENAME]);
        $filepath = $folder . "/" . $file;
        $response_array[$i]['file'] = $file;
        if (!unlink($filepath)) {
            $response_array[$i]['status'] = "error";
            $response_array[$i]['error'] = var_export(error_get_last(), true);
        } else {
            $response_array[$i]['status'] = "success";
        }
    }

    echo json_encode($response_array);
}
