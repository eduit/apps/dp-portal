<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	// Include language file
	if (isset($_SESSION['lang'])) {
		$lang = $_SESSION['lang'];
	} else {
		// default
		$qry = $SELECT_lang_default;
		$res = pg_query($con, $qry);
		$row = pg_fetch_assoc($res);
		$lang = $row['lang'];
	}
	include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

	$room = $_POST["room"];
	$exams = "";

	$exams = $exams . "<select class=\"select custom-select form-control select_exam\" name=\"" . $room . "\" id=\"id_exam_" . $room . "\" required=\"required\" style=\"width: 400px;\">";
	$exams = $exams . "<option id=\"toggle_exam_" . $room . "_01\" name=\"" . _NO_EXAM_ACTIVE . "\" value=\"01\"selected>" . _NO_EXAM_ACTIVE . "</option>\n";

	$qry = $SELECT_exam_people;
	$res2 = pg_query($con, $qry);
	$num2 = pg_num_rows($res2);

	for ($j = 0; $j < $num2; $j++) {
		$row2 = pg_fetch_array($res2);
		$link = $row2['link'];
		$id = $row2['id'];
		$date = date_create($row2['date']);
		$exam = $row2['exam'];
		$lastname = $row2['lastname'];

		$exams = $exams . "<option name=\"" . $link . "\" value=\"" . $id . "\"";
		$exams = $exams . ">" . $exam . " | " . $lastname . " | " . date_format($date, 'Y-m-d') . "</option>\n";
	}
	$exams = $exams . "</select>";

	$response_array['content'] = $exams;

	pg_close($con);

	echo json_encode($response_array);
}
