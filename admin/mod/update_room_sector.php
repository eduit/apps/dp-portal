<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $roomSectorID = $_POST["room_sector_id"];
    $roomID = $_POST["room_id"];
    $sectorID = $_POST["sector_id"];
    $sectorDN = htmlspecialchars($_POST["sector_display_name"]);

    // update database
    $qry = $UPDATE_room_sector . " SET room_id=$roomID, sector_id=$sectorID, sector_display_name='$sectorDN' WHERE room_sector_id=$roomSectorID";

    if (pg_query($con, $qry)) {
        $response_array['status'] = 'success';
    } else {
        $response_array['status'] = 'error';
    }

    pg_close($con);
    echo json_encode($response_array);
}
