<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

// Include language file
if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else {
    // default
    $qry = $SELECT_lang_default;
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $lang = $row['lang'];
}
include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

if ($_SESSION['loggedin']) {
    $mode = $_POST['mode'];
    $date = $_POST['date'];
    $date = date_create($date);
    $date = date_format($date, "Y-m-d");
    $status = 'success';
    $count = false;

    switch ($mode) {
        case "count":
            $start = "SELECT COUNT";
            $count = true;
            break;

        case "delete":
            $start = "DELETE ";
            break;

        default:
            break;
    }

    foreach ($CLEANUP as $key => $sub) {
        $name = eval('return _' . strtoupper($sub['name']) . ';');
        $response_array[$key]['name'] = $name;

        if ($count) {
            $function = $start . "('" . $sub['id'] . "') AS rows ";
        } else {
            $function = $start;
        }

        $qry = $function . $sub['sql'];
        // tbl_exam_student
        if ($key == 0) {
            $qry = $qry . " < '" . $date . "'";
        }
        // tbl_exam_people || tbl_logsheet || tbl_vho_accounts
        if (in_array($key, array(1, 2, 3, 4))) {
            $qry = $qry . " < '" . $date . "')";
        }
        // tbl_exam
        if ($key == 5) {
            $qry = $qry . " >= '" . $date . "') AND exam_date < '" . $date . "'";
        }

        $res = pg_query($con, $qry);

        if (!pg_last_error($con)) {
            $row = pg_fetch_array($res);
            $response_array[$key]['status'] = 'success';
            if ($count) {
                $response_array[$key]['content'] = $row['rows'];
            }
        } else {
            $response_array[$key]['status'] = 'error';
            $response_array[$key]['error'] = pg_last_error($con);
            $status = 'error';
        }
    }

    $response_array[$key + 1]['status'] = $status;

    echo json_encode($response_array);
}
