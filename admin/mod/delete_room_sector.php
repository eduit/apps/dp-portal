<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $room_sector_id = $_POST["room_sector_id"];

    $qry = $DELETE_room_sector . " WHERE room_sector_id = $room_sector_id";

    if (pg_send_query($con, $qry)) {
        $res = pg_get_result($con);
        if ($res) {
            $state = pg_result_error_field($res, PGSQL_DIAG_SQLSTATE);
            if ($state == 0) {
                $response_array['status'] = 'success';
            } else {
                $response_array['status'] = 'error';
            }
        }
    }

    pg_close($con);
    echo json_encode($response_array);
}
