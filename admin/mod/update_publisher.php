<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$id = $_POST['id'];
	$publisher = htmlspecialchars($_POST['publisher']);

	$qry = $SELECT_publisher . " WHERE publisher_name='$publisher'";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$num = pg_num_rows($res);
	if ($num > 0) {
		$publisherID = $row['publisherID'];
	} else {
		$publisherID = '';
	}

	if (!$publisherID || $publisherID == $id) {
		$qry = $UPDATE_publisher . " SET publisher_name='$publisher' WHERE publisher_id=$id";

		if (pg_query($con, $qry)) {
			$response_array['status'] = 'success';
		} else {
			$response_array['status'] = 'error';
		}
	} else {
		$response_array['status'] = 'already existing';
	}

	pg_close($con);
	echo json_encode($response_array);
}
