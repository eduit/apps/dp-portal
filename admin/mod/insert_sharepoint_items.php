<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

// Include language file
if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else {
    // default
    $qry = $SELECT_lang_default;
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $lang = $row['lang'];
}
include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

$n = 0;

if ($_SESSION['loggedin']) {
    if (isset($_POST['selectedRows'])) {
        $data = $_POST['selectedRows'];
    } else {
        $status[$n] = "no data";
        $n++;
        echo json_encode($response_array);
        throw new UnexpectedValueException('no data');
    }

    foreach ($data as $i => $item) {
        $examNr = $item[_EXAM_NR];
        $examName = $item[_EXAM];
        $lecturer = $item[_LECTURER];
        if (isset($item[_VDP])) {
            $vdp = $item[_VDP];
        } else {
            $vdp = false;
        }
        if (isset($item[_SYSADMIN])) {
            $sysadmin = $item[_SYSADMIN];
        } else {
            $sysadmin = false;
        }
        $date = date_create($item[_DATE]);
        $date = date_format($date, "Y-m-d");
        $type = $item[_TYPE];
        $language =  $item[_LANGUAGE];
        $langCode =  strtolower(substr($language, 0, 2));
        if (isset($item[_LINK])) {
            $url =  $item[_LINK];
        } else {
            $url = '';
        }

        // VDI pool
        $vdiPool = $item[_VDI_POOL];
        if ($vdiPool != "") {
            $qry = $SELECT_vdi_pool . " AND tbl_pool_lang.pool_description = '$vdiPool' LIMIT 1";
            $res = pg_query($con, $qry);
            $num = pg_num_rows($res);
            if ($num > 0) {
                $row = pg_fetch_assoc($res);
                $poolID = $row['pool_id'];
            } else {
                $poolID = 1;
            }
        } else {
            $poolID = 'NULL';
        }

        // check lecturer
        if ($lecturer == "Digitale Prüfungen") {
            $qry = $SELECT_people . " WHERE POSITION(tbl_people.people_lastname IN '$lecturer') > 0";
        } else {
            $qry = $SELECT_people . " WHERE POSITION(tbl_people.people_lastname IN '$lecturer') > 0 AND POSITION(tbl_people.people_firstname IN '$lecturer') > 0";
        }
        $res = pg_query($con, $qry);
        $num = pg_num_rows($res);
        $row = pg_fetch_assoc($res);
        $lecturerID = $row['id'];

        if ($num == 0) {
            $status[$n] = "no lecturer found";
            $response_array[$i]['lecturer_id'] = $lecturerID;
            $response_array[$i]['lecturer'] = $lecturer;
            continue;
        } else {
            $status[$n] = "success";
        }
        $n++;

        // check vdp
        if ($vdp) {
            $qry = $SELECT_people . " WHERE POSITION(tbl_people.people_lastname IN '$vdp') > 0 AND POSITION(tbl_people.people_firstname IN '$vdp') > 0";
            $res = pg_query($con, $qry);
            $num = pg_num_rows($res);
            $row = pg_fetch_assoc($res);
            $vdpID = $row['id'];

            if ($num == 0) {
                $status[$n] = "no vdp found";
                $response_array[$i]['vdp_id'] = $vdpID;
                $response_array[$i]['vdp'] = $vdp;
                continue;
            } else {
                $status[$n] = "success";
            }
            $n++;
        }

        // check sysadmin
        if ($sysadmin) {
            $qry = $SELECT_people . " WHERE POSITION(tbl_people.people_lastname IN '$sysadmin') > 0 AND POSITION(tbl_people.people_firstname IN '$sysadmin') > 0";
            $res = pg_query($con, $qry);
            $num = pg_num_rows($res);
            $row = pg_fetch_assoc($res);
            $sysadminID = $row['id'];

            if ($num == 0) {
                $status[$n] = "no sysadmin found";
                $response_array[$i]['lecturer_id'] = $sysadminID;
                $response_array[$i]['lecturer'] = $sysadmin;
                continue;
            } else {
                $status[$n] = "success";
            }
            $n++;
        }

        // insert into portfolio
        $qry = "SELECT exam_name_id FROM tbl_exam_name WHERE exam_nr='$examNr' AND exam_name='$examName'";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $examNameID = $row['exam_name_id'] ?? null;

        if (!$examNameID) {
            $qry = $INSERT_exam_name . " VALUES ('$examNr','$examName') RETURNING exam_name_id";
            if ($res = pg_query($con, $qry)) {
                $status[$n] = "success";
                $row = pg_fetch_row($res);
                $examNameID = $row['0'];
            } else {
                $status[$n] = "error";
                $response_array[$i]['exam'] = $examNr;
            }
        } else {
            $status[$n] = "already existing";
        }
        $n++;

        // get type_id
        $qry = $SELECT_exam_type . " WHERE type_name = '$type'";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $typeID = $row['type_id'];

        // insert type
        if (!$typeID) {
            $qry = $INSERT_exam_type . " VALUES ('$type') RETURNING type_id";
            if ($res = pg_query($con, $qry)) {
                $status[$n] = "success";
                $row = pg_fetch_row($res);
                $typeID = $row['0'];
            } else {
                $status[$n] = "type error";
                $response_array[$i]['exam'] = $examNr;
            }
        }
        $n++;

        // get language_id
        $qry = $SELECT_language . " WHERE lang_code = '$langCode'";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $languageID = $row['lang_id'];

        // insert into dates
        $qry = "SELECT exam_id FROM tbl_exam WHERE exam_date='$date' AND exam_name_id=$examNameID";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $exam_id = $row['exam_id'] ?? 0;

        $qry = "SELECT function_id FROM tbl_exam_people WHERE exam_id=$exam_id AND function_id=1";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $num = pg_num_rows($res);
        if ($num > 0) {
            $function_id = $row['function_id'];
        } else {
            $function_id = '';
        }

        if (!$function_id) {
            $qry = $INSERT_exam . " VALUES ('$date', '$url', '', $examNameID, $typeID, $languageID, $poolID)";
            if (pg_query($con, $qry)) {
                $status[$n] = "success";
            } else {
                $status[$n] = "error";
                $response_array[$i]['exam'] = $examNr;
            }
            $n++;

            $qry = "SELECT exam_id FROM tbl_exam WHERE exam_date='$date' AND exam_name_id=$examNameID";
            $res = pg_query($con, $qry);
            $row = pg_fetch_assoc($res);
            $exam_id = $row['exam_id'];

            // insert lecturer
            $qry = $INSERT_exam_people . " VALUES ($exam_id,$lecturerID,1)";
            if (pg_query($con, $qry)) {
                $status[$n] = "success";
            } else {
                $status[$n] = "error";
                $response_array[$i]['exam'] = $examNr;
            }
            $n++;
            // insert vdp
            if ($vdp) {
                $qry = $INSERT_exam_people . " VALUES ($exam_id,$vdpID,3)";
                if (pg_query($con, $qry)) {
                    $status[$n] = "success";
                } else {
                    $status[$n] = "error";
                    $response_array[$i]['exam'] = $examNr;
                }
                $n++;
            }
            // insert sysadmin
            if ($sysadmin) {
                $qry = $INSERT_exam_people . " VALUES ($exam_id,$sysadminID,4)";
                if (pg_query($con, $qry)) {
                    $status[$n] = "success";
                } else {
                    $status[$n] = "error";
                    $response_array[$i]['exam'] = $examNr;
                }
                $n++;
            }
        } else {
            $status[$n] = "already existing";
        }
        $n++;
        $response_array[$i]['status'] = $status;
    }

    pg_close($con);
    echo json_encode($response_array);
}
