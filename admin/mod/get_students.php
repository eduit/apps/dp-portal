<?php

// Get preferences
$qry = $SELECT_prefs;
$res = pg_query($con, $qry);
$num = pg_num_rows($res);
for ($i = 0; $i < $num; $i++) {
    $row = pg_fetch_array($res);
    if ($row['pref_name'] == "students_per_page") {
        $students_per_page = $row['pref_value'];
    }
}

if (isset($_GET['page']) && is_numeric($_GET['page'])) {
    $page = $_GET['page'];
} else {
    $page = 0;
}

$no_of_records_per_page = $students_per_page;
$offset = ($page) * $no_of_records_per_page;

$totalPages_sql = "SELECT COUNT(*) FROM tbl_student";
$result = pg_query($con, $totalPages_sql);
$total_rows = pg_fetch_array($result)[0];
$totalPages = ceil($total_rows / $no_of_records_per_page);

$qry = $SELECT_students . " ORDER BY student_lastname, student_firstname LIMIT $no_of_records_per_page OFFSET $offset";
$res1 = pg_query($con, $qry);
$num1 = pg_num_rows($res1);

echo "<p><span id=\"count\"></span>" . $num1 . "/" . $total_rows . " " . _COUNT . "</p>";

if ($totalPages > 1) {
    pagination($page, $totalPages);
}

echo "<table id=\"students\" class=\"admintable generaltable\" style=\"width: 100%;\">
					<thead class=\"resizable sticky\">
					<tr>
						<th style=\"width: 25%;\">" . _REGISTRATION . "</th>
						<th style=\"width: 25%;\">" . _LASTNAME . "</th>
						<th style=\"width: 25%;\">" . _FIRSTNAME . "</th>
						<th style=\"width: 25%;\">" . _EDIT . "</th>
					</tr>
					</thead><tbody class=\"resizable\">";

echo "<tr id=\"empty_row\">
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>";

for ($i = 0; $i < $num1; $i++) {
    $row1 = pg_fetch_array($res1);
    $student_id = $row1['student_id'];
    $student_registration = $row1['student_matrikelnr'];
    $student_lastname = $row1['student_lastname'];
    $student_firstname = $row1['student_firstname'];

    echo "<tr id=\"row_" . $student_id . "\">
						<td id=\"reg_nr_" . $student_id . "\">" . $student_registration . "</td>
						<td id=\"lastname_" . $student_id . "\">" . $student_lastname . "</td>
						<td id=\"firstname_" . $student_id . "\">" . $student_firstname . "</td>";
    echo "<td class=\"edit_row\">";
	echo "<i class=\"icon fa fa-edit fa-fw edit_icon\" title=\"" . _EDIT . "\" aria-label=\"" . _EDIT . "\" name=\"edit_row_" . $student_id . "\"></i>";
	echo "<i class=\"icon fa fa-trash fa-fw delete_student_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" name=\"" . $student_id . "\"></i>
							</td>
						</tr>";
    echo "<tr id=\"edit_row_" . $student_id . "\" class=\"edit_rows hidden\">
							<td><input type=\"text\" class=\"form-control\" name=\"reg_nr_" . $student_id . "\" id=\"edit_reg_nr_" . $student_id . "\" required=\"required\" value=\"" . $student_registration . "\" maxlength=\"" . MAXLENGTH . "\"></td>
							<td><input type=\"text\" class=\"form-control\" name=\"lastname_" . $student_id . "\" id=\"edit_lastname_" . $student_id . "\" required=\"required\" value=\"" . $student_lastname . "\" maxlength=\"" . MAXLENGTH . "\"></td>
							<td><input type=\"text\" class=\"form-control\" name=\"firstname_" . $student_id . "\" id=\"edit_firstname_" . $student_id . "\" required=\"required\" value=\"" . $student_firstname . "\" maxlength=\"" . MAXLENGTH . "\"></td>
							<td>";
								echo "<i class=\"icon fa fa-save fa-fw save_student_icon\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"" . $student_id . "\"></i>";
                                echo "<i class=\"icon fa fa-times fa-fw cancel_icon\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"" . $student_id . "\"></i>
							</td>
							</tr>";
}

echo "</tbody></table>";

if ($totalPages > 1) {
    pagination($page, $totalPages);
}

function pagination($page, $totalPages)
{
    $showPages = 4;

    echo "<ul class=\"pagination mt-3\">";
    echo "<li class=\"page-item";
    if ($page == 0) {
        echo " active";
    }
    echo "\">";
    echo "<a href=\"";
    echo paramChanger("page", 0);
    echo "\" class=\"page-link\">";
    echo "1";
    echo "</a></li>";

    if ($page > $showPages + 1) {
        echo "<li class=\"page-item\">
				<a class=\"page-link\" style=\"cursor: default;\">
					<span aria-hidden=\"true\">...</span>
					<span class=\"sr-only\">...</span>
				</a>
			</li>";
    }

    for ($i = max(1, $page - $showPages); $i <= min($page + $showPages, $totalPages - 2); $i++) {
        echo "<li class=\"page-item";
        if ($page == $i) {
            echo " active";
        }
        echo "\">";
        echo "<a href=\"";
        echo paramChanger("page", $i);
        echo "\" class=\"page-link\">";
        echo $i + 1;
        echo "</a></li>";
    }

    if ($page < $totalPages - $showPages - 2) {
        echo "<li class=\"page-item\">
				<a class=\"page-link\" style=\"cursor: default;\">
					<span aria-hidden=\"true\">...</span>
					<span class=\"sr-only\">...</span>
				</a>
			</li>";
    }

    echo "<li class=\"page-item";
    if ($page == $i) {
        echo " active";
    }
    echo "\">";
    echo "<a href=\"";
    echo paramChanger("page", $totalPages - 1);
    echo "\" class=\"page-link\">";
    echo $totalPages;
    echo "</a></li>";
    echo "</ul>";
}
