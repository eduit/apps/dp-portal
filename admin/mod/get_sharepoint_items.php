<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

// Include language file
if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else {
    // default
    $qry = $SELECT_lang_default;
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $lang = $row['lang'];
}
include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

$sp_script = $_SERVER["DOCUMENT_ROOT"] . "/inc/sharepoint/sharepoint_cert.py";

session_start();

if ($_SESSION['loggedin']) {
    $json = shell_exec("python3 " . $sp_script);
    $today = date("Y-m-d");
    $arr = json_decode($json, true);

    foreach ($arr as $i => $item) {
        unset($arr[$i]);

        // Exam number
        $examNr = $item['fields']['Title'] ?? '';
        $arr[$i][_EXAM_NR] = $examNr;

        // Exam name
        $examName = htmlspecialchars($item['fields']['LKTitle']) ?? '';
        $arr[$i][_EXAM] = $examName;

        // Lecturer
        $lecturerTitle = 'Examinator';
        $lecturerValue = $item['fields'][$lecturerTitle];
        if (is_array($item['fields'][$lecturerTitle])) {
            $lecturer = $lecturerValue[0]['LookupValue'];
        } else {
            $lecturer = $lecturerValue['LookupValue'];
        }
        if ($lecturer == 'op') {
            $lecturer = 'Digitale Prüfungen';
        }
        $arr[$i][_LECTURER] = $lecturer;

        // VdP
        $vdp = $item['fields']['VdP'] ?? '';
        $arr[$i][_VDP] = $vdp;

        // Sysadmin
        $sysadmin_title = 'SysAdmins';
        $sysadmin = $item['fields'][$sysadmin_title] ?? '';
        if (is_array($sysadmin) && !empty($sysadmin)) {
            $sysadmin = $sysadmin[0]['LookupValue'];
        } else {
            $sysadmin = '';
        }
        $arr[$i][_SYSADMIN] = $sysadmin;

        // Date
        $date = $item['fields']['Datum'];
        if ($date == null) {
            $date = '';
        } else {
            $date = str_replace(", ", "-", $date);
            $date = date_create($date);
            $date = date_format($date, "Y-m-d");
        }
        $arr[$i][_DATE] = $date;

        // Setup
        $setupValue = $item['fields']['Setup'] ?? '';
        if ($setupValue == null) {
            $arr[$i][_TYPE] = '';
        } else {
            $arr[$i][_TYPE] = $setupValue;
        }

        // Language
        $arr[$i][_LANGUAGE] = $item['fields']['Pruefungssprache'] ?? '';

        // Link
        $linkValue = $item['fields']['Pruefungslink']['Url'] ?? '';
        if ($linkValue == null) {
            $arr[$i][_LINK] = '';
        } else {
            $arr[$i][_LINK] = $linkValue;
        }

        // vdi pool
        $vdiPool = $item['fields']['VDI_x002d_Pool'] ?? '';
        if ($vdiPool == null) {
            $arr[$i][_VDI_POOL] = '';
        } else {
            $arr[$i][_VDI_POOL] = $vdiPool;
        }

        // filter for remote or old exams
        if ($setupValue == "remote" || $date < $today) {
            unset($arr[$i]);
            continue;
        }

        // check if already in database
        $qry = "SELECT exam_name_id FROM tbl_exam_name WHERE exam_nr='$examNr' AND exam_name='$examName'";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $examNameID = $row['exam_name_id'] ?? 0;

        $qry = "SELECT exam_id FROM tbl_exam WHERE exam_date='$date' AND exam_name_id=$examNameID";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $exam_id = $row['exam_id'] ?? 0;

        if ($exam_id) {
            $arr[$i]['existing'] = 'true';
        } else {
            $arr[$i]['existing'] = 'false';
        }

        // check lecturer
        if ($lecturer == "Online-Prüfungen") {
            $qry = $SELECT_people . " WHERE POSITION(tbl_people.people_lastname IN '$lecturer') > 0";
        } else {
            $qry = $SELECT_people . " WHERE POSITION(tbl_people.people_lastname IN '$lecturer') > 0 AND POSITION(tbl_people.people_firstname IN '$lecturer') > 0";
        }
        $res = pg_query($con, $qry);
        $num = pg_num_rows($res);
        $row = pg_fetch_assoc($res);
        $lecturerID = $row['id'] ?? 0;

        if ($lecturerID) {
            $arr[$i]['lecturer_exists'] = 'true';
        } else {
            $arr[$i]['lecturer_exists'] = 'false';
        }

        // check vdp
        $qry = $SELECT_people . " WHERE POSITION(tbl_people.people_lastname IN '$vdp') > 0 AND POSITION(tbl_people.people_firstname IN '$vdp') > 0";
        $res = pg_query($con, $qry);
        $num = pg_num_rows($res);
        $row = pg_fetch_assoc($res);
        $vdpID = $row['id'] ?? 0;

        if ($vdpID) {
            $arr[$i]['vdp_exists'] = 'true';
        } else {
            $arr[$i]['vdp_exists'] = 'false';
        }

        // check lecturer
        $qry = $SELECT_people . " WHERE POSITION(tbl_people.people_lastname IN '$sysadmin') > 0 AND POSITION(tbl_people.people_firstname IN '$sysadmin') > 0";
        $res = pg_query($con, $qry);
        $num = pg_num_rows($res);
        $row = pg_fetch_assoc($res);
        $sysadminID = $row['id'] ?? 0;

        if ($sysadminID) {
            $arr[$i]['sysadmin_exists'] = 'true';
        } else {
            $arr[$i]['sysadmin_exists'] = 'false';
        }
    }

    // sort after date
    function cmp($a, $b)
    {
        return strcmp($a[_DATE], $b[_DATE]);
    }
    usort($arr, 'cmp');

    $json = json_encode($arr, true);
    pg_close($con);
    echo $json;
}
