<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$id = $_POST["id"];
	$username = htmlspecialchars($_POST["username"]);
	$lastname = htmlspecialchars($_POST["lastname"]);
	$firstname = htmlspecialchars($_POST["firstname"]);

	$qry = $SELECT_people_id . " WHERE people_username='$username' AND people_lastname='$lastname' AND people_firstname='$firstname'";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$num = pg_num_rows($res);
	if ($num > 0) {
		$people_id = $row['people_id'];
	} else {
		$people_id = '';
	}

	if (!$people_id || $people_id == $id) {
		$qry = $UPDATE_people . " SET people_username='$username', people_lastname='$lastname', people_firstname='$firstname' WHERE people_id=$id";

		if (pg_query($con, $qry)) {
			$response_array['status'] = 'success';
		} else {
			$response_array['status'] = 'error';
		}
	} else {
		$response_array['status'] = 'already existing';
	}

	pg_close($con);
	echo json_encode($response_array);
}
