<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

$term = $_GET['term'];

// Include language file
if (isset($_SESSION['lang'])) {
    $lang_code = $_SESSION['lang'];
} else {
    $lang_code = "de";
}

if ($_SESSION['loggedin'] || $_SESSION['user_vho']) {
    $response_array[0] = '';
    
    // get language_id
    $qry = $SELECT_language . " WHERE lang_code = '$lang_code'";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $lang_id = $row['lang_id'];

    // get templates
    $qry = $SELECT_templates . " AND tbl_template.lang_id=" . $lang_id . " AND tbl_template.type='logsheet' AND LOWER(tbl_template.text) LIKE LOWER('" . $term . "%') ORDER BY tbl_template.text";
    $res = pg_query($con, $qry);
    $num = pg_num_rows($res);

    for ($i = 0; $i < $num; $i++) {
        $row = pg_fetch_array($res);
        $text = $row['text'];
        $response_array[$i] = $text;
    }

    pg_close($con);

    echo json_encode($response_array);
}
