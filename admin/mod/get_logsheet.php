<?php

if (isset($_POST["exam_id"])) {
    $examSelected = $_POST["exam_id"];
} else {
    $examSelected = 0;
}

if (isset($_POST["room_id"])) {
    $roomSelected = $_POST["room_id"];
} else {
    $roomSelected = 0;
}

if (isset($_GET["download"])) {
    $download = $_GET["download"];
} else {
    $download = 0;
}

if (isset($_GET['archived'])) {
    $archived = $_GET['archived'];
} else {
    $archived = 0;
}

if ($archived != "true" && $archived != "false") {
    $archived = "false";
}

$selected = 0;
$multipleComputers = 0;

if ($examSelected) {
    getSelect("students", $examSelected, $roomSelected, $selected, $multipleComputers);
} elseif ($roomSelected) {
    getSelect("computers", $examSelected, $roomSelected, $selected, $multipleComputers);
} elseif ($download) {
    download($archived);
} else {
    // Include language file
    if (isset($_SESSION['lang'])) {
        $lang = $_SESSION['lang'];
    } else {
        // default
        $qry = $SELECT_lang_default;
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $lang = $row['lang'];
    }

    include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_$lang.php";

    $user = $_SESSION['username'];
    $date = date_create();
    $date = date_format($date, "Y-m-d");
    $str_length = 15;

    $checkbox = "<p><label id=\"archived_label\" title=\"" . _ARCHIVED . "\"><input name=\"archived\" type=\"checkbox\" name=\"\" value=\"0\" id=\"archived\" ";
    if ($archived == "true") {
        $checkbox = $checkbox . "checked";
    }
    $checkbox = $checkbox . "> " . _ARCHIVED . "</label></p>";

    echo $checkbox;

    if ($_SESSION['loggedin']) {
        echo "<i id=\"logsheet_download\" class=\"icon fa fa-download fa-fw\" title=\"" . _DOWNLOAD . "\" aria-label=\"" . _DOWNLOAD . "\"></i>";
        echo "<div id=\"div_notification\" class=\"hidden\"><p id=\"notification_text\"></p></div>";
        echo "<br><br>";
    }

    /* incident list */
    if (!$_SESSION['loggedin']) {
        $sql = " AND reported_by = '$user'";
    } else {
        $sql = "";
    }

    $qry = $SELECT_logsheet . $sql . " AND tbl_exam.archive = $archived ORDER BY created_at DESC";
    $res1 = pg_query($con, $qry);
    $num1 = pg_num_rows($res1);

    echo "<p><span id=\"count\"></span>$num1 " . _COUNT . "</p>";

    echo "<table id=\"logsheet\" class=\"admintable generaltable\" style=\"width: 100%;\">
        <thead class=\"resizable sticky\">
        <tr>
            <th style=\"width: 6%;\">" . _DATE . "<br></th>
            <th style=\"width: 10%;\">" . _EXAM . "<br></th>
            <th style=\"width: 10%;\">" . _STUDENT . "<br></th>
            <th style=\"width: 10%;\">" . _ROOM . "<br></th>
            <th style=\"width: 8%;\">" . _COMPUTER . "<br></th>
            <th style=\"width: 32%;\">" . _DESCRIPTION . "<br></th>
            <th style=\"width: 10%;\">" . _FILE_PHOTO . "<br></th>";
    if ($archived == "false") {
        echo "<th style=\"width: 2%;\" class=\"no-filter\"></th>";
    }
    echo "<th style=\"width: 6%;\">" . _REPORTED_BY . "<br></th>";
    if ($archived == "false") {
        echo "<th style=\"width: 6%;\">" . _EDIT . "</th>";
    }
    echo "</tr></thead><tbody class=\"resizable\">";
    if ($archived == "false") {
        // add row
        echo "<tr><td><input type=\"text\" class=\"form-control\" name=\"0\" id=\"add_date\" disabled value=\"$date\"></td>";
        echo "<td>";
        echo "<select class=\"select custom-select form-control\" name=\"0\" id=\"add_exam\" style=\"width: 100%;\">";
        getSelect("dates", $examSelected, $roomSelected, $selected, $multipleComputers);
        echo "</select>
            <br><span id=\"exam_missing\" class=\"invalid-feedback\">" . _EXAM . " " . _MISSING . "</span></td>";
        echo "<td>";
        echo "<select class=\"select custom-select form-control select_student\" name=\"0\" id=\"add_student\" disabled style=\"width: 100%;\">";
        echo "<option name=\"" . _NO_STUDENT . "\" value=\"0\" selected>" . _NO_STUDENT . "</option>\n";
        echo "</select>
            <br><span id=\"student_missing\" class=\"invalid-feedback\">" . _STUDENT . " " . _MISSING . "</span></td>";
        echo "<td>";
        echo "<select class=\"select custom-select form-control select_room\" name=\"0\" id=\"add_room\" style=\"width: 100%;\">";
        getSelect("rooms", $examSelected, $roomSelected, $selected, $multipleComputers);
        echo "</select>
            <br><span id=\"room_missing\" class=\"invalid-feedback\">" . _ROOM . " " . _MISSING . "</span></td>";
        echo "<td>";
        echo "<select class=\"select custom-select form-control select_computer\" name=\"0\" id=\"add_computer\" style=\"width: 100%;\">";
        getSelect("computers", $examSelected, $roomSelected, $selected, $multipleComputers);
        echo "</select>
            <br><span id=\"computer_missing\" class=\"invalid-feedback\">" . _COMPUTER . " " . _MISSING . "</span></td>";
        echo "<td><input type=\"text\" class=\"form-control\" name=\"0\" id=\"add_description\" maxlength=\"" . MAXLENGTH . "\">
            <span id=\"description_missing\" class=\"invalid-feedback\">" . _DESCRIPTION . " " . _MISSING . "</span></td>";
        echo "<td><input type=\"text\" class=\"form-control\" name=\"file_0\" id=\"add_file\" value=\"\" maxlength=\"" . 2 * MAXLENGTH . "\" readonly>
            <span id=\"file_missing\" class=\"invalid-feedback\">" . _FILE_PHOTO . " " . _MISSING . "</span></td>";
        echo "<td class=\"overflow_visible\"><i id=\"add_file_upload\" class=\"icon fa fa-upload fa-fw upload_icon\" title=\"" . _UPLOAD . "\" aria-label=\"" . _UPLOAD . "\" name=\"0\"></i>
            <input type=\"file\" id=\"fileloader_logsheet\" class=\"fileloader hidden\" name=\"0\" title=\"" . _UPLOAD . "\" data-href=\"$logsheet_dir\"  /></td>";
        echo "<td><input type=\"text\" class=\"form-control\" name=\"0\" id=\"add_user\" disabled value=\"$user\"></td>";
        echo "<td><i id=\"incident_submit\" class=\"icon fa fa-plus fa-fw add_icon\" title=\"" . _ADD . "\" aria-label=\"" . _ADD . "\" name=\"add_incident\"></i></td></tr>";
    }

    for ($i = 0; $i < $num1; $i++) {
        $row1 = pg_fetch_array($res1);
        $id = $row1['incident_id'];
        $date = date_create($row1['created_at']);
        $date = date_format($date, "Y-m-d");
        $updated = date_create($row1['updated_at']);
        $updated = date_format($updated, "Y-m-d");
        $examID = $row1['exam_id'];
        $student_id = $row1['student_id'];
        $room_id = $row1['room_id'];
        $computer_id = $row1['computer_id'];
        $multipleComputers = $row1['multiple_computers'];
        $description = $row1['description'];
        $file = $row1['file'];
        $file_link = "https://$_SERVER[HTTP_HOST]/" . $logsheet_dir . $file;
        $user = $row1['reported_by'];
        $reported_by = $row1['reported_by'];
        $exam = '';
        $student = '';
        $computer = '';

        // get exam
        if ($examID) {
            $qry = $SELECT_exam_name_by_id . $examID;
            $res2 = pg_query($con, $qry);
            $row2 = pg_fetch_assoc($res2);
            $num2 = pg_num_rows($res2);
            if ($num2 > 0) {
                $exam = $row2['exam_name'] . " (" . $row2['exam_nr'] . ")";
            }
        }

        // get student
        if ($student_id) {
            $qry = $SELECT_students . " WHERE student_id=$student_id";
            $res3 = pg_query($con, $qry);
            $row3 = pg_fetch_assoc($res3);
            $num3 = pg_num_rows($res3);
            if ($num3 > 0) {
                $student = $row3['student_lastname'] . " " . $row3['student_firstname'] . " (" . $row3['student_matrikelnr'] . ")";
            }
        }

        // get room
        if ($room_id) {
            $qry = $SELECT_rooms . " AND room_id=$room_id";
            $res4 = pg_query($con, $qry);
            $row4 = pg_fetch_assoc($res4);
            $num4 = pg_num_rows($res4);
            if ($num4 > 0) {
                $room = $row4['room_name'];
                $roomDN = $row4['room_display_name'];

                //show display name if set
                if ($roomDN) {
                    $room = $roomDN;
                }
            }
        } else {
            $room_id = 0;
            $room = '';
        }

        // get computer
        if ($computer_id) {
            $qry = $SELECT_computers . " AND computer_id=$computer_id";
            $res5 = pg_query($con, $qry);
            $row5 = pg_fetch_assoc($res5);
            $num5 = pg_num_rows($res5);
            if ($num5 > 0) {
                $computer = $row5['computer'];
                if (!$room_id) {
                    $room_id = $row5['room_id'];
                    $room = $row5['room_name'];
                }
            } else {
                $computer = '';
            }
        }

        if ($_SESSION['user_vho'] || $_SESSION['loggedin'] || $_SESSION['username'] == $user) {
            echo "<td id=\"date_$id\" title=\"" . _UPDATED . ": $updated\">$date</td>";
            echo "<td id=\"exam_$id\">$exam</td>";
            echo "<td id=\"student_$id\">$student</td>";
            echo "<td id=\"room_$id\">$room</td>";
            if ($multipleComputers == "t") {
                echo "<td id=\"computer_$id\">" . _MULTIPLE_COMPUTERS . "</td>";
            } else {
                echo "<td id=\"computer_$id\">$computer</td>";
            }
            echo "<td id=\"description_$id\">$description</td>";
            echo "<td id=\"file_$id\">";
            if ($file) {
                if ($image_type_check = exif_imagetype($file_link)) {
                    $width = getimagesize($file_link)[0];
                    $height = getimagesize($file_link)[1];
                    echo "<a href=\"$file_link\" target=\"_blank\" class=\"img_dialog\" data-href=\"{'width': $width, 'height': $height}\">";
                    if (strlen($file) > $str_length) {
                        echo "...";
                    }
                    echo substr($file, -$str_length) . "</a>";
                } else {
                    echo "<a href=\"$file_link\" target=\"_blank\">";
                    if (strlen($file) > $str_length) {
                        echo "...";
                    }
                    echo substr($file, -$str_length) . "</a>";
                }
            }
            echo "</td>";
            if ($archived == "false") {
                echo "<td></td>";
            }
            echo "<td id=\"reported_by_$id\">$reported_by</td>";
            if ($archived == "false") {
                echo "<td class=\"edit_row\">";
                echo "<i class=\"icon fa fa-edit fa-fw edit_icon\" title=\"" . _EDIT . "\" aria-label=\"" . _EDIT . "\" name=\"edit_row_$id\"></i>";
                echo "<i class=\"icon fa fa-trash fa-fw delete_incident_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" name=\"$id\"></i>";
                echo "</td>";
            }
            echo "</tr>";

            // edit row
            if ($archived == "false") {
                echo "<tr id=\"edit_row_$id\" class=\"edit_rows hidden\">";
                echo "<td><input type=\"text\" class=\"form-control\" name=\"$id\" id=\"edit_date_$id\" disabled value=\"$date\"></td>";
                echo "<td><input type=\"text\" class=\"form-control\" name=\"$examID\" id=\"edit_exam_$id\" disabled value=\"$exam\"></td>";
                echo "<td>";
                echo "<select class=\"select custom-select form-control edit_student\" name=\"$id\" id=\"edit_student_$id\" style=\"width: 100%;\">";
                getSelect("students", $examID, $roomSelected, $student_id, $multipleComputers);
                echo "</select>
            <br><span id=\"student_missing_$id\" class=\"invalid-feedback\">" . _STUDENT . " " . _MISSING . "</span></td>";
                echo "<td>";
                echo "<select class=\"select custom-select form-control edit_room\" name=\"$id\" id=\"edit_room_$id\" style=\"width: 100%;\">";
                getSelect("rooms", $examSelected, $roomSelected, $room_id, $multipleComputers);
                echo "</select>
            <br><span id=\"room_missing_$id\" class=\"invalid-feedback\">" . _ROOM . " " . _MISSING . "</span></td>";
                echo "<td>";
                echo "<select class=\"select custom-select form-control edit_computer\" name=\"$id\" id=\"edit_computer_$id\" style=\"width: 100%;\">";
                getSelect("computers", $examSelected, $room_id, $computer_id, $multipleComputers);
                echo "</select>
            <br><span id=\"computer_missing_$id\" class=\"invalid-feedback\">" . _COMPUTER . " " . _MISSING . "</span></td>";
                echo "<td><input type=\"text\" class=\"form-control\" name=\"$id\" id=\"edit_description_$id\" maxlength=\"" . MAXLENGTH . "\" value=\"$description\">
            <span id=\"description_missing_$id\" class=\"invalid-feedback\">" . _DESCRIPTION . " " . _MISSING . "</span></td>";
                echo "<td><input type=\"text\" class=\"form-control\" name=\"file_0\" id=\"edit_file_$id\" required=\"required\" maxlength=\"" . 2 * MAXLENGTH . "\" value=\"$file\" dir=\"rtl\" readonly>
            <span id=\"file_missing_$id\" class=\"invalid-feedback\">" . _FILE . " " . _MISSING . "</span></td>";
                echo "<td class=\"overflow_visible\"><i id=\"edit_file_upload_$id\" class=\"icon fa fa-upload fa-fw upload_icon edit_file_upload_logsheet\" title=\"" . _UPLOAD . "\" aria-label=\"" . _UPLOAD . "\" name=\"$id\"></i>
            <input type=\"file\" id=\"edit_fileloader_logsheet_$id\" class=\"edit_fileloader_logsheet hidden\" name=\"$id\" title=\"" . _UPLOAD . "\" data-href=\"$logsheet_dir\"  /></td>";
                echo "<td><input type=\"text\" class=\"form-control\" name=\"$id\" id=\"edit_user_$id\" disabled value=\"$user\"></td>";
                echo "<td><i class=\"icon fa fa-save fa-fw save_incident_icon\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"$id\"></i>";
                echo "<i class=\"icon fa fa-times fa-fw cancel_icon\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"$id\"></i></td>";
                echo "</tr>";
            }
        }
    }
    echo "</tbody></table>";
    if ($archived == "true") {
        echo "<script>
                $(function () {
                    $(\"#logsheet\").excelTableFilter();
                    let rowCount = $(\"#logsheet tbody tr:visible\").length;
                    $(\"#count\").html(rowCount+\"/\");
                    
                    if(/chrom(e|ium)/.test(navigator.userAgent.toLowerCase())){
                        $(\".dropdown-filter-content\").parent().parent(\"th\").addClass(\"drp_menu\");
                    }
                });
            </script>";
    }
    echo "<div id=\"dialog\" class=\"hidden\" title=\"" . _FILE_PHOTO . "\"></div>";
}

function getSelect($mode, $examID, $roomID, $selected, $multipleComputers)
{
    include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
    include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

    session_start();

    if ($_SESSION['loggedin'] || $_SESSION['user_vho']) {

        // Include language file
        if (isset($_SESSION['lang'])) {
            $lang = $_SESSION['lang'];
        } else {
            // default
            $qry = $SELECT_lang_default;
            $res = pg_query($con, $qry);
            $row = pg_fetch_assoc($res);
            $lang = $row['lang'];
        }

        include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_$lang.php";

        switch ($mode) {
            case "dates":
                $qry = $SELECT_dates_light . " AND
							  tbl_exam.archive = false AND tbl_exam.disabled = false
							ORDER BY tbl_exam.exam_date, tbl_exam_name.exam_name";
                $res = pg_query($con, $qry);
                $num = pg_num_rows($res);

                echo "<option name=\"" . _NO_EXAM_2 . "\" value=\"0\" disabled selected>" . _NO_EXAM_2 . "</option>\n";

                for ($i = 0; $i < $num; $i++) {
                    $row = pg_fetch_array($res);
                    $id = $row['id'];
                    $examName = $row['exam_name'];
                    $lastname = $row['lecturer_lastname'];
                    $date = date_create($row['date']);

                    echo "<option name=\"" . $examName . "\" value=\"$id\"";
                    if ($id == $selected) {
                        echo " selected ";
                    }
                    echo ">" . $examName . " | " . $lastname . " | " . date_format($date, 'Y-m-d') . "</option>\n";
                }
                break;

            case "students":
                $qry = $SELECT_exam_students_by_exam_id . " AND tbl_exam.exam_id=$examID GROUP BY tbl_student.student_id, tbl_exam.exam_id ORDER BY student_lastname";
                $res = pg_query($con, $qry);
                $num = pg_num_rows($res);

                echo "<option name=\"" . _NO_STUDENT_2 . "\" value=\"0\" selected>" . _NO_STUDENT_2 . "</option>\n";

                for ($i = 0; $i < $num; $i++) {
                    $row = pg_fetch_array($res);
                    $id = $row['student_id'];
                    $studentName = $row['lastname'] . " " . $row['firstname'];

                    echo "<option name=\"$studentName\" value=\"$id\"";
                    if ($id == $selected) {
                        echo " selected ";
                    }
                    echo ">$studentName</option>\n";
                }
                break;

            case "rooms":
                $qry = $SELECT_rooms . " ORDER BY room_name";
                $res = pg_query($con, $qry);
                $num = pg_num_rows($res);

                echo "<option name=\"" . _NO_ROOM . "\" value=\"0\" disabled selected>" . _NO_ROOM . "</option>\n";

                for ($i = 0; $i < $num; $i++) {
                    $row = pg_fetch_array($res);
                    $id = $row['room_id'];
                    $roomName = $row['room_name'];
                    $roomDN = $row['room_display_name'];

                    //show display name if set
                    if ($roomDN) {
                        $roomName = $roomDN;
                    }

                    echo "<option name=\"$roomName\" value=\"$id\"";
                    if ($id == $selected) {
                        echo " selected ";
                    }
                    echo ">$roomName</option>\n";
                }
                break;

            case "computers":
                if ($roomID) {
                    $room = "AND tbl_room.room_id = $roomID";
                } else {
                    $room = "";
                }
                $qry = $SELECT_computers . " AND tbl_computer.computer_name <> '' AND tbl_computer.computer_name NOT LIKE '%(LAN)%' AND tbl_computer.vdi = false $room ORDER BY computer_name";
                $res = pg_query($con, $qry);
                $num = pg_num_rows($res);

                if ($multipleComputers == "t") {
                    echo "<option name=\"" . _NO_COMPUTER_UNKNOWN . "\" value=\"0\">" . _NO_COMPUTER_UNKNOWN . "</option>\n";
                    echo "<option name=\"" . _MULTIPLE_COMPUTERS . "\" value=\"mc\" selected>" . _MULTIPLE_COMPUTERS . "</option>\n";
                } else {
                    echo "<option name=\"" . _NO_COMPUTER_UNKNOWN . "\" value=\"0\" selected>" . _NO_COMPUTER_UNKNOWN . "</option>\n";
                    echo "<option name=\"" . _MULTIPLE_COMPUTERS . "\" value=\"mc\">" . _MULTIPLE_COMPUTERS . "</option>\n";
                }

                for ($i = 0; $i < $num; $i++) {
                    $row = pg_fetch_array($res);
                    $id = $row['computer_id'];
                    $computerName = $row['computer'];

                    echo "<option name=\"$computerName\" value=\"$id\"";
                    if ($id == $selected) {
                        echo " selected ";
                    }
                    echo ">$computerName</option>\n";
                }
                break;

            default:
                break;
        }
    }
}

function download($archived)
{
    session_start();

    if ($_SESSION['loggedin']) {
        if ($archived == "true") {
            $filename = "logsheet_archive.csv";
        } else {
            $filename = "logsheet.csv";
        }

        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=' . $filename);

        include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
        include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";


        // Include language file
        if (isset($_SESSION['lang'])) {
            $lang = $_SESSION['lang'];
        } else {
            // default
            $qry = $SELECT_lang_default;
            $res = pg_query($con, $qry);
            $row = pg_fetch_assoc($res);
            $lang = $row['lang'];
        }

        include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_$lang.php";

        /* incident list */
        $output = fopen('php://output', 'w');
        $line = array(_DATE, _UPDATED, _EXAM, _STUDENT, _ROOM, _COMPUTER, _DESCRIPTION, _FILE_PHOTO, _REPORTED_BY);
        fwrite($output, implode(',', $line) . "\n");

        $qry = $SELECT_logsheet . " AND tbl_exam.archive = $archived ORDER BY created_at DESC";
        $res1 = pg_query($con, $qry);
        $num1 = pg_num_rows($res1);

        for ($i = 0; $i < $num1; $i++) {
            $row1 = pg_fetch_array($res1);
            $date = date_create($row1['created_at']);
            $date = date_format($date, "Y-m-d");
            $updated = date_create($row1['updated_at']);
            $updated = date_format($updated, "Y-m-d");
            $examID = $row1['exam_id'] ?? 0;
            $studentID = $row1['student_id'] ?? 0;
            $computerID = $row1['computer_id'] ?? 0;
            $multipleComputers = $row1['multiple_computers'] ?? 0;
            $description = $row1['description'] ?? '';
            $file = $row1['file'];
            $reportedBy = $row1['reported_by'];

            // get exam
            $qry = $SELECT_exam_name_by_id . $examID;
            $res2 = pg_query($con, $qry);
            $row2 = pg_fetch_assoc($res2);
            $num2 = pg_num_rows($res2);
            if ($num2 > 0) {
                $exam = $row2['exam_name'] . " (" . $row2['exam_nr'] . ")";
            } else {
                $exam = '';
            }

            // get student
            $qry = $SELECT_students . " WHERE student_id=$studentID";
            $res3 = pg_query($con, $qry);
            $row3 = pg_fetch_assoc($res3);
            $num3 = pg_num_rows($res3);
            if ($num3 > 0) {
                $student = $row3['student_lastname'] . " " . $row3['student_firstname'] . " (" . $row3['student_matrikelnr'] . ")";
            } else {
                $student = '';
            }

            // get computer
            $qry = $SELECT_computers . " AND computer_id=$computerID";
            $res4 = pg_query($con, $qry);
            $row4 = pg_fetch_assoc($res4);
            $num4 = pg_num_rows($res4);
            if ($num4 > 0) {
                $computer = $row4['computer'];
                $room = $row4['room_name'];
            } else {
                $computer = '';
                $room = '';
            }
            if ($multipleComputers == "t") {
                $computer = _MULTIPLE_COMPUTERS;
            }

            $line = array($date, $updated, html_entity_decode($exam), $student, $room, $computer, $description, $file, $reportedBy);
            fwrite($output, implode(',', $line) . "\n");
        }
        fclose($output);
    }
}
