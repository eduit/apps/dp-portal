<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

// Include language file
if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else {
    // default
    $qry = $SELECT_lang_default;
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $lang = $row['lang'];
}
include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

if ($_SESSION['loggedin']) {
    $folder = htmlspecialchars($_POST["folder"]);
    $path = $upload_dir . "/" . $folder;

    $files = array_diff(scandir($path), array('..', '.'));
    natcasesort($files);
    $i = 0;
    $arr = array();

    foreach ($files as $file) {
        $filepath = $path . "/" . $file;
        $arr[$i][_FILENAME] = $file;
        $arr[$i][_FILESIZE] = filesize($filepath) . " bytes";
        $arr[$i][_DATE_MOD] = date("d.m.Y H:i:s", filemtime($filepath));

        $allowed_types = array(IMAGETYPE_PNG, IMAGETYPE_JPEG, IMAGETYPE_GIF, IMAGETYPE_WEBP);
        $detectedType = exif_imagetype($filepath);
        
        if (in_array($detectedType, $allowed_types)){
            $width = getimagesize($filepath)[0];
            $height = getimagesize($filepath)[1];
            $size = "{'width': " . $width . ", 'height': ". $height ."}";
        } else {
            $size = '';
        }

        // check if already in database
        $qry = $SELECT_exam_by_file . "$file' ORDER BY exam_date DESC";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $num = pg_num_rows($res);
        $exam_name = $row['exam_name'] ?? '';
        $exam_date = $row['exam_date'] ?? '';
        if ($exam_name) {
            if ($num > 1) {
                $arr[$i][_EXAM] = _MULTIPLE_EXAMS;
            } else {
                $arr[$i][_EXAM] = $exam_name;
            }
        } else {
            $arr[$i][_EXAM] = '';
        }
        if ($exam_date) {
            if ($num > 1) {
                $arr[$i][_DATE] = "-";
            } else {
                $arr[$i][_DATE] = $exam_date;
            }
        } else {
            $arr[$i][_DATE] = '';
        }

        $url = "https://" . $_SERVER['SERVER_NAME'];
        $arr[$i]['filepath'] = $url . "/upload/" . $folder . "/" . $file;
        $arr[$i]['size'] = $size;
        $i++;
    }

    if (empty($arr)) {
        $response_array["content"] = 'empty';
        echo json_encode($response_array);
    } else {
        echo json_encode($arr, true);
    }
}
