<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $qry = $SELECT_people . " ORDER BY tbl_people.people_lastname";
    $res = pg_query($con, $qry);
    $num = pg_num_rows($res);
    $examPeople = '';

    for ($i = 0; $i < $num; $i++) {
        $row = pg_fetch_array($res);
        $peopleID = $row["id"];
        $peopleUsername = $row['username'];
        $peopleLastname = $row['lastname'];
        $peopleFirstname = $row['firstname'];

        $examPeople = $examPeople . "<option value=\"" . $peopleID . "\" title=\"" . $peopleUsername . "\">$peopleLastname $peopleFirstname</option>\n";
    }

    $response_array["content"] = $examPeople;
    pg_close($con);

    echo json_encode($response_array);
}
