<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$computer = htmlspecialchars($_POST["computer"]);
	$ip_address = htmlspecialchars($_POST["ip_address"]);
	$room_sector = htmlspecialchars($_POST["room_sector"]);
	$vdi = htmlspecialchars($_POST["vdi"]);
	$exam_user_id = htmlspecialchars($_POST["exam_user_id"]);

	$qry = "SELECT computer_id FROM tbl_computer WHERE computer_name='$computer' AND ip_address='$ip_address'";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$num = pg_num_rows($res);
	if ($num > 0) {
		$computer_id = $row['computer_id'];
	} else {
		$computer_id = '';
	}

	if (!$computer_id) {
		$qry = $INSERT_computer . " VALUES ('$computer','$ip_address',$room_sector,'$vdi','$exam_user_id')";

		if (pg_query($con, $qry)) {
			$response_array['status1'] = 'success';
		} else {
			$error = pg_last_error($con);
			if (preg_match('/duplicate/i', $error)) {
				$response_array['status1'] = 'already existing';
			} else {
				$response_array['status1'] = 'error';
			}
		}
	} else {
		$response_array['status1'] = 'already existing';
	}

	pg_close($con);
	echo json_encode($response_array);
}
