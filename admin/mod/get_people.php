<?php

$qry = $SELECT_people . " ORDER BY tbl_people.people_lastname, tbl_people.people_firstname;";
$res1 = pg_query($con, $qry);
$num1 = pg_num_rows($res1);

echo "<p>";
echo "<button id=\"add_ldap_people\" class=\"ui-button ui-widget ui-corner-all\">" . _IMPORT_LDAP_USERS . " (<span id=\"ldap_count\">0</span>)</button>&nbsp;";
echo "<button id=\"update_ldap_people\" class=\"ui-button ui-widget ui-corner-all\">" . _UPDATE_LDAP_USERS . " (<span id=\"ldap_update_count\">0</span>)</button>";
echo "</p>";

echo "<p>" . $num1 . " " . _COUNT . "</p>";

echo "<table id=\"people\" class=\"admintable generaltable\" style=\"width: 100%;\">
					<thead class=\"resizable sticky\">
					<tr>
						<th style=\"width: 10%;\">" . _USERNAME . "</th>
						<th style=\"width: 35%;\">" . _LASTNAME . "</th>
						<th style=\"width: 35%;\">" . _FIRSTNAME . "</th>
						<th style=\"width: 20%;\">" . _EDIT . "</th>
					</tr>
					</thead><tbody class=\"resizable\">";

echo "<tr id=\"add_row\">
					<td><input type=\"text\" class=\"form-control\" name=\"username\" id=\"add_username\" required=\"required\" value=\"\" maxlength=\"" . MAXLENGTH . "\">
					<span id=\"username_missing\" class=\"invalid-feedback\">" . _USERNAME . " " . _MISSING . "</span></td>
					<td><input type=\"text\" class=\"form-control\" name=\"lastname\" id=\"add_lastname\" required=\"required\" value=\"\" maxlength=\"" . MAXLENGTH . "\">
					<span id=\"lastname_missing\" class=\"invalid-feedback\">" . _LASTNAME . " " . _MISSING . "</span></td>
					<td><input type=\"text\" class=\"form-control\" name=\"firstname\" id=\"add_firstname\" required=\"required\" value=\"\" maxlength=\"" . MAXLENGTH . "\">
					<span id=\"firstname_missing\" class=\"invalid-feedback\">" . _FIRSTNAME . " " . _MISSING . "</span></td>
					<td><i id=\"people_submit\" class=\"icon fa fa-plus fa-fw add_icon\" title=\"" . _ADD . "\" aria-label=\"" . _ADD . "\" name=\"add_people\"></i></td>
					</tr>";

for ($i = 0; $i < $num1; $i++) {

	$row1 = pg_fetch_array($res1);
	$id = $row1['id'];
	$username = $row1['username'];
	$lastname = $row1['lastname'];
	$firstname = $row1['firstname'];

	echo "<tr id=\"row_" . $id . "\">
						<td id=\"username_" . $id . "\">" . $username . "</td>
						<td id=\"lastname_" . $id . "\">" . $lastname . "</td>
						<td id=\"firstname_" . $id . "\">" . $firstname . "</td>
						<td class=\"edit_row\"><i class=\"icon fa fa-edit fa-fw edit_icon\" title=\"" . _EDIT . "\" aria-label=\"" . _EDIT . "\" name=\"edit_row_" . $id . "\"></i><i class=\"icon fa fa-trash fa-fw delete_people_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" name=\"" . $id . "\"></i></td>
						</tr>";
	echo "<tr id=\"edit_row_" . $id . "\" class=\"edit_rows hidden\">
						<td><input type=\"text\" class=\"form-control\" name=\"username_" . $id . "\" id=\"edit_username_" . $id . "\" required=\"required\" value=\"" . $username . "\" maxlength=\"" . MAXLENGTH . "\"></td>
						<td><input type=\"text\" class=\"form-control\" name=\"lastname_" . $id . "\" id=\"edit_lastname_" . $id . "\" required=\"required\" value=\"" . $lastname . "\" maxlength=\"" . MAXLENGTH . "\"></td>
						<td><input type=\"text\" class=\"form-control\" name=\"firstname_" . $id . "\" id=\"edit_firstname_" . $id . "\" required=\"required\" value=\"" . $firstname . "\" maxlength=\"" . MAXLENGTH . "\"></td>
						<td>";
	echo "<i class=\"icon fa fa-save fa-fw save_people_icon\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"" . $id . "\"></i>";
	echo "<i class=\"icon fa fa-times fa-fw cancel_icon\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"" . $id . "\"></i>
						</td>
						</tr>";
}
echo "</tbody></table>";
