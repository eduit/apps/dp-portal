<?php

include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

if ($_SESSION['loggedin']) {
    // get preferences
    $qry = $SELECT_prefs;
    $res = pg_query($con, $qry);
    $num = pg_num_rows($res);
    for ($i = 0; $i < $num; $i++) {
        $row = pg_fetch_array($res);
        if ($row['pref_name'] == "expired_color") {
            $expired_color = $row['pref_value'];
        }
        if ($row['pref_name'] == "expired_time") {
            $expired_time = $row['pref_value'];
        }
        if ($row['pref_name'] == "number_of_vdi_backups") {
            $number_of_entries = $row['pref_value'];
        }
    }

    // set timezone
    date_default_timezone_set('Europe/Zurich');

    $json_file = $_SERVER['DOCUMENT_ROOT'] . "/" . $json_dir . "vdi-file-backups_data.json";
    if (file_exists($json_file)) {
        $strJsonFileContents = file_get_contents($json_file);
        if (strpos($strJsonFileContents, 'no exam today') !== false) {
            echo "<h2>" . _NO_VDI_EXAM . "</h2>
					<p>&nbsp;</p>
					<i class=\"fa fa-paper-plane-o fa-fw\" style=\"font-size: 200px; margin-left: 80px;\" title=\"\" aria-label=\"false\" name=\"\"></i>";
        } else {
            if (isset($_GET['unallocated'])) {
                $unallocated = $_GET['unallocated'];
            } else {
                $unallocated = 'false';
            }
            if ($unallocated != 'true' && $unallocated != 'false') {
                $unallocated = 'false';
            }

            $noContent = false;
            $content = '';
            $count = 0;

            $array_from_json = json_decode($strJsonFileContents, true);

            foreach ($array_from_json as $key => $item) {
                $count++;
                if ($count > $number_of_entries) {
                    break;
                }

                $user = $item['user'];
                $vdi = $item['vdi'];
                $roomName = '';

                $qry = $SELECT_exam_students_by_VDI_user . "'$user' ORDER BY tbl_exam.exam_date DESC LIMIT 1";
                $res = pg_query($con, $qry);
                $row = pg_fetch_assoc($res);
                $exam_name = $row['exam_name'] ?? '';
                $lecturer_lastname = $row['lecturer_lastname'] ?? '';
                $lecturer_firstname = $row['lecturer_firstname'] ?? '';
                $lecturer = $lecturer_lastname . " " . $lecturer_firstname;
                $student_lastname = $row['lastname'] ?? '';
                $student_firstname = $row['firstname'] ?? '';

                switch ($tab) {
                    case "userview":
                        $fileTitle = _FILES;
                        if (!empty($exam_name) || $unallocated == 'true') {
                            foreach ($item['files'] as $files) {
                                $lastwritetime = $files['lastwritetime'];
                                $lastwritetime_formatted = date_create($lastwritetime);
                                $lastwritetime_formatted = date_format($lastwritetime_formatted, 'H:i:s d.m.Y');
                                $timenow = date("H:i:s d.m.Y");
                                $timenow_timestamp = strtotime($timenow);
                                $datenow = date("d.m.Y");
                                $datenow_timestamp = strtotime($datenow);
                                $filecount = count($item['files']);
                                $time_array[] = $lastwritetime_formatted;
                            }
                            if (!empty($time_array)) {
                                $newest_savetime_timestamp = max(array_map('strtotime', $time_array));
                                $newest_savetime = date("H:i:s", $newest_savetime_timestamp);
                                $newest_savedate = date("d.m.Y", $newest_savetime_timestamp);
                                $timedelta = $timenow_timestamp - $newest_savetime_timestamp;
                                $array_null = false;
                            } else {
                                $array_null = true;
                                $filecount = '0';
                                $newest_savedate = '-';
                                $newest_savetime = '-';
                            }
                            $time_array = [];
                            if ($array_null) {
                                $content = $content . "<tr style = 'color:$expired_color'>";
                            } else {
                                if ($newest_savetime_timestamp < $datenow_timestamp) {
                                    $content = $content . "<tr style = 'color:$expired_color'>";
                                } else {
                                    if ($timedelta > $expired_time) {
                                        $content = $content . "<tr style = 'color:$expired_color'>";
                                    } else {
                                        $content = $content . "<tr>";
                                    }
                                }
                            }
                            $content = $content . "<td>" . $user . "</td>";
                            $content = $content . "<td>" . $vdi . "</td>";
                            $content = $content . "<td>" . getRoom($vdi) . "</td>";
                            $content = $content . "<td><span title=\"$lecturer\">" . $exam_name . "</span></td>";
                            $content = $content . "<td>" . $student_lastname . "</td>";
                            $content = $content . "<td>" . $student_firstname . "</td>";
                            $content = $content . "<td>" . $filecount . "</td>";
                            $content = $content . "<td>" . $newest_savedate . "</td>";
                            $content = $content . "<td>" . $newest_savetime . "</td>";
                            $content = $content . "</tr>";
                        }
                        break;

                    case "fileview":
                        $fileTitle = _FILENAME;
                        if (!empty($exam_name) || $unallocated == 'true') {
                            foreach ($item['files'] as $files) {
                                $count++;
                                if ($count > $number_of_entries) {
                                    break;
                                }
                                $filename = $files['filename'];
                                $lastwritetime = $files['lastwritetime'];
                                $roomName = '';
                                $lastwritetime_date = date_create($lastwritetime);
                                $lastwritetime_date = date_format($lastwritetime_date, 'd.m.Y');
                                $lastwritetime_date_timestamp = strtotime($lastwritetime_date);
                                $lastwritetime_time = date_create($lastwritetime);
                                $lastwritetime_time = date_format($lastwritetime_time, 'H:i:s');
                                $lastwritetime_formatted = date_create($lastwritetime);
                                $lastwritetime_formatted = date_format($lastwritetime_formatted, 'H:i:s d.m.Y');
                                $timenow = date("H:i:s d.m.Y");
                                $timenow_timestamp = strtotime($timenow);
                                $datenow = date("d.m.Y");
                                $datenow_timestamp = strtotime($datenow);
                                $lastwritetime_timestamp = strtotime($lastwritetime_formatted);
                                $timedelta = $timenow_timestamp - $lastwritetime_timestamp;
                                if (
                                    $lastwritetime_date_timestamp < $datenow_timestamp
                                ) {
                                    $content = $content . "<tr style = 'color:$expired_color'>";
                                } else {
                                    if ($timedelta > $expired_time) {
                                        $content = $content . "<tr style = 'color:$expired_color'>";
                                    } else {
                                        $content = $content . "<tr>";
                                    }
                                }
                                $content = $content . "<td>" . $user . "</td>";
                                $content = $content . "<td>" . $vdi . "</td>";
                                $content = $content . "<td>" . getRoom($vdi) . "</td>";
                                $content = $content . "<td><span title=\"$lecturer\">" . $exam_name . "</span></td>";
                                $content = $content . "<td>" . $student_lastname . "</td>";
                                $content = $content . "<td>" . $student_firstname . "</td>";
                                $content = $content . "<td>" . $filename . "</td>";
                                $content = $content . "<td>" . $lastwritetime_date . "</td>";
                                $content = $content . "<td>" . $lastwritetime_time . "</td>";
                                $content = $content . "</tr>";
                            }
                        }
                        break;

                    default:
                        $noContent = true;
                        break;
                }
                if ($noContent) {
                    break;
                }
            }
            pg_close($con);
            
            if (!$noContent) {
                // show unallocated
                echo "<p><label><input type=\"checkbox\" name=\"\" value=\"0\" id=\"show_empty\" ";
                if ($unallocated == 'true') {
                    echo "onclick=\"location.href='" . paramChanger("unallocated", "false") . "';\" checked";
                } else {
                    echo "onclick=\"location.href='" . paramChanger("unallocated", "true") . "';\"";
                }
                echo "> " . _SHOW_UNALLOCATED . "</label></p>";

                // counter
                echo "<p><span id=\"count\">$count</span> " . _COUNT . "</p>";

                $headers = array(_USER, _VDI, _ROOM, _EXAM, _LASTNAME, _FIRSTNAME, $fileTitle, _DATE, _SAVETIME);

                echo "<table id=\"all_files\" class=\"admintable generaltable\" style=\"width: 100%;\">";
                // header
                echo "<thead class=\"resizable sticky\"><tr>";
                $headerNum = count($headers);
                $width = 100 / $headerNum;

                foreach ($headers as $header) {
                    if ($header == _SAVETIME) {
                        echo "<th style=\"width: " . $width . "%;\" id=\"" . strtolower($header) . "\" class=\"no-values\">" . $header . "<br/></th>";
                    } else {
                        echo "<th style=\"width: " . $width . "%;\">" . $header . "<br/></th>";
                    }
                }
                echo "</tr></thead>";
                // content
                echo "<tbody class=\"resizable\">";
                echo $content;
                echo "</tbody>";
                echo "</table>";

                // filter script
                echo "<script>
                    $(function(){
                        $(\"#all_files\").excelTableFilter();
                        var fullCount = $(\"#all_files tbody tr\").length;
                        let rowCount = $(\"#all_files tbody tr:visible\").length;
                        $(\"#count\").html(rowCount+\"/\"+fullCount);
                        
                        if(/chrom(e|ium)/.test(navigator.userAgent.toLowerCase())){
                            $(\".dropdown-filter-content\").parent().parent(\"th\").addClass(\"drp_menu\");
                        }
                    });
                </script>";
            } else {
                echo "<h2>" . _WRONG_PARAMETER . "</h2>
                    <p>&nbsp;</p>
                    <i class=\"fa fa-exclamation fa-fw\" style=\"font-size: 200px; margin-left: 80px;\" title=\"\" aria-label=\"false\" name=\"\"></i>";
            }
        }
    } else {
        echo "<h2>" . _FILE_NOT_FOUND . "</h2>
				<p>&nbsp;</p>
				<i class=\"fa fa-server fa-fw\" style=\"font-size: 200px; margin-left: 80px;\" title=\"\" aria-label=\"false\" name=\"\"></i>";
    }
}

function getRoom($vdi)
{
    include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
    include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

    $qry = $SELECT_room_by_computer . " AND tbl_computer.computer_name='" . $vdi . "' ORDER BY tbl_computer.updated_at DESC LIMIT 1";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $computerRoomID = $row['room_id'] ?? 0;
    $computerRoomName = $row['room_name'] ?? '';
    $computerRoomDN = $row['room_display_name'] ?? '';
    $left = strpos($vdi, "-") + 1;
    $right = strrpos($vdi, "-");
    $roomName = str_replace("-", " ", substr($vdi, $left, $right - $left));
    $qry = $SELECT_room_by_name . "'$roomName'";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $vdiRoomID = $row['room_id'] ?? 0;
    $vdiRoomName = $row['room_name'] ?? '';
    $vdiRoomDN = $row['room_display_name'] ?? '';

    if ($computerRoomID != $vdiRoomID) {
        if ($vdiRoomDN) {
            $roomName = $vdiRoomDN;
        } else {
            $roomName = $vdiRoomName;
        }
    } else {
        if ($computerRoomDN) {
            $roomName = $computerRoomDN;
        } else {
            $roomName = $computerRoomName;
        }
    }
    return $roomName;
}
