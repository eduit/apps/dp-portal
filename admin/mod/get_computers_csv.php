<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

// Include language file
if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else {
    // default
    $qry = $SELECT_lang_default;
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $lang = $row['lang'];
}
include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

session_start();

if ($_SESSION['loggedin']) {

    $file = $_FILES['file']['tmp_name'];

    // Detect delimiter from first line.
    $handle = fopen($file, "r");
    if ($handle !== false) {
        $delimiter = "";
        $firstLine = fgets($handle);
        if ($firstLine !== false) {
            if (strpos($firstLine, ",") !== false) {
                $delimiter = ",";
            } elseif (strpos($firstLine, ";") !== false) {
                $delimiter = ";";
            } else {
                $delimiter = "";
            }
        }
    }
    $response_array['delimiter'] = $delimiter;

    // compare with data
    $csv = array_map(function ($v) {
        global $delimiter;
        return str_getcsv($v, $delimiter);
    }, file($file));

    $table_header = "<thead><tr>";

    // extract headers
    foreach ($csv[0] as $key => $value) {
        $csv_header[$key] = $value;
        $response_array['header'][$key] = $value;
        $table_header = $table_header . "<th>" . constant(strtoupper("_" . $value)) . "</th>";
    }

    $table_header = $table_header . "</tr></thead>";

    // combine data with headers
    array_walk($csv, function (&$a) use ($csv) {
        $a = array_combine($csv[0], $a);
    });

    // remove column header
    array_shift($csv);

    $len = count($csv);
    $response_array['csv_lines'] = $len;

    $table_body = "<tbody>";

    // process data
    foreach ($csv as $key => $row) {
        $ip_address = $row['ip_address'];
        $computer = $row['computer'];
        $room = $row['room'];
        $sector = $row['sector'];
        $exam_user = $row['exam_user'];
        $error = false;

        // check if computer exists
        $qry = $SELECT_computers . " AND computer_name = '$computer' AND ip_address = '$ip_address'";
        $res = pg_query($con, $qry);
        $num = pg_num_rows($res);
        $row = pg_fetch_assoc($res);

        if ($num == 0) {
            $qry = $SELECT_computers . " AND computer_name = '$computer'";
            $res = pg_query($con, $qry);
            $num = pg_num_rows($res);
            $row = pg_fetch_assoc($res);
        }
        if ($num == 0) {
            $qry = $SELECT_computers . " AND ip_address = '$ip_address'";
            $res = pg_query($con, $qry);
            $num = pg_num_rows($res);
            $row = pg_fetch_assoc($res);
        }

        $computer_id = $row['computer_id'];
        $ip_address_db = $row['ip_address'];
        $computer_db = $row['computer'];
        $room_db = $row['room_name'];
        $sector_db = $row['sector_name'];
        $exam_user_db = $row['exam_user'];

        $error_style = "style=\"color: red;\"";
        $error_text = "";

        // get room_id
        $qry = $SELECT_room_by_name . "'$room'";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $room_id = $row['room_id'] ?? 0;
        if (!$room_id) {
            $error = true;
            $error_text = _ROOM;
            $room_style = $error_style;
        }

        // get room_sector_id
        $qry = $SELECT_sector . "$room_id AND tbl_sector.sector_name = '$sector' LIMIT 1";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $room_sector_id = $row['room_sector_id'] ?? 0;
        if (!$room_sector_id) {
            $error = true;
            if ($error_text) {
                $error_text = $error_text . ", " .  _ROOM . "/" . _SECTOR;
            } else {
                $error_text = _ROOM . "/" . _SECTOR;
            }
            $room_sector_style = $error_style;
        }

        //get exam_user_id
        $qry = $SELECT_exam_user_id . "'" . $exam_user . "'";
        $res = pg_query($con, $qry);
        $row = pg_fetch_assoc($res);
        $exam_user_id = $row['exam_user_id'];
        if ($exam_user_id == null) {
            $exam_user_id = "0";
            $error = true;
            if ($error_text) {
                $error_text = $error_text . ", " . _EXAM_USER;
            } else {
                $error_text = _EXAM_USER;
            }
            $exam_user_style = $error_style;
        }

        $same = true;

        if ($ip_address != $ip_address_db) {
            if ($ip_address_db) {
                $ip_address = $ip_address_db . " &#x2192; <b>" . $ip_address . "</b>";
            }
            $same = false;
        }

        if ($computer != $computer_db) {
            if ($computer_db) {
                $computer = $computer_db . " &#x2192; <b>" . $computer . "</b>";
            }
            $same = false;
        }

        if ($room != $room_db) {
            if ($room_db) {
                $room = $room_db . " &#x2192; <b $room_style>" . $room . "</b>";
            }
            $same = false;
        }

        if ($sector != $sector_db) {
            if ($sector_db) {
                $sector = $sector_db . " &#x2192; <b $room_sector_style>" . $sector . "</b>";
            }
            $same = false;
        }

        if ($exam_user != $exam_user_db) {
            if ($exam_user_db) {
                $exam_user = $exam_user_db . " &#x2192; <b $exam_user_style>" . $exam_user . "</b>";
            }
            $same = false;
        }

        if ($num > 0 && $same) {
            $style = "style=\"font-style: italic;\"";
            $title = "title=\"" . _EXISTING_COMPUTER . "\"";
            $class = "class=\"unmodified\"";
        } elseif ($num == 1 && !$same) {
            $style = "style=\"\"";
            $title = "title=\"" . _UPDATE . "\"";
            $class = "class=\"modified\"";
        } elseif ($num == 0) {
            $style = "style=\"color: red;\"";
            $title = "title=\"" . _NEW . "\"";
            $class = "class=\"new\"";
        } else {
            $style = "";
            $title = "";
            $class = "";
        }
        if ($error) {
            $style = "style=\"text-decoration: line-through;\"";
            $title = "title=\"" . _ERROR . ": $error_text\"";
            $class = "class=\"error\"";
        }
        $table_body = $table_body . "<tr $class $style $title><td>$ip_address</td><td>$computer</td><td>$room</td><td>$sector</td><td>$exam_user</td></tr>";

        $room_style = "";
        $room_sector_style = "";
        $exam_user_style = "";
    }

    $table_body = $table_body . "</tbody>";

    $response_array['content'] = $table_header . $table_body;

    pg_close($con);
    echo json_encode($response_array);
}
