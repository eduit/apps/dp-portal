<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/func.php";

session_start();

// Include language file
if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else {
    // default
    $qry = $SELECT_lang_default;
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $lang = $row['lang'];
}
include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

// get environment
$qry = $SELECT_pref . " WHERE pref_name = 'environment'";
$res = pg_query($con, $qry);
$row = pg_fetch_assoc($res);
$env = $row['pref_value'];

if ($_SESSION['loggedin']) {
    $mode = $_POST['mode'];
    $selected = $_POST['parentID'] ?? 0;
    $cluster = $_POST['cluster'];

    // get parents
    $horizonScript = $_SERVER["DOCUMENT_ROOT"] . "/inc/horizon/get_parents.py";
    $json = shell_exec("python3 " . $horizonScript);
    $parents = json_decode($json, true);
    $sortby = 'name';
    $keyValues = array_column($parents, $sortby);
    array_multisort($keyValues, SORT_NATURAL, $parents);

    $emptyRow = "<option name=\"" . _NO_PARENT . "\" value=\"0\" selected>" . _NO_PARENT . "</option>\n";

    if ($mode == 'pending') {
        $content = $emptyRow;
    } else {
        $content = '';
    }

    foreach ($parents as $parent) {
        $parentID = $parent['id'];
        $parentName = $parent['name'];
        $parentCluster = explode("-", $parentName)[1];

        if (strtolower($parentCluster) == strtolower($cluster) || !$cluster) {
            $content = $content . "<option value=\"" . $parentID . "\" title=\"" . $parentName . "\"";
            if ($parentID == $selected) {
                $content = $content . " selected";
            }
            $content = $content . ">" . $parentName . "</option>\n";
        }
    }

    // if still empty
    if (!$content){
        $content = $emptyRow;
    }

    $response_array["content"] = $content;
    echo json_encode($response_array);
}
