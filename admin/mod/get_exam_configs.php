<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/func.php";

session_start();

// Include language file
if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else {
    // default
    $qry = $SELECT_lang_default;
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $lang = $row['lang'];
}
include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

if ($_SESSION['loggedin'] || $_SESSION['user_vho']) {
    $selected = $_POST['selected'] ?? 0;
    $examID = $_POST['examID'] ?? 0;

    $qry = $SELECT_exam_name_by_exam . $examID;
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $examNameID = $row['exam_name_id'] ?? 0;

    $qry = $SELECT_exam_config . " AND tbl_exam_config.exam_name_id = $examNameID ORDER BY tbl_exam_name.exam_name, tbl_exam_config.updated_at DESC";
    $res = pg_query($con, $qry);
    $num = pg_num_rows($res);

    if ($selected == 0) {
        $selected = "selected";
    } else {
        $selected = "";
    }

    $configs = "<option name=\"" . _NO_CONFIG . "\" value=\"0\" disabled $selected>" . _NO_CONFIG . "</option>\n";

    for ($i = 0; $i < $num; $i++) {
        $row = pg_fetch_array($res);
        $id = $row['id'];
        $examName = $row['exam_name'];
        $semester = $row['semester'];
        $date = date("Y-m-d");
        $semesterNow = getSemester($date);
        
        if ($semester == $semesterNow) {
            if ($selected == $id) {
                $selected = "selected";
            } else {
                $selected = "";
            }
            $configs = $configs . "<option value=\"$id\" $selected>$examName ($semester)</option>\n";
        }
    }

    $response_array["content"] = $configs;

    echo json_encode($response_array);
}
