<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$username = htmlspecialchars($_POST["username"]);
	$lastname = htmlspecialchars($_POST["lastname"]);
	$firstname = htmlspecialchars($_POST["firstname"]);

	$qry = "SELECT people_id FROM tbl_people WHERE people_username='$1' AND people_lastname='$2' AND people_firstname='$3'";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$num = pg_num_rows($res);
	if ($num > 0) {
		$people_id = $row['people_id'];
	} else {
		$people_id = '';
	}

	if (!$people_id) {
		$qry = $INSERT_people . " VALUES ('$username','$lastname','$firstname');";
		if (pg_query($con, $qry)) {
			$response_array['status'] = 'success';
		} else {
			$response_array['status'] = 'error';
		}
	} else {
		$response_array['status'] = 'already existing';
	}

	pg_close($con);
	echo json_encode($response_array);
}
