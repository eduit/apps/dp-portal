<?php

$qry = $SELECT_all_exams;
$res1 = pg_query($con, $qry);
$num1 = pg_num_rows($res1);

echo "<p>" . $num1 . " " . _COUNT . "</p>";

echo "<table id=\"portfolio\" class=\"admintable generaltable\" style=\"width: 100%;\">
					<thead class=\"resizable sticky\">
					<tr>
						<th style=\"width: 15%;\">" . _EXAM_NR . "</th>
						<th style=\"width: 50%;\">" . _EXAM . "</th>
						<th style=\"width: 35%;\">" . _EDIT . "</th>
					</tr>
					</thead><tbody class=\"resizable\">";

echo "<tr id=\"add_row\">
					<td><input placeholder=\"000-0000-00S\" type=\"text\" class=\"form-control\" name=\"exam_nr\" id=\"add_exam_nr\" required=\"required\" value=\"\" maxlength=\"" . MAXLENGTH . "\">
					<span id=\"exam_nr_missing\" class=\"invalid-feedback\">" . _EXAM_NR . " " . _MISSING . "</span></td>
					<td><input type=\"text\" class=\"form-control\" name=\"exam_name\" id=\"add_exam_name\" required=\"required\" value=\"\" maxlength=\"" . 2*MAXLENGTH . "\">
					<span id=\"exam_name_missing\" class=\"invalid-feedback\">" . _EXAM . " " . _MISSING . "</span></td>
					<td><i id=\"portfolio_submit\" class=\"icon fa fa-plus fa-fw add_icon\" title=\"" . _ADD . "\" aria-label=\"" . _ADD . "\" name=\"add_portfolio\"></i></td>
					</tr>";

for ($i = 0; $i < $num1; $i++) {
    $row1 = pg_fetch_array($res1);
    $id = $row1['id'];
    $exam_nr = $row1['exam_nr'];
    $exam_name = $row1['exam_name'];

    $qry = $SELECT_exams . " WHERE exam_name_id = '" . $id . "' AND archive = true";
    $res2 = pg_query($con, $qry);
    $row2 = pg_fetch_assoc($res2);
    $archive = $row2['archive'] ?? '';

    echo "<tr id=\"row_" . $id . "\">
						<td id=\"exam_nr_" . $id . "\">" . $exam_nr . "</td>
						<td id=\"exam_name_" . $id . "\">" . $exam_name . "</td>
						<td>";
    if (empty($archive)) {
        echo "<i class=\"icon fa fa-edit fa-fw edit_icon\" title=\"" . _EDIT . "\" aria-label=\"" . _EDIT . "\" name=\"edit_row_" . $id . "\"></i>";
        echo "<i class=\"icon fa fa-trash fa-fw delete_portfolio_icon\" title=\"" . _DELETE . "\" aria-label=\"" . _DELETE . "\" name=\"" . $id . "\"></i>";
    } else {
        echo "<i class=\"fa fa-lock fa-fw ban_icon\" title=\"" . _ARCHIVED . "\" aria-label=\"" . _ARCHIVED . "\" name=\"archive_row_" . $id . "\"></i>";
    }
    echo "</td></tr>";
    echo "<tr id=\"edit_row_" . $id . "\" class=\"edit_rows hidden\">
						<td><input type=\"text\" class=\"form-control\" name=\"exam_nr_" . $id . "\" id=\"edit_exam_nr_" . $id . "\" required=\"required\" value=\"" . $exam_nr . "\" maxlength=\"" . MAXLENGTH . "\"></td>
						<td><input type=\"text\" class=\"form-control\" name=\"exam_name_" . $id . "\" id=\"edit_exam_name_" . $id . "\" required=\"required\" value=\"" . $exam_name . "\" maxlength=\"" . 2*MAXLENGTH . "\"></td>
						<td>";
					echo "<i class=\"icon fa fa-save fa-fw save_portfolio_icon\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"" . $id . "\"></i>";
					echo "<i class=\"icon fa fa-times fa-fw cancel_icon\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"" . $id . "\"></i>
						</td>
						</tr>";
}

echo "</tbody></table>";
