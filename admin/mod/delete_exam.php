<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$exam_id = $_POST["exam_id"];
	
	// check if enrollments
	$qry = $SELECT_enrollments_by_exam . $exam_id;
	$res = pg_query($con, $qry);
	$num = pg_num_rows($res);
	$response_array['status'] = 'success';

	if (!$num) {
		// delete people
		$qry = $DELETE_exam_people . " WHERE exam_id = '$exam_id'";
		if (pg_send_query($con, $qry)) {
			$res = pg_get_result($con);
			if ($res) {
				$state = pg_result_error_field($res, PGSQL_DIAG_SQLSTATE);
				if ($state == 0) {
					$response_array['status1'] = 'success';
				} else {
					$response_array['status1'] = 'error';
					$response_array['status'] = 'error';
				}
			}
		}
		// delete exam
		$qry = $DELETE_exam . " WHERE exam_id = '$exam_id'";
		if (pg_send_query($con, $qry)) {
			$res = pg_get_result($con);
			if ($res) {
				$state = pg_result_error_field($res, PGSQL_DIAG_SQLSTATE);
				if ($state == 0) {
					$response_array['status2'] = 'success';
				} else {
					$response_array['status2'] = 'error';
					$response_array['status'] = 'error';
				}
			}
		}
	} else {
		$response_array['status'] = 'enrollments_existing';
	}

	pg_close($con);
	echo json_encode($response_array);
}
