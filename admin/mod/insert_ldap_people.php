<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $mode = htmlspecialchars($_POST["mode"]);
    $group = htmlspecialchars($_POST["group"]);

    if ($connect = ldap_connect($ldap_address, $ldap_port)) {
        // connected
        ldap_set_option($connect, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($connect, LDAP_OPT_REFERRALS, 0);

        if (ldap_bind($connect, $ldap_dn, $ldap_password)) {

            // get group info
            $filter = "(&(objectclass=posixgroup)(cn=$group))";
            $sr = ldap_search($connect, $ldap_base_dn, $filter);
            $info = ldap_get_entries($connect, $sr);
            $members = $info[0]["memberuid"] ?? 0;
            $num = $members["count"] ?? 0;
            $obj = $info[0] ?? [];
            $username = "";

            $response_array['status'] = 'success';

            if ($num) {
                array_shift($members);
                asort($members);
            }

            if ($obj && $num) {
                $values = "";
                $i = 0;
                foreach ($members as $key => $member) {
                    // get user info
                    $filter = "(|(cn=$member))";
                    $justthese = array("ou", "sn", "givenname", "mail");
                    $sr = ldap_search($connect, $ldap_base_dn, $filter, $justthese);
                    $userInfo = ldap_get_entries($connect, $sr);
                    $username = $response_array[$key]['username'] = $member;
                    $lastname = $response_array[$key]['lastname'] = htmlspecialchars($userInfo[0]['sn'][0]) ?? '';
                    $firstname = $response_array[$key]['firstname'] = htmlspecialchars($userInfo[0]['givenname'][0]) ?? '';

                    $qry = $SELECT_people . " WHERE (people_username = '') IS NOT FALSE AND people_lastname='$lastname' AND people_firstname='$firstname'";
                    $res = pg_query($con, $qry);
                    $row = pg_fetch_assoc($res);
                    $num = pg_num_rows($res);
                    if ($num > 0) {
                        $people_id = $row['id'];
                    } else {
                        $people_id = '';
                    }

                    if ($people_id) {
                        $qry = $UPDATE_people . " SET people_username = '$username' WHERE people_id = '$people_id'";
                    } else {
                        $qry = $SELECT_people_id . " WHERE people_username = '$username' AND people_lastname='$lastname' AND people_firstname='$firstname'";
                        $res = pg_query($con, $qry);
                        $row = pg_fetch_assoc($res);
                        $num = pg_num_rows($res);
                        if ($num == 0) {
                            $qry = $INSERT_people . " VALUES ('$username', '$lastname', '$firstname')";
                            $i++;
                        }
                    }

                    if ($mode == "write") {
                        if (!$qry) {
                            $response_array[$key]['status'] = 'nothing to do';
                        } else {
                            $response_array[$key]['qry'] = $qry;
                            if (pg_query($con, $qry)) {
                                $response_array[$key]['status'] = 'success';
                            } else {
                                $response_array[$key]['status'] = 'error';
                                $response_array['status'] = 'error';
                            }
                        }
                    }
                }
                if ($mode == "list") {
                    $response_array['number'] = $i;
                }
            }
        }
    }

    ldap_close($connect);
    pg_close($con);
    echo json_encode($response_array);
}
