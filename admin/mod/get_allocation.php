<?php

if ($_SESSION['loggedin']) {
    getAllocationData($tab, 0);
}

function getAllocationData($tab, $selectedRoom)
{
    include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
    include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

    switch ($tab) {
        case "activation":
            $qry = $SELECT_rooms . " ORDER BY room_name";
            $res1 = pg_query($con, $qry);
            $num1 = pg_num_rows($res1);
            echo "<div id=\"div_notification\" class=\"hidden\">
        <p id=\"notification_text\"></p>
    </div>";
            echo "<div>";

            for ($i = 0; $i < $num1; $i++) {
                $row1 = pg_fetch_array($res1);
                $room = $row1['room_id'];
                if ($row1['room_display_name']) {
                    $roomName = $row1['room_display_name'];
                } else {
                    $roomName = $row1['room_name'];
                }
                echo "<h3>$roomName</h3>";
                getAllocationData("active_exam_select_2", $room);
                echo "<hr>";
            }
            echo "<div>";
            break;

        case "participants";
            echo "<div class=\"form-group fitem row\">
    <div class=\"col-md-2\">
        <label class=\"col-form-label\" for=\"id_exam\">" . _EXAM . ": </label>
    </div>
    <div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
        <span data-fieldtype=\"text\">
            <select class=\"select custom-select form-control\" name=\"exam\" id=\"id_exam\" required=\"required\" style=\"width: 400px;\">";
            getAllocationData("active_exam_select_1", 0);
            echo " </select>
        </span>
        <div class=\"form-control-feedback invalid-feedback\" id=\"id_missing_exam\">" . _EXAM . " " . _MISSING . "</div>
    </div>
</div>";
            echo "<div class=\"form-group fitem row\">
    <div class=\"col-md-2\">
        <label id=\"label_student_list\" class=\"col-form-label\" for=\"id_list\">" . _STUDENT_LIST . ": </label>
    </div>
    <div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
        <span data-fieldtype=\"text\">";
            echo "<i id=\"template_list\" class=\"icon fa fa-list-alt fa-fw\" title=\"" . _TEMPLATE . "\" aria-label=\"" . _TEMPLATE . "\"></i>";
            echo "<i id=\"delete_list\" class=\"icon fa fa-trash fa-fw hidden\" title=\"" . _DELETE_LIST . "\" aria-label=\"" . _DELETE . "\"></i>";
            echo "<i id=\"remove_computers\" class=\"icon fa fa-desktop fa-fw hidden\" title=\"" . _REMOVE_COMPUTER . "\" aria-label=\"" . _REMOVE_COMPUTER . "\"></i>
            <input type=\"file\" accept=\".csv\" id=\"fileloader_students\" class=\"btn hidden\" name=\"fileToUpload\" title=\"" . _STUDENT_LIST . "\" />
        </span>
        <div class=\"form-control-feedback invalid-feedback\" id=\"id_missing_list\"></div>
    </div>
</div>";

            echo "<div class=\"form-group fitem row\">
    <div class=\"col-md-2 csv_buttons hidden\">
        <i id=\"student_save_button\" class=\"icon fa fa-save fa-fw\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\"></i>
        <i id=\"cancel_upload\" class=\"icon fa fa-times fa-fw\" title=\"" . _CANCEL . "\" aria-label=\"" . _CANCEL . "\" name=\"\"></i>
    </div>
    <div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
    </div>
</div>";

            $header = "<th style=\"width: 15%;\">" . _LASTNAME . "<br /></th>
<th style=\"width: 15%;\">" . _FIRSTNAME . "<br /></th>
<th style=\"width: 8%;\">" . _REGISTRATION . "<br /></th>
<th style=\"width: 8%;\">" . _ROOM . "<br /></th>
<th style=\"width: 5%;\">" . _KEYBOARD . "<br /></th>
<th style=\"width: 5%;\">" . _GROUP . "<br /></th>
<th style=\"width: 12%;\">" . _REMARK . "<br /></th>
<th style=\"width: 15%;\">" . _URL . "<br /></th>
<th style=\"width: 10%;\">" . _COMPUTER . "<br /></th>
<th style=\"width: 7%;\">" . _ENABLED . "<br /></th>
<th style=\"width: 7%;\">" . _EDIT . "<br /></th>";

            echo "<div id=\"div_student_list\" class=\"hidden\">
    <h3>" . _STUDENT_LIST . "</h3>";
            echo "<p id=\"id_filename\"></p>";
            echo "<p><span id=\"count\"></span> " . _COUNT . "</p>";
            echo "<table id=\"student_list\" class=\"admintable generaltable\" style=\"width: 100%;\">
        <thead class=\"resizable sticky\">$header</thead>
        <tbody class=\"resizable\"></tbody>
    </table>
</div>";
            echo "<div id=\"div_nothing\" class=\"hidden\">
    <h3>" . _NO_ALLOCATION_TITLE . "</h3>
</div>";
            echo "<div id=\"div_notification\" class=\"hidden\">
    <p id=\"notification_text\"></p>
</div>";
            break;

        case "students-csv";
            $dirname = $_SERVER["DOCUMENT_ROOT"] . $csv_dir;

            echo "<option title=\"" . _NO_LIST . "\" value=\"0\" disabled>" . _NO_LIST . "</option>\n";

            $files = array_diff(scandir($dirname), array('.', '..'));

            foreach ($files as &$file) {
                if ($file != "example.csv") {
                    echo "<option value=\"$file\">" . $file . "</option>";
                }
            }
            unset($file);
            break;

        case "active_exam_select_1":
            $qry = $SELECT_dates_light . " AND
                    tbl_exam.disabled = false AND
                    tbl_exam.archive = false
                    ORDER BY tbl_exam.exam_date, tbl_exam_name.exam_name";
            $res1 = pg_query($con, $qry);
            $num1 = pg_num_rows($res1);

            echo "<option name=\"" . _NO_EXAM . "\" value=\"0\" disabled selected>" . _NO_EXAM . "</option>\n";

            for ($i = 0; $i < $num1; $i++) {
                $row1 = pg_fetch_array($res1);
                $id = $row1['id'];
                $examName = $row1['exam_name'];
                $lastname = $row1['lecturer_lastname'];
                $date = date_create($row1['date']);
                echo "<option value=\"" . $id . "\"";
                echo ">" . $examName . " | " . $lastname . " | " . date_format($date, 'Y-m-d') . "</option>\n";
            }
            break;

        case "active_exam_select_2":
            $room = $selectedRoom;
            $qry = $SELECT_exam_by_room . " AND tbl_exam_student.room_id = $room AND tbl_exam.archive = false AND tbl_exam.disabled = false GROUP BY
                        tbl_exam.exam_id,
                        tbl_exam_name.exam_name_id,
                        tbl_exam_name.exam_nr,
                        tbl_exam_name.exam_name,
                        tbl_exam.exam_date,
                        tbl_people.people_id,
                        tbl_people.people_lastname,
                        tbl_people.people_firstname,
                        tbl_exam_student.room_id
                        ORDER BY tbl_exam_name.exam_name";
            $res1 = pg_query($con, $qry);
            $num1 = pg_num_rows($res1);
            $status = false;

            $header = "<th style=\"width: 50%;\">" . _EXAM . "<br /></th>
                        <th style=\"width: 25%;\">" . _STUDENTS . "<br /></th>
                        <th style=\"width: 25%;\">" . _STATUS . "<br /></th>";

            if ($num1 > 0) {
                echo "<table class=\"admintable generaltable\" style=\"width: 100%;\">
                    <thead>
                        <tr>
                            $header
                        </tr>
                    </thead>
                    <tbody>";

                for ($i = 0; $i < $num1; $i++) {
                    $row1 = pg_fetch_array($res1);
                    $active = $row1['active'];
                    if ($active == "t") {
                        $status = true;
                    } else {
                        $status = false;
                    }
                    $examID = $row1['exam_id'];
                    $date = date_create($row1['date']);
                    $students = $row1['students'];
                    echo "<tr id=\" row_" . $examID . "\">
                <td id=\"exam_" . $examID . "\">" . $row1['exam_name'] . " | " . $row1['lastname'] . " | " . date_format($date, 'Y-m-d') . "</td>
                <td id=\"students_" . $examID . "\">" . $students . "</td>
                <td>";
                    if ($status) {
                        echo "<i class=\"icon fa fa-pause fa-fw status_icon\" title=\"" . _PAUSE . "\" aria-label=\"" . _PAUSE . "\" name=\"button_" . $room . "_$examID\" data-href=\"$examID,$room,f\"></i><i class=\"icon fa fa-play fa-fw status_icon hidden\" title=\"" . _PLAY . "\" aria-label=\"" . _PLAY . "\" name=\"button_" . $room . "_$examID\" data-href=\"$examID,$room,t\"></i>";
                    } else {
                        echo "<i class=\"icon fa fa-pause fa-fw status_icon hidden\" title=\"" . _PAUSE . "\" aria-label=\"" . _PAUSE . "\" name=\"button_" . $room . "_$examID\" data-href=\"$examID,$room,f\"></i><i class=\"icon fa fa-play fa-fw status_icon\" title=\"" . _PLAY . "\" aria-label=\"" . _PLAY . "\" name=\"button_" . $room . "_$examID\" data-href=\"$examID,$room,t\"></i>";
                    }

                    echo "</td>
                </tr>";
                }

                echo "</tbody>
                    </table>";
            } else {
                echo _NO_EXAM_ACTIVE;
            }
            break;
        default:
            echo "<h2>" . _WRONG_PARAMETER . "</h2>
				<p>&nbsp;</p>
				<i class=\"fa fa-exclamation fa-fw\" style=\"font-size: 200px; margin-left: 80px;\" title=\"\" aria-label=\"false\" name=\"\"></i>";
            break;
    }
}
