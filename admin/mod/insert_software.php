<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
	$software = htmlspecialchars($_POST["software"]);
	$publisher_id = $_POST["publisher_id"];
	$version = htmlspecialchars($_POST["version"]);
	$remarks = htmlspecialchars($_POST["remarks"]);
	$visible = htmlspecialchars($_POST["visible"]);

	$qry = "SELECT sw_id FROM tbl_vdi_software WHERE sw_name='$software' AND publisher_id=$publisher_id AND sw_version='$version'";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$num = pg_num_rows($res);
	if ($num > 0) {
		$sw_id = $row['sw_id'];
	} else {
		$sw_id = '';
	}

	if (!$sw_id) {
		$qry = $INSERT_software . " VALUES ('$software', $publisher_id, '$version', '$remarks', '$visible')";
		if (pg_query($con, $qry)) {
			$response_array['status'] = 'success';
		} else {
			$response_array['status'] = 'error';
		}
	} else {
		$response_array['status'] = 'already existing';
	}

	pg_close($con);
	echo json_encode($response_array);
}
