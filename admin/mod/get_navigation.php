<?php

$active = "";
$fontWeight = "";

echo "<div id=\"nav-drawer\" data-region=\"drawer\" class=\"d-print-none moodle-has-zindex\" aria-hidden=\"false\" tabindex=\"-1\">";
if ($loggedin || $userVho) {
    $qry = $SELECT_navigation_groups;
    $res1 = pg_query($con, $qry);
    $num1 = pg_num_rows($res1);

    for ($i = 0; $i < $num1; $i++) {
        $row1 = pg_fetch_array($res1);
        $navGroup = $row1['nav_group'];

        if ($i == 0 || $userVho) {
            echo "<nav class=\"list-group\">";
        } else {
            echo "<nav class=\"list-group m-t-1\">";
        }

        if ($userVho) {
            $navVHO = "AND nav_vho = 't'";
        } else {
            $navVHO = "";
        }
        
        $qry = $SELECT_navigation . " WHERE nav_group = " . $navGroup . " AND nav_level = 0 " . $navVHO . " AND nav_visible = 't' ORDER BY nav_sort";
        $res2 = pg_query($con, $qry);
        $num2 = pg_num_rows($res2);

        for ($j = 0; $j < $num2; $j++) {
            $row2 = pg_fetch_array($res2);
            $navElement = $row2['nav_element'];
            $nav_icon = $row2['nav_icon'];

            if ($adminMode == $navElement) {
                $active = "active";
                $fontWeight = "font-weight-bold";
            } else {
                $active = "";
                $fontWeight = "";
            }

            echo "<a class=\"list-group-item list-group-item-action $active\" href=\"index.php?admin_mode=$navElement\" data-key=\"$navElement\" data-isexpandable=\"0\" data-indent=\"0\" data-showdivider=\"0\" data-type=\"60\" data-nodetype=\"0\" data-collapse=\"0\" data-forceopen=\"0\" data-isactive=\"0\" data-hidden=\"0\" data-preceedwithhr=\"0\">
								<div class=\"m-l-0\">
									<div class=\"media\">
										<span id=\"nav_$navElement\" class=\"media-left\">
											<i class=\"icon fa fa-$nav_icon fa-fw \" aria-hidden=\"true\"></i>
										</span>
										<span class=\"media-body $fontWeight\">";
            $const = "_" . strtoupper($navElement);
            if (defined($const)) {
                echo constant($const);
            } else {
                echo "_UNDEFINED";
            }
            echo "</span>
									</div>
								</div>
							</a>";
        }
        echo "</nav>";
    }
} else {
    if (empty($adminMode) || $adminMode == "login") {
        $active = "active";
        $fontWeight = "font-weight-bold";
    } else {
        $active = "";
        $fontWeight = "";
    }
    echo
    "<nav class=\"list-group\">
						<a class=\"list-group-item list-group-item-action $active\" href=\"index.php?admin_mode=login\" data-key=\"login\" data-isexpandable=\"0\" data-indent=\"0\" data-showdivider=\"0\" data-type=\"60\" data-nodetype=\"0\" data-collapse=\"0\" data-forceopen=\"0\" data-isactive=\"0\" data-hidden=\"0\" data-preceedwithhr=\"0\">
							<div class=\"m-l-0\">
								<div class=\"media\">
									<span class=\"media-left\">
										<i class=\"icon fa fa-sign-in fa-fw \" aria-hidden=\"true\"></i>
									</span>
									<span class=\"media-body $fontWeight\" id=\"login\">" . _LOGIN . "</span>
								</div>
							</div>
						</a>
					</nav>";
}
if ($adminMode == "about") {
    $active = "active";
    $fontWeight = "font-weight-bold";
} else {
    $active = "";
    $fontWeight = "";
}
echo
"<nav class=\"list-group m-t-1\">
					<a class=\"list-group-item list-group-item-action $active\" href=\"index.php?admin_mode=about\" data-key=\"about\" data-isexpandable=\"0\" data-indent=\"0\" data-showdivider=\"0\" data-type=\"60\" data-nodetype=\"0\" data-collapse=\"0\" data-forceopen=\"0\" data-isactive=\"0\" data-hidden=\"0\" data-preceedwithhr=\"0\">
						<div class=\"m-l-0\">
							<div class=\"media\">
								<span class=\"media-left\">
									<i class=\"icon fa fa-info fa-fw \" aria-hidden=\"true\"></i>
								</span>
								<span class=\"media-body $fontWeight\">" . _ABOUT . "</span>
							</div>
						</div>
					</a>
				</nav>
			</div>";
