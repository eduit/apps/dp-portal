<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

// Include language file
if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
} else {
    // default
    $qry = $SELECT_lang_default;
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $lang = $row['lang'];
}
include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

if ($_SESSION['loggedin']) {
    $id = $_POST['id'];

    // get internal info
    $qry = $SELECT_vdi_info . " WHERE vdi_exam_id = $id LIMIT 1";
    $res = pg_query($con, $qry);
    $row = pg_fetch_assoc($res);
    $info = $row['info'];

    $content =  "<div class=\"form-group fitem row\">
						<div class=\"col-md-2 admin_task\" id=\"vho_mail_template_buttons\">
							<i id=\"save_internal_info\" class=\"icon fa fa-save fa-fw\" title=\"" . _SAVE . "\" aria-label=\"" . _SAVE . "\" name=\"$id\"></i>
						</div>
						<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
						</div>
					</div>";

    $content .= "<div><p><textarea id=\"vdi_internal_info\" class=\"rte internal_info\" name=\"$id\">$info</textarea></p></div>";
    $content .= "</div>";

    $response_array['content'] = $content;

    pg_close($con);
    echo json_encode($response_array);
}
