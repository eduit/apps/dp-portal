<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

session_start();

if ($_SESSION['loggedin']) {
    $textID = $_POST['text_id'];
    $count = $_POST['count'];

    $vho_script = $_SERVER["DOCUMENT_ROOT"] . "/inc/vho_generator/script.py";

    $json = shell_exec("python3 " . $vho_script . " $textID $count");

    // count entries
    $count = count($csv);

    // encryption
    $key = 'mS3c8X0mJPtfPzyalwpkkimEUfVRS29L';
    $ivlen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
    $iv = openssl_random_pseudo_bytes($ivlen);

    $values = '';

    // process data
    foreach ($csv as $i => $csv_row) {
        $username = $csv_row['username'];
        $plaintext = $csv_row['password'];
        $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, $options = OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary = true);
        $password = base64_encode($iv . $hmac . $ciphertext_raw);

        // check if entry already in database
        $qry = $SELECT_VHO_accounts . " WHERE username='$username'";
        $res = pg_query($con, $qry);
        $num = pg_num_rows($res);

        if ($num == 0) {
            $values = $values . "('$username', '$password')";
            $count--;
            if ($count > 0) {
                $values = $values . ",";
            }
        }
    }

    if (substr($values, -1) == ',') {
        $values = rtrim($values, ',');
    }

    if ($values != '') {
        // insert into database
        $qry = $INSERT_VHO_accounts . " VALUES $values";

        if (pg_query($con, $qry)) {
            $response_array['status'] = 'success';
        } else {
            $response_array['status'] = 'error';
        }
    } else {
        $response_array['status'] = 'nothing to do';
    }

    pg_close($con);
    echo json_encode($response_array);
}
