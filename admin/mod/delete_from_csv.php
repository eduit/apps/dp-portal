<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";

session_start();

if ($_SESSION['loggedin']) {
	$csv_line = $_POST['csv_line'];
	$csv_file = $_POST['csv_file'];

	$relative_path = "../../";
	$file = $_SERVER['DOCUMENT_ROOT'] . "/" . $csv_dir . $csv_file;

	if (substr($csv_file, -11) != "_edited.csv") {
		$new_filename = substr($csv_file, 0, -4) . "_edited.csv";
	} else {
		$new_filename = $csv_file;
	}

	$new_file = $_SERVER['DOCUMENT_ROOT'] . "/" . $csv_dir . $new_filename;

	if ($csv_line) {
		$csv =  array_map(function ($v) {
			return str_getcsv($v, ";");
		}, file($file));

		unset($csv[$csv_line]);

		$fp = fopen($new_file, 'w') || throw new UnexpectedValueException('Unable to open file');

		foreach ($csv as $i => $item) {
			fputcsv($fp, $item, ';', chr(127));
		}
		fclose($fp);

		$response_array['new_filename'] = $new_filename;
		$response_array['status'] = 'success';
	} else {
		$response_array['status'] = 'error';
	}

	echo json_encode($response_array);
}
