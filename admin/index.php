<?php

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/func.php";
include $_SERVER["DOCUMENT_ROOT"] . "/admin/mod/get_content.php";

header('X-Content-Type-Options: nosniff');

session_start();

if (empty($_SESSION['csrf_token'])) {
	$_SESSION['csrf_token'] = bin2hex(random_bytes(32));
}
$csrf_token = $_SESSION['csrf_token'];

if (empty($_SESSION['loggedin'])) {
	$loggedin = 0;
} else {
	$loggedin = $_SESSION['loggedin'];
}

if (empty($_SESSION['user_vho'])) {
	$userVho = 0;
} else {
	$userVho =	$_SESSION['user_vho'];
}

// set admin mode
$adminMode = htmlspecialchars($_GET['admin_mode'] ?? '');
preg_replace('/\w/u', '', $adminMode);

if (empty($adminMode)) {
	if ($userVho) {
		$navUserVho = "nav_element='vho_accounts' OR nav_element='logsheet' OR nav_element='room_maps'";
		$where = " WHERE (" . $navUserVho . ") AND";
	} else {
		$where = " WHERE";
	}
	$qry = $SELECT_navigation . $where . " nav_visible = 't' and nav_level = 0 ORDER BY nav_group, nav_sort LIMIT 1";
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$adminMode = $row['nav_element'] ?? '';
}

$check = file_exists($_SERVER["DOCUMENT_ROOT"] . "/admin/mod/get_$adminMode.php");

if (!$loggedin && $adminMode != "about" && !$userVho) {
	$adminMode = "login";
	$check = true;
}

// init variables
$GLOBALS['version'] = 0;
$GLOBALS['updated'] = 0;

// Get preferences
$qry = $SELECT_prefs;
$res = pg_query($con, $qry);
$num = pg_num_rows($res);
for ($i = 0; $i < $num; $i++) {
	$row = pg_fetch_array($res);
	if ($row['pref_name'] == "allocation_refresh") {
		$allocation_refresh = $row['pref_value'];
	}
	if ($row['pref_name'] == "environment") {
		$env = $row['pref_value'];
	}
	if ($row['pref_name'] == "instruction_sheet_header_color") {
		$instruction_sheet_header_color = $row['pref_value'];
	}
	if ($row['pref_name'] == "login_attempts") {
		$login_attempts = $row['pref_value'];
	}
	if ($row['pref_name'] == "login_disabled") {
		$login_disabled = $row['pref_value'];
	}
	if ($row['pref_name'] == "number_of_computers") {
		$number_of_computers = $row['pref_value'];
	}
	if ($row['pref_name'] == "number_of_dates") {
		$number_of_dates = $row['pref_value'];
	}
	if ($row['pref_name'] == "updated") {
		$GLOBALS['updated'] = $row['pref_value'];
	}
	if ($row['pref_name'] == "version") {
		$GLOBALS['version'] = $row['pref_value'];
	}
	if ($row['pref_name'] == "website_header_color") {
		$website_header_color = $row['pref_value'];
	}
	if ($row['pref_name'] == "website_title") {
		$GLOBALS['website_title'] = $row['pref_value'];
		$website_title = $GLOBALS['website_title'];
	}
	if ($row['pref_name'] == "copyright") {
		$copyright = $row['pref_value'];
	}
}

// Set language
$qry = $SELECT_language;
$res = pg_query($con, $qry);
$num = pg_num_rows($res);
$arrLang = array();
for ($i = 0; $i < $num; $i++) {
	$row = pg_fetch_array($res);
	$arrLang[$i]['code'] = $row['lang_code'];
	$arrLang[$i]['language'] = $row['language'];
}

$lang = urlencode($_GET['lang'] ?? '');

if (isset($lang) && inArrayR($lang, $arrLang)) {
	$_SESSION['lang'] = $lang;

	if (isset($_SESSION['lang']) && $_SESSION['lang'] != $lang) {
		echo "<script type='text/javascript'> location.reload(); </script>";
	}
}

if (isset($_SESSION['lang'])) {
	$lang = $_SESSION['lang'];
} else {
	$lang = $arrLang[0]['code'];
}
include $_SERVER["DOCUMENT_ROOT"] . "/lang/lang_" . $lang . ".php";

// Set header color
$headercolor = $website_header_color;

// get IP address
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
	$ip_address = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
	$ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
	$ip_address = $_SERVER['REMOTE_ADDR'];
}

// read and write login attempt to DB
$qry = $SELECT_login_attempt . "'$ip_address'";
$res = pg_query($con, $qry);
$row = pg_fetch_assoc($res);
$num = pg_num_rows($res);

$attempts = $row['attempts'] ?? 0;
$last_login = $row['last_login'] ?? 0;
$login = true;

date_default_timezone_set("Europe/Zurich");
$last_time = strtotime($last_login);
$actual_time = time() - $login_disabled * 60;

if ($attempts == $login_attempts && $last_time > $actual_time) {
	$login = false;
}

$dark = "";
$displayMode = "light";
if (isset($_SESSION['displayMode'])) {
	$displayMode = $_SESSION['displayMode'];
	if ($displayMode == "dark") {
		$dark = "_dark";
	}
}

?>

<!DOCTYPE html>

<?php
echo "<html dir=\"ltr\" lang=\"$lang\" xml:lang=\"$lang\">"
?>

<head>
	<title><?php echo $website_title . " | ";
			if ($env != "PRD") {
				echo $env;
			} ?> Admin</title>
	<link rel="shortcut icon" href="/img/favicon.ico" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8; X-Content-Type-Options=nosniff" />
	<meta name="keywords" content="<?php echo $website_title . " | ";
									if ($env != "PRD") {
										echo $env;
									} ?> Admin" />
	<meta name="author" content="Flavio Steger" />
	<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate">
	<meta http-equiv="Pragma" content="no-cache">
	<meta http-equiv="Expires" content="0">
	<link rel="stylesheet" type="text/css" href="../style/yui-moodlesimple-min.css" />
	<script id="firstthemesheet" type="text/css">
		/** Required in order to fix style inclusion problems in IE with YUI **/
	</script>
	<?php
	echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"../style/boost_ethz_all$dark.css \" />\n";
	echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"../style/ethz$dark.css\" />\n";
	echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"../style/jquery-ui.min$dark.css \" />\n";
	echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"style/jquery-ui.timepicker$dark.css \" />\n";
	echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"style/table-filter-style$dark.css\" />\n";
	echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"style/richtext.min$dark.css \" />\n";
	echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"style/multi-select$dark.css \" />\n";
	echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"style/custom.css\" />\n";
	echo
	"<style>
		h1,
		.sectionname span a,
		#page-site-index h2 {
			color: $headercolor;
		}
		
		#page-header-color,
		#page-header-color-left,
		#page-header-color-right,
		.drawer-open-left #page-header-color-nav,
		#page-site-index #page-header-color-block,
		.path-mod-forum:target~.forumpost:before,
		.ui-state-active {
			background-color: $headercolor;
		}
		
		.caret::before {
				color: $headercolor;
		}
	</style>\n";
	?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="../js/jquery-3.4.1.min.js"></script>
	<script src="../js/jquery-ui.min.js"></script>
	<script src="js/jquery-ui.timepicker.js"></script>
	<?php
	echo "<script src=\"../lang/lang_$lang.js\"></script>\n";
	if ($lang == "de") {
		echo "	<script src=\"js/jquery-ui.datepicker-de.js\"></script>\n";
		echo "	<script src=\"js/jquery-ui.timepicker-de.js\"></script>\n";
	}
	?>
	<script src="js/table-filter-bundle.js"></script>
	<script src="js/jschardet.js"></script>
	<script src="js/jquery.richtext.min.js"></script>
	<script src="js/jquery.multi-select.js"></script>
	<?php
	if (fnmatch("dates*", $adminMode)) {
		echo "<script>var \$limit = $number_of_dates;</script>\n";
	}
	if (fnmatch("rooms_computers*", $adminMode)) {
		echo "<script>var \$limit = $number_of_computers;</script>\n";
	}
	echo "<script src=\"js/general.js\"></script>\n";
	if ($check) {
		echo "	<script src=\"js/$adminMode.js\"></script>\n";
	}
	if (fnmatch("vdi_backups*", $adminMode)) {
		echo "<script>var reload; reload = window.setTimeout(function () { window.location.reload(); }, " . $allocation_refresh . "000);</script>";
	}
	?>
</head>

<body id="page-mod-quiz-view" class="format-topics path-mod path-mod-quiz gecko dir-ltr <?php echo "lang-$lang" ?> yui-skin-sam yui3-skin-sam pagelayout-incourse drawer-ease drawer-open-left">
	<div id="page-header-color" class="hidden-print">
		<div id="page-header-color-left"></div>
		<div id="page-header-color-nav"></div>
		<div id="page-header-color-block"></div>
		<div id="page-header-color-right"></div>
	</div>
	<div id="page-wrapper">
		<header role="banner" id="header" class="fixed-top navbar row">
			<div id="header-container">
				<div id="header-right" class="hidden-print float-right">
					<ul class="userlang navbar-nav d-md-flex">
						<li class="dropdown nav-item">
							<a class="dropdown-toggle nav-link" id="id-drop-down" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#">
								<?php
								if (isset($_SESSION['lang'])) {
									echo $_SESSION['lang'];
								} else {
									echo $lang;
								}
								echo "&nbsp;</a>";
								echo "<div class=\"dropdown-menu dropdown-menu-right userlang-menu\" aria-labelledby=\"id-drop-down\">";
								foreach ($arrLang as $lang) {
									$code = $lang['code'];
									$language = $lang['language'];
									echo "<a class=\"dropdown-item lang-button\" href=\"";
									echo paramChanger("lang", $code);
									echo "\" title=\"$language ($code)\" name=\"$code\">$language</a>";
								}
								?>
				</div>
				</li>
				</ul>
				<?php
				if ($loggedin || $userVho) {
					echo
					"<div class=\"usermenu\">
								<div class=\"action-menu moodle-actionmenu nowrap-items d-inline\" id=\"action-menu-1\" data-enhance=\"moodle-core-actionmenu\">
									<div class=\"menubar d-flex \" id=\"action-menu-1-menubar\" role=\"menubar\">
										<div class=\"action-menu-trigger\">
											<div class=\"dropdown\">
												<a href=\"#\" tabindex=\"0\" class=\" dropdown-toggle icon-no-margin\" id=\"dropdown-1\" aria-label=\"Nutzermenü\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\" aria-controls=\"action-menu-1-menu\">
													<span class=\"userbutton\"><span id=\"user_info\" class=\"usertext mr-1\" title=\"" . $_SESSION['user_firstname'] . " " . $_SESSION['user_lastname'] . "\">" . $_SESSION['username'] . "</span><span class=\"avatars\"><span class=\"avatar current\"><img src=\"../img/f2$dark.png\" class=\"userpicture defaultuserpic\" role=\"presentation\" aria-hidden=\"true\" width=\"35\" height=\"35\"></span></span></span>
													<b class=\"caret\"></b>
												</a>
												<div class=\"dropdown-menu dropdown-menu-right menu align-tr-br\" id=\"action-menu-1-menu\" data-rel=\"menu-content\" aria-labelledby=\"action-menu-toggle-1\" role=\"menu\" data-align=\"tr-br\">
													<a href=\"login/logout.php?username=" . $_SESSION['username'] . "&csrf_token=" . $csrf_token . "\" class=\"dropdown-item menu-action\" role=\"menuitem\" data-title=\"logout,moodle\" aria-labelledby=\"actionmenuaction-6\">
														<i class=\"icon fa fa-sign-out fa-fw \" aria-hidden=\"true\"></i>
														<span class=\"menu-action-text\" id=\"actionmenuaction-6\">
														Logout
														</span>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>";

					if ($displayMode == "dark") {
						$symbol = "sun";
						$name = "light";
					} else {
						$symbol = "moon";
						$name = "dark";
					}
					echo "<div class=\"float-right\">
							<i id=\"toggle_mode\" title=\"$name mode\" class=\"fa fa-$symbol-o clickable\" style=\"margin-right: 10px; font-size: 20px;\" aria-hidden=\"true\" name=\"$name\" data-href=\"$displayMode\"></i>
						</div>";
				}
				?>
			</div>
			<div id="header-left">
				<div id="logo-ethz">
					<a href="<?php
								if ($loggedin || $userVho) {
									echo paramChanger("admin_mode", $adminMode);
								} else {
									echo "index.php";
								} ?>" id="home-link" target="_self">
						<?php
						echo "<img src=\"../img/ethz_logo$dark.svg\" alt=\"Logo ETH Z&uuml;rich\">";
						?>
					</a>
				</div>
				<div id="logo-button" data-region="drawer-toggle">
					<button id="nav-drawer-button" aria-expanded="true" aria-controls="nav-drawer" type="button" class="pull-xs-left m-r-1 btn-menue" data-action="toggle-drawer" data-side="left" data-preference="drawer-open-nav">
						<?php
						echo "<img class=\"clickable\" src=\"../img/nav$dark.png\" aria-hidden=\"true\" alt=\"\" id=\"burger\">";
						?>
					</button>
				</div>
				<div id="logo-text" class="hidden-md-down"><span id="home-link"><?php echo $website_title . " | ";
																				if ($env != "PRD") {
																					echo $env;
																				} ?> Admin</span></div>
			</div>
	</div>
	</header>

	<?php
	include_once $_SERVER["DOCUMENT_ROOT"] . "/admin/mod/get_navigation.php";
	?>

	<div id="page" class="container-fluid">
		<header id="page-header" class="row">
			<div class="col-12 pt-3 pb-3">
				<div class="card">
					<div class="card-body">
						<div class="d-flex">
							<div class="mr-auto">
								<div class="page-context-header">
									<div class="page-header-headings">
										<h1><?= _ADMINISTRATION ?></h1>
									</div>
								</div>
							</div>
						</div>
						<div class="d-flex flex-wrap">
							<div class="ml-auto d-flex"></div>
							<div id="course-header"></div>
						</div>
					</div>
				</div>
			</div>
		</header>
		<div id="page-content" class="row">
			<div id="region-main-box" class="col-12">
				<section id="region-main">
					<div class="card">
						<div class="card-body">
							<?php
							if (!$loggedin && !$userVho) {
								if ($adminMode == 'login' || $adminMode == 'dates') {
									if ($login) {
										echo "<form id=\"login_form\" action=\"index.php\" method=\"POST\"><div class=\"form-group fitem row\">
														<div class=\"col-md-2\">
															<label class=\"col-form-label\" for=\"id_username\">" . _USERNAME . "</label>
														</div>
														<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
															<span data-fieldtype=\"text\">
																<input placeholder=\"username\" type=\"text\" class=\"form-control\" name=\"username\" id=\"id_username\" required=\"required\" value=\"\">
															</span>
															<div class=\"form-control-feedback invalid-feedback\" id=\"id_error_username\"></div>
														</div>
													</div>
												
													<div class=\"form-group fitem row\">
														<div class=\"col-md-2\">
															<label class=\"col-form-label\" for=\"id_password\">" . _PASSWORD . "</label>
														</div>
														<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
															<span data-fieldtype=\"text\">
																<input placeholder=\"&#9679;&#9679;&#9679;&#9679;&#9679;\" type=\"password\" autocomplete=\"off\" class=\"form-control\" name=\"password\" id=\"id_password\" required=\"required\" value=\"\">
															</span>
															<div class=\"form-control-feedback invalid-feedback\" id=\"id_error_password\"></div>
														</div>
													</div>
												
													<div class=\"form-group fitem row\">
														<div class=\"col-md-2\">
														</div>
														<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
															<span data-fieldtype=\"text\">
																<button type=\"submit\" class=\"btn btn-secondary\" id=\"single_button_login\" title=\"\">Login</button>
																</span>
															<div class=\"form-control-feedback invalid-feedback\" id=\"id_error_password\"></div>
														</div>
													</div>
													<input type=\"hidden\" id=\"id_csrf_token\" name=\"csrf_token\" value=\"" . $csrf_token . "\">
													</form>";
									} else {
										echo "<i class=\"fa fa-window-close fa-fw\" style=\"font-size: 200px;\" title=\"" . _FALSE . "\" aria-label=\"false\" name=\"\"></i>
											<p>&nbsp;</p>
											<p>" . _NO_LOGIN . "</p>";
									}
								} else {
									getData($adminMode);
								}
							} else {
								getData($adminMode);
							}
							?>
							<div id="loader" class="ui-widget-overlay hidden">
								<div id="progressbar"></div>
							</div>
						</div>
					</div>
				</section>
				<div class="clearfix card-body">
					<?php
					echo "<p>" . $copyright;
					if ($loggedin) {
						echo " | <span id=\"server\">";
						echo php_uname('n');
						echo "</span>";
					}
					echo "</p>";
					?>
				</div>
			</div>
		</div>
	</div>
	</div>
</body>

</html>
