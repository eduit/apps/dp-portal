<?php

$qry = $SELECT_exam_people;
$res1 = pg_query($con, $qry);
$num1 = pg_num_rows($res1);

echo "<p>" . _TABLE_TEXT . "</p>";

echo "<table id=\"exams\" class=\"admintable generaltable\" style=\"width: 100%;\">
    <thead>
        <tr>
            <th style=\"width: 10%;\">" . _EXAM_NR . "</th>
            <th style=\"width: 50%;\">" . _EXAM . "</th>
            <th style=\"width: 10%;\">" . _DATE . "</th>
            <th style=\"width: 30%;\">" . _LECTURER . "</th>
        </tr>
    </thead>
    <tbody>";

for ($i = 0; $i < $num1; $i++) {
    $row1 = pg_fetch_array($res1);
    $date = date_create($row1['date']);
    $id = $row1['id'];
    $url = $row1['link'];
    $seb = $row1['seb'];
    $seb_link = "https://$_SERVER[HTTP_HOST]/" . $seb_dir . $seb;
    if ($seb) {
        $link = $seb_link;
        $globe = true;
    } else {
        $link = $url;
        $globe = false;
    }
    $examNr = $row1['exam_nr'];
    $examName = $row1['exam_name'];
    $lastname = $row1['lastname'];
    $firstname = $row1['firstname'];
    echo "<tr id=\" row_" . $id . "\" class=\"clickable-row\" data-href=\"" . $link . "\">
            <td id=\"exam_nr_" . $id . "\" name=\"" . $id . "\">" . $examNr . " <i id=\"clipboard_nr_" . $id . "\" class=\"fa fa-clipboard\" name=\"" . $examNr . "\" title=\"" . _CLIPBOARD . "\" aria-hidden=\"true\"></i></td>
            <td id=\"exam_" . $id . "\" name=\"" . $id . "\">" . $examName;
    if ($globe && $url) {
        echo " <a href=\"$url\" target=\"_blank\"><i id=\"link_" . $id . "\" class=\"fa fa-globe\" name=\"" . $examName . "\" title=\"" . _MOODLE . "\" aria-hidden=\"true\"></i></a>";
    }
    echo " <i id=\"clipboard_" . $id . "\" class=\"fa fa-clipboard\" name=\"" . $examName . "\" title=\"" . _CLIPBOARD . "\" aria-hidden=\"true\"></i></td>
            <td id=\"date_" . $id . "\">" . date_format($date, 'd.m.Y') . "</td>
            <td id=\"lecturer_" . $id . "\">" . $lastname . " " . $firstname . "</td>";
}
echo "</tbody>
</table>";
