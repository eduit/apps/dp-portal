<?php

session_start();

// unset all session data
foreach ($_SESSION as $key => $value) {
    unset($_SESSION[$key]);
}

// delete old session data
session_destroy();
session_start();
session_regenerate_id();

return "success";
