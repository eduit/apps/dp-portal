<?php

echo "<div role=\"main\">
					<div class=\"box py-3\">
						<ul id=\"myUL\">
							<li><span class=\"caret caret-down\"><h3>" . _INSTRUCTION_LOGIN_TITLE . "</h3></span>
								<ul class=\"nested active\">
								  <li class=\"normal\">
									" . _INSTRUCTION_LOGIN_CONTENT . "
								  </li>
								</ul>
							</li>
							<hr>
							<li><span class=\"caret\"><h3>" . _INSTRUCTION_DURING_EXAM_TITLE . "</h3></span>
								<ul class=\"nested\">
								  <li class=\"normal\">
									" . _INSTRUCTION_DURING_EXAM_CONTENT . "
								  </li>
								</ul>
							  </li>
							  <hr>
							  <li><span class=\"caret\"><h3>" . _INSTRUCTION_FINISH_EXAM_TITLE . "</h3></span>
								<ul class=\"nested\">
									<li class=\"normal\">
										" . _INSTRUCTION_FINISH_EXAM_CONTENT . "
									</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<div class=\"m-t-2 m-b-1\">
					<div class=\"row\">
						<div class=\"col-md-4\">
							<div class=\"pull-left\">
							</div>
						</div>
						<div class=\"col-md-4\">
							<div class=\"mdl-align\"></div>
						</div>
						<div class=\"col-md-4\">
							<div class=\"pull-right\">
							</div>
						</div>
					</div>
				</div>";
