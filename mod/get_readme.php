<?php

$id = urlencode($_GET['id'] ?? '');
$before = urlencode($_GET['before'] ?? '');
$url = urlencode($_GET['url'] ?? '');
$examURL = '';

if ($before) {
	$_SESSION['before'] = $before;
	$mode = $before;
}

if ($mode == 'form' || $url) {
	//select url from exam
	$qry = $SELECT_exam_url_by_exam_id . $id;
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$examLink = $row['exam_link'] ?? '';
	$examSEB = $row['exam_seb'] ?? '';
	if ($examSEB) {
		$examURL = "https://$_SERVER[HTTP_HOST]/" . $seb_dir . $examSEB;
	} else {
		$examURL = $examLink;
	}
}

if ($mode == 'allocation' && !$url) {
	//select url from student or allocation
	$qry = $SELECT_exam_url_by_student . $id;
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
	$examURL = $row['url'] ?? '';
}

echo "<div role=\"main\">
		<div class=\"box py-3\">
			<p>" . _README_1 . "</p>
			<p>" . _README_2 . "</p>
			<p>" . _README_3 . "</p>
		</div>
	</div>
	<div class=\"m-t-2 m-b-1\">
		<div class=\"row\">
			<div class=\"col-md-4\">
				<div class=\"pull-left\">
					<button type=\"button\" class=\"btn btn-secondary\" id=\"form_button_to_exam\" title=\"" . _TO_EXAM . "\" value=\"$examURL\">" . _TO_EXAM . "</button>
				</div>
			</div>
			<div class=\"col-md-4\">
				<div class=\"mdl-align\"></div>
			</div>
			<div class=\"col-md-4\">
				<div class=\"pull-right\">
				</div>
			</div>
		</div>
	</div>";
