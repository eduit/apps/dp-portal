<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

$matrikelnummer = htmlspecialchars($_POST["matrikelnummer"]);
$lastname = htmlspecialchars($_POST["lastname"]);
$firstname = htmlspecialchars($_POST["firstname"]);
$exam_id = $_POST["exam"];
$ip_address = $_POST["ip_address"];
$hostname = trim($_POST["hostname"], ".ethz.ch");
$ignored = $_POST["ignored"];
$write = 1;
$vdi_name = "";

$lastname = str_replace("'", "&apos;", $lastname);
$firstname = str_replace("'", "&apos;", $firstname);

$qry = "SELECT student_id, student_lastname, student_firstname, student_matrikelnr FROM tbl_student WHERE student_matrikelnr='$matrikelnummer' ORDER BY updated_at DESC";
$res = pg_query($con, $qry);
$num = pg_num_rows($res);

for ($i = 0; $i < $num; $i++) {
	$row = pg_fetch_array($res);
	$student_id = $row['student_id'];
	$student_lastname = $row['student_lastname'];
	$student_firstname = $row['student_firstname'];
	$student_matrikelnr = $row['student_matrikelnr'];

	if ($lastname == $student_lastname && $firstname == $student_firstname) {
		$write = 0;
		break;
	}
}

$write = false;

if (empty($student_id)) {
	$write = true;
}
if ($student_matrikelnr == "00-000-000" && $write == 1) {
	$write = true;
}
if ($lastname != $student_lastname || $firstname != $student_firstname) {
	$write = true;
}


if ($write) {
	$student_id = writeStudent($con, $response_array, $matrikelnummer, $lastname, $firstname);
	$response_array['status1'] = 'success';
} else {
	$response_array['status1'] = 'nothing to do';
}

$qry = $SELECT_computer_by_ip . "'$ip_address'";
$res = pg_query($con, $qry);
$row = pg_fetch_assoc($res);
$computer_id = $row['computer_id'] ?? 0;
$vdi = $row['vdi'] ?? 0;

if ($vdi == 't') {
	$vdi_name = $row['computer_name'];
}

if (empty($computer_id)) {
	$qry = $INSERT_hostname_ip . " VALUES ('$hostname', '$ip_address')";
	if (pg_query($con, $qry)) {
		$response_array['status2'] = 'success';

		$qry = $SELECT_computer_by_ip . "'$ip_address'";
		$res = pg_query($con, $qry);
		$row = pg_fetch_assoc($res);
		$computer_id = $row['computer_id'];
	} else {
		$response_array['status2'] = 'error';
		echo json_encode($response_array);
		throw new UnexpectedValueException('unexpected computer');
	}
} else {
	$response_array['status2'] = 'nothing to do';
}

$qry = $SELECT_room_by_computer . " AND tbl_computer.computer_id='" . $computer_id . "'";
$res = pg_query($con, $qry);
$row = pg_fetch_assoc($res);
$room_id = $row['room_id'];

$qry = "SELECT exam_student_id FROM tbl_exam_student WHERE student_id='$student_id' AND exam_id='$exam_id' AND computer_id='$computer_id'";
$res = pg_query($con, $qry);
$row = pg_fetch_assoc($res);
$exam_student_id = $row['exam_student_id'] ?? 0;

if (empty($exam_student_id)) {
	$qry = $INSERT_exam_student . " VALUES ($room_id, $student_id, $computer_id, 'true', $exam_id, '$vdi_name', 'true')";

	if (pg_query($con, $qry)) {
		$response_array['status3'] = 'success';
	} else {
		$response_array['status3'] = 'error';
	}
} else {
	$qry = $UPDATE_exam_student . " SET updated_at = NOW() WHERE exam_student_id = $exam_student_id;";
	if (pg_query($con, $qry)) {
		$response_array['status3'] = 'nothing to do';
	} else {
		$response_array['status3'] = 'error';
	}
}

pg_close($con);
echo json_encode($response_array);

function writeStudent($con, $responseArray, $matrikelnummer, $lastname, $firstname)
{
	include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
	include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

	$qry = $INSERT_student . " VALUES ('$matrikelnummer', '$lastname', '$firstname')";

	if (pg_query($con, $qry)) {
		$qry = "SELECT student_id FROM tbl_student WHERE student_matrikelnr='$matrikelnummer' AND student_lastname='$lastname' AND student_firstname='$firstname'";
		$res = pg_query($con, $qry);
		$row = pg_fetch_assoc($res);
		return $row['student_id'];
	} else {
		$responseArray['status1'] = 'error';
		echo json_encode($responseArray);
		throw new UnexpectedValueException('unexpected student');
	}
}
