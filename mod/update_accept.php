<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

$ip_address = $_POST["ip_address"];

$qry = $SELECT_computer_by_ip . "'$ip_address'";
$res = pg_query($con, $qry);
$row = pg_fetch_assoc($res);
$computer_id = $row['computer_id'];
$vdi = $row['vdi'];

if ($vdi == 't') {
	$vdi_name = $row['computer_name'];
} else {
	$vdi_name = null;
}

$response_array['vdi_name'] = $vdi_name;

$exam_student_id = $_POST["exam_student_id"];
$student_id = $_POST["student_id"];

$qry = $UPDATE_exam_student . " SET accepted='true', vdi_name='$vdi_name' WHERE exam_student_id=$exam_student_id AND student_id=$student_id;";

if (pg_query($con, $qry)) {
	$response_array['status'] = 'success';
} else {
	$response_array['status'] = 'error';
}

pg_close($con);
echo json_encode($response_array);
