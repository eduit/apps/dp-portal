<?php

$_SESSION['before'] = "allocation";

// get IP address
if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ipAddress = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ipAddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ipAddress = $_SERVER['REMOTE_ADDR'];
}

// look for computer allocation (VDI)
$qry = $SELECT_computer_by_ip . "'" . $ipAddress . "'";
$res1 = pg_query($con, $qry);
$row1 = pg_fetch_assoc($res1);
$vdi = $row1['vdi'] ?? 'f';
$examUserID = $row1['exam_user_id'] ?? 0;

if ($vdi == 't' && $examUserID != 0) {
    $qry = $SELECT_computer_by_VDI_user . $examUserID;
    $res2 = pg_query($con, $qry);
    $row2 = pg_fetch_assoc($res2);
    $ipAddress = $row2['ip_address'];
}

// look for computer allocation
$qry = $SELECT_student_by_computer . "'" . $ipAddress . "'";
$res1 = pg_query($con, $qry);
$num1 = pg_num_rows($res1);
$row1 = pg_fetch_assoc($res1);
$examStudentID = $row1['exam_student_id'] ?? 0;
$studentID = $row1['student_id'] ?? 0;
$studentRegistration = $row1['student_matrikelnr'] ?? '0';
$studentLastname = $row1['student_lastname'] ?? 'default';
$studentFirstname = $row1['student_firstname'] ?? 'default';
$usKeyboard = $row1['us_keyboard'] ?? 0;
$group = $row1['group'] ?? 0;
$remark = $row1['remark'] ?? '';
$active = $row1['active'] ?? 0;
$examType = $row1['exam_type'] ?? 0;

if (isset($_GET['screen'])) {
    $screen = $_GET['screen'];
} else {
    if ($num1 == 0 || ($num1 == 1 && !$studentID)) {
        $screen = "no_allocation";
    } elseif ($num1 == 1 && $studentID) {
        $screen = "student";
    } elseif ($num1 > 1) {
        $screen = "multiple";
    } else {
        $screen = "";
    }
}
$repeat = false;

do {
    switch ($screen) {
        case "no_allocation":
            $qry = $SELECT_exam_by_computer . " AND ip_address = '" . $ipAddress . "'";
            $res2 = pg_query($con, $qry);
            $num2 = pg_num_rows($res2);
            $roomName = '';

            if ($num2 > 0) {
                $row2 = pg_fetch_assoc($res2);
                $examStudentID = $row2['exam_student_id'];
                $examID = $row2['exam_id'];
                $examName = $row2['exam_name'];
                $roomID = $row2['room_id'];
                $roomName = $row2['room_name'];
                $examDate = date_create($row2['exam_date']);
                $lecturer = $row2['lecturer_lastname'] . " " . $row2['lecturer_firstname'];
            }

            // get room_id from computer
            $qry = $SELECT_room_by_computer . " AND tbl_computer.ip_address='" . $ipAddress . "'";
            $res3 = pg_query($con, $qry);
            $row3 = pg_fetch_assoc($res3);
            $computerRoomID = $row3['room_id'] ?? 0;
            if (!$roomName) {
                $roomName = $row3['room_name'] ?? 'default';
            }

            if (!$computerRoomID) {
                $computerRoomID = 0;
            }

            // get room if allocated
            $qry = $SELECT_exam_students_by_room . $computerRoomID . " AND active='t'";
            $res5 = pg_query($con, $qry);
            $num5 = pg_num_rows($res5);

            if ($num2 == 0) {
                $row5 = pg_fetch_assoc($res5);
                $examStudentID = $row5['exam_student_id'] ?? 0;
                $examID = $row5['exam_id'] ?? 0;
                $examName = $row5['exam_name'] ?? 'default';
                $roomID = $row5['room_id'] ?? 0;
                $roomName = $row5['room_name'] ?? 'default';
                $roomDisplayName = $row5['room_display_name'] ?? 0;
                $examDate = date_create($row5['exam_date'] ?? '2020-01-01');
                $lecturerLastname = $row5['lecturer_lastname'] ?? 'default';
                $lecturerFirstname = $row5['lecturer_firstname'] ?? 'default';
                $lecturer = $lecturerLastname . " " . $lecturerFirstname;
            }

            if ($roomDisplayName) {
                $roomName = $roomDisplayName;
            }

            // look for exam students
            if (!$examID) {
                $examID = 0;
            }
            $qry = $SELECT_exam_students . " AND tbl_exam_student.active = 't' AND tbl_exam.exam_id = $examID AND tbl_exam_student.computer_id IS NULL ORDER BY tbl_exam_student.created_at";
            $res7 = pg_query($con, $qry);
            $num7 = pg_num_rows($res7);

            if ($num2 == 0 && $num5 == 0 && !$active) {
                echo "<div role=\"main\" class=\"quizinfo\">";
                echo "<i class=\"fa fa-exclamation-triangle fa-fw\" style=\"font-size: 200px;\" title=\"\" aria-label=\"false\" name=\"\"></i>
									<p>&nbsp;</p>
									<p style=\"font-size: 28px;\">" . _NO_ALLOCATION_FOUND . "</p>";
                echo "</div>";
            } elseif ($num2 > 1) {
                $repeat = true;
                $screen = "multiple";
            } elseif ($num2 == 1 || $num5 > 0) {
                echo "<div role=\"main\" class=\"quizinfo\">";
                echo "<p " . FONTSIZE . ">$examName | $lecturer | " . date_format($examDate, 'd.m.Y') . "</p>";
                echo "<p>" . _ROOM . ": $roomName</p>";
                echo "</div>";
                if ($num7 > 0) {
                    echo "<div class=\"m-t-2 m-b-1\">
											<div class=\"row\">
												<div class=\"col-md-4\">
													<div class=\"pull-left\">
													</div>
												</div>
												<div class=\"col-md-4\">
													<div class=\"mdl-align\">
														<p><b>" . _NO_ALLOCATION_TEXT . "</b></p>
														<p><button type=\"button\" class=\"btn btn-secondary no_allocation_button\" id=\"no_allocation_auto_forward\" title=\"" . _AUTO_ALLOCATION_FORWARD . "\" data-href=\"$examStudentID,$examID,$roomID,$computerRoomID\">" . _AUTO_ALLOCATION_FORWARD . "</button></p>
														<p><button type=\"button\" class=\"btn btn-secondary no_allocation_button\" id=\"no_allocation_auto_backward\" title=\"" . _AUTO_ALLOCATION_BACKWARD . "\" data-href=\"$examStudentID,$examID,$roomID,$computerRoomID\">" . _AUTO_ALLOCATION_BACKWARD . "</button></p>
														<p><button type=\"button\" class=\"btn btn-secondary no_allocation_button\" id=\"no_allocation_manual\" title=\"" . _MANUAL_ALLOCATION . "\">" . _MANUAL_ALLOCATION . " (M)</button></p>
														<p><button type=\"button\" class=\"btn btn-secondary no_allocation_button\" id=\"no_allocation_form\" title=\"" . _ALLOCATION_FORM . "\">" . _ALLOCATION_FORM . "</button></p>
													</div>
												</div>
												<div class=\"col-md-4\">
													<div class=\"pull-right\"></div>
												</div>
											</div>
										</div>";
                } else {
                    echo "<div role=\"main\" class=\"quizinfo\">
											<i class=\"fa fa-search fa-fw\" style=\"font-size: 200px;\" title=\"" . _FALSE . "\" aria-label=\"false\" name=\"No student left\"></i>
											<p>&nbsp;</p>
											<p style=\"font-size: 28px; white-space: nowrap;\">" . _NO_COMPUTERS_LEFT . "</p>
											<p><button type=\"button\" class=\"btn btn-secondary no_allocation_button\" id=\"no_allocation_form\" title=\"" . _ALLOCATION_FORM . "\">" . _ALLOCATION_FORM . "</button></p>
										</div>";
                    echo "<script>
											$(document).on(\"keydown\", function (e) {
												// CTRL+ALT+Q
												if (e.ctrlKey  &&  e.altKey  &&  e.which === 81) {
													e.preventDefault();
													location.href=\"index.php?mode=form\";
												}
											});
										</script>";
                }
            } else {
                return;
            }
            break;

        case "manual":
            $qry = $SELECT_exam_by_computer . " AND ip_address = '" . $ipAddress . "'";
            $res6 = pg_query($con, $qry);
            $num6 = pg_num_rows($res6);
            $row6 = pg_fetch_assoc($res6);
            $examID = $row6['exam_id'] ?? 0;
            $examStudentID = $row6['exam_student_id'] ?? 0;
            $examName = $row6['exam_name'] ?? 'default';
            $roomName = $row6['room_name'] ?? 'default';
            $roomDisplayName = $row6['room_display_name'] ?? 0;
            $examDate = date_create($row6['exam_date'] ?? '2020-01-01');
            $lecturerLastname = $row6['lecturer_lastname'] ?? 'default';
            $lecturerFirstname = $row6['lecturer_firstname'] ?? 'default';
            $lecturer = $lecturerLastname . " " . $lecturerFirstname;

            // get room_id from computer
            $qry = $SELECT_room_by_computer . " AND tbl_computer.ip_address='" . $ipAddress . "'";
            $res11 = pg_query($con, $qry);
            $row11 = pg_fetch_assoc($res11);
            $computerRoomID = $row11['room_id'];

            if (!$computerRoomID) {
                $computerRoomID = 0;
            }

            // get room if allocated
            $qry = $SELECT_exam_students_by_room . $computerRoomID . " AND active='t'";
            $res5 = pg_query($con, $qry);

            if ($num6 == 0) {
                $row5 = pg_fetch_assoc($res5);
                $examStudentID = $row5['exam_student_id'] ?? 0;
                $examID = $row5['exam_id'] ?? 0;
                $examName = $row5['exam_name'] ?? 'default';
                $roomID = $row5['room_id'] ?? 0;
                $roomName = $row5['room_name'] ?? 'default';
                $roomDisplayName = $row5['room_display_name'] ?? 0;
                $examDate = date_create($row5['exam_date'] ?? '2020-01-01');
                $lecturerLastname = $row5['lecturer_lastname'] ?? 'default';
                $lecturerFirstname = $row5['lecturer_firstname'] ?? 'default';
                $lecturer = $lecturerLastname . " " . $lecturerFirstname;
            }

            if ($examID) {
                $qry = $SELECT_exam_students . " AND tbl_exam_student.room_id = $roomID AND tbl_exam_student.active = 't' AND tbl_exam.exam_id = $examID AND tbl_exam_student.computer_id IS NULL ORDER BY tbl_exam_student.created_at";
                $res7 = pg_query($con, $qry);
                $num7 = pg_num_rows($res7);
            }

            if ($roomDisplayName) {
                $roomName = $roomDisplayName;
            }

            echo "<div role=\"main\" class=\"quizinfo\">";
            echo "<p " . FONTSIZE . ">$examName | $lecturer | " . date_format($examDate, 'd.m.Y') . "</p>";
            echo "<p>" . _ROOM . ": $roomName</p>";
            echo "</div>";

            echo "<div role=\"main\" class=\"quizinfo\">";
            echo "<p><b>" . _MANUAL_ALLOCATION_TEXT . "</b></p>";
            if ($num7 == 0) {
                echo "<p>" . _NO_STUDENTS_TEXT . "</p>";
            } else {
                echo "<p style=\"text-align: left;\">" . $num7 . " " . _COUNT . "</p>";
                echo "<table id=\"students\" class=\"admintable generaltable\" style=\"width: 100%;\">
										<thead>
										<tr>
										<th style=\"width: 20%;\">" . _LASTNAME . "</th>
										<th style=\"width: 20%;\">" . _FIRSTNAME . "</th>
										<th style=\"width: 15%;\">" . _REGISTRATION . "</th>
										<th style=\"width: 15%;\">" . _KEYBOARD . "</th>
										<th style=\"width: 15%;\">" . _GROUP . "</th>
										<th style=\"width: 15%;\">" . _REMARK . "</th>
										</tr>
										</thead><tbody>";

                $res7 = pg_query($con, $qry);

                for ($i = 0; $i < $num7; $i++) {
                    $row7 = pg_fetch_array($res7);
                    $studentID = $row7['student_id'] ?? 0;
                    $matrikelnr = $row7['registration'] ?? '0';
                    $lastname = $row7['lastname'] ?? 'default';
                    $firstname = $row7['firstname'] ?? 'default';
                    $usKeyboard = $row7['us_keyboard'] ?? 0;
                    $group = $row7['group'] ?? 0;
                    $remark = $row7['remark'] ?? '';
                    $examStudentID = $row7['exam_student_id'] ?? 0;

                    if ($usKeyboard == "f") {
                        $usKeyboard = "<i class=\"fa fa-square-o fa-fw false_icon\" title=\"" . _FALSE . "\" aria-label=\"false\" name=\"us_keyboard_" . $examStudentID . "\"></i>";
                    } else {
                        $usKeyboard = "<i class=\"fa fa-check-square-o fa-fw true_icon\" title=\"" . _TRUE . "\" aria-label=\"true\" name=\"us_keyboard_" . $examStudentID . "\"></i>";
                    }

                    echo "<tr id=\"row_$studentID\" class=\"clickable-row_manual_allocation\" data-href=\"$examStudentID,$studentID,$computerRoomID\">
											<td id=\"lastname_$studentID\">$lastname</td>
											<td id=\"firstname_$studentID\">$firstname</td>
											<td id=\"matrikelnr_$studentID\">$matrikelnr</td>
											<td id=\"keyboard_$studentID\">$usKeyboard</td>
											<td id=\"group_$studentID\">$group</td>
											<td id=\"remark_$studentID\">$remark</td>";
                }
                echo "</tbody></table>";
            }
            echo "</div>";

            echo "<div class=\"m-t-2 m-b-1\">
									<div class=\"row\">
										<div class=\"col-md-4\">
											<div class=\"pull-left\">
												<button type=\"button\" class=\"btn btn-secondary\" id=\"manual_allocation_button_back\" title=\"" . _BACK . "\">" . _BACK . "</button>
											</div>
										</div>
										<div class=\"col-md-4\">
											<div class=\"mdl-align\"></div>
										</div>
										<div class=\"col-md-4\">
											<div class=\"pull-right\">
											</div>
										</div>
									</div>
								</div>";
            break;

        case "student":
            $qry = $SELECT_exam_by_computer . " AND active = 't' AND ip_address = '" . $ipAddress . "'";
            $res8 = pg_query($con, $qry);
            $row8 = pg_fetch_assoc($res8);
            $examID = $row8['exam_id'] ?? 0;
            $examStudentID = $row8['exam_student_id'] ?? 0;
            $examName = $row8['exam_name'] ?? 'default';
            $examDate = date_create($row8['exam_date'] ?? '2020-01-01');
            $lecturerLastname = $row8['lecturer_lastname'] ?? 'default';
            $lecturerFirstname = $row8['lecturer_firstname'] ?? 'default';
            $lecturer = $lecturerLastname . " " . $lecturerFirstname;


            //select url from student or allocation
            $qry = $SELECT_exam_url_by_student . $examStudentID;
            $res9 = pg_query($con, $qry);
            $row9 = pg_fetch_assoc($res9);
            $examURL = $row9['url'];

            echo "<div role=\"main\" class=\"quizinfo\">";
            echo "<p " . FONTSIZE . ">$examName | $lecturer | " . date_format($examDate, 'd.m.Y') . "</p>";
            echo "<h2 id=\"student_name\" style=\"font-size: 90px; font-weight: bold;\">$studentLastname $studentFirstname ($studentRegistration)</h2>";

            if ($group) {
                echo "<p " . FONTSIZE . ">" . _GROUP . ": $group</p>";
            }
            if ($remark) {
                echo "<p " . FONTSIZE . ">" . _REMARK . ": $remark</p>";
            }
            if ($usKeyboard == "t") {
                //max 619px
                echo "<p><img src=\"img/us_keyboard_with_flag.png\" alt=\"" . _KEYBOARD . "\" width=\"256px\" /></p>";
            }
            echo "<p>&nbsp;</p>
								<p style=\"font-size: 30px; font-weight: bold;\">" . _ALLOCATION_TEXT . "</p>
                                <input type=\"text\" class=\"hidden\" name=\"exam_id\" id=\"exam_id\" readonly value=\"$examID\">
								<input type=\"text\" class=\"hidden\" name=\"exam_student_id\" id=\"exam_student_id\" readonly value=\"$examStudentID\">
								<input type=\"text\" class=\"hidden\" name=\"student_id\" id=\"student_id\" readonly value=\"$studentID\">
								<input type=\"text\" class=\"hidden\" name=\"exam_url\" id=\"exam_url\" readonly value=\"$examURL\">";
            echo "</div>";

            echo "<div class=\"form-group fitem info\" id=\"allocation_accept\" style=\"background-color: #eaecee; padding-left: 20px; display: none;\">
										<p>&nbsp;<br>" . _FORM_TEXT_2 . "<br>" . _FORM_TEXT_3 . "</p>
										<p><input type=\"checkbox\" class=\"tooltip-wrapper\" name=\"accepted\" id=\"id_accepted\" title=\"" . _NO_CHECKBOX . "\"> <label class=\"col-form-label\" for=\"id_accepted\">" . _ACCEPTED . "</label><br>&nbsp;</p>
									</div>";

            echo "<div class=\"m-t-2 m-b-1\">
									<div class=\"row\">
										<div class=\"col-md-4\">
											<div class=\"pull-left\">
												<button type=\"button\" class=\"btn btn-secondary hidden\" id=\"allocation_button_back\" title=\"" . _BACK . "\">" . _BACK . "</button>
											</div>
										</div>
										<div class=\"col-md-4\">
											<div class=\"mdl-align\"></div>
										</div>
										<div class=\"col-md-4\">
											<div class=\"pull-right\">
												<button type=\"button\" class=\"btn btn-secondary\" id=\"allocation_button_next\" title=\"" . _NEXT . "\" data-href=\"" . $examType . "\">" . _NEXT . "</button>
											</div>
										</div>
									</div>
								</div>";
            break;

        case "multiple":
            $repeat = false;
            echo "<div role=\"main\" class=\"quizinfo\">";
            echo "<h2 class=\"alert-warning\">" . _MULTIPLE_ALLOCATION_TEXT . "</h2>";
            echo "</div>";
            echo "<div class=\"m-t-2 m-b-1\">
									<div class=\"row\">
										<div class=\"col-md-4\">
											<div class=\"pull-left\">
												<button type=\"button\" class=\"btn btn-secondary\" id=\"allocation_button_back\" title=\"" . _BACK . "\">" . _BACK . "</button>
											</div>
										</div>
										<div class=\"col-md-4\">
											<div class=\"mdl-align\"></div>
										</div>
										<div class=\"col-md-4\">
											<div class=\"pull-right\"></div>
										</div>
									</div>
								</div>";
            break;

        default:
            echo "<h2>" . _WRONG_PARAMETER . "</h2>
				<p>&nbsp;</p>
				<i class=\"fa fa-exclamation fa-fw\" style=\"font-size: 200px; margin-left: 80px;\" title=\"\" aria-label=\"false\" name=\"\"></i>";
            break;
    }
} while ($repeat);
