<?php

// Get preference form_enabled
$formEnabled = "";
$qry = $SELECT_pref . " WHERE pref_name = 'form_enabled'";
$res = pg_query($con, $qry);
$row = pg_fetch_assoc($res);
$formEnabled = $row['pref_value'];

if ($formEnabled == "false") {
    echo _FORM_DISABLED;
} else {
    if (isset($_GET['screen'])) {
        $screen = $_GET['screen'];
    } else {
        $screen = "empty";
    }
    getFormData($screen);
}

function getFormData($screen)
{
	include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
	include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

    // set lastname
    if (isset($_POST['lastname']) && !empty($_POST['lastname'])) {
        $_SESSION['lastname'] = htmlspecialchars($_POST['lastname']);
    }

    if (isset($_SESSION['lastname'])) {
        $lastname = htmlspecialchars($_SESSION['lastname']);
    } else {
        $lastname = '';
    }

    // set firstname
    if (isset($_POST['firstname']) && !empty($_POST['firstname'])) {
        $_SESSION['firstname'] = htmlspecialchars($_POST['firstname']);
    }

    if (isset($_SESSION['firstname'])) {
        $firstname = htmlspecialchars($_SESSION['firstname']);
    } else {
        $firstname = '';
    }

    // set registration
    if (isset($_POST['matrikelnummer']) && !empty($_POST['matrikelnummer'])) {
        $_SESSION['matrikelnummer'] = htmlspecialchars($_POST['matrikelnummer']);
    }

    if (isset($_SESSION['matrikelnummer'])) {
        $matrikelnummer = htmlspecialchars($_SESSION['matrikelnummer']);
    } else {
        $matrikelnummer = '';
    }

    // set noregistration
    if (isset($_POST['noregistration']) && $_POST['noregistration'] == 1) {
        $_SESSION['noregistration'] = $_POST['noregistration'];
    } else {
        $_SESSION['noregistration'] = 0;
    }
    $noregistration = $_SESSION['noregistration'];

    // set exam
    if (isset($_POST['exam']) && !empty($_POST['exam'])) {
        $_SESSION['exam'] = $_POST['exam'];
    }

    if (isset($_SESSION['exam'])) {
        $exam = $_SESSION['exam'];
    } else {
        $exam = '';
    }

    switch ($screen) {
        case "empty":
            $_SESSION['before'] = "form";
            echo "<form autocomplete=\"off\" action=\"index.php?mode=form&screen=filled\" method=\"post\" accept-charset=\"utf-8\" id=\"mform1\" class=\"mform\">
						<div role=\"main\">
							
							<div class=\"box py-3\">
								<p>" . _FORM_TEXT_1 . "</p>
																	
								<div>
									<div class=\"form-group fitem row\">
										<div class=\"col-md-2\">
											<label class=\"col-form-label\" for=\"id_lastname\">" . _LASTNAME . "</label>
										</div>
										<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
											<span data-fieldtype=\"text\">
												<input placeholder=\"" . _DOE . "\" type=\"text\" class=\"form-control\" name=\"lastname\" id=\"id_lastname\" required value=\"$lastname\" maxlength=\"" . MAXLENGTH . "\">
											</span>
											<div class=\"form-control-feedback invalid-feedback\" id=\"id_error_lastname\">" . _REQUIRED . "</div>
										</div>
									</div>
									
									<div class=\"form-group fitem row\">
										<div class=\"col-md-2\">
											<label class=\"col-form-label\" for=\"id_firstname\">" . _FIRSTNAME . "</label>
										</div>
										<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
											<span data-fieldtype=\"text\">
												<input placeholder=\"" . _JOHN . "\" type=\"text\" class=\"form-control \" name=\"firstname\" id=\"id_firstname\" required value=\"$firstname\" maxlength=\"" . MAXLENGTH . "\">
											</span>
											<div class=\"form-control-feedback invalid-feedback\" id=\"id_error_firstname\">" . _REQUIRED . "</div>
										</div>
									</div>
									
									<div class=\"form-group fitem row\">
										<div class=\"col-md-2\">
											<label class=\"col-form-label\" for=\"id_matrikelnummer\">" . _REGISTRATION . "</label>
										</div>
										<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
											<span data-fieldtype=\"text\">
												<input placeholder=\"00-000-000\" type=\"text\" class=\"form-control \" name=\"matrikelnummer\" id=\"id_matrikelnummer\" required value=\"$matrikelnummer\" maxlength=\"" . MAXLENGTH . "\"";
            if ($noregistration) {
                echo " readonly";
            }
            echo ">&nbsp;&nbsp;<nobr><input type=\"checkbox\" name=\"noregistration\" value=\"1\" id=\"id_noregistration\"";
            if ($noregistration) {
                echo " checked";
            }
            echo "> <label class=\"col-form-label\" for=\"id_noregistration\" style=\"display: inline;\">" . _NOREGISTRATION . "</nobr></label>
											</span>
											<div class=\"form-control-feedback invalid-feedback\" id=\"id_error_matrikelnummer\">" . _REQUIRED . "</div>
										</div>
									</div>
									
									<div class=\"form-group fitem row\">
										<div class=\"col-md-2\">
											<label class=\"col-form-label\" for=\"id_exam\">" . _EXAM . "</label>
										</div>
										<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
											<span data-fieldtype=\"text\">
												<select class=\"select custom-select form-control\" name=\"exam\" id=\"id_exam\" style=\"width: 400px;\">";

            getExams($exam);

            echo "</select>
											</span>
											<div class=\"form-control-feedback invalid-feedback\" id=\"id_error_exam\">" . _NO_EXAM . "</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>
						<div class=\"m-t-2 m-b-1\">
							<div class=\"row\">
								<div class=\"col-md-4\">
									<div class=\"pull-left\">
										<button type=\"submit\" class=\"btn btn-secondary\" id=\"form_button_next\" title=\"\">" . _NEXT . "</button>
									</div>
								</div>
								<div class=\"col-md-4\">
									<div class=\"mdl-align\"></div>
								</div>
								<div class=\"col-md-4\">
									<div class=\"pull-right\">
									</div>
								</div>
							</div>
						</div>
					</form>";
            break;

        case "filled":
            echo "<form autocomplete=\"off\" action=\"index.php?mode=form\" method=\"post\" accept-charset=\"utf-8\" id=\"mform1\" class=\"mform\">
					<div role=\"main\">
						<div class=\"box py-3\">
							<p>" . _FORM_TEXT_4 . "</p>
																
							<div>
								<div class=\"form-group fitem row\">
									<div class=\"col-md-2\">
										<label class=\"col-form-label\" for=\"id_lastname\">" . _LASTNAME . "</label>
									</div>
									<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
										<span data-fieldtype=\"text\">
											<input type=\"text\" class=\"form-control\" name=\"lastname\" id=\"id_lastname\" readonly value=\"$lastname\" maxlength=\"" . MAXLENGTH . "\">
										</span>
										<div class=\"form-control-feedback invalid-feedback\" id=\"id_error_lastname\">" . _REQUIRED . "</div>
									</div>
								</div>
								
								<div class=\"form-group fitem row\">
									<div class=\"col-md-2\">
										<label class=\"col-form-label\" for=\"id_firstname\">" . _FIRSTNAME . "</label>
									</div>
									<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
										<span data-fieldtype=\"text\">
											<input type=\"text\" class=\"form-control \" name=\"firstname\" id=\"id_firstname\" readonly value=\"$firstname\" maxlength=\"" . MAXLENGTH . "\">
										</span>
										<div class=\"form-control-feedback invalid-feedback\" id=\"id_error_firstname\">" . _REQUIRED . "</div>
									</div>
								</div>
								
								<div class=\"form-group fitem row\">
									<div class=\"col-md-2\">
										<label class=\"col-form-label\" for=\"id_matrikelnummer\">" . _REGISTRATION . "</label>
									</div>
									<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
										<span data-fieldtype=\"text\">
											<input type=\"text\" class=\"form-control \" name=\"matrikelnummer\" id=\"id_matrikelnummer\" readonly value=\"$matrikelnummer\" maxlength=\"" . MAXLENGTH . "\">
											<input type=\"checkbox\" name=\"noregistration\" value=\"1\" id=\"id_noregistration\" style=\"display:none;\"";
            if ($noregistration) {
                echo " checked";
            }
            echo "></span>
										<div class=\"form-control-feedback invalid-feedback\" id=\"id_error_matrikelnummer\">" . _REGISTRATION_ERROR . "</div>
									</div>
								</div>
								
								<div class=\"form-group fitem row\" id=\"id_ignore_registration\" style=\"display:none;\">
									<div class=\"col-md-2\">
										<label class=\"col-form-label\" for=\"id_matrikelnummer\"></label>
									</div>
									<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
										<span data-fieldtype=\"text\">
											<input type=\"checkbox\" class=\"\" name=\"ignored\" id=\"id_ignored\"> <label style=\"display: inline;\" class=\"col-form-label\" for=\"id_ignored\">" . _IGNORE_REGISTRATION . "</label>
										</span>
									</div>
								</div>
								
								<div class=\"form-group fitem row\">
									<div class=\"col-md-2\">
										<label class=\"col-form-label\" for=\"id_exam\">" . _EXAM . "</label>
									</div>
									<div class=\"col-md-9 form-inline felement\" data-fieldtype=\"text\">
										<span data-fieldtype=\"text\">
											<select class=\"select custom-select form-control\" name=\"exam\" id=\"id_exam\" disabled style=\"width: 400px;\">";

            getExams($exam);

            echo "</select>
										</span>
										<div class=\"form-control-feedback invalid-feedback\" id=\"id_error_exam\">" . _NO_EXAM . "</div>
									</div>
								</div>
								
								<div class=\"form-group fitem info\" style=\"background-color: #eaecee; padding-left: 20px;\">
									<p>&nbsp;<br>" . _FORM_TEXT_2 . "<br>" . _FORM_TEXT_3 . "</p>
									<p><input type=\"checkbox\" class=\"tooltip-wrapper\" name=\"accepted\" id=\"id_accepted\" title=\"" . _NO_CHECKBOX . "\"> <label class=\"col-form-label\" for=\"id_accepted\">" . _ACCEPTED . "</label><br>&nbsp;</p>
								</div>
															
							</div>
						</div>
					</div>
					<div class=\"m-t-2 m-b-1\">
						<div class=\"row\">
							<div class=\"col-md-4\">
								<div class=\"pull-left\">
									<button type=\"submit\" class=\"btn btn-secondary\" id=\"form_button_back\" title=\"" . _BACK . "\">" . _BACK . "</button>&nbsp;&nbsp;
									<button type=\"button\" class=\"btn btn-secondary\" id=\"form_button_save\" title=\"" . _NEXT . "\">" . _NEXT . "</button>
								</div>
							</div>
							<div class=\"col-md-4\">
								<div class=\"mdl-align\"></div>
							</div>
							<div class=\"col-md-4\">
								<div class=\"pull-right\">
								</div>
							</div>
						</div>
					</div>
				</form>";
            break;

        default:
			echo "<h2>" . _WRONG_PARAMETER . "</h2>
				<p>&nbsp;</p>
				<i class=\"fa fa-exclamation fa-fw\" style=\"font-size: 200px; margin-left: 80px;\" title=\"\" aria-label=\"false\" name=\"\"></i>";
            break;
    }
}

function getExams($exam)
{
    include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
    include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

    $qry = $SELECT_exam_people;
    $res1 = pg_query($con, $qry);
    $num1 = pg_num_rows($res1);

    echo "<option name=\"" . _NO_EXAM . "\" value=\"0\" disabled ";
    if (!$exam) {
        echo " selected";
    }
    echo ">" . _NO_EXAM . "</option>\n";

    for ($i = 0; $i < $num1; $i++) {
        $row1 = pg_fetch_array($res1);
        $id = $row1['id'];
        $url = $row1['link'];
        $seb = $row1['seb'];
		$seb_link = "https://$_SERVER[HTTP_HOST]/" . $seb_dir . $seb;
        if ($seb) {
            $link = $seb_link;
        } else {
            $link = $url;
        }
        $typeID = $row1['type_id'];
        $examName = $row1['exam_name'];
        $lastname = $row1['lastname'];
        $date = date_create($row1['date']);

        echo "<option name=\"" . $link . "\" value=\"" . $id . "\" data-href=\"" . $typeID . "\" ";
        if ($id == $exam) {
            echo " selected";
        }

        echo ">" . $examName . " | " . $lastname . " | " . date_format($date, 'd.m.Y') . "</option>\n";
    }
}
