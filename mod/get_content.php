<?php

define("VARPHP", "/inc/var.php");
define("DBPHP", "/inc/db.php");
define("FONTSIZE", "style=\"font-size: 30px;\"");
define("MAXLENGTH", 100);

function getData($mode)
{
	include $_SERVER["DOCUMENT_ROOT"] . VARPHP;
	include $_SERVER["DOCUMENT_ROOT"] . DBPHP;

	$link = $_SERVER["DOCUMENT_ROOT"] . "/mod/get_$mode.php";

	if (file_exists($link)) {
		include_once $link;
	} else {
		echo "<h2>" . _WRONG_PARAMETER . "</h2>
				<p>&nbsp;</p>
				<i class=\"fa fa-exclamation fa-fw\" style=\"font-size: 200px; margin-left: 80px;\" title=\"\" aria-label=\"false\" name=\"\"></i>";
	}
}

function paramChanger($param, $lang)
{
	// get parameters
	$query = $_GET;
	// replace parameter(s)
	$query[$param] = $lang;
	// rebuild url
	$queryResult = http_build_query($query);

	return $_SERVER['PHP_SELF'] . "?" . $queryResult;
}
