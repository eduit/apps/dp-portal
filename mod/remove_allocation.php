<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

$student_id = $_POST["student_id"];

if (empty($student_id)) {
	$response_array['test'] = 'test';
}

ob_start();
include $_SERVER["DOCUMENT_ROOT"] . "/mod/get_ip.php";
$out1 = ob_get_clean();
$json = json_decode($out1);
$ip_address = $json->ip;
ob_end_flush();

$qry = $SELECT_computer_by_ip . "'" . $ip_address . "'";
$res = pg_query($con, $qry);
$row = pg_fetch_assoc($res);
$vdi = $row['vdi'];
$exam_user_id = $row['exam_user_id'];

if ($vdi == 't') {
	$qry = $SELECT_computer_by_VDI_user . $exam_user_id;
	$res = pg_query($con, $qry);
	$row = pg_fetch_assoc($res);
}

$computer_id = $row['computer_id'];

$qry = $UPDATE_exam_student . " SET computer_id = null WHERE computer_id=$computer_id AND accepted=false;";

if ($res = pg_query($con, $qry)) {
	$response_array['status'] = 'success';
	$response_array['affected_rows'] = pg_affected_rows($res);
} else {
	$response_array['status'] = 'error';
}

pg_close($con);
echo json_encode($response_array);
