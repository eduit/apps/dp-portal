<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

$student_id = $_POST["student_id"];
$exam_student_id = $_POST["exam_student_id"];
$exam_id = $_POST["exam_id"];
$room_id = $_POST["room_id"];
$computer_room_id = $_POST["computer_room_id"];
$mode = $_POST["mode"];
$no_students = false;
$vdi_name = "";

ob_start();
include $_SERVER["DOCUMENT_ROOT"] . "/mod/get_ip.php";
$out1 = ob_get_clean();
$json = json_decode($out1);
$ip_address = $json->ip;
ob_end_flush();

$qry = $SELECT_computer_by_ip . "'" . $ip_address . "'";
$res2 = pg_query($con, $qry);
$row2 = pg_fetch_assoc($res2);
$vdi = $row2['vdi'];
$exam_user_id = $row2['exam_user_id'];

if ($vdi == 't') {
	$qry = $SELECT_computer_by_VDI_user . $exam_user_id;
	$res2 = pg_query($con, $qry);
	$row2 = pg_fetch_assoc($res2);
	$vdi_name = $row2['computer_name'];
}

$computer_id = $row2['computer_id'];
$room_sector_id = $row2['room_sector_id'];

$qry = $SELECT_room_by_sector . $room_sector_id;
$res3 = pg_query($con, $qry);
$row3 = pg_fetch_assoc($res3);
$room_id = $row3['room_id'];

if ($exam_id) {
	$qry = $SELECT_exam_students . " AND tbl_exam.exam_id = $exam_id AND tbl_exam_student.computer_id IS NULL AND tbl_exam_student.room_id = $room_id AND tbl_exam_student.active=true ORDER BY tbl_exam_student.created_at";
} else {
	$qry = $SELECT_exam_students_by_room . $computer_room_id . "AND tbl_exam_student.computer_id IS NULL AND tbl_exam_student.active=true ORDER BY tbl_exam_student.created_at";
}

if ($mode == "forward") {
	$qry = $qry . " ASC LIMIT 1";
	$res4 = pg_query($con, $qry);
	$num4 = pg_num_rows($res4);
	if ($num4 == 0) {
		$no_students = true;
	} else {
		$row4 = pg_fetch_assoc($res4);
		$student_id = $row4["student_id"];
		$exam_student_id = $row4["exam_student_id"];
	}
} elseif ($mode == "backward") {
	$qry = $qry . " DESC LIMIT 1";
	$res4 = pg_query($con, $qry);
	$num4 = pg_num_rows($res4);
	if ($num4 == 0) {
		$no_students = true;
	} else {
		$row4 = pg_fetch_assoc($res4);
		$student_id = $row4["student_id"];
		$exam_student_id = $row4["exam_student_id"];
	}
} else {
	//nothing to do
}

if ($no_students) {
	$response_array['status1'] = 'no_students';
} else {
	$qry = $UPDATE_exam_student . " SET computer_id=$computer_id, accepted=false, vdi_name='$vdi_name', active=true WHERE exam_student_id=$exam_student_id;";
	if (pg_query($con, $qry)) {
		$response_array['status1'] = 'success';
	} else {
		$response_array['status1'] = 'error';
	}
}

pg_close($con);
echo json_encode($response_array);
