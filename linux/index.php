<?php

header("Content-type: application/json");
header("Cache-Control: no-cache, no-store, must-revalidate");
header("Pragma: no-cache");
header("Expires: 0");

header('X-Content-Type-Options: nosniff');

include $_SERVER["DOCUMENT_ROOT"] . "/inc/var.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/func.php";
include $_SERVER["DOCUMENT_ROOT"] . "/inc/db.php";

$hash = "$2y$10$5layxDdqL/GOFn.51.e.lO4iCBV7.CLu5p2WVhFeHpNBApxoD1bH2";

if (isset($_GET['key'])) {
	if (!password_verify($_GET['key'], $hash)) {
		$response_array = "wrong authentication key";
		echo json_encode($response_array);
		return;
	}
} else {
	$response_array = "authentication key is missing";
	echo json_encode($response_array);
	return;
}

if (isset($_GET['exam_id'])) {
	$exam_nr = urlencode($_GET['exam_id']);
} else {
	$response_array = "exam_id is missing";
	echo json_encode($response_array);
	return;
}

if (isset($_GET['date'])) {
	$date = urlencode($_GET['date']);
} else {
	$date = '';
}

if (isset($_GET['ip'])) {
	$ip_address = urlencode($_GET['ip']);
} else {
	$ip_address = '';
}

if (isset($_GET['accepted'])) {
	$accepted = '';
} else {
	$accepted = " AND tbl_exam_student.accepted='true'";
}

$qry = $SELECT_exam_students . " AND tbl_exam_name.exam_nr='$exam_nr'" . $accepted;
if ($date) {
	$qry = $qry . " AND tbl_exam.exam_date='$date'";
}
if ($ip_address) {
	$qry = $qry . " AND tbl_computer.ip_address='$ip_address'";
}

$res = pg_query($con, $qry);
$num = pg_num_rows($res);

$response_array['exam_nr'] = $exam_nr;

$student_ids = array();

if ($num > 0) {
	for ($i = 0; $i < $num; $i++) {
		$row = pg_fetch_array($res);
		$response_array['exam_name'] = $row['exam_name'];
		$response_array['exam_type'] = $row['exam_type_name'];
		$response_array['exam_date'] = $row['exam_date'];
		$response_array['participants'][$i]['ip'] = $row['ip_address'];
		$lastname = $row['lastname'];
		$firstname = $row['firstname'];
		$response_array['participants'][$i]['lastname'] = $lastname;
		$response_array['participants'][$i]['firstname'] = $firstname;
		$student_id = left(clean($firstname), 1) . left(clean($lastname), 10);
		if (in_array($student_id, $student_ids)) {
			$student_ids[$i] = $student_id;
			$student_id = $student_id . $i;
		} else {
			$student_ids[$i] = $student_id;
		}
		$response_array['participants'][$i]['student_id'] = strtolower($student_id);
		$response_array['participants'][$i]['registration'] = $row['registration'];
		$response_array['participants'][$i]['accepted'] = $row['accepted'];
	}
} else {
	$response_array['participants'] = 0;
}

pg_close($con);
echo json_encode($response_array);
